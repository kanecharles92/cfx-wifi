# Dometic CFX95 WiFi Communications

Dometic Fridged (CFX range) that host a WiFi module are able to be monitored and/or controlled from an app that Dometic published to the Apple App Store and Google Play Store.

It is possible to interface with the fridge(s) using other tools however it is necessary to understand the way in which the phone app(s) communicate with the fridge(s). This repository will attempt to do that.

## Known information

### WiFi Module

The Dometic CFX range uses a Texas Instruments "SimpleLink" IoT module that provides an interface between an IO interface and the WiFi network.

This device can be accessed at http://192.168.1.1 while connected to the CFX WiFi. There is very little information available in this interface, and no credentials are required.

### Network Details

| Item | Value | Notes |
|---|---|---|
| IP Address | 192.168.1.1 | Default value for IoT SimpleLink module |
| Port | TCP/6378 | Port exposed on IoT module for the app to interface with |

### Variables

#### Constants

| Data Type | Variable Name | Value | Notes |
|---|---|---|---|
| byte | ABS_ERROR_MASK | 1 |
| byte | AC_AVAILABLE_MASK | 4 |
| byte | BOTH_DOORS_MASK | 3 |
| byte | BOX_OFF | 0 |
| byte | BOX_ON | 1 |
| int | CELSIUS | 0 |
| String | CFX_IP | "192.168.1.1" |
| int | CFX_PORT | 6378 |
| byte | COMMAND_CHECK_INFO | 0 |
| byte | COMMAND_RESET_WIFI | 8 |
| byte | COMMAND_SET_ABS | 3 |
| byte | COMMAND_SET_ALARM | 7 |
| byte | COMMAND_SET_CF | 4 |
| byte | COMMAND_SET_TEMP_CALIB | 6 |
| byte | COMMAND_SET_TEMP_COMP1 | 1 |
| byte | COMMAND_SET_TEMP_COMP2 | 2 |
| byte | COMMAND_TURN_ON_OFF | 5 |
| byte | COMPARTMENT_1_MASK | 2 |
| byte | COMPARTMENT_2_MASK | 4 |
| byte | COMPRESSOR_ERROR_MASK | 2 |
| byte | COMPRESSOR_ONE_CYCLE_ERROR_MASK | 32 |
| byte | DOOR_1_ALARM_MASK | 16 |
| byte | DOOR_1_MASK | 1 |
| byte | DOOR_2_ALARM_MASK | 32 |
| byte | DOOR_2_MASK | 2 |
| byte | DOOR_OPEN_ERROR_MASK | 16 |
| int | FARENHEIT | 1 |
| byte | SOLENOID_VALVE_ERROR_MASK | 8 |
| int | TEMPOFFSET | -50 |
| byte | TEMP_UNIT_MASK | 1 |
| byte | THERMOSTAT_ERROR_MASK | 4 |
| byte | COMPRESSOR_ON_MASK | 2 |
| byte | COMPRESSOR_WORKING_FOR_COMPARTMENT_MASK | 1 |
| byte | f56FF | 12 | Derived from `org/roboguice/shaded/goole/common/Ascii.java` |

#### Position indicators within the bytes array sent to the fridge

| Data Type | Variable Name | Value | Notes |
|---|---|---|---|
| int | POS_INITIALIZATION | 0 |
| int | POS_COMMAND | 1 |
| int | POS_PAYLOAD_SIZE | 2 |
| int | POS_SET_TEMP_COMPARTMENT_1 | 3 |
| int | POS_SET_TEMP_COMPARTMENT_2 | 4 |
| int | POS_ABS | 5 |
| int | POS_CF_COMPONOFF | 6 |
| int | POS_REFRIGERATOR_ONOFF | 7 |
| int | POS_TEMP_CALIBRATION | 8 |
| int | POS_ALARM_SETTING | 9 |
| int | POS_DC_SUPPLY_VOLTAGE | 10 |
| int | POS_TEMP_OF_COMPARTMENT1 | 11 |
| int | POS_TEMP_OF_COMPARTMENT2 | 12 |
| int | POS_COMPRESSOR_WORKS | 13 |
| int | POS_STATUS_OF_DOORS_AND_AC | 14 |
| int | POS_ERROR_CODES | 15 | Unsure if this used in sending to the fridge |




#### Data send structure

> Default

```
byte[] cfxSendDatStructure = {51, 0, Ascii.f56FF, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0}
```

> Constant names
```
byte[] cfxSendDatStructure =
{
    INITIALIZATION          // [0]  Default: 51
    COMMAND                 // [1]  Default: 0
    PAYLOAD_SIZE            // [2]  Default: Ascii.f56FF (12)
    SET_TEMP_COMPARTMENT_1  // [3]  Default: 0
    SET_TEMP_COMPARTMENT_2  // [4]  Default: 0
    ABS                     // [5]  Default: 0
    CF_COMPONOFF            // [6]  Default: 6
    REFRIGERATOR_ONOFF      // [7]  Default: 0
    TEMP_CALIBRATION        // [8]  Default: 0
    ALARM_SETTING           // [9]  Default: 0
    DC_SUPPLY_VOLTAGE       // [10] Default: 0
    TEMP_OF_COMPARTMENT1    // [11] Default: 0
    TEMP_OF_COMPARTMENT2    // [12] Default: 0
    COMPRESSOR_WORKS        // [13] Default: 0
    STATUS_OF_DOORS_AND_AC  // [14] Default: 0
}
```

### Communication Steps

1. Create TCP socket to 192.168.1.1:6378
2. Create input/output streams
3. Build `byte[]` array and send to input stream of TCP socket
4. Retrieve data
