# Get CFX Info

> Raw dump from Android Code

```Java hl_lines="4"
public byte[] getCfxInfo() throws IOException, CfxProtocolException {

    if (System.currentTimeMillis() - this.timeCached > ((long) this.boxStateCacheTime) || !this.boxCacheOn) {
        // POS_COMMAND = 1
        // COMMAND_CHECK_INFO = 0
        // Hence:
        // cfxSendDatStructure = {51, --->0<---, Ascii.f56FF, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0}
        this.cfxSendDatStructure[POS_COMMAND] = COMMAND_CHECK_INFO;
        sendTCPCommand(this.cfxSendDatStructure);
        byte[] response = readCommandResponse();
        if (!verifyCommand(response)) {
            throw new CfxProtocolException("Unknown command sent from CFX");
        } else if (!verifyChecksum(response)) {
            throw new CfxProtocolException("Checksum invalid");
        } else if (!verifyPayloadLength(response)) {
            throw new CfxProtocolException("Payload length faulty");
        } else {
            this.cachedBoxState = response;
            this.timeCached = System.currentTimeMillis();
        }
    }
    return this.cachedBoxState;

}
```
