# Data Integrity

> Raw dump from Android App

```Java
public class CfxTcpBase {
    /*
    This method returns a sum total of all byte values in the data send string
    For example:
    byte[] cfxSendDatStructure = {51, 0, 12, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0}
    Would yield: 69, ie (51 + 12 + 6)
     */
    public byte calculateChecksum(byte[] buffer) {
        byte checksum = 0;
        for (int i = 0; i < buffer.length - 1; i++) {
            checksum = (byte) (checksum + buffer[i]);
        }
        return checksum;
    }

    /*
    This method checks if the last value in the buffer is equal to the value generated above
     */
    public boolean verifyChecksum(byte[] buffer) {
        if (buffer[buffer.length - 1] == calculateChecksum(buffer)) {
            return true;
        }
        return false;
    }

    /*
    This method checks if the PAYLOAD_SIZE (index 2) in the buffer does not equal the total length minus 3
     */
    public boolean verifyPayloadLength(byte[] buffer) {
        if (buffer[2] != buffer.length - 3) {
            return false;
        }
        return true;
    }

    /*
    This method returns false (bad command) if all of the following conditions are met:
        - The buffer length is greater than 1, ie it's got some data in it
        - The buffer in position 1 (COMMAND) equals 0 (CHECK_INFO)
        - The buffer in position 1 (COMMAND) equals 238 (TBD???)
     */
    public boolean verifyCommand(byte[] buffer) {
        if (buffer.length > 1 && buffer[1] == 0 && buffer[1] == 238) {
            return false;
        }
        return true;
    }
}
```
