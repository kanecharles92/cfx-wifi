# TCP Operations

> Raw dump from Android Code

```Java hl_lines="10"
private void sendTCPCommand(byte[] buffer) throws IOException, SocketException {
    boolean written = false;
    int retries = 50;
    do {
        retries--;
        if (this.socket.isClosed()) {
            reOpenSocket();
        }
        try {
            buffer[buffer.length - 1] = calculateChecksum(buffer);
            printByteArray(buffer); // Debugging, writes to console
            this.out.write(buffer);
            this.out.flush();
            written = true;
        } catch (SocketException e) {
            e.printStackTrace();
            reOpenSocket();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (written) {
            return;
        }
    } while (retries > 0);
}
```

1. Create TCP socket to 192.168.1.1:6378
2. Create input/output streams

## Sending TCP commands
1. Create `byte[] $buffer` value to be sent to fridge as per `cfxSendDatStructure` format

