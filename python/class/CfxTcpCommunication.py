import socket

class CfxTcpCommunication:
    # Define POSITION CONSTANTS
    POS_INITIALIZATION          = 0
    POS_COMMAND                 = 1
    POS_PAYLOAD_SIZE            = 2
    POS_SET_TEMP_COMPARTMENT_1  = 3
    POS_SET_TEMP_COMPARTMENT_2  = 4
    POS_ABS                     = 5
    POS_CF_COMPONOFF            = 6
    POS_REFRIGERATOR_ONOFF      = 7
    POS_TEMP_CALIBRATION        = 8
    POS_ALARM_SETTING           = 9
    POS_DC_SUPPLY_VOLTAGE       = 10
    POS_TEMP_OF_COMPARTMENT1    = 11
    POS_TEMP_OF_COMPARTMENT2    = 12
    POS_COMPRESSOR_WORKS        = 13
    POS_STATUS_OF_DOORS_AND_AC  = 14
    POS_ERROR_CODES             = 15

    # Define COMMAND CONSTANTS
    COMMAND_CHECK_INFO      = 0
    COMMAND_SET_TEMP_COMP1  = 1
    COMMAND_SET_TEMP_COMP2  = 2
    COMMAND_SET_ABS         = 3
    COMMAND_SET_CF          = 4
    COMMAND_TURN_ON_OFF     = 5
    COMMAND_SET_TEMP_CALIB  = 6
    COMMAND_SET_ALARM       = 7
    COMMAND_RESET_WIFI      = 8

    # Define MASK CONSTANTS
    ABS_ERROR_MASK                           = 1
    AC_AVAILABLE_MASK                        = 4
    BOTH_DOORS_MASK                          = 3
    COMPARTMENT_1_MASK                       = 2
    COMPARTMENT_2_MASK                       = 4
    COMPRESSOR_ERROR_MASK                    = 2
    COMPRESSOR_ON_MASK                       = 2
    COMPRESSOR_ONE_CYCLE_ERROR_MASK          = 32
    COMPRESSOR_WORKING_FOR_COMPARTMENT_MASK  = 1
    DOOR_1_ALARM_MASK                        = 16
    DOOR_1_MASK                              = 1
    DOOR_2_ALARM_MASK                        = 32
    DOOR_2_MASK                              = 2
    DOOR_OPEN_ERROR_MASK                     = 16
    SOLENOID_VALVE_ERROR_MASK                = 8
    TEMP_UNIT_MASK                           = 1
    THERMOSTAT_ERROR_MASK                    = 4

    # Define TEMPERATURE UNIT CONSTANTS
    CELCIUS   = 0
    FARENHEIT = 1

    # Constructor
    def __init__(self, cfx_ip, cfx_port):
        self.cfx_ip    = cfx_ip
        self.cfx_port  = cfx_port
        self.socket    = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.cfx_ip, self.cfx_port))
        self.cfxSendDatStructure = bytearray(
            [51, 0, 12, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0])

    def calculateChecksum(self, buffer: bytearray) -> int:
        # This function adds together all integer values in the bytes array and returns a sum total as the decimal
        checksum = 0
        for buffer_member in buffer[:-1]:
            # for buffer_member in buffer:
            checksum += buffer_member
        return checksum
        """
        # JAVA CODE
        public byte calculateChecksum(byte[] buffer) {
            byte checksum = 0
            for (int i=0
                 i < buffer.length - 1
                 i++) {
                checksum = (byte)(checksum + buffer[i])
            }
            return checksum
        }
        """

    def changeTempUnit(self, tempUnit: int) -> int:
        self.cfxSendDatStructure[self.POS_COMMAND] = self.COMMAND_SET_CF
        if (tempUnit == self.CELCIUS):
            self.cfxSendDatStructure[self.POS_CF_COMPONOFF] = 0
        elif (tempUnit == self.FARENHEIT):
            self.cfxSendDatStructure[self.POS_CF_COMPONOFF] = 1
        else:
            print("INVALID TEMPERATURE UNIT! VALUE MUST BE 0 (CELCIUS) or 1 (FARENHEIT)")
            return 9999
            # TODO: Proper error handling
        self.sendCfxTcpCommand(self.cfxSendDatStructure)
        if ((self.extractCfxReturnDataValue(self.readCommandResponse(), self.POS_CF_COMPONOFF) & 1) == 1):
            return 1
        else:
            return 0
        """
        JAVA CODE
        public byte changeTempUnit(byte tempUnit) throws IOException, CfxProtocolException {
            this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_CF;
            if (tempUnit == CELSIUS) {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 0;
            } else {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 1;
            }
            sendTCPCommand(this.cfxSendDatStructure);
            if ((extractCfxReturnDataValue(readCommandResponse(), POS_CF_COMPONOFF) & 1) == 1) {
                return 1;
            }
            return 0;
        }
        """

    def changeWifiCredentials(self, wifi_ssid: str, wifi_password: str):
        # Convert input strings to byte arrays
        wifi_ssid_bytes = bytearray()
        wifi_ssid_bytes.extend(map(ord, wifi_ssid))

        wifi_password_bytes = bytearray()
        wifi_password_bytes.extend(map(ord, wifi_password))

        # Calculate length of bytes
        length = len(wifi_ssid_bytes) + len(wifi_password_bytes) + 2

        # Build bytearray
        payload = bytearray()
        payload.append(51)
        # TODO: FIX -> Tried to use -18 but got the error: "ValueError: byte must be in range(0, 256)"
        payload.append(-18)
        payload.append(len(wifi_ssid_bytes))
        payload.append(len(wifi_password_bytes))
        payload.extend(wifi_ssid_bytes)
        payload.extend(wifi_password_bytes)

        # Print it
        self.printByteArray(payload)

        """
        JAVA CODE
        public String change_Wifi_Credentials(String wifi_name, String wifi_pass) {
            Log.i("Call", "Calling the changing credentials method");
            byte[] name_bytes = wifi_name.getBytes();
            byte[] pass_bytes = wifi_pass.getBytes();
            int length = name_bytes.length + 2 + pass_bytes.length;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(51);
            bout.write(-18);
            bout.write((byte) name_bytes.length);
            bout.write((byte) pass_bytes.length);
            for (byte write : name_bytes) {
                bout.write(write);
            }
            for (byte write2 : pass_bytes) {
                bout.write(write2);
            }
            byte[] cfxChangeWiFiCredentials = bout.toByteArray();
            int length2 = cfxChangeWiFiCredentials.length;
            for (int i = 0; i < length2; i++) {
                System.out.print(cfxChangeWiFiCredentials[i] + " ");
            }
            try {
                this.socket.getOutputStream().write(cfxChangeWiFiCredentials);
                return readCommandResponseWifi();
            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            }
            }

        """

    def compressorWorkingForCompartment(self) -> int:
        if ((self.COMPRESSOR_WORKING_FOR_COMPARTMENT_MASK & self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_COMPRESSOR_WORKS)) == 1):
            return 1
        else:
            return 2
        """
        JAVA CODE
        public int compressorWorkingForCompartment() throws IOException, CfxProtocolException {
            if ((this.COMPRESSOR_WORKING_FOR_COMPARTMENT_MASK & extractCfxReturnDataValue(getCfxInfo(), POS_COMPRESSOR_WORKS)) == 1) {
                return 1;
            }
            return 2;
        }
        """

    def extractCfxReturnDataValue(self, buffer: bytearray, value: int) -> int:
        if (len(buffer) == 20):
            return buffer[value]
        else:
            return 9999
            # TODO: Throw proper error
        """
        JAVA CODE
        private byte extractCfxReturnDataValue(byte[] buffer, int value) throws CfxProtocolException {
        if (buffer.length == 20) {
            return buffer[value];
        }
        throw new CfxProtocolException("Returned message not of expected length. Expected 20 bytes, got " + buffer.length);
        }
        """

    def getAbsSetting(self) -> int:
        return self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_ABS)
        """
        JAVA CODE
        public int getAbsSetting() throws IOException, CfxProtocolException {
            return extractCfxReturnDataValue(getCfxInfo(), POS_ABS);
        }
        """

    def getCfxErrorCodes(self) -> list:
        reading = self.extractCfxReturnDataValue(
            self.getCfxInfo(), self.POS_ERROR_CODES)
        errorCodes = []
        # TODO: Fix up all of the error codes so that they properly reference an error code class
        if ((reading & 1) != 0):
            errorCodes.append("CFXError.ABS_ERROR")
        if ((reading & 2) != 0):
            errorCodes.append("CFXError.COMPRESSOR_FAULTY")
        if ((reading & 4) != 0):
            errorCodes.append("CFXError.THERMOSTAT_FAULTY")
        if ((reading & 8) != 0):
            errorCodes.append("CFXError.SOLENOID_VALVE_FAULTY")
        if ((reading & 16) != 0):
            errorCodes.append("CFXError.DOOR_OPEN")
        if ((reading & 32) != 0):
            errorCodes.append("CFXError.COMPRESSOR_WORKED_FOR_ONE_CYCLE")
        return errorCodes

        """
        JAVA CODE
        public ArrayList<CFXError> getErrorCodes() throws IOException, CfxProtocolException {
            ArrayList<CFXError> codes = new ArrayList<>();
            byte reading = extractCfxReturnDataValue(getCfxInfo(), POS_ERROR_CODES);
            if ((reading & 1) != 0) {
                codes.add(CFXError.ABS_ERROR);
            }
            if ((reading & 2) != 0) {
                codes.add(CFXError.COMPRESSOR_FAULTY);
            }
            if ((reading & 4) != 0) {
                codes.add(CFXError.THERMOSTAT_FAULTY);
            }
            if ((reading & 8) != 0) {
                codes.add(CFXError.SOLENOID_VALVE_FAULTY);
            }
            if ((reading & 16) != 0) {
                codes.add(CFXError.DOOR_OPEN);
            }
            if ((reading & 32) != 0) {
                codes.add(CFXError.COMPRESSOR_WORKED_FOR_ONE_CYCLE);
            }
            return codes;
        }
        """

    def getCfxInfo(self) -> bytearray:
        # Update data structure
        self.cfxSendDatStructure[CfxTcpCommunication.POS_COMMAND] = CfxTcpCommunication.COMMAND_CHECK_INFO

        # Send the request
        self.sendCfxTcpCommand(self.cfxSendDatStructure)

        # Retrieve response
        response = self.readCommandResponse()

        return response

        """
        # JAVA CODE
        public byte[] getCfxInfo() throws IOException, CfxProtocolException {

            if (System.currentTimeMillis() - this.timeCached > ((long) this.boxStateCacheTime) | | !this.boxCacheOn) {
                this.cfxSendDatStructure[POS_COMMAND] = COMMAND_CHECK_INFO
                sendTCPCommand(this.cfxSendDatStructure)
                byte[] response = readCommandResponse()
                if (!verifyCommand(response)) {
                    throw new CfxProtocolException("Unknown command sent from CFX")
                } else if (!verifyChecksum(response)) {
                    throw new CfxProtocolException("Checksum invalid")
                } else if (!verifyPayloadLength(response)) {
                    throw new CfxProtocolException("Payload length faulty")
                } else {
                    this.cachedBoxState = response
                    this.timeCached = System.currentTimeMillis()
                }
            }
            return this.cachedBoxState
        }
        """

    def getCompartmentCount(self) -> int:
        return 2
        """
        JAVA CODE
        public int getCompartmentCount() {
            return 2;
        }
        """

    def getCompartmentOn(self, compartmentNumber: int) -> bool:
        retVal = self.extractCfxReturnDataValue(
            self.getCfxInfo(), self.POS_CF_COMPONOFF)
        if (compartmentNumber == 1 and (self.COMPARTMENT_1_MASK & retVal) == self.COMPARTMENT_1_MASK):
            return bool(False)
        if (compartmentNumber == 2 and (self.COMPARTMENT_2_MASK & retVal) == self.COMPARTMENT_2_MASK):
            return bool(False)
        else:
            return bool(True)
        """
        JAVA CODE
        public boolean getCompartmentOn(int compartmentNumber) throws IOException, CfxProtocolException {
            byte ret = extractCfxReturnDataValue(getCfxInfo(), POS_CF_COMPONOFF);
            if (compartmentNumber == 1 && (COMPARTMENT_1_MASK & ret) == COMPARTMENT_1_MASK) {
                return false;
            }
            if (compartmentNumber == 2 && (COMPARTMENT_2_MASK & ret) == COMPARTMENT_2_MASK) {
                return false;
            }
            return true;
        }
        """

    def getDCSupplyVoltage(self) -> float:
        reading = self.extractCfxReturnDataValue(
            self.getCfxInfo(), self.POS_DC_SUPPLY_VOLTAGE)
        if (reading < 40):
            return (float(reading + 256) / 10.0)
        else:
            return float(double(reading) / 10.0)
        """
        JAVA CODE
        public float getDCSupplyVoltage() throws IOException, CfxProtocolException {
            byte reading = extractCfxReturnDataValue(getCfxInfo(), POS_DC_SUPPLY_VOLTAGE);
            if (reading < 40) {
                return ((float) (reading + 256)) / 10.0f;
            }
            return (float) (((double) reading) / 10.0d);
        }
        """

    def getRefrigeratorOn(self) -> int:
        if (self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_REFRIGERATOR_ONOFF) == 0):
            return 0
        else:
            return 1

        """
        JAVA CODE
        public int getRefrigeratorOn() throws IOException, CfxProtocolException {
        if (extractCfxReturnDataValue(getCfxInfo(), POS_REFRIGERATOR_ONOFF) == 0) {
            return 0;
        }
            return 1;
        }
        """

    def getSetTempOfCompartment(self, compartmentNumber: int) -> int:
        if (compartmentNumber == 1):
            return self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_SET_TEMP_COMPARTMENT_1) - 50
        if (compartmentNumber == 2):
            return self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_SET_TEMP_COMPARTMENT_2) - 50
        else:
            return 9999

            # TODO: do proper error handling here if invalid compartment number is specified

        """
        JAVA CODE
            public int getSetTempOfCompartment(int compartmentNumber) throws IOException, CfxProtocolException {
            if (compartmentNumber == 1) {
                return extractCfxReturnDataValue(getCfxInfo(), POS_SET_TEMP_COMPARTMENT_1) - 50;
            }
            if (compartmentNumber == 2) {
                return extractCfxReturnDataValue(getCfxInfo(), POS_SET_TEMP_COMPARTMENT_2) - 50;
            }
            throw new CfxProtocolException("Compartment: " + compartmentNumber + " not available in this model");
        }
        """

    def getTempOfCompartment(self, compartmentNumber: int) -> int:
        if (compartmentNumber == 1):
            return self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_TEMP_OF_COMPARTMENT1) - 50
        if (compartmentNumber == 2):
            return self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_TEMP_OF_COMPARTMENT2) - 50
        else:
            print("Compartment " + compartmentNumber +
                  " not available in this model")
            return 9999
            # TODO: do proper error handling here if invalid compartment number is specified
        """
        JAVA CODE
        public int getTempOfCompartment(int compartmentNumber) throws IOException, CfxProtocolException {
            if (compartmentNumber == 1) {
                return extractCfxReturnDataValue(getCfxInfo(), POS_TEMP_OF_COMPARTMENT1) - 50;
            }
            if (compartmentNumber == 2) {
                return extractCfxReturnDataValue(getCfxInfo(), POS_TEMP_OF_COMPARTMENT2) - 50;
            }
            throw new CfxProtocolException("Compartment: " + compartmentNumber + " not available in this model");
        }
        """

    def getTempUnit(self) -> int:
        return self.TEMP_UNIT_MASK & (self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_CF_COMPONOFF))
        """
        JAVA CODE
        public int getTempUnit() throws IOException, CfxProtocolException {
            return TEMP_UNIT_MASK & extractCfxReturnDataValue(getCfxInfo(), POS_CF_COMPONOFF);
        }
        """

    def isAcAvailable(self) -> bool:
        if ((self.AC_AVAILABLE_MASK & self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_STATUS_OF_DOORS_AND_AC)) == self.AC_AVAILABLE_MASK):
            return bool(True)
        else:
            return bool(False)

        """
        JAVA CODE
        public boolean isAcAvailable() throws IOException, CfxProtocolException {
        if ((AC_AVAILABLE_MASK & extractCfxReturnDataValue(getCfxInfo(), POS_STATUS_OF_DOORS_AND_AC)) == AC_AVAILABLE_MASK) {
            return true;
        }
        return false;
        }
        """

    def isCompressorWorking(self) -> bool:
        if ((self.COMPRESSOR_ON_MASK & self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_COMPRESSOR_WORKS) == 2)):
            return bool(True)
        else:
            return bool(False)
        """
        JAVA CODE
        public boolean isCompressorWorking() throws IOException, CfxProtocolException {
            if ((this.COMPRESSOR_ON_MASK & extractCfxReturnDataValue(getCfxInfo(), POS_COMPRESSOR_WORKS)) == 2) {
                return true;
            }
            return false;
        }
        """

    def isDoorAlarming(self, doorNumber: int) -> bool:
        reading = self.extractCfxReturnDataValue(
            self.getCfxInfo(), self.POS_STATUS_OF_DOORS_AND_AC)
        if ((self.DOOR_1_ALARM_MASK & reading) == self.DOOR_1_ALARM_MASK and doorNumber == 1):
            return bool(True)
        if ((self.DOOR_2_ALARM_MASK & reading) == self.DOOR_2_ALARM_MASK and doorNumber == 2):
            return bool(True)
        else:
            return bool(False)
        """
        JAVA CODE
        public boolean isDoorAlarming(int doorNumber) throws IOException, CfxProtocolException {
            byte reading = extractCfxReturnDataValue(getCfxInfo(), POS_STATUS_OF_DOORS_AND_AC);
            if ((DOOR_1_ALARM_MASK & reading) == DOOR_1_ALARM_MASK && doorNumber == 1) {
                return true;
            }
            if ((DOOR_2_ALARM_MASK & reading) == DOOR_2_ALARM_MASK && doorNumber == 2) {
                return true;
            }
            return false;
        }
        """

    def isDoorOpen(self, doorNumber: int) -> bool:
        reading = self.extractCfxReturnDataValue(self.getCfxInfo(), self.POS_STATUS_OF_DOORS_AND_AC)
        if ((self.BOTH_DOORS_MASK & reading) == 0):
            return bool(False)
        if ((self.BOTH_DOORS_MASK & reading) == 3):
            return bool(True)
        if ((self.DOOR_1_MASK & reading) == self.DOOR_1_MASK and doorNumber == 1):
            return bool(True)
        if ((self.DOOR_1_MASK & reading) == 0 and doorNumber == 1):
            return bool(False)
        if ((self.DOOR_2_MASK & reading) == self.DOOR_2_MASK and doorNumber == 2):
            return bool(True)
        if ((self.DOOR_1_MASK & reading) != 0 and doorNumber == 2):
            return bool(False)
        else:
            return bool(False)
        """
        JAVA CODE
        public boolean isDoorOpen(int doorNumber) throws IOException, CfxProtocolException {
            byte reading = extractCfxReturnDataValue(getCfxInfo(), POS_STATUS_OF_DOORS_AND_AC);
            if ((BOTH_DOORS_MASK & reading) == 0) {
                return false;
            }
            if ((BOTH_DOORS_MASK & reading) == 3) {
                return true;
            }
            if ((DOOR_1_MASK & reading) == DOOR_1_MASK && doorNumber == 1) {
                return true;
            }
            if ((DOOR_1_MASK & reading) == 0 && doorNumber == 1) {
                return false;
            }
            if ((DOOR_2_MASK & reading) == DOOR_2_MASK && doorNumber == 2) {
                return true;
            }
            if ((DOOR_2_MASK & reading) != 0 || doorNumber == 2) {
            }
            return false;
        }
        """

    def printByteArray(self, buffer: bytearray):
        print("BYTE ARRAY: ", end="")
        for buffer_member in buffer:
            print(str(buffer_member) + " ", end="")
        print("")
        """
        # JAVA CODE
        private void printByteArray(byte[] buffer) {
            int length = buffer.length
            for (int i=0
                i < length
                i++) {
                System.out.print(buffer[i] + " ")
            }
            System.out.println()
        }
        """

    def readCommandResponse(self) -> bytearray:
            # If the socket is closed, reopen it
        if (self.socket._closed == bool(True)):
            self.reopenCfxTcpSocket()

        out = self.socket.recv(1024)

        # self.printByteArray(out)

        return out

        """
        # JAVA CODE
        private byte[] readCommandResponse() throws IOException {
            if (this.socket.isClosed()) {
                System.out.println("Reopening socket")
                reOpenSocket()
            }
            ByteArrayOutputStream inBuffer = new ByteArrayOutputStream()
            System.out.print("Reading stream ")
            do {
                inBuffer.write(this.f50in.read())
                System.out.print(".")
            } while (this.f50in.available() != 0)
            System.out.println("")
            System.out.print("IN:")
            printByteArray(inBuffer.toByteArray())
            return inBuffer.toByteArray()
        }
        """

    def reopenCfxTcpSocket(self):
        # Check to see if socket is null
        if (self.socket is None):
            print('Socket is invalid!')
            exit - 1

        # Reinstantiate socket instance
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect(
            (self.cfx_ip, self.cfx_port))

    def sendCfxTcpCommand(self, buffer: bytearray):
        # Boolean to indicate whether the data has been written or not
        written = bool(False)

        # Number of retries for the write operation
        retries = 50

        # Keep attemping to write while we are still valid for number of retry operations
        while (retries > 0):
            # If the socket is closed, reopen it
            if (self.socket._closed == bool(True)):
                self.reopenCfxTcpSocket()

            try:
                # Set checksum value in last index of buffer
                buffer[len(buffer) - 1] = self.calculateChecksum(buffer)

                # Write to socket
                self.socket.send(buffer)

                # Update written to indicate that the operation was successful
                written = bool(True)

            except ValueError:
                print("Error in sendCfxTcpCommand function!")
                self.reopenCfxTcpSocket()

            if (written):
                return
        """
        # JAVA CODE
        do {
            retries--
            if (this.socket.isClosed()) {
                reOpenSocket()
            }
            try {
                buffer[buffer.length - 1] = calculateChecksum(buffer)
                printByteArray(buffer)
                this.out.write(buffer)
                this.out.flush()
                written = true
            } catch(SocketException e) {
                e.printStackTrace()
                reOpenSocket()
            } catch(Exception e2) {
                e2.printStackTrace()
            }
            if (written) {
                return
            }
        } while (retries > 0)
        """

    def setABS(self, absLevel: int) -> int:
        if (absLevel < 0 or absLevel > 2):
            print("INVALID ABSLEVEL, VALID RANGE IS 0-2")
            return 9999
            # TODO: Proper error handling
        self.cfxSendDatStructure[self.POS_COMMAND] = self.COMMAND_SET_ABS
        self.cfxSendDatStructure[self.POS_ABS] = absLevel
        self.sendTCPCommand(self.cfxSendDatStructure)
        return self.extractCfxReturnDataValue(self.readCommandResponse(), self.POS_ABS)

    def setAlarm(self, alarm: int) -> int:
        z = bool(True)
        z2 = alarm < 0
        if (alarm <= 2):
            z = bool(False)
        if (z or z2):
            print("Alarm level not within allowed limits (0,1,2). Tried to set: " + alarm)
            return 9999
            # TODO: Proper error handling
        self.cfxSendDatStructure[self.POS_COMMAND] = self.COMMAND_SET_ALARM
        self.cfxSendDatStructure[self.POS_ALARM_SETTING] = alarm
        self.sendTCPCommand(self.cfxSendDatStructure)
        return self.extractCfxReturnDataValue(self.readCommandResponse(), self.POS_ALARM_SETTING)

    def setCompartmentOn(self, compartmentNumber: int, on: bool) -> bool:
        # Get current CfxInfo state
        tmp = self.getCfxInfo()

        # Maintain current temps for the data being sent to the fridge
        self.cfxSendDatStructure[self.POS_SET_TEMP_COMPARTMENT_1] = tmp[self.POS_SET_TEMP_COMPARTMENT_1]
        self.cfxSendDatStructure[self.POS_SET_TEMP_COMPARTMENT_2] = tmp[self.POS_SET_TEMP_COMPARTMENT_2]

        if (compartmentNumber == 1):
            self.cfxSendDatStructure[self.POS_COMMAND] = self.COMMAND_SET_TEMP_COMP1
            if (on):
                self.cfxSendDatStructure[self.POS_CF_COMPONOFF] = 0
            else:
                self.cfxSendDatStructure[self.POS_CF_COMPONOFF] = 2
        elif (compartmentNumber == 2):
            self.cfxSendDatStructure[self.POS_COMMAND] = self.COMMAND_SET_TEMP_COMP2
            if (on):
                self.cfxSendDatStructure[self.POS_CF_COMPONOFF] = 0
            else:
                self.cfxSendDatStructure[self.POS_CF_COMPONOFF] = 4
        else:
            print("Invalid compartment number specified, exiting -1!")
            exit(-1)

        # Send data to fridge
        self.sendCfxTcpCommand(self.cfxSendDatStructure)

        # Retrieve response
        response = self.extractCfxReturnDataValue(
            self.readCommandResponse(), self.POS_CF_COMPONOFF)

        # Determine whether the command was successful
        # Do a bitwise comparison on the mask and returned value, does that equal the mask itself? Check compartment number
        if ((self.COMPARTMENT_1_MASK & response) == self.COMPARTMENT_1_MASK and compartmentNumber == 1):
            return bool(False)
        if ((self.COMPARTMENT_2_MASK & response) == self.COMPARTMENT_2_MASK and compartmentNumber == 2):
            return bool(False)
        return bool(True)

        """
        JAVA CODE
        public boolean setCompartmentOn(int compartmentNumber, boolean on) throws IOException, CfxProtocolException {
        byte[] tmp = getCfxInfo();
        this.cfxSendDatStructure[POS_SET_TEMP_COMPARTMENT_1] = tmp[POS_SET_TEMP_COMPARTMENT_1];
        this.cfxSendDatStructure[POS_SET_TEMP_COMPARTMENT_2] = tmp[POS_SET_TEMP_COMPARTMENT_2];
        if (compartmentNumber == 1) {
            this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_TEMP_COMP1;
            if (on) {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 0;
            } else {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 2;
            }
        } else if (compartmentNumber == 2) {
            this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_TEMP_COMP2;
            if (on) {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 0;
            } else {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 4;
            }
        } else {
            throw new CfxProtocolException("Compartment " + compartmentNumber + " does not exist in this box");
        }
        sendTCPCommand(this.cfxSendDatStructure);
        byte b = extractCfxReturnDataValue(readCommandResponse(), POS_CF_COMPONOFF);
        if ((COMPARTMENT_1_MASK & b) == COMPARTMENT_1_MASK && compartmentNumber == 1) {
            return false;
        }
        if ((COMPARTMENT_2_MASK & b) == COMPARTMENT_2_MASK && compartmentNumber == 2) {
            return false;
        }
        return true;
        }
        """

    def setRefrigeratorOn(self, powerOn: int) -> int:
        self.cfxSendDatStructure[self.POS_COMMAND] = self.COMMAND_TURN_ON_OFF
        if (powerOn == 1):
            self.cfxSendDatStructure[self.POS_REFRIGERATOR_ONOFF] = 1
        elif (powerOn == 0):
            self.cfxSendDatStructure[self.POS_REFRIGERATOR_ONOFF] = 0
        else:
            print("Shit is cooked")
            return 9999
        self.sendCfxTcpCommand(self.cfxSendDatStructure)
        if (self.extractCfxReturnDataValue(self.readCommandResponse(), self.POS_REFRIGERATOR_ONOFF) == 1):
            return 1
        else:
            return 0

        """
        JAVA CODE
        public int setRefrigeratorOn(int i) throws IOException, CfxProtocolException {
        this.cfxSendDatStructure[POS_COMMAND] = COMMAND_TURN_ON_OFF;
        if (i == 1) {
            this.cfxSendDatStructure[POS_REFRIGERATOR_ONOFF] = 1;
        } else {
            this.cfxSendDatStructure[POS_REFRIGERATOR_ONOFF] = 0;
        }
        sendTCPCommand(this.cfxSendDatStructure);
        if (extractCfxReturnDataValue(readCommandResponse(), POS_REFRIGERATOR_ONOFF) == 1) {
            return 1;
        }
        return 0;
        }
        """

    def setTempCompartment(self, temp: int, compartment: int) -> int:
        if (temp < -127 or temp > 127):
            print("Temperature out of range!")
            return 9999
            # TODO: Proper error handling
        self.cfxSendDatStructure[self.POS_CF_COMPONOFF] = self.getCfxInfo()[
            self.POS_CF_COMPONOFF]
        if (compartment == 1):
            self.cfxSendDatStructure[self.POS_COMMAND] = self.COMMAND_SET_TEMP_COMP1
            self.cfxSendDatStructure[self.POS_SET_TEMP_COMPARTMENT_1] = temp + 50
        if (compartment == 2):
            self.cfxSendDatStructure[self.POS_COMMAND] = self.COMMAND_SET_TEMP_COMP2
            self.cfxSendDatStructure[self.POS_SET_TEMP_COMPARTMENT_2] = temp + 50
        self.sendCfxTcpCommand(self.cfxSendDatStructure)
        response = self.readCommandResponse()
        if (compartment == 1):
            return self.extractCfxReturnDataValue(response, self.POS_SET_TEMP_COMPARTMENT_1) - 50
        if (compartment == 2):
            return self.extractCfxReturnDataValue(response, self.POS_SET_TEMP_COMPARTMENT_2) - 50
        else:
            print("INVALID COMPARTMENT!")
            return 9999
            # TODO: Proper error handling
        """
        JAVA CODE
        public byte setTempCompartment(int temp, int compartment) throws IOException, CfxProtocolException {
            if (temp < -127 || temp > 127) {
                throw new CfxProtocolException("Temperature out of range");
            }
            this.cfxSendDatStructure[POS_CF_COMPONOFF] = getCfxInfo()[
                                                                    POS_CF_COMPONOFF];
            if (compartment == 1) {
                this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_TEMP_COMP1;
                this.cfxSendDatStructure[POS_SET_TEMP_COMPARTMENT_1] = (
                    byte) (temp + 50);
            }
            if (compartment == 2) {
                this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_TEMP_COMP2;
                this.cfxSendDatStructure[POS_SET_TEMP_COMPARTMENT_2] = (
                    byte) (temp + 50);
            }
            sendTCPCommand(this.cfxSendDatStructure);
            byte[] response = readCommandResponse();
            byte b = 0;
            if (compartment == 1) {
                b = (byte) (extractCfxReturnDataValue(
                    response, POS_SET_TEMP_COMPARTMENT_1) - 50);
            }
            if (compartment == 2) {
                return (byte) (extractCfxReturnDataValue(response, POS_SET_TEMP_COMPARTMENT_2) - 50);
            }
            return b;
        }
        """

    def isDoorOpen(self, doorNumber: int) -> bool:
        reading = self.extractCfxReturnDataValue(
            self.getCfxInfo(), self.POS_STATUS_OF_DOORS_AND_AC)
        if ((self.BOTH_DOORS_MASK & reading) == 0):
            return bool(False)
        if ((self.BOTH_DOORS_MASK & reading) == 3):
            return bool(True)
        if ((self.DOOR_1_MASK & reading) == self.DOOR_1_MASK and doorNumber == 1):
            return bool(True)
        if ((self.DOOR_1_MASK & reading) == 0 and doorNumber == 1):
            return bool(False)
        if ((self.DOOR_2_MASK & reading) == self.DOOR_2_MASK and doorNumber == 2):
            return bool(True)
        if ((self.DOOR_2_MASK & reading) != 0 and doorNumber == 2):
            # TODO: Verify this code is legit
            return bool(False)
        else:
            return bool(False)
        """
        JAVA CODE
        public boolean isDoorOpen(int doorNumber) throws IOException, CfxProtocolException {
            byte reading = extractCfxReturnDataValue(getCfxInfo(), POS_STATUS_OF_DOORS_AND_AC);
            if ((BOTH_DOORS_MASK & reading) == 0) {
                return false;
            }
            if ((BOTH_DOORS_MASK & reading) == 3) {
                return true;
            }
            if ((DOOR_1_MASK & reading) == DOOR_1_MASK && doorNumber == 1) {
                return true;
            }
            if ((DOOR_1_MASK & reading) == 0 && doorNumber == 1) {
                return false;
            }
            if ((DOOR_2_MASK & reading) == DOOR_2_MASK && doorNumber == 2) {
                return true;
            }
            if ((DOOR_2_MASK & reading) != 0 || doorNumber == 2) {
            }
            return false;
        }
        """

    def verifyChecksum(self, buffer: bytearray) -> bool:
        if (buffer[len(buffer) - 1] == self.calculateChecksum(buffer)):
            return bool(True)
        else:
            return bool(False)
        """
        # JAVA CODE
        public boolean verifyChecksum(byte[] buffer) {
            if (buffer[buffer.length - 1] == calculateChecksum(buffer)) {
                return true
            }
            return false
        }
        """

    def verifyCommand(self, buffer: bytearray) -> bool:
        # How will this ever work? Transposed directly from the Android code
        if (len(buffer) > 1 and buffer[1] == 0 and buffer[1] == 238):
            return bool(False)
        else:
            return bool(True)
        """
        # Java Code
        public boolean verifyCommand(byte[] buffer) {
            if (buffer.length > 1 & & buffer[1] == 0 & & buffer[1] == 238) {
                return false
            }
            return true
        }
        """

    def verifyPayloadLength(self, buffer: bytearray) -> bool:
        # If buffer value at POS_PAYLOAD_SIZE does not equal total buffer length minus 3
        if (buffer[POS_PAYLOAD_SIZE] != (len(buffer) - 3)):
            return bool(False)
        else:
            return bool(True)
        """
        # JAVA CODE
        public boolean verifyPayloadLength(byte[] buffer) {
            if (buffer[2] != buffer.length - 3) {
                return false
            }
            return true
        }
        """
