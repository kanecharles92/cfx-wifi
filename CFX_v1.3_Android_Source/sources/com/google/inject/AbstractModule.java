package com.google.inject;

import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.binder.AnnotatedConstantBindingBuilder;
import com.google.inject.binder.LinkedBindingBuilder;
import com.google.inject.matcher.Matcher;
import com.google.inject.spi.Message;
import com.google.inject.spi.ProvisionListener;
import com.google.inject.spi.TypeConverter;
import com.google.inject.spi.TypeListener;
import java.lang.annotation.Annotation;
import org.roboguice.shaded.goole.common.base.Preconditions;

public abstract class AbstractModule implements Module {
    private AnnotationDatabaseFinder annotationDatabaseFinder;
    protected Binder binder;
    private AnnotatedBindingBuilder noOpAnnotatedBindingBuilder = new NoOpAnnotatedBindingBuilder();

    /* access modifiers changed from: protected */
    public abstract void configure();

    public void setAnnotationDatabaseFinder(AnnotationDatabaseFinder annotationDatabaseFinder2) {
        this.annotationDatabaseFinder = annotationDatabaseFinder2;
    }

    public final synchronized void configure(Binder builder) {
        Preconditions.checkState(this.binder == null, "Re-entry is not allowed.");
        this.binder = (Binder) Preconditions.checkNotNull(builder, "builder");
        try {
            configure();
            this.binder = null;
        } catch (Throwable th) {
            this.binder = null;
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public Binder binder() {
        Preconditions.checkState(this.binder != null, "The binder can only be used inside configure()");
        return this.binder;
    }

    /* access modifiers changed from: protected */
    public void bindScope(Class<? extends Annotation> scopeAnnotation, Scope scope) {
        binder().bindScope(scopeAnnotation, scope);
    }

    /* access modifiers changed from: protected */
    public <T> LinkedBindingBuilder<T> bind(Key<T> key) {
        return binder().bind(key);
    }

    /* access modifiers changed from: protected */
    public <T> AnnotatedBindingBuilder<T> bind(TypeLiteral<T> typeLiteral) {
        return binder().bind(typeLiteral);
    }

    /* access modifiers changed from: protected */
    public <T> AnnotatedBindingBuilder<T> bind(Class<T> clazz) {
        if (isInjectable(clazz)) {
            return this.binder.bind(clazz);
        }
        return this.noOpAnnotatedBindingBuilder;
    }

    /* access modifiers changed from: protected */
    public <T> AnnotatedBindingBuilder<T> superbind(Class<T> clazz) {
        return this.binder.bind(clazz);
    }

    /* access modifiers changed from: protected */
    public AnnotatedConstantBindingBuilder bindConstant() {
        return binder().bindConstant();
    }

    /* access modifiers changed from: protected */
    public void install(Module module) {
        if (this.annotationDatabaseFinder != null && (module instanceof AbstractModule)) {
            ((AbstractModule) module).setAnnotationDatabaseFinder(this.annotationDatabaseFinder);
        }
        binder().install(module);
    }

    /* access modifiers changed from: protected */
    public void addError(String message, Object... arguments) {
        binder().addError(message, arguments);
    }

    /* access modifiers changed from: protected */
    public void addError(Throwable t) {
        binder().addError(t);
    }

    /* access modifiers changed from: protected */
    public void addError(Message message) {
        binder().addError(message);
    }

    /* access modifiers changed from: protected */
    public void requestInjection(Object instance) {
        binder().requestInjection(instance);
    }

    /* access modifiers changed from: protected */
    public void requestStaticInjection(Class<?>... types) {
        binder().requestStaticInjection(types);
    }

    /* access modifiers changed from: protected */
    public void requireBinding(Key<?> key) {
        binder().getProvider(key);
    }

    /* access modifiers changed from: protected */
    public void requireBinding(Class<?> type) {
        binder().getProvider(type);
    }

    /* access modifiers changed from: protected */
    public <T> Provider<T> getProvider(Key<T> key) {
        return binder().getProvider(key);
    }

    /* access modifiers changed from: protected */
    public <T> Provider<T> getProvider(Class<T> type) {
        return binder().getProvider(type);
    }

    /* access modifiers changed from: protected */
    public void convertToTypes(Matcher<? super TypeLiteral<?>> typeMatcher, TypeConverter converter) {
        binder().convertToTypes(typeMatcher, converter);
    }

    /* access modifiers changed from: protected */
    public Stage currentStage() {
        return binder().currentStage();
    }

    /* access modifiers changed from: protected */
    public <T> MembersInjector<T> getMembersInjector(Class<T> type) {
        return binder().getMembersInjector(type);
    }

    /* access modifiers changed from: protected */
    public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> type) {
        return binder().getMembersInjector(type);
    }

    /* access modifiers changed from: protected */
    public void bindListener(Matcher<? super TypeLiteral<?>> typeMatcher, TypeListener listener) {
        binder().bindListener(typeMatcher, listener);
    }

    /* access modifiers changed from: protected */
    public void bindListener(Matcher<? super Binding<?>> bindingMatcher, ProvisionListener... listener) {
        binder().bindListener(bindingMatcher, listener);
    }

    /* access modifiers changed from: protected */
    public boolean isInjectable(Class clazz) {
        return this.annotationDatabaseFinder == null || this.annotationDatabaseFinder.getBindableClassesSet().contains(clazz.getName());
    }

    /* access modifiers changed from: protected */
    public boolean hasInjectionPointsForAnnotation(Class annotationClass) {
        return this.annotationDatabaseFinder == null || this.annotationDatabaseFinder.mo6946x236aef0().containsKey(annotationClass.getName()) || this.annotationDatabaseFinder.getMapAnnotationToMapClassContainingInjectionToInjectedMethodSet().containsKey(annotationClass.getName()) || this.annotationDatabaseFinder.getMapAnnotationToMapClassContainingInjectionToInjectedFieldSet().containsKey(annotationClass.getName());
    }

    public AnnotationDatabaseFinder getAnnotationDatabaseFinder() {
        return this.annotationDatabaseFinder;
    }
}
