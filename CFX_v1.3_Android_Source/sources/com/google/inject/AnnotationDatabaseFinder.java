package com.google.inject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AnnotationDatabaseFinder {
    private HashSet<String> bindableClassesSet = new HashSet<>();
    private HashSet<String> classesContainingInjectionPointsSet = new HashSet<>();

    /* renamed from: mapAnnotationToMapClassContainingInjectionToInjectedConstructorSet */
    private HashMap<String, Map<String, Set<String>>> f24xe54650fa = new HashMap<>();
    private HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassContainingInjectionToInjectedFieldSet = new HashMap<>();
    private HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassContainingInjectionToInjectedMethodSet = new HashMap<>();

    public AnnotationDatabaseFinder(String[] additionalPackageNames) {
        String[] arr$;
        try {
            for (String pkg : additionalPackageNames) {
                String annotationDatabaseClassName = "AnnotationDatabaseImpl";
                if (pkg != null && !"".equals(pkg)) {
                    annotationDatabaseClassName = pkg + "." + annotationDatabaseClassName;
                }
                addAnnotationDatabase(getAnnotationDatabaseInstance(annotationDatabaseClassName));
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2);
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
            throw new RuntimeException(e3);
        }
    }

    public HashSet<String> getClassesContainingInjectionPointsSet() {
        return this.classesContainingInjectionPointsSet;
    }

    public HashMap<String, Map<String, Set<String>>> getMapAnnotationToMapClassContainingInjectionToInjectedFieldSet() {
        return this.mapAnnotationToMapClassContainingInjectionToInjectedFieldSet;
    }

    public HashMap<String, Map<String, Set<String>>> getMapAnnotationToMapClassContainingInjectionToInjectedMethodSet() {
        return this.mapAnnotationToMapClassContainingInjectionToInjectedMethodSet;
    }

    /* renamed from: getMapAnnotationToMapClassContainingInjectionToInjectedConstructorSet */
    public HashMap<String, Map<String, Set<String>>> mo6946x236aef0() {
        return this.f24xe54650fa;
    }

    public Set<String> getBindableClassesSet() {
        return this.bindableClassesSet;
    }

    private AnnotationDatabase getAnnotationDatabaseInstance(String annotationDatabaseClassName) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        return (AnnotationDatabase) Class.forName(annotationDatabaseClassName).newInstance();
    }

    private void addAnnotationDatabase(AnnotationDatabase annotationDatabase) {
        annotationDatabase.fillAnnotationClassesAndFieldsNames(this.mapAnnotationToMapClassContainingInjectionToInjectedFieldSet);
        annotationDatabase.fillAnnotationClassesAndMethods(this.mapAnnotationToMapClassContainingInjectionToInjectedMethodSet);
        annotationDatabase.fillAnnotationClassesAndConstructors(this.f24xe54650fa);
        annotationDatabase.fillClassesContainingInjectionPointSet(this.classesContainingInjectionPointsSet);
        annotationDatabase.fillBindableClasses(this.bindableClassesSet);
    }
}
