package com.google.inject;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AnnotatedGuiceHierarchyTraversalFilter extends HierarchyTraversalFilter {
    private HashSet<String> classesContainingInjectionPointsSet = new HashSet<>();
    private HierarchyTraversalFilter delegate;
    private boolean hasHadInjectionPoints;
    private HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToConstructorSet;
    private HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToFieldSet;
    private HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToMethodSet;

    public AnnotatedGuiceHierarchyTraversalFilter(AnnotationDatabaseFinder annotationDatabaseFinder, HierarchyTraversalFilter delegate2) {
        this.delegate = delegate2;
        this.mapAnnotationToMapClassWithInjectionNameToFieldSet = annotationDatabaseFinder.getMapAnnotationToMapClassContainingInjectionToInjectedFieldSet();
        this.mapAnnotationToMapClassWithInjectionNameToMethodSet = annotationDatabaseFinder.getMapAnnotationToMapClassContainingInjectionToInjectedMethodSet();
        this.mapAnnotationToMapClassWithInjectionNameToConstructorSet = annotationDatabaseFinder.mo6946x236aef0();
        this.classesContainingInjectionPointsSet = annotationDatabaseFinder.getClassesContainingInjectionPointsSet();
        if (this.classesContainingInjectionPointsSet.isEmpty()) {
            throw new IllegalStateException("Unable to find Annotation Database which should be output as part of annotation processing");
        }
    }

    public void reset() {
        this.delegate.reset();
        this.hasHadInjectionPoints = false;
    }

    public boolean isWorthScanning(Class<?> c) {
        if (this.hasHadInjectionPoints) {
            return this.delegate.isWorthScanning(c);
        }
        if (c != null) {
            while (!this.classesContainingInjectionPointsSet.contains(c.getName())) {
                c = c.getSuperclass();
                if (!this.delegate.isWorthScanning(c)) {
                }
            }
            this.hasHadInjectionPoints = true;
            return true;
        }
        return false;
    }

    public boolean isWorthScanningForFields(String annotationClassName, Class<?> c) {
        if (this.hasHadInjectionPoints) {
            return this.delegate.isWorthScanning(c);
        }
        if (c == null) {
            return false;
        }
        Map<String, Set<String>> classesContainingInjectionPointsForAnnotation = (Map) this.mapAnnotationToMapClassWithInjectionNameToFieldSet.get(annotationClassName);
        if (classesContainingInjectionPointsForAnnotation == null) {
            return false;
        }
        while (!classesContainingInjectionPointsForAnnotation.containsKey(c.getName())) {
            c = c.getSuperclass();
            if (!this.delegate.isWorthScanning(c)) {
                return false;
            }
        }
        this.hasHadInjectionPoints = true;
        return true;
    }

    public Set<Field> getAllFields(String annotationClassName, Class<?> c) {
        Map<String, Set<String>> classesContainingInjectionPointsForAnnotation = (Map) this.mapAnnotationToMapClassWithInjectionNameToFieldSet.get(annotationClassName);
        if (!(c == null || classesContainingInjectionPointsForAnnotation == null)) {
            Set<String> fieldNameSet = (Set) classesContainingInjectionPointsForAnnotation.get(c.getName());
            if (fieldNameSet != null) {
                Set<Field> fieldSet = new HashSet<>();
                try {
                    for (String fieldName : fieldNameSet) {
                        fieldSet.add(c.getDeclaredField(fieldName));
                    }
                    return fieldSet;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return Collections.emptySet();
    }

    public boolean isWorthScanningForMethods(String annotationClassName, Class<?> c) {
        if (this.hasHadInjectionPoints) {
            return this.delegate.isWorthScanning(c);
        }
        if (c == null) {
            return false;
        }
        Map<String, Set<String>> classesContainingInjectionPointsForAnnotation = (Map) this.mapAnnotationToMapClassWithInjectionNameToMethodSet.get(annotationClassName);
        if (classesContainingInjectionPointsForAnnotation == null) {
            return false;
        }
        while (!classesContainingInjectionPointsForAnnotation.containsKey(c.getName())) {
            c = c.getSuperclass();
            if (!this.delegate.isWorthScanning(c)) {
                return false;
            }
        }
        this.hasHadInjectionPoints = true;
        return true;
    }

    public Set<Method> getAllMethods(String annotationClassName, Class<?> c) {
        Map<String, Set<String>> classesContainingInjectionPointsForAnnotation = (Map) this.mapAnnotationToMapClassWithInjectionNameToMethodSet.get(annotationClassName);
        if (!(c == null || classesContainingInjectionPointsForAnnotation == null)) {
            Set<String> methodNameSet = (Set) classesContainingInjectionPointsForAnnotation.get(c.getName());
            if (methodNameSet != null) {
                Set<Method> methodSet = new HashSet<>();
                try {
                    for (String methodNameAndParamClasses : methodNameSet) {
                        String[] split = methodNameAndParamClasses.split(":");
                        String methodName = split[0];
                        Class<?>[] paramClass = new Class[(split.length - 1)];
                        for (int i = 1; i < split.length; i++) {
                            paramClass[i - 1] = getClass().getClassLoader().loadClass(split[i]);
                        }
                        methodSet.add(c.getDeclaredMethod(methodName, paramClass));
                    }
                    return methodSet;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return Collections.emptySet();
    }

    public boolean isWorthScanningForConstructors(String annotationClassName, Class<?> c) {
        if (this.hasHadInjectionPoints) {
            return this.delegate.isWorthScanning(c);
        }
        if (c == null) {
            return false;
        }
        Map<String, Set<String>> classesContainingInjectionPointsForAnnotation = (Map) this.mapAnnotationToMapClassWithInjectionNameToConstructorSet.get(annotationClassName);
        if (classesContainingInjectionPointsForAnnotation == null) {
            return false;
        }
        while (!classesContainingInjectionPointsForAnnotation.containsKey(c.getName())) {
            c = c.getSuperclass();
            if (!this.delegate.isWorthScanning(c)) {
                return false;
            }
        }
        this.hasHadInjectionPoints = true;
        return true;
    }

    public Set<Constructor<?>> getAllConstructors(String annotationClassName, Class<?> c) {
        Map<String, Set<String>> classesContainingInjectionPointsForAnnotation = (Map) this.mapAnnotationToMapClassWithInjectionNameToConstructorSet.get(annotationClassName);
        if (!(c == null || classesContainingInjectionPointsForAnnotation == null)) {
            Set<String> methodNameSet = (Set) classesContainingInjectionPointsForAnnotation.get(c.getName());
            if (methodNameSet != null) {
                Set<Constructor<?>> methodSet = new HashSet<>();
                try {
                    for (String methodNameAndParamClasses : methodNameSet) {
                        String[] split = methodNameAndParamClasses.split(":");
                        Class<?>[] paramClass = new Class[(split.length - 1)];
                        for (int i = 1; i < split.length; i++) {
                            paramClass[i - 1] = getClass().getClassLoader().loadClass(split[i]);
                        }
                        methodSet.add(c.getDeclaredConstructor(paramClass));
                    }
                    return methodSet;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return Collections.emptySet();
    }
}
