package com.google.inject.util;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.ProviderWithDependencies;
import java.util.Collection;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.Sets;

public final class Providers {

    private static final class ConstantProvider<T> implements Provider<T> {
        private final T instance;

        private ConstantProvider(T instance2) {
            this.instance = instance2;
        }

        public T get() {
            return this.instance;
        }

        public String toString() {
            return "of(" + this.instance + ")";
        }

        public boolean equals(Object obj) {
            return (obj instanceof ConstantProvider) && Objects.equal(this.instance, ((ConstantProvider) obj).instance);
        }

        public int hashCode() {
            return Objects.hashCode(this.instance);
        }
    }

    private static class GuicifiedProvider<T> implements Provider<T> {
        protected final javax.inject.Provider<T> delegate;

        private GuicifiedProvider(javax.inject.Provider<T> delegate2) {
            this.delegate = delegate2;
        }

        public T get() {
            return this.delegate.get();
        }

        public String toString() {
            return "guicified(" + this.delegate + ")";
        }

        public boolean equals(Object obj) {
            return (obj instanceof GuicifiedProvider) && Objects.equal(this.delegate, ((GuicifiedProvider) obj).delegate);
        }

        public int hashCode() {
            return Objects.hashCode(this.delegate);
        }
    }

    private static final class GuicifiedProviderWithDependencies<T> extends GuicifiedProvider<T> implements ProviderWithDependencies<T> {
        private final Set<Dependency<?>> dependencies;

        private GuicifiedProviderWithDependencies(Set<Dependency<?>> dependencies2, javax.inject.Provider<T> delegate) {
            super(delegate);
            this.dependencies = dependencies2;
        }

        /* access modifiers changed from: 0000 */
        @Inject
        public void initialize(Injector injector) {
            injector.injectMembers(this.delegate);
        }

        public Set<Dependency<?>> getDependencies() {
            return this.dependencies;
        }
    }

    private Providers() {
    }

    /* renamed from: of */
    public static <T> Provider<T> m71of(T instance) {
        return new ConstantProvider(instance);
    }

    public static <T> Provider<T> guicify(javax.inject.Provider<T> provider) {
        if (provider instanceof Provider) {
            return (Provider) provider;
        }
        javax.inject.Provider<T> delegate = (javax.inject.Provider) Preconditions.checkNotNull(provider, "provider");
        Set<InjectionPoint> injectionPoints = InjectionPoint.forInstanceMethodsAndFields(provider.getClass());
        if (injectionPoints.isEmpty()) {
            return new GuicifiedProvider(delegate);
        }
        Set<Dependency<?>> mutableDeps = Sets.newHashSet();
        for (InjectionPoint ip : injectionPoints) {
            mutableDeps.addAll(ip.getDependencies());
        }
        return new GuicifiedProviderWithDependencies(ImmutableSet.copyOf((Collection<? extends E>) mutableDeps), delegate);
    }
}
