package com.google.inject.util;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Binding;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.PrivateBinder;
import com.google.inject.Scope;
import com.google.inject.internal.Errors;
import com.google.inject.spi.DefaultBindingScopingVisitor;
import com.google.inject.spi.DefaultElementVisitor;
import com.google.inject.spi.Element;
import com.google.inject.spi.Elements;
import com.google.inject.spi.PrivateElements;
import com.google.inject.spi.ScopeBinding;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.Iterables;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Maps;
import org.roboguice.shaded.goole.common.collect.Sets;

public final class Modules {
    public static final Module EMPTY_MODULE = new EmptyModule();

    private static class CombinedModule implements Module {
        final Set<Module> modulesSet;

        CombinedModule(Iterable<? extends Module> modules) {
            this.modulesSet = ImmutableSet.copyOf(modules);
        }

        public void configure(Binder binder) {
            Binder binder2 = binder.skipSources(getClass());
            for (Module module : this.modulesSet) {
                binder2.install(module);
            }
        }
    }

    private static class EmptyModule implements Module {
        private EmptyModule() {
        }

        public void configure(Binder binder) {
        }
    }

    private static class ModuleWriter extends DefaultElementVisitor<Void> {
        protected final Binder binder;

        ModuleWriter(Binder binder2) {
            this.binder = binder2.skipSources(getClass());
        }

        /* access modifiers changed from: protected */
        public Void visitOther(Element element) {
            element.applyTo(this.binder);
            return null;
        }

        /* access modifiers changed from: 0000 */
        public void writeAll(Iterable<? extends Element> elements) {
            for (Element element : elements) {
                element.acceptVisitor(this);
            }
        }
    }

    public interface OverriddenModuleBuilder {
        Module with(Iterable<? extends Module> iterable);

        Module with(Module... moduleArr);
    }

    static class OverrideModule extends AbstractModule {
        private final ImmutableSet<Module> baseModules;
        private final ImmutableSet<Module> overrides;

        OverrideModule(Iterable<? extends Module> overrides2, ImmutableSet<Module> baseModules2) {
            this.overrides = ImmutableSet.copyOf(overrides2);
            this.baseModules = baseModules2;
        }

        public void configure() {
            Binder baseBinder = binder();
            List<Element> baseElements = Elements.getElements(currentStage(), (Iterable<? extends Module>) this.baseModules);
            if (baseElements.size() == 1) {
                Element element = (Element) Iterables.getOnlyElement(baseElements);
                if (element instanceof PrivateElements) {
                    PrivateElements privateElements = (PrivateElements) element;
                    PrivateBinder privateBinder = baseBinder.newPrivateBinder().withSource(privateElements.getSource());
                    for (Key exposed : privateElements.getExposedKeys()) {
                        privateBinder.withSource(privateElements.getExposedSource(exposed)).expose(exposed);
                    }
                    baseBinder = privateBinder;
                    baseElements = privateElements.getElements();
                }
            }
            Binder binder = baseBinder.skipSources(getClass());
            LinkedHashSet<Element> elements = new LinkedHashSet<>(baseElements);
            List<Element> overrideElements = Elements.getElements(currentStage(), (Iterable<? extends Module>) this.overrides);
            final Set<Key<?>> overriddenKeys = Sets.newHashSet();
            final Map<Class<? extends Annotation>, ScopeBinding> overridesScopeAnnotations = Maps.newHashMap();
            new ModuleWriter(binder) {
                public <T> Void visit(Binding<T> binding) {
                    overriddenKeys.add(binding.getKey());
                    return (Void) super.visit(binding);
                }

                public Void visit(ScopeBinding scopeBinding) {
                    overridesScopeAnnotations.put(scopeBinding.getAnnotationType(), scopeBinding);
                    return (Void) super.visit(scopeBinding);
                }

                public Void visit(PrivateElements privateElements) {
                    overriddenKeys.addAll(privateElements.getExposedKeys());
                    return (Void) super.visit(privateElements);
                }
            }.writeAll(overrideElements);
            final Map<Scope, List<Object>> scopeInstancesInUse = Maps.newHashMap();
            final List<ScopeBinding> scopeBindings = Lists.newArrayList();
            new ModuleWriter(binder) {
                public <T> Void visit(Binding<T> binding) {
                    if (!overriddenKeys.remove(binding.getKey())) {
                        super.visit(binding);
                        Scope scope = OverrideModule.this.getScopeInstanceOrNull(binding);
                        if (scope != null) {
                            List<Object> existing = (List) scopeInstancesInUse.get(scope);
                            if (existing == null) {
                                existing = Lists.newArrayList();
                                scopeInstancesInUse.put(scope, existing);
                            }
                            existing.add(binding.getSource());
                        }
                    }
                    return null;
                }

                /* access modifiers changed from: 0000 */
                public void rewrite(Binder binder, PrivateElements privateElements, Set<Key<?>> keysToSkip) {
                    PrivateBinder privateBinder = binder.withSource(privateElements.getSource()).newPrivateBinder();
                    Set<Key<?>> skippedExposes = Sets.newHashSet();
                    for (Key<?> key : privateElements.getExposedKeys()) {
                        if (keysToSkip.remove(key)) {
                            skippedExposes.add(key);
                        } else {
                            privateBinder.withSource(privateElements.getExposedSource(key)).expose(key);
                        }
                    }
                    for (Element element : privateElements.getElements()) {
                        if (!(element instanceof Binding) || !skippedExposes.remove(((Binding) element).getKey())) {
                            if (element instanceof PrivateElements) {
                                rewrite(privateBinder, (PrivateElements) element, skippedExposes);
                            } else {
                                element.applyTo(privateBinder);
                            }
                        }
                    }
                }

                public Void visit(PrivateElements privateElements) {
                    rewrite(this.binder, privateElements, overriddenKeys);
                    return null;
                }

                public Void visit(ScopeBinding scopeBinding) {
                    scopeBindings.add(scopeBinding);
                    return null;
                }
            }.writeAll(elements);
            new ModuleWriter(binder) {
                public Void visit(ScopeBinding scopeBinding) {
                    ScopeBinding overideBinding = (ScopeBinding) overridesScopeAnnotations.remove(scopeBinding.getAnnotationType());
                    if (overideBinding == null) {
                        super.visit(scopeBinding);
                    } else {
                        List<Object> usedSources = (List) scopeInstancesInUse.get(scopeBinding.getScope());
                        if (usedSources != null) {
                            StringBuilder sb = new StringBuilder("The scope for @%s is bound directly and cannot be overridden.");
                            sb.append("%n     original binding at " + Errors.convert(scopeBinding.getSource()));
                            for (Object usedSource : usedSources) {
                                sb.append("%n     bound directly at " + Errors.convert(usedSource) + "");
                            }
                            this.binder.withSource(overideBinding.getSource()).addError(sb.toString(), scopeBinding.getAnnotationType().getSimpleName());
                        }
                    }
                    return null;
                }
            }.writeAll(scopeBindings);
        }

        /* access modifiers changed from: private */
        public Scope getScopeInstanceOrNull(Binding<?> binding) {
            return (Scope) binding.acceptScopingVisitor(new DefaultBindingScopingVisitor<Scope>() {
                public Scope visitScope(Scope scope) {
                    return scope;
                }
            });
        }
    }

    private static final class RealOverriddenModuleBuilder implements OverriddenModuleBuilder {
        private final ImmutableSet<Module> baseModules;

        private RealOverriddenModuleBuilder(Iterable<? extends Module> baseModules2) {
            this.baseModules = ImmutableSet.copyOf(baseModules2);
        }

        public Module with(Module... overrides) {
            return with((Iterable<? extends Module>) Arrays.asList(overrides));
        }

        public Module with(Iterable<? extends Module> overrides) {
            return new OverrideModule(overrides, this.baseModules);
        }
    }

    private Modules() {
    }

    public static OverriddenModuleBuilder override(Module... modules) {
        return new RealOverriddenModuleBuilder(Arrays.asList(modules));
    }

    public static OverriddenModuleBuilder override(Iterable<? extends Module> modules) {
        return new RealOverriddenModuleBuilder(modules);
    }

    public static Module combine(Module... modules) {
        return combine((Iterable<? extends Module>) ImmutableSet.copyOf((E[]) modules));
    }

    public static Module combine(Iterable<? extends Module> modules) {
        return new CombinedModule(modules);
    }
}
