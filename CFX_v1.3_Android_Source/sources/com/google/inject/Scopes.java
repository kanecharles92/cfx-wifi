package com.google.inject;

import com.google.inject.internal.CircularDependencyProxy;
import com.google.inject.internal.InternalInjectorCreator;
import com.google.inject.internal.LinkedBindingImpl;
import com.google.inject.spi.BindingScopingVisitor;
import com.google.inject.spi.ExposedBinding;
import java.lang.annotation.Annotation;
import javax.inject.Singleton;

public class Scopes {
    private static final BindingScopingVisitor<Boolean> IS_SINGLETON_VISITOR = new BindingScopingVisitor<Boolean>() {
        public Boolean visitNoScoping() {
            return Boolean.valueOf(false);
        }

        public Boolean visitScopeAnnotation(Class<? extends Annotation> scopeAnnotation) {
            return Boolean.valueOf(scopeAnnotation == Singleton.class || scopeAnnotation == Singleton.class);
        }

        public Boolean visitScope(Scope scope) {
            return Boolean.valueOf(scope == Scopes.SINGLETON);
        }

        public Boolean visitEagerSingleton() {
            return Boolean.valueOf(true);
        }
    };
    public static final Scope NO_SCOPE = new Scope() {
        public <T> Provider<T> scope(Key<T> key, Provider<T> unscoped) {
            return unscoped;
        }

        public String toString() {
            return "Scopes.NO_SCOPE";
        }
    };
    /* access modifiers changed from: private */
    public static final Object NULL = new Object();
    public static final Scope SINGLETON = new Scope() {
        public <T> Provider<T> scope(Key<T> key, final Provider<T> creator) {
            return new Provider<T>() {
                private volatile Object instance;

                public T get() {
                    Object providedOrSentinel;
                    if (this.instance == null) {
                        synchronized (InternalInjectorCreator.class) {
                            if (this.instance == null) {
                                Object provided = creator.get();
                                if (Scopes.isCircularProxy(provided)) {
                                    return provided;
                                }
                                if (provided == null) {
                                    providedOrSentinel = Scopes.NULL;
                                } else {
                                    providedOrSentinel = provided;
                                }
                                if (this.instance == null || this.instance == providedOrSentinel) {
                                    this.instance = providedOrSentinel;
                                } else {
                                    throw new ProvisionException("Provider was reentrant while creating a singleton");
                                }
                            }
                        }
                    }
                    Object localInstance = this.instance;
                    return localInstance != Scopes.NULL ? localInstance : null;
                }

                public String toString() {
                    return String.format("%s[%s]", new Object[]{creator, Scopes.SINGLETON});
                }
            };
        }

        public String toString() {
            return "Scopes.SINGLETON";
        }
    };

    private Scopes() {
    }

    public static boolean isSingleton(Binding<?> binding) {
        while (!((Boolean) binding.acceptScopingVisitor(IS_SINGLETON_VISITOR)).booleanValue()) {
            if (binding instanceof LinkedBindingImpl) {
                LinkedBindingImpl<?> linkedBinding = (LinkedBindingImpl) binding;
                Injector injector = linkedBinding.getInjector();
                if (injector != null) {
                    binding = injector.getBinding(linkedBinding.getLinkedKey());
                }
            } else if (binding instanceof ExposedBinding) {
                ExposedBinding<?> exposedBinding = (ExposedBinding) binding;
                Injector injector2 = exposedBinding.getPrivateElements().getInjector();
                if (injector2 != null) {
                    binding = injector2.getBinding(exposedBinding.getKey());
                }
            }
            return false;
        }
        return true;
    }

    public static boolean isScoped(Binding<?> binding, final Scope scope, final Class<? extends Annotation> scopeAnnotation) {
        while (!((Boolean) binding.acceptScopingVisitor(new BindingScopingVisitor<Boolean>() {
            public Boolean visitNoScoping() {
                return Boolean.valueOf(false);
            }

            public Boolean visitScopeAnnotation(Class<? extends Annotation> visitedAnnotation) {
                return Boolean.valueOf(visitedAnnotation == scopeAnnotation);
            }

            public Boolean visitScope(Scope visitedScope) {
                return Boolean.valueOf(visitedScope == scope);
            }

            public Boolean visitEagerSingleton() {
                return Boolean.valueOf(false);
            }
        })).booleanValue()) {
            if (binding instanceof LinkedBindingImpl) {
                LinkedBindingImpl<?> linkedBinding = (LinkedBindingImpl) binding;
                Injector injector = linkedBinding.getInjector();
                if (injector != null) {
                    binding = injector.getBinding(linkedBinding.getLinkedKey());
                }
            } else if (binding instanceof ExposedBinding) {
                ExposedBinding<?> exposedBinding = (ExposedBinding) binding;
                Injector injector2 = exposedBinding.getPrivateElements().getInjector();
                if (injector2 != null) {
                    binding = injector2.getBinding(exposedBinding.getKey());
                }
            }
            return false;
        }
        return true;
    }

    public static boolean isCircularProxy(Object object) {
        return object instanceof CircularDependencyProxy;
    }
}
