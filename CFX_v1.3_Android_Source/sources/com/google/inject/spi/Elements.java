package com.google.inject.spi;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Binding;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Module;
import com.google.inject.PrivateBinder;
import com.google.inject.PrivateModule;
import com.google.inject.Provider;
import com.google.inject.Scope;
import com.google.inject.Stage;
import com.google.inject.TypeLiteral;
import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.binder.AnnotatedConstantBindingBuilder;
import com.google.inject.binder.AnnotatedElementBuilder;
import com.google.inject.internal.AbstractBindingBuilder;
import com.google.inject.internal.BindingBuilder;
import com.google.inject.internal.ConstantBindingBuilderImpl;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ExposureBuilder;
import com.google.inject.internal.InternalFlags;
import com.google.inject.internal.InternalFlags.IncludeStackTraceOption;
import com.google.inject.internal.PrivateElementsImpl;
import com.google.inject.internal.ProviderMethodsModule;
import com.google.inject.internal.util.SourceProvider;
import com.google.inject.internal.util.StackTraceElements;
import com.google.inject.matcher.Matcher;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Sets;

public final class Elements {
    private static final BindingTargetVisitor<Object, Object> GET_INSTANCE_VISITOR = new DefaultBindingTargetVisitor<Object, Object>() {
        public Object visit(InstanceBinding<?> binding) {
            return binding.getInstance();
        }

        /* access modifiers changed from: protected */
        public Object visitOther(Binding<?> binding) {
            throw new IllegalArgumentException();
        }
    };

    private static class ElementsAsModule implements Module {
        private final Iterable<? extends Element> elements;

        ElementsAsModule(Iterable<? extends Element> elements2) {
            this.elements = elements2;
        }

        public void configure(Binder binder) {
            for (Element element : this.elements) {
                element.applyTo(binder);
            }
        }
    }

    private static class RecordingBinder implements Binder, PrivateBinder {
        /* access modifiers changed from: private */
        public final List<Element> elements;
        private ModuleSource moduleSource;
        private final Set<Module> modules;
        private final RecordingBinder parent;
        private final PrivateElementsImpl privateElements;
        private final Object source;
        private final SourceProvider sourceProvider;
        private final Stage stage;

        private RecordingBinder(Stage stage2) {
            this.moduleSource = null;
            this.stage = stage2;
            this.modules = Sets.newHashSet();
            this.elements = Lists.newArrayList();
            this.source = null;
            this.sourceProvider = SourceProvider.DEFAULT_INSTANCE.plusSkippedClasses(Elements.class, RecordingBinder.class, AbstractModule.class, ConstantBindingBuilderImpl.class, AbstractBindingBuilder.class, BindingBuilder.class);
            this.parent = null;
            this.privateElements = null;
        }

        private RecordingBinder(RecordingBinder prototype, Object source2, SourceProvider sourceProvider2) {
            boolean z = true;
            this.moduleSource = null;
            boolean z2 = source2 == null;
            if (sourceProvider2 != null) {
                z = false;
            }
            Preconditions.checkArgument(z ^ z2);
            this.stage = prototype.stage;
            this.modules = prototype.modules;
            this.elements = prototype.elements;
            this.source = source2;
            this.moduleSource = prototype.moduleSource;
            this.sourceProvider = sourceProvider2;
            this.parent = prototype.parent;
            this.privateElements = prototype.privateElements;
        }

        private RecordingBinder(RecordingBinder parent2, PrivateElementsImpl privateElements2) {
            this.moduleSource = null;
            this.stage = parent2.stage;
            this.modules = Sets.newHashSet();
            this.elements = privateElements2.getElementsMutable();
            this.source = parent2.source;
            this.moduleSource = parent2.moduleSource;
            this.sourceProvider = parent2.sourceProvider;
            this.parent = parent2;
            this.privateElements = privateElements2;
        }

        public void bindScope(Class<? extends Annotation> annotationType, Scope scope) {
            this.elements.add(new ScopeBinding(getElementSource(), annotationType, scope));
        }

        public void requestInjection(Object instance) {
            requestInjection(TypeLiteral.get(instance.getClass()), instance);
        }

        public <T> void requestInjection(TypeLiteral<T> type, T instance) {
            this.elements.add(new InjectionRequest(getElementSource(), type, instance));
        }

        public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> typeLiteral) {
            MembersInjectorLookup<T> element = new MembersInjectorLookup<>(getElementSource(), typeLiteral);
            this.elements.add(element);
            return element.getMembersInjector();
        }

        public <T> MembersInjector<T> getMembersInjector(Class<T> type) {
            return getMembersInjector(TypeLiteral.get(type));
        }

        public void bindListener(Matcher<? super TypeLiteral<?>> typeMatcher, TypeListener listener) {
            this.elements.add(new TypeListenerBinding(getElementSource(), listener, typeMatcher));
        }

        public void bindListener(Matcher<? super Binding<?>> bindingMatcher, ProvisionListener... listeners) {
            this.elements.add(new ProvisionListenerBinding(getElementSource(), bindingMatcher, listeners));
        }

        public void requestStaticInjection(Class<?>... types) {
            for (Class<?> type : types) {
                this.elements.add(new StaticInjectionRequest(getElementSource(), type));
            }
        }

        public void install(Module module) {
            if (this.modules.add(module)) {
                Binder binder = this;
                if (!(module instanceof ProviderMethodsModule)) {
                    this.moduleSource = getModuleSource(module);
                }
                if (module instanceof PrivateModule) {
                    binder = binder.newPrivateBinder();
                }
                try {
                    module.configure(binder);
                } catch (RuntimeException e) {
                    Collection<Message> messages = Errors.getMessagesFromThrowable(e);
                    if (!messages.isEmpty()) {
                        this.elements.addAll(messages);
                    } else {
                        addError((Throwable) e);
                    }
                }
                binder.install(ProviderMethodsModule.forModule(module));
                if (!(module instanceof ProviderMethodsModule)) {
                    this.moduleSource = this.moduleSource.getParent();
                }
            }
        }

        public Stage currentStage() {
            return this.stage;
        }

        public void addError(String message, Object... arguments) {
            this.elements.add(new Message((Object) getElementSource(), Errors.format(message, arguments)));
        }

        public void addError(Throwable t) {
            this.elements.add(new Message(ImmutableList.m109of(getElementSource()), "An exception was caught and reported. Message: " + t.getMessage(), t));
        }

        public void addError(Message message) {
            this.elements.add(message);
        }

        public <T> AnnotatedBindingBuilder<T> bind(Key<T> key) {
            return new BindingBuilder<>(this, this.elements, getElementSource(), key);
        }

        public <T> AnnotatedBindingBuilder<T> bind(TypeLiteral<T> typeLiteral) {
            return bind(Key.get(typeLiteral));
        }

        public <T> AnnotatedBindingBuilder<T> bind(Class<T> type) {
            return bind(Key.get(type));
        }

        public AnnotatedConstantBindingBuilder bindConstant() {
            return new ConstantBindingBuilderImpl(this, this.elements, getElementSource());
        }

        public <T> Provider<T> getProvider(Key<T> key) {
            ProviderLookup<T> element = new ProviderLookup<>(getElementSource(), key);
            this.elements.add(element);
            return element.getProvider();
        }

        public <T> Provider<T> getProvider(Class<T> type) {
            return getProvider(Key.get(type));
        }

        public void convertToTypes(Matcher<? super TypeLiteral<?>> typeMatcher, TypeConverter converter) {
            this.elements.add(new TypeConverterBinding(getElementSource(), typeMatcher, converter));
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        public RecordingBinder withSource(Object source2) {
            return source2 == this.source ? this : new RecordingBinder(this, source2, null);
        }

        /* Debug info: failed to restart local var, previous not found, register: 3 */
        public RecordingBinder skipSources(Class... classesToSkip) {
            return this.source != null ? this : new RecordingBinder(this, null, this.sourceProvider.plusSkippedClasses(classesToSkip));
        }

        public PrivateBinder newPrivateBinder() {
            PrivateElementsImpl privateElements2 = new PrivateElementsImpl(getElementSource());
            RecordingBinder binder = new RecordingBinder(this, privateElements2);
            this.elements.add(privateElements2);
            return binder;
        }

        public void disableCircularProxies() {
            this.elements.add(new DisableCircularProxiesOption(getElementSource()));
        }

        public void requireExplicitBindings() {
            this.elements.add(new RequireExplicitBindingsOption(getElementSource()));
        }

        public void requireAtInjectOnConstructors() {
            this.elements.add(new RequireAtInjectOnConstructorsOption(getElementSource()));
        }

        public void requireExactBindingAnnotations() {
            this.elements.add(new RequireExactBindingAnnotationsOption(getElementSource()));
        }

        public void expose(Key<?> key) {
            exposeInternal(key);
        }

        public AnnotatedElementBuilder expose(Class<?> type) {
            return exposeInternal(Key.get(type));
        }

        public AnnotatedElementBuilder expose(TypeLiteral<?> type) {
            return exposeInternal(Key.get(type));
        }

        private <T> AnnotatedElementBuilder exposeInternal(Key<T> key) {
            if (this.privateElements == null) {
                addError("Cannot expose %s on a standard binder. Exposed bindings are only applicable to private binders.", key);
                return new AnnotatedElementBuilder() {
                    public void annotatedWith(Class<? extends Annotation> cls) {
                    }

                    public void annotatedWith(Annotation annotation) {
                    }
                };
            }
            ExposureBuilder<T> builder = new ExposureBuilder<>(this, getElementSource(), key);
            this.privateElements.addExposureBuilder(builder);
            return builder;
        }

        private ModuleSource getModuleSource(Module module) {
            StackTraceElement[] partialCallStack;
            if (InternalFlags.getIncludeStackTraceOption() == IncludeStackTraceOption.COMPLETE) {
                partialCallStack = getPartialCallStack(new Throwable().getStackTrace());
            } else {
                partialCallStack = new StackTraceElement[0];
            }
            if (this.moduleSource == null) {
                return new ModuleSource(module, partialCallStack);
            }
            return this.moduleSource.createChild(module, partialCallStack);
        }

        private ElementSource getElementSource() {
            StackTraceElement[] callStack = null;
            StackTraceElement[] partialCallStack = new StackTraceElement[0];
            ElementSource originalSource = null;
            Object declaringSource = this.source;
            if (declaringSource instanceof ElementSource) {
                originalSource = (ElementSource) declaringSource;
                declaringSource = originalSource.getDeclaringSource();
            }
            IncludeStackTraceOption stackTraceOption = InternalFlags.getIncludeStackTraceOption();
            if (stackTraceOption == IncludeStackTraceOption.COMPLETE || (stackTraceOption == IncludeStackTraceOption.ONLY_FOR_DECLARING_SOURCE && declaringSource == null)) {
                callStack = new Throwable().getStackTrace();
            }
            if (stackTraceOption == IncludeStackTraceOption.COMPLETE) {
                partialCallStack = getPartialCallStack(callStack);
            }
            if (declaringSource == null) {
                if (stackTraceOption == IncludeStackTraceOption.COMPLETE || stackTraceOption == IncludeStackTraceOption.ONLY_FOR_DECLARING_SOURCE) {
                    declaringSource = this.sourceProvider.get(callStack);
                } else {
                    declaringSource = this.sourceProvider.getFromClassNames(this.moduleSource.getModuleClassNames());
                }
            }
            return new ElementSource(originalSource, declaringSource, this.moduleSource, partialCallStack);
        }

        private StackTraceElement[] getPartialCallStack(StackTraceElement[] callStack) {
            int toSkip = 0;
            if (this.moduleSource != null) {
                toSkip = this.moduleSource.getStackTraceSize();
            }
            int chunkSize = (callStack.length - toSkip) - 1;
            StackTraceElement[] partialCallStack = new StackTraceElement[chunkSize];
            System.arraycopy(callStack, 1, partialCallStack, 0, chunkSize);
            return partialCallStack;
        }

        public String toString() {
            return "Binder";
        }
    }

    public static List<Element> getElements(Module... modules) {
        return getElements(Stage.DEVELOPMENT, (Iterable<? extends Module>) Arrays.asList(modules));
    }

    public static List<Element> getElements(Stage stage, Module... modules) {
        return getElements(stage, (Iterable<? extends Module>) Arrays.asList(modules));
    }

    public static List<Element> getElements(Iterable<? extends Module> modules) {
        return getElements(Stage.DEVELOPMENT, modules);
    }

    public static List<Element> getElements(Stage stage, Iterable<? extends Module> modules) {
        RecordingBinder binder = new RecordingBinder(stage);
        for (Module module : modules) {
            binder.install(module);
        }
        StackTraceElements.clearCache();
        return Collections.unmodifiableList(binder.elements);
    }

    public static Module getModule(Iterable<? extends Element> elements) {
        return new ElementsAsModule(elements);
    }

    static <T> BindingTargetVisitor<T, T> getInstanceVisitor() {
        return GET_INSTANCE_VISITOR;
    }
}
