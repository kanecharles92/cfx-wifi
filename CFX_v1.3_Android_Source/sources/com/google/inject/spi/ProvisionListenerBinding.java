package com.google.inject.spi;

import com.google.inject.Binder;
import com.google.inject.Binding;
import com.google.inject.matcher.Matcher;
import java.util.List;
import org.roboguice.shaded.goole.common.collect.ImmutableList;

public final class ProvisionListenerBinding implements Element {
    private final Matcher<? super Binding<?>> bindingMatcher;
    private final List<ProvisionListener> listeners;
    private final Object source;

    ProvisionListenerBinding(Object source2, Matcher<? super Binding<?>> bindingMatcher2, ProvisionListener[] listeners2) {
        this.source = source2;
        this.bindingMatcher = bindingMatcher2;
        this.listeners = ImmutableList.copyOf((E[]) listeners2);
    }

    public List<ProvisionListener> getListeners() {
        return this.listeners;
    }

    public Matcher<? super Binding<?>> getBindingMatcher() {
        return this.bindingMatcher;
    }

    public Object getSource() {
        return this.source;
    }

    public <R> R acceptVisitor(ElementVisitor<R> visitor) {
        return visitor.visit(this);
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).bindListener(this.bindingMatcher, (ProvisionListener[]) this.listeners.toArray(new ProvisionListener[this.listeners.size()]));
    }
}
