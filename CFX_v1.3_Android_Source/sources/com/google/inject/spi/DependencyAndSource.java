package com.google.inject.spi;

import com.google.inject.internal.util.StackTraceElements;
import java.lang.reflect.Member;

public final class DependencyAndSource {
    private final Dependency<?> dependency;
    private final Object source;

    public DependencyAndSource(Dependency<?> dependency2, Object source2) {
        this.dependency = dependency2;
        this.source = source2;
    }

    public Dependency<?> getDependency() {
        return this.dependency;
    }

    public String getBindingSource() {
        if (this.source instanceof Class) {
            return StackTraceElements.forType((Class) this.source).toString();
        }
        if (this.source instanceof Member) {
            return StackTraceElements.forMember((Member) this.source).toString();
        }
        return this.source.toString();
    }

    public String toString() {
        Dependency<?> dep = getDependency();
        String source2 = getBindingSource();
        if (dep != null) {
            return "Dependency: " + dep + ", source: " + source2;
        }
        return "Source: " + source2;
    }
}
