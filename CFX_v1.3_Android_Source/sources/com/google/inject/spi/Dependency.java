package com.google.inject.spi;

import com.google.inject.Key;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.Lists;

public final class Dependency<T> {
    private final InjectionPoint injectionPoint;
    private final Key<T> key;
    private final boolean nullable;
    private final int parameterIndex;

    Dependency(InjectionPoint injectionPoint2, Key<T> key2, boolean nullable2, int parameterIndex2) {
        this.injectionPoint = injectionPoint2;
        this.key = (Key) Preconditions.checkNotNull(key2, "key");
        this.nullable = nullable2;
        this.parameterIndex = parameterIndex2;
    }

    public static <T> Dependency<T> get(Key<T> key2) {
        return new Dependency<>(null, key2, true, -1);
    }

    public static Set<Dependency<?>> forInjectionPoints(Set<InjectionPoint> injectionPoints) {
        List<Dependency<?>> dependencies = Lists.newArrayList();
        for (InjectionPoint injectionPoint2 : injectionPoints) {
            dependencies.addAll(injectionPoint2.getDependencies());
        }
        return ImmutableSet.copyOf((Collection<? extends E>) dependencies);
    }

    public Key<T> getKey() {
        return this.key;
    }

    public boolean isNullable() {
        return this.nullable;
    }

    public InjectionPoint getInjectionPoint() {
        return this.injectionPoint;
    }

    public int getParameterIndex() {
        return this.parameterIndex;
    }

    public int hashCode() {
        return Objects.hashCode(this.injectionPoint, Integer.valueOf(this.parameterIndex), this.key);
    }

    public boolean equals(Object o) {
        if (!(o instanceof Dependency)) {
            return false;
        }
        Dependency dependency = (Dependency) o;
        if (!Objects.equal(this.injectionPoint, dependency.injectionPoint) || !Objects.equal(Integer.valueOf(this.parameterIndex), Integer.valueOf(dependency.parameterIndex)) || !Objects.equal(this.key, dependency.key)) {
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.key);
        if (this.injectionPoint != null) {
            builder.append("@").append(this.injectionPoint);
            if (this.parameterIndex != -1) {
                builder.append("[").append(this.parameterIndex).append("]");
            }
        }
        return builder.toString();
    }
}
