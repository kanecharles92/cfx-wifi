package com.google.inject.spi;

import com.google.inject.Binder;
import org.roboguice.shaded.goole.common.base.Preconditions;

public final class RequireExplicitBindingsOption implements Element {
    private final Object source;

    RequireExplicitBindingsOption(Object source2) {
        this.source = Preconditions.checkNotNull(source2, "source");
    }

    public Object getSource() {
        return this.source;
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).requireExplicitBindings();
    }

    public <T> T acceptVisitor(ElementVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
