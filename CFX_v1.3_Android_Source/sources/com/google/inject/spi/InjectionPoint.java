package com.google.inject.spi;

import com.google.inject.ConfigurationException;
import com.google.inject.Guice;
import com.google.inject.HierarchyTraversalFilter;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.Annotations;
import com.google.inject.internal.Errors;
import com.google.inject.internal.ErrorsException;
import com.google.inject.internal.MoreTypes;
import com.google.inject.internal.Nullability;
import com.google.inject.internal.util.Classes;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.ImmutableSet.Builder;
import org.roboguice.shaded.goole.common.collect.Lists;

public final class InjectionPoint {
    private static HierarchyTraversalFilter filter = Guice.createHierarchyTraversalFilter();
    private static final Logger logger = Logger.getLogger(InjectionPoint.class.getName());
    private final TypeLiteral<?> declaringType;
    private final ImmutableList<Dependency<?>> dependencies;
    private final Member member;
    private final boolean optional;

    static class InjectableField extends InjectableMember {
        final Field field;

        InjectableField(TypeLiteral<?> declaringType, Field field2, Annotation atInject) {
            super(declaringType, atInject);
            this.field = field2;
        }

        /* access modifiers changed from: 0000 */
        public InjectionPoint toInjectionPoint() {
            return new InjectionPoint(this.declaringType, this.field, this.optional);
        }
    }

    static abstract class InjectableMember {
        final TypeLiteral<?> declaringType;
        final boolean jsr330;
        InjectableMember next;
        final boolean optional;
        InjectableMember previous;

        /* access modifiers changed from: 0000 */
        public abstract InjectionPoint toInjectionPoint();

        InjectableMember(TypeLiteral<?> declaringType2, Annotation atInject) {
            this.declaringType = declaringType2;
            if (atInject.annotationType() == Inject.class) {
                this.optional = false;
                this.jsr330 = true;
                return;
            }
            this.jsr330 = false;
            this.optional = ((com.google.inject.Inject) atInject).optional();
        }
    }

    static class InjectableMembers {
        InjectableMember head;
        InjectableMember tail;

        InjectableMembers() {
        }

        /* access modifiers changed from: 0000 */
        public void add(InjectableMember member) {
            if (this.head == null) {
                this.tail = member;
                this.head = member;
                return;
            }
            member.previous = this.tail;
            this.tail.next = member;
            this.tail = member;
        }

        /* access modifiers changed from: 0000 */
        public void addAll(InjectableMembers members) {
            for (InjectableMember memberToAdd = members.head; memberToAdd != null; memberToAdd = memberToAdd.next) {
                add(memberToAdd);
            }
        }

        /* access modifiers changed from: 0000 */
        public void remove(InjectableMember member) {
            if (member.previous != null) {
                member.previous.next = member.next;
            }
            if (member.next != null) {
                member.next.previous = member.previous;
            }
            if (this.head == member) {
                this.head = member.next;
            }
            if (this.tail == member) {
                this.tail = member.previous;
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean isEmpty() {
            return this.head == null;
        }
    }

    static class InjectableMethod extends InjectableMember {
        final Method method;
        boolean overrodeGuiceInject;

        InjectableMethod(TypeLiteral<?> declaringType, Method method2, Annotation atInject) {
            super(declaringType, atInject);
            this.method = method2;
        }

        /* access modifiers changed from: 0000 */
        public InjectionPoint toInjectionPoint() {
            return new InjectionPoint(this.declaringType, this.method, this.optional);
        }

        public boolean isFinal() {
            return Modifier.isFinal(this.method.getModifiers());
        }
    }

    static class OverrideIndex {
        Map<Signature, List<InjectableMethod>> bySignature;
        final InjectableMembers injectableMembers;
        Method lastMethod;
        Signature lastSignature;
        Position position = Position.TOP;

        OverrideIndex(InjectableMembers injectableMembers2) {
            this.injectableMembers = injectableMembers2;
        }

        /* access modifiers changed from: 0000 */
        public boolean removeIfOverriddenBy(Method method, boolean alwaysRemove, InjectableMethod injectableMethod) {
            boolean wasGuiceInject;
            if (this.position == Position.TOP) {
                return false;
            }
            if (this.bySignature == null) {
                this.bySignature = new HashMap();
                for (InjectableMember member = this.injectableMembers.head; member != null; member = member.next) {
                    if (member instanceof InjectableMethod) {
                        InjectableMethod im = (InjectableMethod) member;
                        if (!im.isFinal()) {
                            List<InjectableMethod> methods = new ArrayList<>();
                            methods.add(im);
                            this.bySignature.put(new Signature(im.method), methods);
                        }
                    }
                }
            }
            this.lastMethod = method;
            Signature signature = new Signature(method);
            this.lastSignature = signature;
            List<InjectableMethod> methods2 = (List) this.bySignature.get(signature);
            boolean removed = false;
            if (methods2 == null) {
                return false;
            }
            Iterator<InjectableMethod> iterator = methods2.iterator();
            while (iterator.hasNext()) {
                InjectableMethod possiblyOverridden = (InjectableMethod) iterator.next();
                if (InjectionPoint.overrides(method, possiblyOverridden.method)) {
                    if (!possiblyOverridden.jsr330 || possiblyOverridden.overrodeGuiceInject) {
                        wasGuiceInject = true;
                    } else {
                        wasGuiceInject = false;
                    }
                    if (injectableMethod != null) {
                        injectableMethod.overrodeGuiceInject = wasGuiceInject;
                    }
                    if (alwaysRemove || !wasGuiceInject) {
                        removed = true;
                        iterator.remove();
                        this.injectableMembers.remove(possiblyOverridden);
                    }
                }
            }
            return removed;
        }

        /* access modifiers changed from: 0000 */
        public void add(InjectableMethod injectableMethod) {
            this.injectableMembers.add(injectableMethod);
            if (this.position != Position.BOTTOM && !injectableMethod.isFinal() && this.bySignature != null) {
                Signature signature = injectableMethod.method == this.lastMethod ? this.lastSignature : new Signature(injectableMethod.method);
                List<InjectableMethod> methods = (List) this.bySignature.get(signature);
                if (methods == null) {
                    methods = new ArrayList<>();
                    this.bySignature.put(signature, methods);
                }
                methods.add(injectableMethod);
            }
        }
    }

    static class Pair<A, B> {

        /* renamed from: a */
        final A f29a;

        /* renamed from: b */
        final B f30b;

        public Pair(A a, B b) {
            this.f29a = a;
            this.f30b = b;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Pair pair = (Pair) o;
            if (!this.f29a.equals(pair.f29a) || !this.f30b.equals(pair.f30b)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (this.f29a.hashCode() * 31) + this.f30b.hashCode();
        }
    }

    enum Position {
        TOP,
        MIDDLE,
        BOTTOM
    }

    static class Signature {
        final int hash;
        final String name;
        final Class[] parameterTypes;

        Signature(Method method) {
            this.name = method.getName();
            this.parameterTypes = method.getParameterTypes();
            int h = (this.name.hashCode() * 31) + this.parameterTypes.length;
            for (Class parameterType : this.parameterTypes) {
                h = (h * 31) + parameterType.hashCode();
            }
            this.hash = h;
        }

        public int hashCode() {
            return this.hash;
        }

        public boolean equals(Object o) {
            if (!(o instanceof Signature)) {
                return false;
            }
            Signature other = (Signature) o;
            if (!this.name.equals(other.name) || this.parameterTypes.length != other.parameterTypes.length) {
                return false;
            }
            for (int i = 0; i < this.parameterTypes.length; i++) {
                if (this.parameterTypes[i] != other.parameterTypes[i]) {
                    return false;
                }
            }
            return true;
        }
    }

    InjectionPoint(TypeLiteral<?> declaringType2, Method method, boolean optional2) {
        this.member = method;
        this.declaringType = declaringType2;
        this.optional = optional2;
        this.dependencies = forMember(method, declaringType2, method.getParameterAnnotations());
    }

    InjectionPoint(TypeLiteral<?> declaringType2, Constructor<?> constructor) {
        this.member = constructor;
        this.declaringType = declaringType2;
        this.optional = false;
        this.dependencies = forMember(constructor, declaringType2, constructor.getParameterAnnotations());
    }

    InjectionPoint(TypeLiteral<?> declaringType2, Field field, boolean optional2) {
        this.member = field;
        this.declaringType = declaringType2;
        this.optional = optional2;
        Annotation[] annotations = field.getAnnotations();
        Errors errors = new Errors(field);
        Key<?> key = null;
        try {
            key = Annotations.getKey(declaringType2.getFieldType(field), field, annotations, errors);
        } catch (ConfigurationException e) {
            errors.merge(e.getErrorMessages());
        } catch (ErrorsException e2) {
            errors.merge(e2.getErrors());
        }
        errors.throwConfigurationExceptionIfErrorsExist();
        this.dependencies = ImmutableList.m109of(newDependency(key, Nullability.allowsNull(annotations), -1));
    }

    private ImmutableList<Dependency<?>> forMember(Member member2, TypeLiteral<?> type, Annotation[][] paramterAnnotations) {
        Errors errors = new Errors(member2);
        Iterator<Annotation[]> annotationsIterator = Arrays.asList(paramterAnnotations).iterator();
        List<Dependency<?>> dependencies2 = Lists.newArrayList();
        int index = 0;
        for (TypeLiteral<?> parameterType : type.getParameterTypes(member2)) {
            try {
                Annotation[] parameterAnnotations = (Annotation[]) annotationsIterator.next();
                dependencies2.add(newDependency(Annotations.getKey(parameterType, member2, parameterAnnotations, errors), Nullability.allowsNull(parameterAnnotations), index));
                index++;
            } catch (ConfigurationException e) {
                errors.merge(e.getErrorMessages());
            } catch (ErrorsException e2) {
                errors.merge(e2.getErrors());
            }
        }
        errors.throwConfigurationExceptionIfErrorsExist();
        return ImmutableList.copyOf((Collection<? extends E>) dependencies2);
    }

    private <T> Dependency<T> newDependency(Key<T> key, boolean allowsNull, int parameterIndex) {
        return new Dependency<>(this, key, allowsNull, parameterIndex);
    }

    public Member getMember() {
        return this.member;
    }

    public List<Dependency<?>> getDependencies() {
        return this.dependencies;
    }

    public boolean isOptional() {
        return this.optional;
    }

    public boolean isToolable() {
        return ((AnnotatedElement) this.member).isAnnotationPresent(Toolable.class);
    }

    public TypeLiteral<?> getDeclaringType() {
        return this.declaringType;
    }

    public boolean equals(Object o) {
        return (o instanceof InjectionPoint) && this.member.equals(((InjectionPoint) o).member) && this.declaringType.equals(((InjectionPoint) o).declaringType);
    }

    public int hashCode() {
        return this.member.hashCode() ^ this.declaringType.hashCode();
    }

    public String toString() {
        return Classes.toString(this.member);
    }

    public static <T> InjectionPoint forConstructor(Constructor<T> constructor) {
        return new InjectionPoint(TypeLiteral.get(constructor.getDeclaringClass()), constructor);
    }

    public static <T> InjectionPoint forConstructor(Constructor<T> constructor, TypeLiteral<? extends T> type) {
        if (type.getRawType() != constructor.getDeclaringClass()) {
            new Errors(type).constructorNotDefinedByType(constructor, type).throwConfigurationExceptionIfErrorsExist();
        }
        return new InjectionPoint(type, constructor);
    }

    public static InjectionPoint forConstructorOf(TypeLiteral<?> type) {
        boolean optional2;
        Class<?> rawType = MoreTypes.getRawType(type.getType());
        Errors errors = new Errors(rawType);
        filter.reset();
        Constructor<?> injectableConstructor = null;
        if (filter.isWorthScanningForConstructors(com.google.inject.Inject.class.getName(), rawType)) {
            for (Constructor<?> constructor : filter.getAllConstructors(com.google.inject.Inject.class.getName(), rawType)) {
                com.google.inject.Inject guiceInject = (com.google.inject.Inject) constructor.getAnnotation(com.google.inject.Inject.class);
                if (guiceInject != null) {
                    optional2 = guiceInject.optional();
                } else if (((Inject) constructor.getAnnotation(Inject.class)) != null) {
                    optional2 = false;
                }
                if (optional2) {
                    errors.optionalConstructor(constructor);
                }
                if (injectableConstructor != null) {
                    errors.tooManyConstructors(rawType);
                }
                injectableConstructor = constructor;
                checkForMisplacedBindingAnnotations(injectableConstructor, errors);
            }
        }
        errors.throwConfigurationExceptionIfErrorsExist();
        if (injectableConstructor != null) {
            return new InjectionPoint(type, injectableConstructor);
        }
        try {
            Constructor<?> noArgConstructor = rawType.getDeclaredConstructor(new Class[0]);
            if (!Modifier.isPrivate(noArgConstructor.getModifiers()) || Modifier.isPrivate(rawType.getModifiers())) {
                checkForMisplacedBindingAnnotations(noArgConstructor, errors);
                return new InjectionPoint(type, noArgConstructor);
            }
            errors.missingConstructor(rawType);
            throw new ConfigurationException(errors.getMessages());
        } catch (NoSuchMethodException e) {
            errors.missingConstructor(rawType);
            throw new ConfigurationException(errors.getMessages());
        }
    }

    public static InjectionPoint forConstructorOf(Class<?> type) {
        return forConstructorOf(TypeLiteral.get(type));
    }

    public static Set<InjectionPoint> forStaticMethodsAndFields(TypeLiteral<?> type) {
        Set<InjectionPoint> result;
        Errors errors = new Errors();
        if (type.getRawType().isInterface()) {
            errors.staticInjectionOnInterface(type.getRawType());
            result = null;
        } else {
            result = getInjectionPoints(type, true, errors);
        }
        if (!errors.hasErrors()) {
            return result;
        }
        throw new ConfigurationException(errors.getMessages()).withPartialValue(result);
    }

    public static Set<InjectionPoint> forStaticMethodsAndFields(Class<?> type) {
        return forStaticMethodsAndFields(TypeLiteral.get(type));
    }

    public static Set<InjectionPoint> forInstanceMethodsAndFields(TypeLiteral<?> type) {
        Errors errors = new Errors();
        Set<InjectionPoint> result = getInjectionPoints(type, false, errors);
        if (!errors.hasErrors()) {
            return result;
        }
        throw new ConfigurationException(errors.getMessages()).withPartialValue(result);
    }

    public static Set<InjectionPoint> forInstanceMethodsAndFields(Class<?> type) {
        return forInstanceMethodsAndFields(TypeLiteral.get(type));
    }

    private static boolean checkForMisplacedBindingAnnotations(Member member2, Errors errors) {
        Annotation misplacedBindingAnnotation = Annotations.findBindingAnnotation(errors, member2, ((AnnotatedElement) member2).getAnnotations());
        if (misplacedBindingAnnotation == null) {
            return false;
        }
        if (member2 instanceof Method) {
            try {
                if (member2.getDeclaringClass().getDeclaredField(member2.getName()) != null) {
                    return false;
                }
            } catch (NoSuchFieldException e) {
            }
        }
        errors.misplacedBindingAnnotation(member2, misplacedBindingAnnotation);
        return true;
    }

    static Annotation getAtInject(AnnotatedElement member2) {
        Annotation a = member2.getAnnotation(Inject.class);
        return a == null ? member2.getAnnotation(com.google.inject.Inject.class) : a;
    }

    private static Set<InjectionPoint> getInjectionPoints(TypeLiteral<?> type, boolean statics, Errors errors) {
        InjectableMembers injectableMembers = new InjectableMembers();
        OverrideIndex overrideIndex = new OverrideIndex(injectableMembers);
        overrideIndex.position = Position.BOTTOM;
        filter.reset();
        computeInjectableMembers(type, statics, errors, injectableMembers, overrideIndex, filter);
        if (injectableMembers.isEmpty()) {
            return Collections.emptySet();
        }
        Builder<InjectionPoint> builder = ImmutableSet.builder();
        for (InjectableMember im = injectableMembers.head; im != null; im = im.next) {
            try {
                builder.add((Object) im.toInjectionPoint());
            } catch (ConfigurationException ignorable) {
                if (!im.optional) {
                    errors.merge(ignorable.getErrorMessages());
                }
            }
        }
        return builder.build();
    }

    private static void computeInjectableMembers(TypeLiteral<?> type, boolean statics, Errors errors, InjectableMembers injectableMembers, OverrideIndex overrideIndex, HierarchyTraversalFilter filter2) {
        Class<?> rawType = type.getRawType();
        if (isWorthScanning(filter2, rawType)) {
            Class<?> parentRawType = rawType.getSuperclass();
            if (isWorthScanning(filter2, parentRawType)) {
                computeInjectableMembers(type.getSupertype(parentRawType), statics, errors, injectableMembers, overrideIndex, filter2);
                overrideIndex.position = Position.MIDDLE;
            } else {
                overrideIndex.position = Position.TOP;
            }
            Set<Field> allFields = filter2.getAllFields(com.google.inject.Inject.class.getName(), rawType);
            if (allFields != null) {
                for (Field field : allFields) {
                    if (Modifier.isStatic(field.getModifiers()) == statics) {
                        Annotation atInject = getAtInject(field);
                        if (atInject != null) {
                            InjectableField injectableField = new InjectableField(type, field, atInject);
                            if (injectableField.jsr330 && Modifier.isFinal(field.getModifiers())) {
                                errors.cannotInjectFinalField(field);
                            }
                            injectableMembers.add(injectableField);
                        }
                    }
                }
            }
            Set<Method> allMethods = filter2.getAllMethods(com.google.inject.Inject.class.getName(), rawType);
            if (allMethods != null) {
                for (Method method : allMethods) {
                    if (isEligibleForInjection(method, statics)) {
                        Annotation atInject2 = getAtInject(method);
                        if (atInject2 != null) {
                            InjectableMethod injectableMethod = new InjectableMethod(type, method, atInject2);
                            if (checkForMisplacedBindingAnnotations(method, errors) || !isValidMethod(injectableMethod, errors)) {
                                if (overrideIndex.removeIfOverriddenBy(method, false, injectableMethod)) {
                                    logger.log(Level.WARNING, "Method: {0} is not a valid injectable method (because it either has misplaced binding annotations or specifies type parameters) but is overriding a method that is valid. Because it is not valid, the method will not be injected. To fix this, make the method a valid injectable method.", method);
                                }
                            } else if (statics) {
                                injectableMembers.add(injectableMethod);
                            } else {
                                overrideIndex.removeIfOverriddenBy(method, true, injectableMethod);
                                overrideIndex.add(injectableMethod);
                            }
                        } else if (overrideIndex.removeIfOverriddenBy(method, false, null)) {
                            logger.log(Level.WARNING, "Method: {0} is not annotated with @Inject but is overriding a method that is annotated with @javax.inject.Inject.  Because it is not annotated with @Inject, the method will not be injected. To fix this, annotate the method with @Inject.", method);
                        }
                    }
                }
            }
        }
    }

    private static boolean isWorthScanning(HierarchyTraversalFilter filter2, Class<?> rawType) {
        return filter2.isWorthScanningForFields(com.google.inject.Inject.class.getName(), rawType);
    }

    private static boolean isEligibleForInjection(Method method, boolean statics) {
        return Modifier.isStatic(method.getModifiers()) == statics && !method.isBridge() && !method.isSynthetic();
    }

    private static boolean isValidMethod(InjectableMethod injectableMethod, Errors errors) {
        boolean result = true;
        if (!injectableMethod.jsr330) {
            return true;
        }
        Method method = injectableMethod.method;
        if (Modifier.isAbstract(method.getModifiers())) {
            errors.cannotInjectAbstractMethod(method);
            result = false;
        }
        if (method.getTypeParameters().length <= 0) {
            return result;
        }
        errors.cannotInjectMethodWithTypeParameters(method);
        return false;
    }

    private static List<TypeLiteral<?>> hierarchyFor(TypeLiteral<?> type) {
        List<TypeLiteral<?>> hierarchy = new ArrayList<>();
        for (TypeLiteral<?> current = type; current.getRawType() != Object.class; current = current.getSupertype(current.getRawType().getSuperclass())) {
            hierarchy.add(current);
        }
        return hierarchy;
    }

    /* access modifiers changed from: private */
    public static boolean overrides(Method a, Method b) {
        int modifiers = b.getModifiers();
        if (Modifier.isPublic(modifiers) || Modifier.isProtected(modifiers)) {
            return true;
        }
        if (Modifier.isPrivate(modifiers)) {
            return false;
        }
        return a.getDeclaringClass().getPackage().equals(b.getDeclaringClass().getPackage());
    }
}
