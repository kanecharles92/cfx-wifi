package com.google.inject.spi;

import com.google.inject.Binder;
import com.google.inject.MembersInjector;
import com.google.inject.TypeLiteral;
import org.roboguice.shaded.goole.common.base.Preconditions;

public final class MembersInjectorLookup<T> implements Element {
    /* access modifiers changed from: private */
    public MembersInjector<T> delegate;
    private final Object source;
    /* access modifiers changed from: private */
    public final TypeLiteral<T> type;

    public MembersInjectorLookup(Object source2, TypeLiteral<T> type2) {
        this.source = Preconditions.checkNotNull(source2, "source");
        this.type = (TypeLiteral) Preconditions.checkNotNull(type2, "type");
    }

    public Object getSource() {
        return this.source;
    }

    public TypeLiteral<T> getType() {
        return this.type;
    }

    public <T> T acceptVisitor(ElementVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public void initializeDelegate(MembersInjector<T> delegate2) {
        Preconditions.checkState(this.delegate == null, "delegate already initialized");
        this.delegate = (MembersInjector) Preconditions.checkNotNull(delegate2, "delegate");
    }

    public void applyTo(Binder binder) {
        initializeDelegate(binder.withSource(getSource()).getMembersInjector(this.type));
    }

    public MembersInjector<T> getDelegate() {
        return this.delegate;
    }

    public MembersInjector<T> getMembersInjector() {
        return new MembersInjector<T>() {
            public void injectMembers(T instance) {
                Preconditions.checkState(MembersInjectorLookup.this.delegate != null, "This MembersInjector cannot be used until the Injector has been created.");
                MembersInjectorLookup.this.delegate.injectMembers(instance);
            }

            public String toString() {
                return "MembersInjector<" + MembersInjectorLookup.this.type + ">";
            }
        };
    }
}
