package com.google.inject.spi;

import com.google.inject.Binder;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.Errors;
import com.google.inject.matcher.Matcher;
import org.roboguice.shaded.goole.common.base.Preconditions;

public final class TypeConverterBinding implements Element {
    private final Object source;
    private final TypeConverter typeConverter;
    private final Matcher<? super TypeLiteral<?>> typeMatcher;

    public TypeConverterBinding(Object source2, Matcher<? super TypeLiteral<?>> typeMatcher2, TypeConverter typeConverter2) {
        this.source = Preconditions.checkNotNull(source2, "source");
        this.typeMatcher = (Matcher) Preconditions.checkNotNull(typeMatcher2, "typeMatcher");
        this.typeConverter = (TypeConverter) Preconditions.checkNotNull(typeConverter2, "typeConverter");
    }

    public Object getSource() {
        return this.source;
    }

    public Matcher<? super TypeLiteral<?>> getTypeMatcher() {
        return this.typeMatcher;
    }

    public TypeConverter getTypeConverter() {
        return this.typeConverter;
    }

    public <T> T acceptVisitor(ElementVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).convertToTypes(this.typeMatcher, this.typeConverter);
    }

    public String toString() {
        return this.typeConverter + " which matches " + this.typeMatcher + " (bound at " + Errors.convert(this.source) + ")";
    }
}
