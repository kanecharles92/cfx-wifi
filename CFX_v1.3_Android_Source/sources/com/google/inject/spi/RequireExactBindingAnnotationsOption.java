package com.google.inject.spi;

import com.google.inject.Binder;
import org.roboguice.shaded.goole.common.base.Preconditions;

public final class RequireExactBindingAnnotationsOption implements Element {
    private final Object source;

    RequireExactBindingAnnotationsOption(Object source2) {
        this.source = Preconditions.checkNotNull(source2, "source");
    }

    public Object getSource() {
        return this.source;
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).requireExactBindingAnnotations();
    }

    public <T> T acceptVisitor(ElementVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
