package com.google.inject.spi;

import com.google.inject.Module;
import com.google.inject.internal.util.StackTraceElements;
import com.google.inject.internal.util.StackTraceElements.InMemoryStackTraceElement;
import java.util.List;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.ImmutableList.Builder;

final class ModuleSource {
    private final String moduleClassName;
    private final ModuleSource parent;
    private final InMemoryStackTraceElement[] partialCallStack;

    ModuleSource(Module module, StackTraceElement[] partialCallStack2) {
        this(null, module, partialCallStack2);
    }

    private ModuleSource(ModuleSource parent2, Module module, StackTraceElement[] partialCallStack2) {
        Preconditions.checkNotNull(module, "module cannot be null.");
        Preconditions.checkNotNull(partialCallStack2, "partialCallStack cannot be null.");
        this.parent = parent2;
        this.moduleClassName = module.getClass().getName();
        this.partialCallStack = StackTraceElements.convertToInMemoryStackTraceElement(partialCallStack2);
    }

    /* access modifiers changed from: 0000 */
    public String getModuleClassName() {
        return this.moduleClassName;
    }

    /* access modifiers changed from: 0000 */
    public StackTraceElement[] getPartialCallStack() {
        return StackTraceElements.convertToStackTraceElement(this.partialCallStack);
    }

    /* access modifiers changed from: 0000 */
    public int getPartialCallStackSize() {
        return this.partialCallStack.length;
    }

    /* access modifiers changed from: 0000 */
    public ModuleSource createChild(Module module, StackTraceElement[] partialCallStack2) {
        return new ModuleSource(this, module, partialCallStack2);
    }

    /* access modifiers changed from: 0000 */
    public ModuleSource getParent() {
        return this.parent;
    }

    /* access modifiers changed from: 0000 */
    public List<String> getModuleClassNames() {
        Builder<String> classNames = ImmutableList.builder();
        for (ModuleSource current = this; current != null; current = current.parent) {
            classNames.add((Object) current.moduleClassName);
        }
        return classNames.build();
    }

    /* access modifiers changed from: 0000 */
    public int size() {
        if (this.parent == null) {
            return 1;
        }
        return this.parent.size() + 1;
    }

    /* access modifiers changed from: 0000 */
    public int getStackTraceSize() {
        if (this.parent == null) {
            return this.partialCallStack.length;
        }
        return this.parent.getStackTraceSize() + this.partialCallStack.length;
    }

    /* access modifiers changed from: 0000 */
    public StackTraceElement[] getStackTrace() {
        StackTraceElement[] callStack = new StackTraceElement[getStackTraceSize()];
        int cursor = 0;
        ModuleSource current = this;
        while (current != null) {
            StackTraceElement[] chunk = StackTraceElements.convertToStackTraceElement(current.partialCallStack);
            int chunkSize = chunk.length;
            System.arraycopy(chunk, 0, callStack, cursor, chunkSize);
            current = current.parent;
            cursor += chunkSize;
        }
        return callStack;
    }
}
