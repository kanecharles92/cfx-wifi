package com.google.inject.spi;

import com.google.inject.Binder;
import com.google.inject.Scope;
import java.lang.annotation.Annotation;
import org.roboguice.shaded.goole.common.base.Preconditions;

public final class ScopeBinding implements Element {
    private final Class<? extends Annotation> annotationType;
    private final Scope scope;
    private final Object source;

    ScopeBinding(Object source2, Class<? extends Annotation> annotationType2, Scope scope2) {
        this.source = Preconditions.checkNotNull(source2, "source");
        this.annotationType = (Class) Preconditions.checkNotNull(annotationType2, "annotationType");
        this.scope = (Scope) Preconditions.checkNotNull(scope2, "scope");
    }

    public Object getSource() {
        return this.source;
    }

    public Class<? extends Annotation> getAnnotationType() {
        return this.annotationType;
    }

    public Scope getScope() {
        return this.scope;
    }

    public <T> T acceptVisitor(ElementVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).bindScope(this.annotationType, this.scope);
    }
}
