package com.google.inject.spi;

import com.google.inject.Binder;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.Matcher;

public final class TypeListenerBinding implements Element {
    private final TypeListener listener;
    private final Object source;
    private final Matcher<? super TypeLiteral<?>> typeMatcher;

    TypeListenerBinding(Object source2, TypeListener listener2, Matcher<? super TypeLiteral<?>> typeMatcher2) {
        this.source = source2;
        this.listener = listener2;
        this.typeMatcher = typeMatcher2;
    }

    public TypeListener getListener() {
        return this.listener;
    }

    public Matcher<? super TypeLiteral<?>> getTypeMatcher() {
        return this.typeMatcher;
    }

    public Object getSource() {
        return this.source;
    }

    public <T> T acceptVisitor(ElementVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public void applyTo(Binder binder) {
        binder.withSource(getSource()).bindListener(this.typeMatcher, this.listener);
    }
}
