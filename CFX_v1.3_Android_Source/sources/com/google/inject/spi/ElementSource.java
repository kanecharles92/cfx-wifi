package com.google.inject.spi;

import com.google.inject.internal.util.StackTraceElements;
import com.google.inject.internal.util.StackTraceElements.InMemoryStackTraceElement;
import java.util.List;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableList;

public final class ElementSource {
    final Object declaringSource;
    final ModuleSource moduleSource;
    final ElementSource originalElementSource;
    final InMemoryStackTraceElement[] partialCallStack;

    ElementSource(ElementSource originalSource, Object declaringSource2, ModuleSource moduleSource2, StackTraceElement[] partialCallStack2) {
        Preconditions.checkNotNull(declaringSource2, "declaringSource cannot be null.");
        Preconditions.checkNotNull(moduleSource2, "moduleSource cannot be null.");
        Preconditions.checkNotNull(partialCallStack2, "partialCallStack cannot be null.");
        this.originalElementSource = originalSource;
        this.declaringSource = declaringSource2;
        this.moduleSource = moduleSource2;
        this.partialCallStack = StackTraceElements.convertToInMemoryStackTraceElement(partialCallStack2);
    }

    public ElementSource getOriginalElementSource() {
        return this.originalElementSource;
    }

    public Object getDeclaringSource() {
        return this.declaringSource;
    }

    public List<String> getModuleClassNames() {
        return this.moduleSource.getModuleClassNames();
    }

    public List<Integer> getModuleConfigurePositionsInStackTrace() {
        int size = this.moduleSource.size();
        Integer[] positions = new Integer[size];
        positions[0] = Integer.valueOf(this.partialCallStack.length - 1);
        ModuleSource current = this.moduleSource;
        for (int cursor = 1; cursor < size; cursor++) {
            positions[cursor] = Integer.valueOf(positions[cursor - 1].intValue() + current.getPartialCallStackSize());
            current = current.getParent();
        }
        return ImmutableList.copyOf((E[]) positions);
    }

    public StackTraceElement[] getStackTrace() {
        int modulesCallStackSize = this.moduleSource.getStackTraceSize();
        int chunkSize = this.partialCallStack.length;
        StackTraceElement[] callStack = new StackTraceElement[(this.moduleSource.getStackTraceSize() + chunkSize)];
        System.arraycopy(StackTraceElements.convertToStackTraceElement(this.partialCallStack), 0, callStack, 0, chunkSize);
        System.arraycopy(this.moduleSource.getStackTrace(), 0, callStack, chunkSize, modulesCallStackSize);
        return callStack;
    }

    public String toString() {
        return getDeclaringSource().toString();
    }
}
