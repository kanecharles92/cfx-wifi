package com.google.inject.binder;

import com.google.inject.Scope;
import java.lang.annotation.Annotation;

public interface ScopedBindingBuilder {
    void asEagerSingleton();

    /* renamed from: in */
    void mo7038in(Scope scope);

    /* renamed from: in */
    void mo7039in(Class<? extends Annotation> cls);
}
