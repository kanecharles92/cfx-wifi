package com.google.inject.binder;

public interface ConstantBindingBuilder {
    /* renamed from: to */
    void mo7108to(byte b);

    /* renamed from: to */
    void mo7109to(char c);

    /* renamed from: to */
    void mo7110to(double d);

    /* renamed from: to */
    void mo7111to(float f);

    /* renamed from: to */
    void mo7112to(int i);

    /* renamed from: to */
    void mo7113to(long j);

    /* renamed from: to */
    void mo7114to(Class<?> cls);

    /* renamed from: to */
    <E extends Enum<E>> void mo7115to(E e);

    /* renamed from: to */
    void mo7116to(String str);

    /* renamed from: to */
    void mo7117to(short s);

    /* renamed from: to */
    void mo7118to(boolean z);
}
