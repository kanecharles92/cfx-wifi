package com.google.inject;

import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.binder.AnnotatedConstantBindingBuilder;
import com.google.inject.binder.AnnotatedElementBuilder;
import com.google.inject.binder.LinkedBindingBuilder;
import com.google.inject.matcher.Matcher;
import com.google.inject.spi.Message;
import com.google.inject.spi.ProvisionListener;
import com.google.inject.spi.TypeConverter;
import com.google.inject.spi.TypeListener;
import java.lang.annotation.Annotation;
import org.roboguice.shaded.goole.common.base.Preconditions;

public abstract class PrivateModule implements Module {
    private PrivateBinder binder;

    /* access modifiers changed from: protected */
    public abstract void configure();

    public final synchronized void configure(Binder binder2) {
        boolean z = true;
        synchronized (this) {
            if (this.binder != null) {
                z = false;
            }
            Preconditions.checkState(z, "Re-entry is not allowed.");
            this.binder = (PrivateBinder) binder2.skipSources(PrivateModule.class);
            try {
                configure();
                this.binder = null;
            } catch (Throwable th) {
                this.binder = null;
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final <T> void expose(Key<T> key) {
        binder().expose(key);
    }

    /* access modifiers changed from: protected */
    public final AnnotatedElementBuilder expose(Class<?> type) {
        return binder().expose(type);
    }

    /* access modifiers changed from: protected */
    public final AnnotatedElementBuilder expose(TypeLiteral<?> type) {
        return binder().expose(type);
    }

    /* access modifiers changed from: protected */
    public final PrivateBinder binder() {
        Preconditions.checkState(this.binder != null, "The binder can only be used inside configure()");
        return this.binder;
    }

    /* access modifiers changed from: protected */
    public final void bindScope(Class<? extends Annotation> scopeAnnotation, Scope scope) {
        binder().bindScope(scopeAnnotation, scope);
    }

    /* access modifiers changed from: protected */
    public final <T> LinkedBindingBuilder<T> bind(Key<T> key) {
        return binder().bind(key);
    }

    /* access modifiers changed from: protected */
    public final <T> AnnotatedBindingBuilder<T> bind(TypeLiteral<T> typeLiteral) {
        return binder().bind(typeLiteral);
    }

    /* access modifiers changed from: protected */
    public final <T> AnnotatedBindingBuilder<T> bind(Class<T> clazz) {
        return binder().bind(clazz);
    }

    /* access modifiers changed from: protected */
    public final AnnotatedConstantBindingBuilder bindConstant() {
        return binder().bindConstant();
    }

    /* access modifiers changed from: protected */
    public final void install(Module module) {
        binder().install(module);
    }

    /* access modifiers changed from: protected */
    public final void addError(String message, Object... arguments) {
        binder().addError(message, arguments);
    }

    /* access modifiers changed from: protected */
    public final void addError(Throwable t) {
        binder().addError(t);
    }

    /* access modifiers changed from: protected */
    public final void addError(Message message) {
        binder().addError(message);
    }

    /* access modifiers changed from: protected */
    public final void requestInjection(Object instance) {
        binder().requestInjection(instance);
    }

    /* access modifiers changed from: protected */
    public final void requestStaticInjection(Class<?>... types) {
        binder().requestStaticInjection(types);
    }

    /* access modifiers changed from: protected */
    public final void requireBinding(Key<?> key) {
        binder().getProvider(key);
    }

    /* access modifiers changed from: protected */
    public final void requireBinding(Class<?> type) {
        binder().getProvider(type);
    }

    /* access modifiers changed from: protected */
    public final <T> Provider<T> getProvider(Key<T> key) {
        return binder().getProvider(key);
    }

    /* access modifiers changed from: protected */
    public final <T> Provider<T> getProvider(Class<T> type) {
        return binder().getProvider(type);
    }

    /* access modifiers changed from: protected */
    public final void convertToTypes(Matcher<? super TypeLiteral<?>> typeMatcher, TypeConverter converter) {
        binder().convertToTypes(typeMatcher, converter);
    }

    /* access modifiers changed from: protected */
    public final Stage currentStage() {
        return binder().currentStage();
    }

    /* access modifiers changed from: protected */
    public <T> MembersInjector<T> getMembersInjector(Class<T> type) {
        return binder().getMembersInjector(type);
    }

    /* access modifiers changed from: protected */
    public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> type) {
        return binder().getMembersInjector(type);
    }

    /* access modifiers changed from: protected */
    public void bindListener(Matcher<? super TypeLiteral<?>> typeMatcher, TypeListener listener) {
        binder().bindListener(typeMatcher, listener);
    }

    /* access modifiers changed from: protected */
    public void bindListener(Matcher<? super Binding<?>> bindingMatcher, ProvisionListener... listeners) {
        binder().bindListener(bindingMatcher, listeners);
    }
}
