package com.google.inject.name;

import android.support.p000v4.media.TransportMediator;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import org.roboguice.shaded.goole.common.base.Preconditions;

class NamedImpl implements Named, Serializable {
    private static final long serialVersionUID = 0;
    private final String value;

    public NamedImpl(String value2) {
        this.value = (String) Preconditions.checkNotNull(value2, "name");
    }

    public String value() {
        return this.value;
    }

    public int hashCode() {
        return ("value".hashCode() * TransportMediator.KEYCODE_MEDIA_PAUSE) ^ this.value.hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof Named)) {
            return false;
        }
        return this.value.equals(((Named) o).value());
    }

    public String toString() {
        return "@" + Named.class.getName() + "(value=" + this.value + ")";
    }

    public Class<? extends Annotation> annotationType() {
        return Named.class;
    }
}
