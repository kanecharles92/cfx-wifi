package com.google.inject.name;

import com.google.inject.Binder;
import com.google.inject.Key;
import java.lang.annotation.Annotation;
import java.util.Enumeration;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class Names {
    private Names() {
    }

    public static Named named(String name) {
        return new NamedImpl(name);
    }

    public static void bindProperties(Binder binder, Map<String, String> properties) {
        Binder binder2 = binder.skipSources(Names.class);
        for (Entry<String, String> entry : properties.entrySet()) {
            String value = (String) entry.getValue();
            binder2.bind(Key.get(String.class, (Annotation) new NamedImpl((String) entry.getKey()))).toInstance(value);
        }
    }

    public static void bindProperties(Binder binder, Properties properties) {
        Binder binder2 = binder.skipSources(Names.class);
        Enumeration<?> e = properties.propertyNames();
        while (e.hasMoreElements()) {
            String propertyName = (String) e.nextElement();
            binder2.bind(Key.get(String.class, (Annotation) new NamedImpl(propertyName))).toInstance(properties.getProperty(propertyName));
        }
    }
}
