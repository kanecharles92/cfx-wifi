package com.google.inject;

import com.google.inject.internal.InternalInjectorCreator;
import java.util.Arrays;

public final class Guice {
    private static AnnotationDatabaseFinder annotationDatabaseFinder;
    private static HierarchyTraversalFilterFactory hierarchyTraversalFilterFactory = new HierarchyTraversalFilterFactory();

    private Guice() {
    }

    public static Injector createInjector(Module... modules) {
        return createInjector((Iterable<? extends Module>) Arrays.asList(modules));
    }

    public static Injector createInjector(Iterable<? extends Module> modules) {
        return createInjector(Stage.DEVELOPMENT, modules);
    }

    public static Injector createInjector(Stage stage, Module... modules) {
        return createInjector(stage, (Iterable<? extends Module>) Arrays.asList(modules));
    }

    public static Injector createInjector(Stage stage, Iterable<? extends Module> modules) {
        doSetAnnotationDatabaseFinderToModules(modules);
        return new InternalInjectorCreator().stage(stage).addModules(modules).build();
    }

    public static HierarchyTraversalFilter createHierarchyTraversalFilter() {
        HierarchyTraversalFilter hierarchyTraversalFilter = hierarchyTraversalFilterFactory.createHierarchyTraversalFilter();
        return annotationDatabaseFinder == null ? hierarchyTraversalFilter : new AnnotatedGuiceHierarchyTraversalFilter(annotationDatabaseFinder, hierarchyTraversalFilter);
    }

    public static void setHierarchyTraversalFilterFactory(HierarchyTraversalFilterFactory hierarchyTraversalFilterFactory2) {
        hierarchyTraversalFilterFactory = hierarchyTraversalFilterFactory2;
    }

    public static void setAnnotationDatabasePackageNames(String[] packageNames) {
        if (packageNames == null || packageNames.length == 0) {
            annotationDatabaseFinder = null;
        } else {
            annotationDatabaseFinder = new AnnotationDatabaseFinder(packageNames);
        }
    }

    public static AnnotationDatabaseFinder getAnnotationDatabaseFinder() {
        return annotationDatabaseFinder;
    }

    private static void doSetAnnotationDatabaseFinderToModules(Iterable<? extends Module> modules) {
        for (Module module : modules) {
            if (module instanceof AbstractModule) {
                ((AbstractModule) module).setAnnotationDatabaseFinder(annotationDatabaseFinder);
            }
        }
    }
}
