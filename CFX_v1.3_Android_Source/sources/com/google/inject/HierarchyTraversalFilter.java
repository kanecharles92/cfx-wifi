package com.google.inject;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

public class HierarchyTraversalFilter {
    public boolean isWorthScanning(Class<?> c) {
        return (c == null || c == Object.class) ? false : true;
    }

    public boolean isWorthScanningForFields(String AnnotationClassName, Class<?> c) {
        return isWorthScanning(c);
    }

    public Set<Field> getAllFields(String annotationClassName, Class<?> c) {
        HashSet<Field> set = new HashSet<>();
        for (Field field : c.getDeclaredFields()) {
            set.add(field);
        }
        return set;
    }

    public boolean isWorthScanningForMethods(String AnnotationClassName, Class<?> c) {
        return isWorthScanning(c);
    }

    public Set<Method> getAllMethods(String annotationClassName, Class<?> c) {
        HashSet<Method> set = new HashSet<>();
        for (Method method : c.getDeclaredMethods()) {
            set.add(method);
        }
        return set;
    }

    public boolean isWorthScanningForConstructors(String AnnotationClassName, Class<?> c) {
        return isWorthScanning(c);
    }

    public Set<Constructor<?>> getAllConstructors(String annotationClassName, Class<?> c) {
        HashSet<Constructor<?>> set = new HashSet<>();
        for (Constructor<?> method : c.getDeclaredConstructors()) {
            set.add(method);
        }
        return set;
    }

    public void reset() {
    }
}
