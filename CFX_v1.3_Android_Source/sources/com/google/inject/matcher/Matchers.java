package com.google.inject.matcher;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import org.roboguice.shaded.goole.common.base.Preconditions;

public class Matchers {
    private static final Matcher<Object> ANY = new Any();

    private static class AnnotatedWith extends AbstractMatcher<AnnotatedElement> implements Serializable {
        private static final long serialVersionUID = 0;
        private final Annotation annotation;

        public AnnotatedWith(Annotation annotation2) {
            this.annotation = (Annotation) Preconditions.checkNotNull(annotation2, "annotation");
            Matchers.checkForRuntimeRetention(annotation2.annotationType());
        }

        public boolean matches(AnnotatedElement element) {
            Annotation fromElement = element.getAnnotation(this.annotation.annotationType());
            return fromElement != null && this.annotation.equals(fromElement);
        }

        public boolean equals(Object other) {
            return (other instanceof AnnotatedWith) && ((AnnotatedWith) other).annotation.equals(this.annotation);
        }

        public int hashCode() {
            return this.annotation.hashCode() * 37;
        }

        public String toString() {
            return "annotatedWith(" + this.annotation + ")";
        }
    }

    private static class AnnotatedWithType extends AbstractMatcher<AnnotatedElement> implements Serializable {
        private static final long serialVersionUID = 0;
        private final Class<? extends Annotation> annotationType;

        public AnnotatedWithType(Class<? extends Annotation> annotationType2) {
            this.annotationType = (Class) Preconditions.checkNotNull(annotationType2, "annotation type");
            Matchers.checkForRuntimeRetention(annotationType2);
        }

        public boolean matches(AnnotatedElement element) {
            return element.isAnnotationPresent(this.annotationType);
        }

        public boolean equals(Object other) {
            return (other instanceof AnnotatedWithType) && ((AnnotatedWithType) other).annotationType.equals(this.annotationType);
        }

        public int hashCode() {
            return this.annotationType.hashCode() * 37;
        }

        public String toString() {
            return "annotatedWith(" + this.annotationType.getSimpleName() + ".class)";
        }
    }

    private static class Any extends AbstractMatcher<Object> implements Serializable {
        private static final long serialVersionUID = 0;

        private Any() {
        }

        public boolean matches(Object o) {
            return true;
        }

        public String toString() {
            return "any()";
        }

        public Object readResolve() {
            return Matchers.any();
        }
    }

    private static class IdenticalTo extends AbstractMatcher<Object> implements Serializable {
        private static final long serialVersionUID = 0;
        private final Object value;

        public IdenticalTo(Object value2) {
            this.value = Preconditions.checkNotNull(value2, "value");
        }

        public boolean matches(Object other) {
            return this.value == other;
        }

        public boolean equals(Object other) {
            return (other instanceof IdenticalTo) && ((IdenticalTo) other).value == this.value;
        }

        public int hashCode() {
            return System.identityHashCode(this.value) * 37;
        }

        public String toString() {
            return "identicalTo(" + this.value + ")";
        }
    }

    private static class InPackage extends AbstractMatcher<Class> implements Serializable {
        private static final long serialVersionUID = 0;
        private final String packageName;
        private final transient Package targetPackage;

        public InPackage(Package targetPackage2) {
            this.targetPackage = (Package) Preconditions.checkNotNull(targetPackage2, "package");
            this.packageName = targetPackage2.getName();
        }

        public boolean matches(Class c) {
            return c.getPackage().equals(this.targetPackage);
        }

        public boolean equals(Object other) {
            return (other instanceof InPackage) && ((InPackage) other).targetPackage.equals(this.targetPackage);
        }

        public int hashCode() {
            return this.targetPackage.hashCode() * 37;
        }

        public String toString() {
            return "inPackage(" + this.targetPackage.getName() + ")";
        }

        public Object readResolve() {
            return Matchers.inPackage(Package.getPackage(this.packageName));
        }
    }

    private static class InSubpackage extends AbstractMatcher<Class> implements Serializable {
        private static final long serialVersionUID = 0;
        private final String targetPackageName;

        public InSubpackage(String targetPackageName2) {
            this.targetPackageName = targetPackageName2;
        }

        public boolean matches(Class c) {
            String classPackageName = c.getPackage().getName();
            return classPackageName.equals(this.targetPackageName) || classPackageName.startsWith(new StringBuilder().append(this.targetPackageName).append(".").toString());
        }

        public boolean equals(Object other) {
            return (other instanceof InSubpackage) && ((InSubpackage) other).targetPackageName.equals(this.targetPackageName);
        }

        public int hashCode() {
            return this.targetPackageName.hashCode() * 37;
        }

        public String toString() {
            return "inSubpackage(" + this.targetPackageName + ")";
        }
    }

    private static class Not<T> extends AbstractMatcher<T> implements Serializable {
        private static final long serialVersionUID = 0;
        final Matcher<? super T> delegate;

        private Not(Matcher<? super T> delegate2) {
            this.delegate = (Matcher) Preconditions.checkNotNull(delegate2, "delegate");
        }

        public boolean matches(T t) {
            return !this.delegate.matches(t);
        }

        public boolean equals(Object other) {
            return (other instanceof Not) && ((Not) other).delegate.equals(this.delegate);
        }

        public int hashCode() {
            return -this.delegate.hashCode();
        }

        public String toString() {
            return "not(" + this.delegate + ")";
        }
    }

    private static class Only extends AbstractMatcher<Object> implements Serializable {
        private static final long serialVersionUID = 0;
        private final Object value;

        public Only(Object value2) {
            this.value = Preconditions.checkNotNull(value2, "value");
        }

        public boolean matches(Object other) {
            return this.value.equals(other);
        }

        public boolean equals(Object other) {
            return (other instanceof Only) && ((Only) other).value.equals(this.value);
        }

        public int hashCode() {
            return this.value.hashCode() * 37;
        }

        public String toString() {
            return "only(" + this.value + ")";
        }
    }

    private static class Returns extends AbstractMatcher<Method> implements Serializable {
        private static final long serialVersionUID = 0;
        private final Matcher<? super Class<?>> returnType;

        public Returns(Matcher<? super Class<?>> returnType2) {
            this.returnType = (Matcher) Preconditions.checkNotNull(returnType2, "return type matcher");
        }

        public boolean matches(Method m) {
            return this.returnType.matches(m.getReturnType());
        }

        public boolean equals(Object other) {
            return (other instanceof Returns) && ((Returns) other).returnType.equals(this.returnType);
        }

        public int hashCode() {
            return this.returnType.hashCode() * 37;
        }

        public String toString() {
            return "returns(" + this.returnType + ")";
        }
    }

    private static class SubclassesOf extends AbstractMatcher<Class> implements Serializable {
        private static final long serialVersionUID = 0;
        private final Class<?> superclass;

        public SubclassesOf(Class<?> superclass2) {
            this.superclass = (Class) Preconditions.checkNotNull(superclass2, "superclass");
        }

        public boolean matches(Class subclass) {
            return this.superclass.isAssignableFrom(subclass);
        }

        public boolean equals(Object other) {
            return (other instanceof SubclassesOf) && ((SubclassesOf) other).superclass.equals(this.superclass);
        }

        public int hashCode() {
            return this.superclass.hashCode() * 37;
        }

        public String toString() {
            return "subclassesOf(" + this.superclass.getSimpleName() + ".class)";
        }
    }

    private Matchers() {
    }

    public static Matcher<Object> any() {
        return ANY;
    }

    public static <T> Matcher<T> not(Matcher<? super T> p) {
        return new Not(p);
    }

    /* access modifiers changed from: private */
    public static void checkForRuntimeRetention(Class<? extends Annotation> annotationType) {
        boolean z;
        Retention retention = (Retention) annotationType.getAnnotation(Retention.class);
        if (retention == null || retention.value() != RetentionPolicy.RUNTIME) {
            z = false;
        } else {
            z = true;
        }
        Preconditions.checkArgument(z, "Annotation %s is missing RUNTIME retention", annotationType.getSimpleName());
    }

    public static Matcher<AnnotatedElement> annotatedWith(Class<? extends Annotation> annotationType) {
        return new AnnotatedWithType(annotationType);
    }

    public static Matcher<AnnotatedElement> annotatedWith(Annotation annotation) {
        return new AnnotatedWith(annotation);
    }

    public static Matcher<Class> subclassesOf(Class<?> superclass) {
        return new SubclassesOf(superclass);
    }

    public static Matcher<Object> only(Object value) {
        return new Only(value);
    }

    public static Matcher<Object> identicalTo(Object value) {
        return new IdenticalTo(value);
    }

    public static Matcher<Class> inPackage(Package targetPackage) {
        return new InPackage(targetPackage);
    }

    public static Matcher<Class> inSubpackage(String targetPackageName) {
        return new InSubpackage(targetPackageName);
    }

    public static Matcher<Method> returns(Matcher<? super Class<?>> returnType) {
        return new Returns(returnType);
    }
}
