package com.google.inject.matcher;

import java.io.Serializable;

public abstract class AbstractMatcher<T> implements Matcher<T> {

    private static class AndMatcher<T> extends AbstractMatcher<T> implements Serializable {
        private static final long serialVersionUID = 0;

        /* renamed from: a */
        private final Matcher<? super T> f25a;

        /* renamed from: b */
        private final Matcher<? super T> f26b;

        public AndMatcher(Matcher<? super T> a, Matcher<? super T> b) {
            this.f25a = a;
            this.f26b = b;
        }

        public boolean matches(T t) {
            return this.f25a.matches(t) && this.f26b.matches(t);
        }

        public boolean equals(Object other) {
            return (other instanceof AndMatcher) && ((AndMatcher) other).f25a.equals(this.f25a) && ((AndMatcher) other).f26b.equals(this.f26b);
        }

        public int hashCode() {
            return (this.f25a.hashCode() ^ this.f26b.hashCode()) * 41;
        }

        public String toString() {
            return "and(" + this.f25a + ", " + this.f26b + ")";
        }
    }

    private static class OrMatcher<T> extends AbstractMatcher<T> implements Serializable {
        private static final long serialVersionUID = 0;

        /* renamed from: a */
        private final Matcher<? super T> f27a;

        /* renamed from: b */
        private final Matcher<? super T> f28b;

        public OrMatcher(Matcher<? super T> a, Matcher<? super T> b) {
            this.f27a = a;
            this.f28b = b;
        }

        public boolean matches(T t) {
            return this.f27a.matches(t) || this.f28b.matches(t);
        }

        public boolean equals(Object other) {
            return (other instanceof OrMatcher) && ((OrMatcher) other).f27a.equals(this.f27a) && ((OrMatcher) other).f28b.equals(this.f28b);
        }

        public int hashCode() {
            return (this.f27a.hashCode() ^ this.f28b.hashCode()) * 37;
        }

        public String toString() {
            return "or(" + this.f27a + ", " + this.f28b + ")";
        }
    }

    public Matcher<T> and(Matcher<? super T> other) {
        return new AndMatcher(this, other);
    }

    /* renamed from: or */
    public Matcher<T> mo7542or(Matcher<? super T> other) {
        return new OrMatcher(this, other);
    }
}
