package com.google.inject;

import com.google.inject.internal.Errors;
import com.google.inject.spi.Message;
import java.util.Collection;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;

public final class ProvisionException extends RuntimeException {
    private static final long serialVersionUID = 0;
    private final ImmutableSet<Message> messages;

    public ProvisionException(Iterable<Message> messages2) {
        this.messages = ImmutableSet.copyOf(messages2);
        Preconditions.checkArgument(!this.messages.isEmpty());
        initCause(Errors.getOnlyCause(this.messages));
    }

    public ProvisionException(String message, Throwable cause) {
        super(cause);
        this.messages = ImmutableSet.m147of(new Message(message, cause));
    }

    public ProvisionException(String message) {
        this.messages = ImmutableSet.m147of(new Message(message));
    }

    public Collection<Message> getErrorMessages() {
        return this.messages;
    }

    public String getMessage() {
        return Errors.format("Unable to provision, see the following errors", (Collection<Message>) this.messages);
    }
}
