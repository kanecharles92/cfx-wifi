package com.google.inject.internal;

import java.lang.annotation.Annotation;

public class Nullability {
    private Nullability() {
    }

    public static boolean allowsNull(Annotation[] annotations) {
        for (Annotation a : annotations) {
            if ("Nullable".equals(a.annotationType().getSimpleName())) {
                return true;
            }
        }
        return false;
    }
}
