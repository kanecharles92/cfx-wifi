package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Exposed;
import com.google.inject.Key;
import com.google.inject.PrivateBinder;
import com.google.inject.Provider;
import com.google.inject.internal.util.StackTraceElements;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.HasDependencies;
import com.google.inject.spi.ProviderInstanceBinding;
import com.google.inject.spi.ProviderWithExtensionVisitor;
import com.google.inject.spi.ProvidesMethodBinding;
import com.google.inject.spi.ProvidesMethodTargetVisitor;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;

public abstract class ProviderMethod<T> implements ProviderWithExtensionVisitor<T>, HasDependencies, ProvidesMethodBinding<T> {
    private final ImmutableSet<Dependency<?>> dependencies;
    private final boolean exposed;
    protected final Object instance;
    private final Key<T> key;
    protected final Method method;
    private final List<Provider<?>> parameterProviders;
    private final Class<? extends Annotation> scopeAnnotation;

    private static final class ReflectionProviderMethod<T> extends ProviderMethod<T> {
        ReflectionProviderMethod(Key<T> key, Method method, Object instance, ImmutableSet<Dependency<?>> dependencies, List<Provider<?>> parameterProviders, Class<? extends Annotation> scopeAnnotation) {
            super(key, method, instance, dependencies, parameterProviders, scopeAnnotation);
        }

        /* access modifiers changed from: 0000 */
        public Object doProvision(Object[] parameters) throws IllegalAccessException, InvocationTargetException {
            return this.method.invoke(this.instance, parameters);
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract Object doProvision(Object[] objArr) throws IllegalAccessException, InvocationTargetException;

    static <T> ProviderMethod<T> create(Key<T> key2, Method method2, Object instance2, ImmutableSet<Dependency<?>> dependencies2, List<Provider<?>> parameterProviders2, Class<? extends Annotation> scopeAnnotation2, boolean skipFastClassGeneration) {
        if (!Modifier.isPublic(method2.getModifiers()) || !Modifier.isPublic(method2.getDeclaringClass().getModifiers())) {
            method2.setAccessible(true);
        }
        return new ReflectionProviderMethod(key2, method2, instance2, dependencies2, parameterProviders2, scopeAnnotation2);
    }

    private ProviderMethod(Key<T> key2, Method method2, Object instance2, ImmutableSet<Dependency<?>> dependencies2, List<Provider<?>> parameterProviders2, Class<? extends Annotation> scopeAnnotation2) {
        this.key = key2;
        this.scopeAnnotation = scopeAnnotation2;
        this.instance = instance2;
        this.dependencies = dependencies2;
        this.method = method2;
        this.parameterProviders = parameterProviders2;
        this.exposed = method2.isAnnotationPresent(Exposed.class);
    }

    public Key<T> getKey() {
        return this.key;
    }

    public Method getMethod() {
        return this.method;
    }

    public Object getInstance() {
        return this.instance;
    }

    public Object getEnclosingInstance() {
        return this.instance;
    }

    public void configure(Binder binder) {
        Binder binder2 = binder.withSource(this.method);
        if (this.scopeAnnotation != null) {
            binder2.bind(this.key).toProvider((Provider<? extends T>) this).mo7039in(this.scopeAnnotation);
        } else {
            binder2.bind(this.key).toProvider((Provider<? extends T>) this);
        }
        if (this.exposed) {
            ((PrivateBinder) binder2).expose(this.key);
        }
    }

    public T get() {
        Object[] parameters = new Object[this.parameterProviders.size()];
        for (int i = 0; i < parameters.length; i++) {
            parameters[i] = ((Provider) this.parameterProviders.get(i)).get();
        }
        try {
            return doProvision(parameters);
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        } catch (InvocationTargetException e2) {
            throw Exceptions.rethrowCause(e2);
        }
    }

    public Set<Dependency<?>> getDependencies() {
        return this.dependencies;
    }

    public <B, V> V acceptExtensionVisitor(BindingTargetVisitor<B, V> visitor, ProviderInstanceBinding<? extends B> binding) {
        if (visitor instanceof ProvidesMethodTargetVisitor) {
            return ((ProvidesMethodTargetVisitor) visitor).visit(this);
        }
        return visitor.visit(binding);
    }

    public String toString() {
        return "@Provides " + StackTraceElements.forMember(this.method);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ProviderMethod)) {
            return false;
        }
        ProviderMethod o = (ProviderMethod) obj;
        if (!this.method.equals(o.method) || !this.instance.equals(o.instance)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hashCode(this.method);
    }
}
