package com.google.inject.internal;

import com.google.inject.Binding;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Scope;
import com.google.inject.Stage;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.util.Stopwatch;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.TypeConverterBinding;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.Iterables;

public final class InternalInjectorCreator {
    private final ProcessedBindingData bindingData = new ProcessedBindingData();
    private final Errors errors = new Errors();
    private final Initializer initializer = new Initializer();
    private final InjectionRequestProcessor injectionRequestProcessor = new InjectionRequestProcessor(this.errors, this.initializer);
    private final Builder shellBuilder = new Builder();
    private List<InjectorShell> shells;
    private final Stopwatch stopwatch = new Stopwatch();

    static class ToolStageInjector implements Injector {
        private final Injector delegateInjector;

        ToolStageInjector(Injector delegateInjector2) {
            this.delegateInjector = delegateInjector2;
        }

        public void injectMembers(Object o) {
            throw new UnsupportedOperationException("Injector.injectMembers(Object) is not supported in Stage.TOOL");
        }

        public Map<Key<?>, Binding<?>> getBindings() {
            return this.delegateInjector.getBindings();
        }

        public Map<Key<?>, Binding<?>> getAllBindings() {
            return this.delegateInjector.getAllBindings();
        }

        public <T> Binding<T> getBinding(Key<T> key) {
            return this.delegateInjector.getBinding(key);
        }

        public <T> Binding<T> getBinding(Class<T> type) {
            return this.delegateInjector.getBinding(type);
        }

        public <T> Binding<T> getExistingBinding(Key<T> key) {
            return this.delegateInjector.getExistingBinding(key);
        }

        public <T> List<Binding<T>> findBindingsByType(TypeLiteral<T> type) {
            return this.delegateInjector.findBindingsByType(type);
        }

        public Injector getParent() {
            return this.delegateInjector.getParent();
        }

        public Injector createChildInjector(Iterable<? extends Module> modules) {
            return this.delegateInjector.createChildInjector(modules);
        }

        public Injector createChildInjector(Module... modules) {
            return this.delegateInjector.createChildInjector(modules);
        }

        public Map<Class<? extends Annotation>, Scope> getScopeBindings() {
            return this.delegateInjector.getScopeBindings();
        }

        public Set<TypeConverterBinding> getTypeConverterBindings() {
            return this.delegateInjector.getTypeConverterBindings();
        }

        public <T> Provider<T> getProvider(Key<T> key) {
            throw new UnsupportedOperationException("Injector.getProvider(Key<T>) is not supported in Stage.TOOL");
        }

        public <T> Provider<T> getProvider(Class<T> cls) {
            throw new UnsupportedOperationException("Injector.getProvider(Class<T>) is not supported in Stage.TOOL");
        }

        public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> typeLiteral) {
            throw new UnsupportedOperationException("Injector.getMembersInjector(TypeLiteral<T>) is not supported in Stage.TOOL");
        }

        public <T> MembersInjector<T> getMembersInjector(Class<T> cls) {
            throw new UnsupportedOperationException("Injector.getMembersInjector(Class<T>) is not supported in Stage.TOOL");
        }

        public <T> T getInstance(Key<T> key) {
            throw new UnsupportedOperationException("Injector.getInstance(Key<T>) is not supported in Stage.TOOL");
        }

        public <T> T getInstance(Class<T> cls) {
            throw new UnsupportedOperationException("Injector.getInstance(Class<T>) is not supported in Stage.TOOL");
        }
    }

    public InternalInjectorCreator stage(Stage stage) {
        this.shellBuilder.stage(stage);
        return this;
    }

    public InternalInjectorCreator parentInjector(InjectorImpl parent) {
        this.shellBuilder.parent(parent);
        return this;
    }

    public InternalInjectorCreator addModules(Iterable<? extends Module> modules) {
        this.shellBuilder.addModules(modules);
        return this;
    }

    public Injector build() {
        if (this.shellBuilder == null) {
            throw new AssertionError("Already built, builders are not reusable.");
        }
        synchronized (this.shellBuilder.lock()) {
            this.shells = this.shellBuilder.build(this.initializer, this.bindingData, this.stopwatch, this.errors);
            this.stopwatch.resetAndLog("Injector construction");
            initializeStatically();
        }
        injectDynamically();
        if (this.shellBuilder.getStage() == Stage.TOOL) {
            return new ToolStageInjector(primaryInjector());
        }
        return primaryInjector();
    }

    private void initializeStatically() {
        this.bindingData.initializeBindings();
        this.stopwatch.resetAndLog("Binding initialization");
        for (InjectorShell shell : this.shells) {
            shell.getInjector().index();
        }
        this.stopwatch.resetAndLog("Binding indexing");
        this.injectionRequestProcessor.process(this.shells);
        this.stopwatch.resetAndLog("Collecting injection requests");
        this.bindingData.runCreationListeners(this.errors);
        this.stopwatch.resetAndLog("Binding validation");
        this.injectionRequestProcessor.validate();
        this.stopwatch.resetAndLog("Static validation");
        this.initializer.validateOustandingInjections(this.errors);
        this.stopwatch.resetAndLog("Instance member validation");
        new LookupProcessor(this.errors).process(this.shells);
        for (InjectorShell shell2 : this.shells) {
            ((DeferredLookups) shell2.getInjector().lookups).initialize(this.errors);
        }
        this.stopwatch.resetAndLog("Provider verification");
        for (InjectorShell shell3 : this.shells) {
            if (!shell3.getElements().isEmpty()) {
                throw new AssertionError("Failed to execute " + shell3.getElements());
            }
        }
        this.errors.throwCreationExceptionIfErrorsExist();
    }

    private Injector primaryInjector() {
        return ((InjectorShell) this.shells.get(0)).getInjector();
    }

    private void injectDynamically() {
        this.injectionRequestProcessor.injectMembers();
        this.stopwatch.resetAndLog("Static member injection");
        this.initializer.injectAll(this.errors);
        this.stopwatch.resetAndLog("Instance injection");
        this.errors.throwCreationExceptionIfErrorsExist();
        if (this.shellBuilder.getStage() != Stage.TOOL) {
            for (InjectorShell shell : this.shells) {
                loadEagerSingletons(shell.getInjector(), this.shellBuilder.getStage(), this.errors);
            }
            this.stopwatch.resetAndLog("Preloading singletons");
        }
        this.errors.throwCreationExceptionIfErrorsExist();
    }

    /* access modifiers changed from: 0000 */
    public void loadEagerSingletons(InjectorImpl injector, Stage stage, final Errors errors2) {
        for (final BindingImpl<?> binding : ImmutableList.copyOf(Iterables.concat(injector.state.getExplicitBindingsThisLevel().values(), injector.jitBindings.values()))) {
            if (isEagerSingleton(injector, binding, stage)) {
                try {
                    injector.callInContext(new ContextualCallable<Void>() {
                        Dependency<?> dependency = Dependency.get(binding.getKey());

                        public Void call(InternalContext context) {
                            Dependency previous = context.pushDependency(this.dependency, binding.getSource());
                            Errors errorsForBinding = errors2.withSource(this.dependency);
                            try {
                                binding.getInternalFactory().get(errorsForBinding, context, this.dependency, false);
                            } catch (ErrorsException e) {
                                errorsForBinding.merge(e.getErrors());
                            } finally {
                                context.popStateAndSetDependency(previous);
                            }
                            return null;
                        }
                    });
                } catch (ErrorsException e) {
                    throw new AssertionError();
                }
            }
        }
    }

    private boolean isEagerSingleton(InjectorImpl injector, BindingImpl<?> binding, Stage stage) {
        if (binding.getScoping().isEagerSingleton(stage)) {
            return true;
        }
        if (binding instanceof LinkedBindingImpl) {
            return isEagerSingleton(injector, injector.getBinding((Key) ((LinkedBindingImpl) binding).getLinkedKey()), stage);
        }
        return false;
    }
}
