package com.google.inject.internal;

import com.google.inject.Key;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.DependencyAndSource;
import java.util.List;
import java.util.Map;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.ImmutableList.Builder;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Maps;

final class InternalContext {
    private Map<Object, ConstructionContext<?>> constructionContexts = Maps.newHashMap();
    private Dependency<?> dependency;
    private final List<Object> state = Lists.newArrayList();

    InternalContext() {
    }

    public <T> ConstructionContext<T> getConstructionContext(Object key) {
        ConstructionContext<T> constructionContext = (ConstructionContext) this.constructionContexts.get(key);
        if (constructionContext != null) {
            return constructionContext;
        }
        ConstructionContext<T> constructionContext2 = new ConstructionContext<>();
        this.constructionContexts.put(key, constructionContext2);
        return constructionContext2;
    }

    public Dependency<?> getDependency() {
        return this.dependency;
    }

    public Dependency<?> pushDependency(Dependency<?> dependency2, Object source) {
        Dependency<?> previous = this.dependency;
        this.dependency = dependency2;
        this.state.add(dependency2);
        this.state.add(source);
        return previous;
    }

    public void popStateAndSetDependency(Dependency<?> newDependency) {
        popState();
        this.dependency = newDependency;
    }

    public void pushState(Key<?> key, Object source) {
        this.state.add(key == null ? null : Dependency.get(key));
        this.state.add(source);
    }

    public void popState() {
        this.state.remove(this.state.size() - 1);
        this.state.remove(this.state.size() - 1);
    }

    public List<DependencyAndSource> getDependencyChain() {
        Builder<DependencyAndSource> builder = ImmutableList.builder();
        for (int i = 0; i < this.state.size(); i += 2) {
            builder.add((Object) new DependencyAndSource((Dependency) this.state.get(i), this.state.get(i + 1)));
        }
        return builder.build();
    }
}
