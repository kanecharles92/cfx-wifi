package com.google.inject.internal;

import com.google.inject.Binding;
import com.google.inject.Key;
import com.google.inject.Scope;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.ProvisionListenerBinding;
import com.google.inject.spi.ScopeBinding;
import com.google.inject.spi.TypeConverterBinding;
import com.google.inject.spi.TypeListenerBinding;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableMap;
import org.roboguice.shaded.goole.common.collect.ImmutableMap.Builder;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Maps;

final class InheritingState implements State {
    private final WeakKeySet blacklistedKeys;
    private final List<TypeConverterBinding> converters = Lists.newArrayList();
    private final Map<Key<?>, Binding<?>> explicitBindings = Collections.unmodifiableMap(this.explicitBindingsMutable);
    private final Map<Key<?>, Binding<?>> explicitBindingsMutable = Maps.newLinkedHashMap();
    private final Object lock;
    private final State parent;
    private final List<ProvisionListenerBinding> provisionListenerBindings = Lists.newArrayList();
    private final Map<Class<? extends Annotation>, ScopeBinding> scopes = Maps.newHashMap();
    private final List<TypeListenerBinding> typeListenerBindings = Lists.newArrayList();

    InheritingState(State parent2) {
        this.parent = (State) Preconditions.checkNotNull(parent2, "parent");
        this.lock = parent2 == State.NONE ? this : parent2.lock();
        this.blacklistedKeys = new WeakKeySet(this.lock);
    }

    public State parent() {
        return this.parent;
    }

    public <T> BindingImpl<T> getExplicitBinding(Key<T> key) {
        Binding<?> binding = (Binding) this.explicitBindings.get(key);
        return binding != null ? (BindingImpl) binding : this.parent.getExplicitBinding(key);
    }

    public Map<Key<?>, Binding<?>> getExplicitBindingsThisLevel() {
        return this.explicitBindings;
    }

    public void putBinding(Key<?> key, BindingImpl<?> binding) {
        this.explicitBindingsMutable.put(key, binding);
    }

    public ScopeBinding getScopeBinding(Class<? extends Annotation> annotationType) {
        ScopeBinding scopeBinding = (ScopeBinding) this.scopes.get(annotationType);
        return scopeBinding != null ? scopeBinding : this.parent.getScopeBinding(annotationType);
    }

    public void putScopeBinding(Class<? extends Annotation> annotationType, ScopeBinding scope) {
        this.scopes.put(annotationType, scope);
    }

    public Iterable<TypeConverterBinding> getConvertersThisLevel() {
        return this.converters;
    }

    public void addConverter(TypeConverterBinding typeConverterBinding) {
        this.converters.add(typeConverterBinding);
    }

    public TypeConverterBinding getConverter(String stringValue, TypeLiteral<?> type, Errors errors, Object source) {
        TypeConverterBinding matchingConverter = null;
        for (State s = this; s != State.NONE; s = s.parent()) {
            for (TypeConverterBinding converter : s.getConvertersThisLevel()) {
                if (converter.getTypeMatcher().matches(type)) {
                    if (matchingConverter != null) {
                        errors.ambiguousTypeConversion(stringValue, source, type, matchingConverter, converter);
                    }
                    matchingConverter = converter;
                }
            }
        }
        return matchingConverter;
    }

    public void addTypeListener(TypeListenerBinding listenerBinding) {
        this.typeListenerBindings.add(listenerBinding);
    }

    public List<TypeListenerBinding> getTypeListenerBindings() {
        List<TypeListenerBinding> parentBindings = this.parent.getTypeListenerBindings();
        List<TypeListenerBinding> result = new ArrayList<>(parentBindings.size() + 1);
        result.addAll(parentBindings);
        result.addAll(this.typeListenerBindings);
        return result;
    }

    public void addProvisionListener(ProvisionListenerBinding listenerBinding) {
        this.provisionListenerBindings.add(listenerBinding);
    }

    public List<ProvisionListenerBinding> getProvisionListenerBindings() {
        List<ProvisionListenerBinding> parentBindings = this.parent.getProvisionListenerBindings();
        List<ProvisionListenerBinding> result = new ArrayList<>(parentBindings.size() + 1);
        result.addAll(parentBindings);
        result.addAll(this.provisionListenerBindings);
        return result;
    }

    public void blacklist(Key<?> key, State state, Object source) {
        this.parent.blacklist(key, state, source);
        this.blacklistedKeys.add(key, state, source);
    }

    public boolean isBlacklisted(Key<?> key) {
        return this.blacklistedKeys.contains(key);
    }

    public Set<Object> getSourcesForBlacklistedKey(Key<?> key) {
        return this.blacklistedKeys.getSources(key);
    }

    public Object lock() {
        return this.lock;
    }

    public Map<Class<? extends Annotation>, Scope> getScopes() {
        Builder<Class<? extends Annotation>, Scope> builder = ImmutableMap.builder();
        for (Entry<Class<? extends Annotation>, ScopeBinding> entry : this.scopes.entrySet()) {
            builder.put(entry.getKey(), ((ScopeBinding) entry.getValue()).getScope());
        }
        return builder.build();
    }
}
