package com.google.inject.internal;

import com.google.inject.Key;
import com.google.inject.internal.util.SourceProvider;
import java.util.Map;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.cache.Cache;
import org.roboguice.shaded.goole.common.cache.CacheBuilder;
import org.roboguice.shaded.goole.common.cache.RemovalCause;
import org.roboguice.shaded.goole.common.cache.RemovalListener;
import org.roboguice.shaded.goole.common.cache.RemovalNotification;
import org.roboguice.shaded.goole.common.collect.LinkedHashMultiset;
import org.roboguice.shaded.goole.common.collect.Maps;
import org.roboguice.shaded.goole.common.collect.Multiset;
import org.roboguice.shaded.goole.common.collect.Sets;

final class WeakKeySet {
    private Map<Key<?>, Multiset<Object>> backingMap;
    private final Cache<State, Set<KeyAndSource>> evictionCache = CacheBuilder.newBuilder().weakKeys().removalListener(new RemovalListener<State, Set<KeyAndSource>>() {
        public void onRemoval(RemovalNotification<State, Set<KeyAndSource>> notification) {
            Preconditions.checkState(RemovalCause.COLLECTED.equals(notification.getCause()));
            WeakKeySet.this.cleanUpForCollectedState((Set) notification.getValue());
        }
    }).build();
    private final Object lock;

    private static final class KeyAndSource {
        final Key<?> key;
        final Object source;

        KeyAndSource(Key<?> key2, Object source2) {
            this.key = key2;
            this.source = source2;
        }

        public int hashCode() {
            return Objects.hashCode(this.key, this.source);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof KeyAndSource)) {
                return false;
            }
            KeyAndSource other = (KeyAndSource) obj;
            if (!Objects.equal(this.key, other.key) || !Objects.equal(this.source, other.source)) {
                return false;
            }
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void cleanUpForCollectedState(Set<KeyAndSource> keysAndSources) {
        synchronized (this.lock) {
            for (KeyAndSource keyAndSource : keysAndSources) {
                Multiset<Object> set = (Multiset) this.backingMap.get(keyAndSource.key);
                if (set != null) {
                    set.remove(keyAndSource.source);
                    if (set.isEmpty()) {
                        this.backingMap.remove(keyAndSource.key);
                    }
                }
            }
        }
    }

    WeakKeySet(Object lock2) {
        this.lock = lock2;
    }

    public void add(Key<?> key, State state, Object source) {
        if (this.backingMap == null) {
            this.backingMap = Maps.newHashMap();
        }
        if ((source instanceof Class) || source == SourceProvider.UNKNOWN_SOURCE) {
            source = null;
        }
        Multiset<Object> sources = (Multiset) this.backingMap.get(key);
        if (sources == null) {
            sources = LinkedHashMultiset.create();
            this.backingMap.put(key, sources);
        }
        Object convertedSource = Errors.convert(source);
        sources.add(convertedSource);
        if (state.parent() != State.NONE) {
            Set<KeyAndSource> keyAndSources = (Set) this.evictionCache.getIfPresent(state);
            if (keyAndSources == null) {
                Cache<State, Set<KeyAndSource>> cache = this.evictionCache;
                keyAndSources = Sets.newHashSet();
                cache.put(state, keyAndSources);
            }
            keyAndSources.add(new KeyAndSource(key, convertedSource));
        }
    }

    public boolean contains(Key<?> key) {
        this.evictionCache.cleanUp();
        return this.backingMap != null && this.backingMap.containsKey(key);
    }

    public Set<Object> getSources(Key<?> key) {
        this.evictionCache.cleanUp();
        Multiset multiset = this.backingMap == null ? null : (Multiset) this.backingMap.get(key);
        if (multiset == null) {
            return null;
        }
        return multiset.elementSet();
    }
}
