package com.google.inject.internal;

import com.google.inject.spi.Dependency;

final class SingleParameterInjector<T> {
    private static final Object[] NO_ARGUMENTS = new Object[0];
    private final BindingImpl<? extends T> binding;
    private final Dependency<T> dependency;

    SingleParameterInjector(Dependency<T> dependency2, BindingImpl<? extends T> binding2) {
        this.dependency = dependency2;
        this.binding = binding2;
    }

    private T inject(Errors errors, InternalContext context) throws ErrorsException {
        Dependency previous = context.pushDependency(this.dependency, this.binding.getSource());
        try {
            return this.binding.getInternalFactory().get(errors.withSource(this.dependency), context, this.dependency, false);
        } finally {
            context.popStateAndSetDependency(previous);
        }
    }

    static Object[] getAll(Errors errors, InternalContext context, SingleParameterInjector<?>[] parameterInjectors) throws ErrorsException {
        if (parameterInjectors == null) {
            return NO_ARGUMENTS;
        }
        int numErrorsBefore = errors.size();
        int size = parameterInjectors.length;
        Object[] parameters = new Object[size];
        for (int i = 0; i < size; i++) {
            try {
                parameters[i] = parameterInjectors[i].inject(errors, context);
            } catch (ErrorsException e) {
                errors.merge(e.getErrors());
            }
        }
        errors.throwIfNewErrors(numErrorsBefore);
        return parameters;
    }
}
