package com.google.inject.internal;

import com.google.inject.Key;
import com.google.inject.spi.Dependency;
import javax.inject.Provider;
import org.roboguice.shaded.goole.common.base.Preconditions;

final class BoundProviderFactory<T> extends ProviderInternalFactory<T> implements CreationListener {
    private final InjectorImpl injector;
    private InternalFactory<? extends Provider<? extends T>> providerFactory;
    final Key<? extends Provider<? extends T>> providerKey;
    private final ProvisionListenerStackCallback<T> provisionCallback;

    BoundProviderFactory(InjectorImpl injector2, Key<? extends Provider<? extends T>> providerKey2, Object source, boolean allowProxy, ProvisionListenerStackCallback<T> provisionCallback2) {
        super(source, allowProxy);
        this.provisionCallback = (ProvisionListenerStackCallback) Preconditions.checkNotNull(provisionCallback2, "provisionCallback");
        this.injector = injector2;
        this.providerKey = providerKey2;
    }

    public void notify(Errors errors) {
        try {
            this.providerFactory = this.injector.getInternalFactory(this.providerKey, errors.withSource(this.source), JitLimitation.NEW_OR_EXISTING_JIT);
        } catch (ErrorsException e) {
            errors.merge(e.getErrors());
        }
    }

    public T get(Errors errors, InternalContext context, Dependency<?> dependency, boolean linked) throws ErrorsException {
        context.pushState(this.providerKey, this.source);
        try {
            Errors errors2 = errors.withSource(this.providerKey);
            return circularGet((Provider) this.providerFactory.get(errors2, context, dependency, true), errors2, context, dependency, linked, this.provisionCallback);
        } finally {
            context.popState();
        }
    }

    /* access modifiers changed from: protected */
    public T provision(Provider<? extends T> provider, Errors errors, Dependency<?> dependency, ConstructionContext<T> constructionContext) throws ErrorsException {
        try {
            return super.provision(provider, errors, dependency, constructionContext);
        } catch (RuntimeException userException) {
            throw errors.errorInProvider(userException).toException();
        }
    }

    public String toString() {
        return this.providerKey.toString();
    }
}
