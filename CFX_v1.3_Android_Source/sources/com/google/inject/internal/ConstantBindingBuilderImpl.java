package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Key;
import com.google.inject.binder.AnnotatedConstantBindingBuilder;
import com.google.inject.binder.ConstantBindingBuilder;
import com.google.inject.spi.Element;
import java.lang.annotation.Annotation;
import java.util.List;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;

public final class ConstantBindingBuilderImpl<T> extends AbstractBindingBuilder<T> implements AnnotatedConstantBindingBuilder, ConstantBindingBuilder {
    public ConstantBindingBuilderImpl(Binder binder, List<Element> elements, Object source) {
        super(binder, elements, source, NULL_KEY);
    }

    public ConstantBindingBuilder annotatedWith(Class<? extends Annotation> annotationType) {
        annotatedWithInternal(annotationType);
        return this;
    }

    public ConstantBindingBuilder annotatedWith(Annotation annotation) {
        annotatedWithInternal(annotation);
        return this;
    }

    /* renamed from: to */
    public void mo7116to(String value) {
        toConstant(String.class, value);
    }

    /* renamed from: to */
    public void mo7112to(int value) {
        toConstant(Integer.class, Integer.valueOf(value));
    }

    /* renamed from: to */
    public void mo7113to(long value) {
        toConstant(Long.class, Long.valueOf(value));
    }

    /* renamed from: to */
    public void mo7118to(boolean value) {
        toConstant(Boolean.class, Boolean.valueOf(value));
    }

    /* renamed from: to */
    public void mo7110to(double value) {
        toConstant(Double.class, Double.valueOf(value));
    }

    /* renamed from: to */
    public void mo7111to(float value) {
        toConstant(Float.class, Float.valueOf(value));
    }

    /* renamed from: to */
    public void mo7117to(short value) {
        toConstant(Short.class, Short.valueOf(value));
    }

    /* renamed from: to */
    public void mo7109to(char value) {
        toConstant(Character.class, Character.valueOf(value));
    }

    /* renamed from: to */
    public void mo7108to(byte value) {
        toConstant(Byte.class, Byte.valueOf(value));
    }

    /* renamed from: to */
    public void mo7114to(Class<?> value) {
        toConstant(Class.class, value);
    }

    /* renamed from: to */
    public <E extends Enum<E>> void mo7115to(E value) {
        toConstant(value.getDeclaringClass(), value);
    }

    private void toConstant(Class<?> type, Object instance) {
        Key<T> key;
        Class<?> cls = type;
        Object obj = instance;
        if (keyTypeIsSet()) {
            this.binder.addError(AbstractBindingBuilder.CONSTANT_VALUE_ALREADY_SET, new Object[0]);
            return;
        }
        BindingImpl<T> base = getBinding();
        if (base.getKey().getAnnotation() != null) {
            key = Key.get(cls, base.getKey().getAnnotation());
        } else if (base.getKey().getAnnotationType() != null) {
            key = Key.get(cls, base.getKey().getAnnotationType());
        } else {
            key = Key.get(cls);
        }
        if (obj == null) {
            this.binder.addError(AbstractBindingBuilder.BINDING_TO_NULL, new Object[0]);
        }
        setBinding(new InstanceBindingImpl(base.getSource(), key, base.getScoping(), ImmutableSet.m146of(), obj));
    }

    public String toString() {
        return "ConstantBindingBuilder";
    }
}
