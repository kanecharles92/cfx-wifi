package com.google.inject.internal;

import com.google.inject.Stage;
import com.google.inject.spi.DisableCircularProxiesOption;
import com.google.inject.spi.RequireAtInjectOnConstructorsOption;
import com.google.inject.spi.RequireExactBindingAnnotationsOption;
import com.google.inject.spi.RequireExplicitBindingsOption;
import org.roboguice.shaded.goole.common.base.Preconditions;

class InjectorOptionsProcessor extends AbstractProcessor {
    private boolean atInjectRequired = false;
    private boolean disableCircularProxies = false;
    private boolean exactBindingAnnotationsRequired = false;
    private boolean jitDisabled = false;

    InjectorOptionsProcessor(Errors errors) {
        super(errors);
    }

    public Boolean visit(DisableCircularProxiesOption option) {
        this.disableCircularProxies = true;
        return Boolean.valueOf(true);
    }

    public Boolean visit(RequireExplicitBindingsOption option) {
        this.jitDisabled = true;
        return Boolean.valueOf(true);
    }

    public Boolean visit(RequireAtInjectOnConstructorsOption option) {
        this.atInjectRequired = true;
        return Boolean.valueOf(true);
    }

    public Boolean visit(RequireExactBindingAnnotationsOption option) {
        this.exactBindingAnnotationsRequired = true;
        return Boolean.valueOf(true);
    }

    /* access modifiers changed from: 0000 */
    public InjectorOptions getOptions(Stage stage, InjectorOptions parentOptions) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        Preconditions.checkNotNull(stage, "stage must be set");
        if (parentOptions == null) {
            return new InjectorOptions(stage, this.jitDisabled, this.disableCircularProxies, this.atInjectRequired, this.exactBindingAnnotationsRequired);
        }
        if (stage == parentOptions.stage) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z, "child & parent stage don't match");
        if (this.jitDisabled || parentOptions.jitDisabled) {
            z2 = true;
        } else {
            z2 = false;
        }
        boolean z4 = this.disableCircularProxies || parentOptions.disableCircularProxies;
        boolean z5 = this.atInjectRequired || parentOptions.atInjectRequired;
        if (this.exactBindingAnnotationsRequired || parentOptions.exactBindingAnnotationsRequired) {
            z3 = true;
        }
        return new InjectorOptions(stage, z2, z4, z5, z3);
    }
}
