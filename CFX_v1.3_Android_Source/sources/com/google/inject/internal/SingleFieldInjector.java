package com.google.inject.internal;

import com.google.inject.spi.Dependency;
import com.google.inject.spi.InjectionPoint;
import java.lang.reflect.Field;

final class SingleFieldInjector implements SingleMemberInjector {
    final BindingImpl<?> binding;
    final Dependency<?> dependency;
    final Field field;
    final InjectionPoint injectionPoint;

    public SingleFieldInjector(InjectorImpl injector, InjectionPoint injectionPoint2, Errors errors) throws ErrorsException {
        this.injectionPoint = injectionPoint2;
        this.field = (Field) injectionPoint2.getMember();
        this.dependency = (Dependency) injectionPoint2.getDependencies().get(0);
        this.field.setAccessible(true);
        this.binding = injector.getBindingOrThrow(this.dependency.getKey(), errors, JitLimitation.NO_JIT);
    }

    public InjectionPoint getInjectionPoint() {
        return this.injectionPoint;
    }

    public void inject(Errors errors, InternalContext context, Object o) {
        Errors errors2 = errors.withSource(this.dependency);
        Dependency previous = context.pushDependency(this.dependency, this.binding.getSource());
        try {
            this.field.set(o, this.binding.getInternalFactory().get(errors2, context, this.dependency, false));
            context.popStateAndSetDependency(previous);
        } catch (ErrorsException e) {
            errors2.withSource(this.injectionPoint).merge(e.getErrors());
            context.popStateAndSetDependency(previous);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (Throwable th) {
            context.popStateAndSetDependency(previous);
            throw th;
        }
    }
}
