package com.google.inject.internal;

import com.google.inject.Provider;
import com.google.inject.ProvisionException;
import com.google.inject.spi.Message;

final class ProviderToInternalFactoryAdapter<T> implements Provider<T> {
    private final InjectorImpl injector;
    /* access modifiers changed from: private */
    public final InternalFactory<? extends T> internalFactory;

    public ProviderToInternalFactoryAdapter(InjectorImpl injector2, InternalFactory<? extends T> internalFactory2) {
        this.injector = injector2;
        this.internalFactory = internalFactory2;
    }

    public T get() {
        final Errors errors = new Errors();
        try {
            T t = this.injector.callInContext(new ContextualCallable<T>() {
                public T call(InternalContext context) throws ErrorsException {
                    return ProviderToInternalFactoryAdapter.this.internalFactory.get(errors, context, context.getDependency(), true);
                }
            });
            errors.throwIfNewErrors(0);
            return t;
        } catch (ErrorsException e) {
            throw new ProvisionException((Iterable<Message>) errors.merge(e.getErrors()).getMessages());
        }
    }

    public String toString() {
        return this.internalFactory.toString();
    }
}
