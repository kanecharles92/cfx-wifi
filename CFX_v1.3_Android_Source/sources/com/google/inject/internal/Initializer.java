package com.google.inject.internal;

import com.google.inject.Binding;
import com.google.inject.Key;
import com.google.inject.Stage;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.InjectionPoint;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Maps;

final class Initializer {
    /* access modifiers changed from: private */
    public final Thread creatingThread = Thread.currentThread();
    /* access modifiers changed from: private */
    public final Map<Object, InjectableReference<?>> pendingInjection = Maps.newIdentityHashMap();
    /* access modifiers changed from: private */
    public final Map<Object, MembersInjectorImpl<?>> pendingMembersInjectors = Maps.newIdentityHashMap();
    /* access modifiers changed from: private */
    public final CountDownLatch ready = new CountDownLatch(1);

    private class InjectableReference<T> implements Initializable<T> {
        private final InjectorImpl injector;
        /* access modifiers changed from: private */
        public final T instance;
        private final Key<T> key;
        private final ProvisionListenerStackCallback<T> provisionCallback;
        private final Object source;

        public InjectableReference(InjectorImpl injector2, T instance2, Key<T> key2, ProvisionListenerStackCallback<T> provisionCallback2, Object source2) {
            this.injector = injector2;
            this.key = key2;
            this.provisionCallback = provisionCallback2;
            this.instance = Preconditions.checkNotNull(instance2, "instance");
            this.source = Preconditions.checkNotNull(source2, "source");
        }

        public MembersInjectorImpl<T> validate(Errors errors) throws ErrorsException {
            return this.injector.membersInjectorStore.get(TypeLiteral.get(this.instance.getClass()), errors.withSource(this.source));
        }

        public T get(Errors errors) throws ErrorsException {
            boolean z;
            boolean z2 = true;
            if (Initializer.this.ready.getCount() == 0) {
                return this.instance;
            }
            if (Thread.currentThread() != Initializer.this.creatingThread) {
                try {
                    Initializer.this.ready.await();
                    return this.instance;
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            } else {
                if (Initializer.this.pendingInjection.remove(this.instance) != null) {
                    MembersInjectorImpl<T> membersInjector = (MembersInjectorImpl) Initializer.this.pendingMembersInjectors.remove(this.instance);
                    if (membersInjector != null) {
                        z = true;
                    } else {
                        z = false;
                    }
                    Preconditions.checkState(z, "No membersInjector available for instance: %s, from key: %s", this.instance, this.key);
                    T t = this.instance;
                    Errors withSource = errors.withSource(this.source);
                    Key<T> key2 = this.key;
                    ProvisionListenerStackCallback<T> provisionListenerStackCallback = this.provisionCallback;
                    Object obj = this.source;
                    if (this.injector.options.stage != Stage.TOOL) {
                        z2 = false;
                    }
                    membersInjector.injectAndNotify(t, withSource, key2, provisionListenerStackCallback, obj, z2);
                }
                return this.instance;
            }
        }

        public String toString() {
            return this.instance.toString();
        }
    }

    Initializer() {
    }

    /* access modifiers changed from: 0000 */
    public <T> Initializable<T> requestInjection(InjectorImpl injector, T instance, Binding<T> binding, Object source, Set<InjectionPoint> injectionPoints) {
        Key key = null;
        Preconditions.checkNotNull(source);
        ProvisionListenerStackCallback provisionListenerStackCallback = binding == null ? null : injector.provisionListenerStore.get(binding);
        if (instance == null || (injectionPoints.isEmpty() && !injector.membersInjectorStore.hasTypeListeners() && (provisionListenerStackCallback == null || !provisionListenerStackCallback.hasListeners()))) {
            return Initializables.m68of(instance);
        }
        if (binding != null) {
            key = binding.getKey();
        }
        InjectableReference<T> initializable = new InjectableReference<>(injector, instance, key, provisionListenerStackCallback, source);
        this.pendingInjection.put(instance, initializable);
        return initializable;
    }

    /* access modifiers changed from: 0000 */
    public void validateOustandingInjections(Errors errors) {
        for (InjectableReference<?> reference : this.pendingInjection.values()) {
            try {
                this.pendingMembersInjectors.put(reference.instance, reference.validate(errors));
            } catch (ErrorsException e) {
                errors.merge(e.getErrors());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void injectAll(Errors errors) {
        Iterator i$ = Lists.newArrayList((Iterable<? extends E>) this.pendingInjection.values()).iterator();
        while (i$.hasNext()) {
            try {
                ((InjectableReference) i$.next()).get(errors);
            } catch (ErrorsException e) {
                errors.merge(e.getErrors());
            }
        }
        if (!this.pendingInjection.isEmpty()) {
            throw new AssertionError("Failed to satisfy " + this.pendingInjection);
        }
        this.ready.countDown();
    }
}
