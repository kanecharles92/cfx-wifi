package com.google.inject.internal;

import com.google.inject.Binding;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Stage;
import com.google.inject.spi.ProvisionListener;
import com.google.inject.spi.ProvisionListenerBinding;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import org.roboguice.shaded.goole.common.cache.CacheBuilder;
import org.roboguice.shaded.goole.common.cache.CacheLoader;
import org.roboguice.shaded.goole.common.cache.LoadingCache;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.Lists;

final class ProvisionListenerCallbackStore {
    private static final Set<Key<?>> INTERNAL_BINDINGS = ImmutableSet.m149of(Key.get(Injector.class), Key.get(Stage.class), Key.get(Logger.class));
    private final LoadingCache<KeyBinding, ProvisionListenerStackCallback<?>> cache = CacheBuilder.newBuilder().build(new CacheLoader<KeyBinding, ProvisionListenerStackCallback<?>>() {
        public ProvisionListenerStackCallback<?> load(KeyBinding key) {
            return ProvisionListenerCallbackStore.this.create(key.binding);
        }
    });
    private final ImmutableList<ProvisionListenerBinding> listenerBindings;

    private static class KeyBinding {
        final Binding<?> binding;
        final Key<?> key;

        KeyBinding(Key<?> key2, Binding<?> binding2) {
            this.key = key2;
            this.binding = binding2;
        }

        public boolean equals(Object obj) {
            return (obj instanceof KeyBinding) && this.key.equals(((KeyBinding) obj).key);
        }

        public int hashCode() {
            return this.key.hashCode();
        }
    }

    ProvisionListenerCallbackStore(List<ProvisionListenerBinding> listenerBindings2) {
        this.listenerBindings = ImmutableList.copyOf((Collection<? extends E>) listenerBindings2);
    }

    public <T> ProvisionListenerStackCallback<T> get(Binding<T> binding) {
        if (!INTERNAL_BINDINGS.contains(binding.getKey())) {
            return (ProvisionListenerStackCallback) this.cache.getUnchecked(new KeyBinding(binding.getKey(), binding));
        }
        return ProvisionListenerStackCallback.emptyListener();
    }

    /* access modifiers changed from: 0000 */
    public boolean remove(Binding<?> type) {
        return this.cache.asMap().remove(type) != null;
    }

    /* access modifiers changed from: private */
    public <T> ProvisionListenerStackCallback<T> create(Binding<T> binding) {
        List<ProvisionListener> listeners = null;
        Iterator i$ = this.listenerBindings.iterator();
        while (i$.hasNext()) {
            ProvisionListenerBinding provisionBinding = (ProvisionListenerBinding) i$.next();
            if (provisionBinding.getBindingMatcher().matches(binding)) {
                if (listeners == null) {
                    listeners = Lists.newArrayList();
                }
                listeners.addAll(provisionBinding.getListeners());
            }
        }
        if (listeners == null || listeners.isEmpty()) {
            return ProvisionListenerStackCallback.emptyListener();
        }
        return new ProvisionListenerStackCallback<>(binding, listeners);
    }
}
