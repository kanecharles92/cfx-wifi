package com.google.inject.internal;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

final class ConstructionContext<T> {
    boolean constructing;
    T currentReference;
    List<DelegatingInvocationHandler<T>> invocationHandlers;

    ConstructionContext() {
    }

    public T getCurrentReference() {
        return this.currentReference;
    }

    public void removeCurrentReference() {
        this.currentReference = null;
    }

    public void setCurrentReference(T currentReference2) {
        this.currentReference = currentReference2;
    }

    public boolean isConstructing() {
        return this.constructing;
    }

    public void startConstruction() {
        this.constructing = true;
    }

    public void finishConstruction() {
        this.constructing = false;
        this.invocationHandlers = null;
    }

    public Object createProxy(Errors errors, Class<?> expectedType) throws ErrorsException {
        if (!expectedType.isInterface()) {
            throw errors.cannotSatisfyCircularDependency(expectedType).toException();
        }
        if (this.invocationHandlers == null) {
            this.invocationHandlers = new ArrayList();
        }
        DelegatingInvocationHandler<T> invocationHandler = new DelegatingInvocationHandler<>();
        this.invocationHandlers.add(invocationHandler);
        return expectedType.cast(Proxy.newProxyInstance(BytecodeGen.getClassLoader(expectedType), new Class[]{expectedType, CircularDependencyProxy.class}, invocationHandler));
    }

    public void setProxyDelegates(T delegate) {
        if (this.invocationHandlers != null) {
            for (DelegatingInvocationHandler<T> handler : this.invocationHandlers) {
                handler.setDelegate(delegate);
            }
        }
    }
}
