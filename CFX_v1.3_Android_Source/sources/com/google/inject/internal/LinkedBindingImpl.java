package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Key;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.HasDependencies;
import com.google.inject.spi.LinkedKeyBinding;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;

public final class LinkedBindingImpl<T> extends BindingImpl<T> implements LinkedKeyBinding<T>, HasDependencies {
    final Key<? extends T> targetKey;

    public LinkedBindingImpl(InjectorImpl injector, Key<T> key, Object source, InternalFactory<? extends T> internalFactory, Scoping scoping, Key<? extends T> targetKey2) {
        super(injector, key, source, internalFactory, scoping);
        this.targetKey = targetKey2;
    }

    public LinkedBindingImpl(Object source, Key<T> key, Scoping scoping, Key<? extends T> targetKey2) {
        super(source, key, scoping);
        this.targetKey = targetKey2;
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        return visitor.visit((LinkedKeyBinding<? extends T>) this);
    }

    public Key<? extends T> getLinkedKey() {
        return this.targetKey;
    }

    public Set<Dependency<?>> getDependencies() {
        return ImmutableSet.m147of(Dependency.get(this.targetKey));
    }

    public BindingImpl<T> withScoping(Scoping scoping) {
        return new LinkedBindingImpl(getSource(), getKey(), scoping, this.targetKey);
    }

    public BindingImpl<T> withKey(Key<T> key) {
        return new LinkedBindingImpl(getSource(), key, getScoping(), this.targetKey);
    }

    public void applyTo(Binder binder) {
        getScoping().applyTo(binder.withSource(getSource()).bind(getKey()).mo7040to(getLinkedKey()));
    }

    public String toString() {
        return Objects.toStringHelper(LinkedKeyBinding.class).add("key", (Object) getKey()).add("source", getSource()).add("scope", (Object) getScoping()).add("target", (Object) this.targetKey).toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof LinkedBindingImpl)) {
            return false;
        }
        LinkedBindingImpl<?> o = (LinkedBindingImpl) obj;
        if (!getKey().equals(o.getKey()) || !getScoping().equals(o.getScoping()) || !Objects.equal(this.targetKey, o.targetKey)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hashCode(getKey(), getScoping(), this.targetKey);
    }
}
