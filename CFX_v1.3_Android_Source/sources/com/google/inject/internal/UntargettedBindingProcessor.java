package com.google.inject.internal;

import com.google.inject.Binding;
import com.google.inject.spi.UntargettedBinding;

class UntargettedBindingProcessor extends AbstractBindingProcessor {
    UntargettedBindingProcessor(Errors errors, ProcessedBindingData bindingData) {
        super(errors, bindingData);
    }

    public <T> Boolean visit(Binding<T> binding) {
        return (Boolean) binding.acceptTargetVisitor(new Processor<T, Boolean>((BindingImpl) binding) {
            public Boolean visit(UntargettedBinding<? extends T> untargettedBinding) {
                prepareBinding();
                if (this.key.getAnnotationType() != null) {
                    UntargettedBindingProcessor.this.errors.missingImplementation(this.key);
                    UntargettedBindingProcessor.this.putBinding(UntargettedBindingProcessor.this.invalidBinding(UntargettedBindingProcessor.this.injector, this.key, this.source));
                    return Boolean.valueOf(true);
                }
                try {
                    BindingImpl<T> binding = UntargettedBindingProcessor.this.injector.createUninitializedBinding(this.key, this.scoping, this.source, UntargettedBindingProcessor.this.errors, false);
                    scheduleInitialization(binding);
                    UntargettedBindingProcessor.this.putBinding(binding);
                } catch (ErrorsException e) {
                    UntargettedBindingProcessor.this.errors.merge(e.getErrors());
                    UntargettedBindingProcessor.this.putBinding(UntargettedBindingProcessor.this.invalidBinding(UntargettedBindingProcessor.this.injector, this.key, this.source));
                }
                return Boolean.valueOf(true);
            }

            /* access modifiers changed from: protected */
            public Boolean visitOther(Binding<? extends T> binding) {
                return Boolean.valueOf(false);
            }
        });
    }
}
