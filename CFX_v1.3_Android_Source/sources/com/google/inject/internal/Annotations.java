package com.google.inject.internal;

import android.support.p000v4.media.TransportMediator;
import com.google.inject.BindingAnnotation;
import com.google.inject.Key;
import com.google.inject.ScopeAnnotation;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.util.Classes;
import com.google.inject.name.Names;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import javax.inject.Named;
import javax.inject.Qualifier;
import javax.inject.Scope;
import org.roboguice.shaded.goole.common.base.Function;
import org.roboguice.shaded.goole.common.base.Joiner;
import org.roboguice.shaded.goole.common.base.Joiner.MapJoiner;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.cache.CacheBuilder;
import org.roboguice.shaded.goole.common.cache.CacheLoader;
import org.roboguice.shaded.goole.common.cache.LoadingCache;
import org.roboguice.shaded.goole.common.collect.ImmutableMap;
import org.roboguice.shaded.goole.common.collect.ImmutableMap.Builder;
import org.roboguice.shaded.goole.common.collect.Maps;

public class Annotations {
    private static final Function<Object, String> DEEP_TO_STRING_FN = new Function<Object, String>() {
        public String apply(Object arg) {
            String s = Arrays.deepToString(new Object[]{arg});
            return s.substring(1, s.length() - 1);
        }
    };
    private static final MapJoiner JOINER = Joiner.m84on(", ").withKeyValueSeparator("=");
    private static final AnnotationChecker bindingAnnotationChecker = new AnnotationChecker(Arrays.asList(new Class[]{BindingAnnotation.class, Qualifier.class}));
    private static final LoadingCache<Class<? extends Annotation>, Annotation> cache = CacheBuilder.newBuilder().weakKeys().build(new CacheLoader<Class<? extends Annotation>, Annotation>() {
        public Annotation load(Class<? extends Annotation> input) {
            return Annotations.generateAnnotationImpl(input);
        }
    });
    private static final AnnotationChecker scopeChecker = new AnnotationChecker(Arrays.asList(new Class[]{ScopeAnnotation.class, Scope.class}));

    static class AnnotationChecker {
        /* access modifiers changed from: private */
        public final Collection<Class<? extends Annotation>> annotationTypes;
        final LoadingCache<Class<? extends Annotation>, Boolean> cache = CacheBuilder.newBuilder().weakKeys().build(this.hasAnnotations);
        private CacheLoader<Class<? extends Annotation>, Boolean> hasAnnotations = new CacheLoader<Class<? extends Annotation>, Boolean>() {
            public Boolean load(Class<? extends Annotation> annotationType) {
                for (Annotation annotation : annotationType.getAnnotations()) {
                    if (AnnotationChecker.this.annotationTypes.contains(annotation.annotationType())) {
                        return Boolean.valueOf(true);
                    }
                }
                return Boolean.valueOf(false);
            }
        };

        AnnotationChecker(Collection<Class<? extends Annotation>> annotationTypes2) {
            this.annotationTypes = annotationTypes2;
        }

        /* access modifiers changed from: 0000 */
        public boolean hasAnnotations(Class<? extends Annotation> annotated) {
            return ((Boolean) this.cache.getUnchecked(annotated)).booleanValue();
        }
    }

    public static boolean isMarker(Class<? extends Annotation> annotationType) {
        return annotationType.getDeclaredMethods().length == 0;
    }

    public static boolean isAllDefaultMethods(Class<? extends Annotation> annotationType) {
        boolean hasMethods = false;
        for (Method m : annotationType.getDeclaredMethods()) {
            hasMethods = true;
            if (m.getDefaultValue() == null) {
                return false;
            }
        }
        return hasMethods;
    }

    public static <T extends Annotation> T generateAnnotation(Class<T> annotationType) {
        Preconditions.checkState(isAllDefaultMethods(annotationType), "%s is not all default methods", annotationType);
        return (Annotation) cache.getUnchecked(annotationType);
    }

    /* access modifiers changed from: private */
    public static <T extends Annotation> T generateAnnotationImpl(final Class<T> annotationType) {
        final Map<String, Object> members = resolveMembers(annotationType);
        return (Annotation) annotationType.cast(Proxy.newProxyInstance(annotationType.getClassLoader(), new Class[]{annotationType}, new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) throws Exception {
                String name = method.getName();
                if (name.equals("annotationType")) {
                    return annotationType;
                }
                if (name.equals("toString")) {
                    return Annotations.annotationToString(annotationType, members);
                }
                if (name.equals("hashCode")) {
                    return Integer.valueOf(Annotations.annotationHashCode(annotationType, members));
                }
                if (name.equals("equals")) {
                    return Boolean.valueOf(Annotations.annotationEquals(annotationType, members, args[0]));
                }
                return members.get(name);
            }
        }));
    }

    private static ImmutableMap<String, Object> resolveMembers(Class<? extends Annotation> annotationType) {
        Method[] arr$;
        Builder<String, Object> result = ImmutableMap.builder();
        for (Method method : annotationType.getDeclaredMethods()) {
            result.put(method.getName(), method.getDefaultValue());
        }
        return result.build();
    }

    /* access modifiers changed from: private */
    public static boolean annotationEquals(Class<? extends Annotation> type, Map<String, Object> members, Object other) throws Exception {
        Method[] arr$;
        if (!type.isInstance(other)) {
            return false;
        }
        for (Method method : type.getDeclaredMethods()) {
            if (!Arrays.deepEquals(new Object[]{method.invoke(other, new Object[0])}, new Object[]{members.get(method.getName())})) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static int annotationHashCode(Class<? extends Annotation> type, Map<String, Object> members) throws Exception {
        int result = 0;
        for (Method method : type.getDeclaredMethods()) {
            String name = method.getName();
            result += (name.hashCode() * TransportMediator.KEYCODE_MEDIA_PAUSE) ^ (Arrays.deepHashCode(new Object[]{members.get(name)}) - 31);
        }
        return result;
    }

    /* access modifiers changed from: private */
    public static String annotationToString(Class<? extends Annotation> type, Map<String, Object> members) throws Exception {
        StringBuilder sb = new StringBuilder().append("@").append(type.getName()).append("(");
        JOINER.appendTo(sb, Maps.transformValues(members, DEEP_TO_STRING_FN));
        return sb.append(")").toString();
    }

    public static boolean isRetainedAtRuntime(Class<? extends Annotation> annotationType) {
        Retention retention = (Retention) annotationType.getAnnotation(Retention.class);
        return retention != null && retention.value() == RetentionPolicy.RUNTIME;
    }

    public static Class<? extends Annotation> findScopeAnnotation(Errors errors, Class<?> implementation) {
        return findScopeAnnotation(errors, implementation.getAnnotations());
    }

    public static Class<? extends Annotation> findScopeAnnotation(Errors errors, Annotation[] annotations) {
        Class<? extends Annotation> found = null;
        for (Annotation annotation : annotations) {
            Class<? extends Annotation> annotationType = annotation.annotationType();
            if (isScopeAnnotation(annotationType)) {
                if (found != null) {
                    errors.duplicateScopeAnnotations(found, annotationType);
                } else {
                    found = annotationType;
                }
            }
        }
        return found;
    }

    public static boolean isScopeAnnotation(Class<? extends Annotation> annotationType) {
        return scopeChecker.hasAnnotations(annotationType);
    }

    public static void checkForMisplacedScopeAnnotations(Class<?> type, Object source, Errors errors) {
        if (!Classes.isConcrete(type)) {
            Class<? extends Annotation> scopeAnnotation = findScopeAnnotation(errors, type);
            if (scopeAnnotation != null) {
                errors.withSource(type).scopeAnnotationOnAbstractType(scopeAnnotation, type, source);
            }
        }
    }

    public static Key<?> getKey(TypeLiteral<?> type, Member member, Annotation[] annotations, Errors errors) throws ErrorsException {
        int numErrorsBefore = errors.size();
        Annotation found = findBindingAnnotation(errors, member, annotations);
        errors.throwIfNewErrors(numErrorsBefore);
        return found == null ? Key.get(type) : Key.get(type, found);
    }

    public static Annotation findBindingAnnotation(Errors errors, Member member, Annotation[] annotations) {
        Annotation[] arr$;
        Annotation found = null;
        for (Annotation annotation : annotations) {
            Class<? extends Annotation> annotationType = annotation.annotationType();
            if (isBindingAnnotation(annotationType)) {
                if (found != null) {
                    errors.duplicateBindingAnnotations(member, found.annotationType(), annotationType);
                } else {
                    found = annotation;
                }
            }
        }
        return found;
    }

    public static boolean isBindingAnnotation(Class<? extends Annotation> annotationType) {
        return bindingAnnotationChecker.hasAnnotations(annotationType);
    }

    public static Annotation canonicalizeIfNamed(Annotation annotation) {
        if (annotation instanceof Named) {
            return Names.named(((Named) annotation).value());
        }
        return annotation;
    }

    public static Class<? extends Annotation> canonicalizeIfNamed(Class<? extends Annotation> annotationType) {
        if (annotationType == Named.class) {
            return com.google.inject.name.Named.class;
        }
        return annotationType;
    }
}
