package com.google.inject.internal;

import com.google.inject.internal.ProvisionListenerStackCallback.ProvisionCallback;
import com.google.inject.spi.Dependency;
import javax.inject.Provider;
import org.roboguice.shaded.goole.common.base.Preconditions;

abstract class ProviderInternalFactory<T> implements InternalFactory<T> {
    private final boolean allowProxy;
    protected final Object source;

    ProviderInternalFactory(Object source2, boolean allowProxy2) {
        this.source = Preconditions.checkNotNull(source2, "source");
        this.allowProxy = allowProxy2;
    }

    /* access modifiers changed from: protected */
    public T circularGet(Provider<? extends T> provider, Errors errors, InternalContext context, Dependency<?> dependency, boolean linked, ProvisionListenerStackCallback<T> provisionCallback) throws ErrorsException {
        Class<?> expectedType = dependency.getKey().getTypeLiteral().getRawType();
        final ConstructionContext<T> constructionContext = context.getConstructionContext(this);
        if (!constructionContext.isConstructing()) {
            constructionContext.startConstruction();
            try {
                if (!provisionCallback.hasListeners()) {
                    return provision(provider, errors, dependency, constructionContext);
                }
                final Provider<? extends T> provider2 = provider;
                final Errors errors2 = errors;
                final Dependency<?> dependency2 = dependency;
                T provision = provisionCallback.provision(errors, context, new ProvisionCallback<T>() {
                    public T call() throws ErrorsException {
                        return ProviderInternalFactory.this.provision(provider2, errors2, dependency2, constructionContext);
                    }
                });
                constructionContext.removeCurrentReference();
                constructionContext.finishConstruction();
                return provision;
            } finally {
                constructionContext.removeCurrentReference();
                constructionContext.finishConstruction();
            }
        } else if (this.allowProxy) {
            return constructionContext.createProxy(errors, expectedType);
        } else {
            throw errors.circularProxiesDisabled(expectedType).toException();
        }
    }

    /* access modifiers changed from: protected */
    public T provision(Provider<? extends T> provider, Errors errors, Dependency<?> dependency, ConstructionContext<T> constructionContext) throws ErrorsException {
        T t = errors.checkForNull(provider.get(), this.source, dependency);
        constructionContext.setProxyDelegates(t);
        return t;
    }
}
