package com.google.inject.internal;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.logging.Logger;
import org.roboguice.shaded.goole.common.cache.CacheBuilder;
import org.roboguice.shaded.goole.common.cache.CacheLoader;
import org.roboguice.shaded.goole.common.cache.LoadingCache;

public final class BytecodeGen {
    private static final String CGLIB_PACKAGE = " ";
    private static final LoadingCache<ClassLoader, ClassLoader> CLASS_LOADER_CACHE;
    private static final boolean CUSTOM_LOADER_ENABLED = Boolean.parseBoolean(System.getProperty("guice.custom.loader", "true"));
    static final ClassLoader GUICE_CLASS_LOADER = canonicalize(BytecodeGen.class.getClassLoader());
    static final String GUICE_INTERNAL_PACKAGE = BytecodeGen.class.getName().replaceFirst("\\.internal\\..*$", ".internal");
    static final Logger logger = Logger.getLogger(BytecodeGen.class.getName());

    private static class BridgeClassLoader extends ClassLoader {
        BridgeClassLoader() {
        }

        BridgeClassLoader(ClassLoader usersClassLoader) {
            super(usersClassLoader);
        }

        /* access modifiers changed from: protected */
        public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
            if (name.startsWith("sun.reflect")) {
                return SystemBridgeHolder.SYSTEM_BRIDGE.classicLoadClass(name, resolve);
            }
            if (name.startsWith(BytecodeGen.GUICE_INTERNAL_PACKAGE) || name.startsWith(BytecodeGen.CGLIB_PACKAGE)) {
                if (BytecodeGen.GUICE_CLASS_LOADER == null) {
                    return SystemBridgeHolder.SYSTEM_BRIDGE.classicLoadClass(name, resolve);
                }
                try {
                    Class<?> clazz = BytecodeGen.GUICE_CLASS_LOADER.loadClass(name);
                    if (!resolve) {
                        return clazz;
                    }
                    resolveClass(clazz);
                    return clazz;
                } catch (Throwable th) {
                }
            }
            return classicLoadClass(name, resolve);
        }

        /* access modifiers changed from: 0000 */
        public Class<?> classicLoadClass(String name, boolean resolve) throws ClassNotFoundException {
            return super.loadClass(name, resolve);
        }
    }

    private static class SystemBridgeHolder {
        static final BridgeClassLoader SYSTEM_BRIDGE = new BridgeClassLoader();

        private SystemBridgeHolder() {
        }
    }

    public enum Visibility {
        PUBLIC {
            public Visibility and(Visibility that) {
                return that;
            }
        },
        SAME_PACKAGE {
            public Visibility and(Visibility that) {
                return this;
            }
        };

        public abstract Visibility and(Visibility visibility);

        public static Visibility forMember(Member member) {
            Class[] parameterTypes;
            if ((member.getModifiers() & 5) == 0) {
                return SAME_PACKAGE;
            }
            if (member instanceof Constructor) {
                parameterTypes = ((Constructor) member).getParameterTypes();
            } else {
                Method method = (Method) member;
                if (forType(method.getReturnType()) == SAME_PACKAGE) {
                    return SAME_PACKAGE;
                }
                parameterTypes = method.getParameterTypes();
            }
            for (Class forType : parameterTypes) {
                if (forType(forType) == SAME_PACKAGE) {
                    return SAME_PACKAGE;
                }
            }
            return PUBLIC;
        }

        public static Visibility forType(Class<?> type) {
            return (type.getModifiers() & 5) != 0 ? PUBLIC : SAME_PACKAGE;
        }
    }

    static {
        CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder().weakKeys().weakValues();
        if (!CUSTOM_LOADER_ENABLED) {
            builder.maximumSize(0);
        }
        CLASS_LOADER_CACHE = builder.build(new CacheLoader<ClassLoader, ClassLoader>() {
            public ClassLoader load(final ClassLoader typeClassLoader) {
                BytecodeGen.logger.fine("Creating a bridge ClassLoader for " + typeClassLoader);
                return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
                    public ClassLoader run() {
                        return new BridgeClassLoader(typeClassLoader);
                    }
                });
            }
        });
    }

    private static ClassLoader canonicalize(ClassLoader classLoader) {
        return classLoader != null ? classLoader : SystemBridgeHolder.SYSTEM_BRIDGE.getParent();
    }

    public static ClassLoader getClassLoader(Class<?> type) {
        return getClassLoader(type, type.getClassLoader());
    }

    private static ClassLoader getClassLoader(Class<?> type, ClassLoader delegate) {
        if (!CUSTOM_LOADER_ENABLED) {
            return delegate;
        }
        if (type.getName().startsWith("java.")) {
            return GUICE_CLASS_LOADER;
        }
        ClassLoader delegate2 = canonicalize(delegate);
        if (delegate2 == GUICE_CLASS_LOADER || (delegate2 instanceof BridgeClassLoader)) {
            return delegate2;
        }
        if (Visibility.forType(type) != Visibility.PUBLIC) {
            return delegate2;
        }
        if (delegate2 != SystemBridgeHolder.SYSTEM_BRIDGE.getParent()) {
            return (ClassLoader) CLASS_LOADER_CACHE.getUnchecked(delegate2);
        }
        return SystemBridgeHolder.SYSTEM_BRIDGE;
    }
}
