package com.google.inject.internal;

import com.google.inject.Binding;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.spi.ConstructorBinding;
import com.google.inject.spi.ConvertedConstantBinding;
import com.google.inject.spi.ExposedBinding;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.InstanceBinding;
import com.google.inject.spi.LinkedKeyBinding;
import com.google.inject.spi.PrivateElements;
import com.google.inject.spi.ProviderBinding;
import com.google.inject.spi.ProviderInstanceBinding;
import com.google.inject.spi.ProviderKeyBinding;
import com.google.inject.spi.UntargettedBinding;
import java.util.Set;

final class BindingProcessor extends AbstractBindingProcessor {
    /* access modifiers changed from: private */
    public final Initializer initializer;

    BindingProcessor(Errors errors, Initializer initializer2, ProcessedBindingData bindingData) {
        super(errors, bindingData);
        this.initializer = initializer2;
    }

    public <T> Boolean visit(Binding<T> command) {
        Class<?> rawType = command.getKey().getTypeLiteral().getRawType();
        if (Void.class.equals(rawType)) {
            if (!(command instanceof ProviderInstanceBinding) || !(((ProviderInstanceBinding) command).getUserSuppliedProvider() instanceof ProviderMethod)) {
                this.errors.missingConstantValues();
            } else {
                this.errors.voidProviderMethod();
            }
            return Boolean.valueOf(true);
        } else if (rawType != Provider.class) {
            return (Boolean) command.acceptTargetVisitor(new Processor<T, Boolean>((BindingImpl) command) {
                public Boolean visit(ConstructorBinding<? extends T> binding) {
                    prepareBinding();
                    try {
                        ConstructorBindingImpl<T> onInjector = ConstructorBindingImpl.create(BindingProcessor.this.injector, this.key, binding.getConstructor(), this.source, this.scoping, BindingProcessor.this.errors, false, false);
                        scheduleInitialization(onInjector);
                        BindingProcessor.this.putBinding(onInjector);
                    } catch (ErrorsException e) {
                        BindingProcessor.this.errors.merge(e.getErrors());
                        BindingProcessor.this.putBinding(BindingProcessor.this.invalidBinding(BindingProcessor.this.injector, this.key, this.source));
                    }
                    return Boolean.valueOf(true);
                }

                public Boolean visit(InstanceBinding<? extends T> binding) {
                    prepareBinding();
                    Set<InjectionPoint> injectionPoints = binding.getInjectionPoints();
                    T instance = binding.getInstance();
                    BindingProcessor.this.putBinding(new InstanceBindingImpl(BindingProcessor.this.injector, this.key, this.source, Scoping.scope(this.key, BindingProcessor.this.injector, new ConstantFactory<>(BindingProcessor.this.initializer.requestInjection(BindingProcessor.this.injector, instance, binding, this.source, injectionPoints)), this.source, this.scoping), injectionPoints, instance));
                    return Boolean.valueOf(true);
                }

                public Boolean visit(ProviderInstanceBinding<? extends T> binding) {
                    prepareBinding();
                    javax.inject.Provider<? extends T> provider = binding.getUserSuppliedProvider();
                    Set<InjectionPoint> injectionPoints = binding.getInjectionPoints();
                    BindingProcessor.this.putBinding(new ProviderInstanceBindingImpl(BindingProcessor.this.injector, this.key, this.source, Scoping.scope(this.key, BindingProcessor.this.injector, new InternalFactoryToInitializableAdapter<>(BindingProcessor.this.initializer.requestInjection(BindingProcessor.this.injector, provider, null, this.source, injectionPoints), this.source, !BindingProcessor.this.injector.options.disableCircularProxies, BindingProcessor.this.injector.provisionListenerStore.get(binding)), this.source, this.scoping), this.scoping, provider, injectionPoints));
                    return Boolean.valueOf(true);
                }

                public Boolean visit(ProviderKeyBinding<? extends T> binding) {
                    prepareBinding();
                    Key<? extends javax.inject.Provider<? extends T>> providerKey = binding.getProviderKey();
                    BoundProviderFactory<T> boundProviderFactory = new BoundProviderFactory<>(BindingProcessor.this.injector, providerKey, this.source, !BindingProcessor.this.injector.options.disableCircularProxies, BindingProcessor.this.injector.provisionListenerStore.get(binding));
                    BindingProcessor.this.bindingData.addCreationListener(boundProviderFactory);
                    BindingProcessor.this.putBinding(new LinkedProviderBindingImpl(BindingProcessor.this.injector, this.key, this.source, Scoping.scope(this.key, BindingProcessor.this.injector, boundProviderFactory, this.source, this.scoping), this.scoping, providerKey));
                    return Boolean.valueOf(true);
                }

                public Boolean visit(LinkedKeyBinding<? extends T> binding) {
                    prepareBinding();
                    Key<? extends T> linkedKey = binding.getLinkedKey();
                    if (this.key.equals(linkedKey)) {
                        BindingProcessor.this.errors.recursiveBinding();
                    }
                    FactoryProxy<T> factory = new FactoryProxy<>(BindingProcessor.this.injector, this.key, linkedKey, this.source);
                    BindingProcessor.this.bindingData.addCreationListener(factory);
                    BindingProcessor.this.putBinding(new LinkedBindingImpl(BindingProcessor.this.injector, this.key, this.source, Scoping.scope(this.key, BindingProcessor.this.injector, factory, this.source, this.scoping), this.scoping, linkedKey));
                    return Boolean.valueOf(true);
                }

                public Boolean visit(UntargettedBinding<? extends T> untargettedBinding) {
                    return Boolean.valueOf(false);
                }

                public Boolean visit(ExposedBinding<? extends T> exposedBinding) {
                    throw new IllegalArgumentException("Cannot apply a non-module element");
                }

                public Boolean visit(ConvertedConstantBinding<? extends T> convertedConstantBinding) {
                    throw new IllegalArgumentException("Cannot apply a non-module element");
                }

                public Boolean visit(ProviderBinding<? extends T> providerBinding) {
                    throw new IllegalArgumentException("Cannot apply a non-module element");
                }

                /* access modifiers changed from: protected */
                public Boolean visitOther(Binding<? extends T> binding) {
                    throw new IllegalStateException("BindingProcessor should override all visitations");
                }
            });
        } else {
            this.errors.bindingToProvider();
            return Boolean.valueOf(true);
        }
    }

    public Boolean visit(PrivateElements privateElements) {
        for (Key<?> key : privateElements.getExposedKeys()) {
            bindExposed(privateElements, key);
        }
        return Boolean.valueOf(false);
    }

    private <T> void bindExposed(PrivateElements privateElements, Key<T> key) {
        ExposedKeyFactory<T> exposedKeyFactory = new ExposedKeyFactory<>(key, privateElements);
        this.bindingData.addCreationListener(exposedKeyFactory);
        putBinding(new ExposedBindingImpl(this.injector, privateElements.getExposedSource(key), key, exposedKeyFactory, privateElements));
    }
}
