package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.PrivateBinder;
import com.google.inject.spi.Element;
import com.google.inject.spi.ElementVisitor;
import com.google.inject.spi.PrivateElements;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.ImmutableMap;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Maps;

public final class PrivateElementsImpl implements PrivateElements {
    private ImmutableList<Element> elements;
    private List<Element> elementsMutable = Lists.newArrayList();
    private ImmutableMap<Key<?>, Object> exposedKeysToSources;
    private List<ExposureBuilder<?>> exposureBuilders = Lists.newArrayList();
    private Injector injector;
    private final Object source;

    public PrivateElementsImpl(Object source2) {
        this.source = Preconditions.checkNotNull(source2, "source");
    }

    public Object getSource() {
        return this.source;
    }

    public List<Element> getElements() {
        if (this.elements == null) {
            this.elements = ImmutableList.copyOf((Collection<? extends E>) this.elementsMutable);
            this.elementsMutable = null;
        }
        return this.elements;
    }

    public Injector getInjector() {
        return this.injector;
    }

    public void initInjector(Injector injector2) {
        Preconditions.checkState(this.injector == null, "injector already initialized");
        this.injector = (Injector) Preconditions.checkNotNull(injector2, "injector");
    }

    public Set<Key<?>> getExposedKeys() {
        if (this.exposedKeysToSources == null) {
            Map<Key<?>, Object> exposedKeysToSourcesMutable = Maps.newLinkedHashMap();
            for (ExposureBuilder<?> exposureBuilder : this.exposureBuilders) {
                exposedKeysToSourcesMutable.put(exposureBuilder.getKey(), exposureBuilder.getSource());
            }
            this.exposedKeysToSources = ImmutableMap.copyOf(exposedKeysToSourcesMutable);
            this.exposureBuilders = null;
        }
        return this.exposedKeysToSources.keySet();
    }

    public <T> T acceptVisitor(ElementVisitor<T> visitor) {
        return visitor.visit((PrivateElements) this);
    }

    public List<Element> getElementsMutable() {
        return this.elementsMutable;
    }

    public void addExposureBuilder(ExposureBuilder<?> exposureBuilder) {
        this.exposureBuilders.add(exposureBuilder);
    }

    public void applyTo(Binder binder) {
        PrivateBinder privateBinder = binder.withSource(this.source).newPrivateBinder();
        for (Element element : getElements()) {
            element.applyTo(privateBinder);
        }
        getExposedKeys();
        Iterator i$ = this.exposedKeysToSources.entrySet().iterator();
        while (i$.hasNext()) {
            Entry<Key<?>, Object> entry = (Entry) i$.next();
            privateBinder.withSource(entry.getValue()).expose((Key) entry.getKey());
        }
    }

    public Object getExposedSource(Key<?> key) {
        boolean z;
        getExposedKeys();
        Object source2 = this.exposedKeysToSources.get(key);
        if (source2 != null) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "%s not exposed by %s.", key, this);
        return source2;
    }

    public String toString() {
        return Objects.toStringHelper(PrivateElements.class).add("exposedKeys", (Object) getExposedKeys()).add("source", getSource()).toString();
    }
}
