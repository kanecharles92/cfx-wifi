package com.google.inject.internal;

import com.google.inject.ConfigurationException;
import com.google.inject.CreationException;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Provider;
import com.google.inject.ProvisionException;
import com.google.inject.Scope;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.util.Classes;
import com.google.inject.internal.util.SourceProvider;
import com.google.inject.internal.util.StackTraceElements;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.ElementSource;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.Message;
import com.google.inject.spi.ScopeBinding;
import com.google.inject.spi.TypeConverterBinding;
import com.google.inject.spi.TypeListenerBinding;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Formatter;
import java.util.List;
import java.util.Set;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Ordering;

public final class Errors implements Serializable {
    private static final String CONSTRUCTOR_RULES = "Classes must have either one (and only one) constructor annotated with @Inject or a zero-argument constructor that is not private.";
    private static final Collection<Converter<?>> converters = ImmutableList.m111of(new Converter<Class>(Class.class) {
        public String toString(Class c) {
            return c.getName();
        }
    }, new Converter<Member>(Member.class) {
        public String toString(Member member) {
            return Classes.toString(member);
        }
    }, new Converter<Key>(Key.class) {
        public String toString(Key key) {
            if (key.getAnnotationType() == null) {
                return key.getTypeLiteral().toString();
            }
            return key.getTypeLiteral() + " annotated with " + (key.getAnnotation() != null ? key.getAnnotation() : key.getAnnotationType());
        }
    });
    private List<Message> errors;
    private final Errors parent;
    private final Errors root;
    private final Object source;

    private static abstract class Converter<T> {
        final Class<T> type;

        /* access modifiers changed from: 0000 */
        public abstract String toString(T t);

        Converter(Class<T> type2) {
            this.type = type2;
        }

        /* access modifiers changed from: 0000 */
        public boolean appliesTo(Object o) {
            return o != null && this.type.isAssignableFrom(o.getClass());
        }

        /* access modifiers changed from: 0000 */
        public String convert(Object o) {
            return toString(this.type.cast(o));
        }
    }

    public Errors() {
        this.root = this;
        this.parent = null;
        this.source = SourceProvider.UNKNOWN_SOURCE;
    }

    public Errors(Object source2) {
        this.root = this;
        this.parent = null;
        this.source = source2;
    }

    private Errors(Errors parent2, Object source2) {
        this.root = parent2.root;
        this.parent = parent2;
        this.source = source2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Errors withSource(Object source2) {
        return (source2 == this.source || source2 == SourceProvider.UNKNOWN_SOURCE) ? this : new Errors(this, source2);
    }

    public Errors missingImplementation(Key key) {
        return addMessage("No implementation for %s was bound.", key);
    }

    public Errors jitDisabled(Key key) {
        return addMessage("Explicit bindings are required and %s is not explicitly bound.", key);
    }

    public Errors atInjectRequired(Class clazz) {
        return addMessage("Explicit @Inject annotations are required on constructors, but %s has no constructors annotated with @Inject.", clazz);
    }

    public Errors converterReturnedNull(String stringValue, Object source2, TypeLiteral<?> type, TypeConverterBinding typeConverterBinding) {
        return addMessage("Received null converting '%s' (bound at %s) to %s%n using %s.", stringValue, convert(source2), type, typeConverterBinding);
    }

    public Errors conversionTypeError(String stringValue, Object source2, TypeLiteral<?> type, TypeConverterBinding typeConverterBinding, Object converted) {
        return addMessage("Type mismatch converting '%s' (bound at %s) to %s%n using %s.%n Converter returned %s.", stringValue, convert(source2), type, typeConverterBinding, converted);
    }

    public Errors conversionError(String stringValue, Object source2, TypeLiteral<?> type, TypeConverterBinding typeConverterBinding, RuntimeException cause) {
        return errorInUserCode(cause, "Error converting '%s' (bound at %s) to %s%n using %s.%n Reason: %s", stringValue, convert(source2), type, typeConverterBinding, cause);
    }

    public Errors ambiguousTypeConversion(String stringValue, Object source2, TypeLiteral<?> type, TypeConverterBinding a, TypeConverterBinding b) {
        return addMessage("Multiple converters can convert '%s' (bound at %s) to %s:%n %s and%n %s.%n Please adjust your type converter configuration to avoid overlapping matches.", stringValue, convert(source2), type, a, b);
    }

    public Errors bindingToProvider() {
        return addMessage("Binding to Provider is not allowed.", new Object[0]);
    }

    public Errors subtypeNotProvided(Class<? extends Provider<?>> providerType, Class<?> type) {
        return addMessage("%s doesn't provide instances of %s.", providerType, type);
    }

    public Errors notASubtype(Class<?> implementationType, Class<?> type) {
        return addMessage("%s doesn't extend %s.", implementationType, type);
    }

    public Errors recursiveImplementationType() {
        return addMessage("@ImplementedBy points to the same class it annotates.", new Object[0]);
    }

    public Errors recursiveProviderType() {
        return addMessage("@ProvidedBy points to the same class it annotates.", new Object[0]);
    }

    public Errors missingRuntimeRetention(Class<? extends Annotation> annotation) {
        return addMessage(format("Please annotate %s with @Retention(RUNTIME).", annotation), new Object[0]);
    }

    public Errors missingScopeAnnotation(Class<? extends Annotation> annotation) {
        return addMessage(format("Please annotate %s with @ScopeAnnotation.", annotation), new Object[0]);
    }

    public Errors optionalConstructor(Constructor constructor) {
        return addMessage("%s is annotated @Inject(optional=true), but constructors cannot be optional.", constructor);
    }

    public Errors cannotBindToGuiceType(String simpleName) {
        return addMessage("Binding to core guice framework type is not allowed: %s.", simpleName);
    }

    public Errors scopeNotFound(Class<? extends Annotation> scopeAnnotation) {
        return addMessage("No scope is bound to %s.", scopeAnnotation);
    }

    public Errors scopeAnnotationOnAbstractType(Class<? extends Annotation> scopeAnnotation, Class<?> type, Object source2) {
        return addMessage("%s is annotated with %s, but scope annotations are not supported for abstract types.%n Bound at %s.", type, scopeAnnotation, convert(source2));
    }

    public Errors misplacedBindingAnnotation(Member member, Annotation bindingAnnotation) {
        return addMessage("%s is annotated with %s, but binding annotations should be applied to its parameters instead.", member, bindingAnnotation);
    }

    public Errors missingConstructor(Class<?> implementation) {
        return addMessage("Could not find a suitable constructor in %s. Classes must have either one (and only one) constructor annotated with @Inject or a zero-argument constructor that is not private.", implementation);
    }

    public Errors tooManyConstructors(Class<?> implementation) {
        return addMessage("%s has more than one constructor annotated with @Inject. Classes must have either one (and only one) constructor annotated with @Inject or a zero-argument constructor that is not private.", implementation);
    }

    public Errors constructorNotDefinedByType(Constructor<?> constructor, TypeLiteral<?> type) {
        return addMessage("%s does not define %s", type, constructor);
    }

    public Errors duplicateScopes(ScopeBinding existing, Class<? extends Annotation> annotationType, Scope scope) {
        return addMessage("Scope %s is already bound to %s at %s.%n Cannot bind %s.", existing.getScope(), annotationType, existing.getSource(), scope);
    }

    public Errors voidProviderMethod() {
        return addMessage("Provider methods must return a value. Do not return void.", new Object[0]);
    }

    public Errors missingConstantValues() {
        return addMessage("Missing constant value. Please call to(...).", new Object[0]);
    }

    public Errors cannotInjectInnerClass(Class<?> type) {
        return addMessage("Injecting into inner classes is not supported.  Please use a 'static' class (top-level or nested) instead of %s.", type);
    }

    public Errors duplicateBindingAnnotations(Member member, Class<? extends Annotation> a, Class<? extends Annotation> b) {
        return addMessage("%s has more than one annotation annotated with @BindingAnnotation: %s and %s", member, a, b);
    }

    public Errors staticInjectionOnInterface(Class<?> clazz) {
        return addMessage("%s is an interface, but interfaces have no static injection points.", clazz);
    }

    public Errors cannotInjectFinalField(Field field) {
        return addMessage("Injected field %s cannot be final.", field);
    }

    public Errors cannotInjectAbstractMethod(Method method) {
        return addMessage("Injected method %s cannot be abstract.", method);
    }

    public Errors cannotInjectNonVoidMethod(Method method) {
        return addMessage("Injected method %s must return void.", method);
    }

    public Errors cannotInjectMethodWithTypeParameters(Method method) {
        return addMessage("Injected method %s cannot declare type parameters of its own.", method);
    }

    public Errors duplicateScopeAnnotations(Class<? extends Annotation> a, Class<? extends Annotation> b) {
        return addMessage("More than one scope annotation was found: %s and %s.", a, b);
    }

    public Errors recursiveBinding() {
        return addMessage("Binding points to itself.", new Object[0]);
    }

    public Errors bindingAlreadySet(Key<?> key, Object source2) {
        return addMessage("A binding to %s was already configured at %s.", key, convert(source2));
    }

    public Errors jitBindingAlreadySet(Key<?> key) {
        return addMessage("A just-in-time binding to %s was already configured on a parent injector.", key);
    }

    public Errors childBindingAlreadySet(Key<?> key, Set<Object> sources) {
        Formatter allSources = new Formatter();
        for (Object source2 : sources) {
            if (source2 == null) {
                allSources.format("%n    (bound by a just-in-time binding)", new Object[0]);
            } else {
                allSources.format("%n    bound at %s", new Object[]{source2});
            }
        }
        return addMessage("Unable to create binding for %s. It was already configured on one or more child injectors or private modules%s%n  If it was in a PrivateModule, did you forget to expose the binding?", key, allSources.out());
    }

    public Errors errorCheckingDuplicateBinding(Key<?> key, Object source2, Throwable t) {
        return addMessage("A binding to %s was already configured at %s and an error was thrown while checking duplicate bindings.  Error: %s", key, convert(source2), t);
    }

    public Errors errorInjectingMethod(Throwable cause) {
        return errorInUserCode(cause, "Error injecting method, %s", cause);
    }

    public Errors errorNotifyingTypeListener(TypeListenerBinding listener, TypeLiteral<?> type, Throwable cause) {
        return errorInUserCode(cause, "Error notifying TypeListener %s (bound at %s) of %s.%n Reason: %s", listener.getListener(), convert(listener.getSource()), type, cause);
    }

    public Errors errorInjectingConstructor(Throwable cause) {
        return errorInUserCode(cause, "Error injecting constructor, %s", cause);
    }

    public Errors errorInProvider(RuntimeException runtimeException) {
        Throwable unwrapped = unwrap(runtimeException);
        return errorInUserCode(unwrapped, "Error in custom provider, %s", unwrapped);
    }

    public Errors errorInUserInjector(MembersInjector<?> listener, TypeLiteral<?> type, RuntimeException cause) {
        return errorInUserCode(cause, "Error injecting %s using %s.%n Reason: %s", type, listener, cause);
    }

    public Errors errorNotifyingInjectionListener(InjectionListener<?> listener, TypeLiteral<?> type, RuntimeException cause) {
        return errorInUserCode(cause, "Error notifying InjectionListener %s of %s.%n Reason: %s", listener, type, cause);
    }

    public Errors exposedButNotBound(Key<?> key) {
        return addMessage("Could not expose() %s, it must be explicitly bound.", key);
    }

    public Errors keyNotFullySpecified(TypeLiteral<?> typeLiteral) {
        return addMessage("%s cannot be used as a key; It is not fully specified.", typeLiteral);
    }

    public Errors errorEnhancingClass(Class<?> clazz, Throwable cause) {
        return errorInUserCode(cause, "Unable to method intercept: %s", clazz);
    }

    public static Collection<Message> getMessagesFromThrowable(Throwable throwable) {
        if (throwable instanceof ProvisionException) {
            return ((ProvisionException) throwable).getErrorMessages();
        }
        if (throwable instanceof ConfigurationException) {
            return ((ConfigurationException) throwable).getErrorMessages();
        }
        if (throwable instanceof CreationException) {
            return ((CreationException) throwable).getErrorMessages();
        }
        return ImmutableSet.m146of();
    }

    public Errors errorInUserCode(Throwable cause, String messageFormat, Object... arguments) {
        Collection<Message> messages = getMessagesFromThrowable(cause);
        if (!messages.isEmpty()) {
            return merge(messages);
        }
        return addMessage(cause, messageFormat, arguments);
    }

    private Throwable unwrap(RuntimeException runtimeException) {
        if (runtimeException instanceof UnhandledCheckedUserException) {
            return runtimeException.getCause();
        }
        return runtimeException;
    }

    public Errors cannotInjectRawProvider() {
        return addMessage("Cannot inject a Provider that has no type parameter", new Object[0]);
    }

    public Errors cannotInjectRawMembersInjector() {
        return addMessage("Cannot inject a MembersInjector that has no type parameter", new Object[0]);
    }

    public Errors cannotInjectTypeLiteralOf(Type unsupportedType) {
        return addMessage("Cannot inject a TypeLiteral of %s", unsupportedType);
    }

    public Errors cannotInjectRawTypeLiteral() {
        return addMessage("Cannot inject a TypeLiteral that has no type parameter", new Object[0]);
    }

    public Errors cannotSatisfyCircularDependency(Class<?> expectedType) {
        return addMessage("Tried proxying %s to support a circular dependency, but it is not an interface.", expectedType);
    }

    public Errors circularProxiesDisabled(Class<?> expectedType) {
        return addMessage("Tried proxying %s to support a circular dependency, but circular proxies are disabled.", expectedType);
    }

    public void throwCreationExceptionIfErrorsExist() {
        if (hasErrors()) {
            throw new CreationException(getMessages());
        }
    }

    public void throwConfigurationExceptionIfErrorsExist() {
        if (hasErrors()) {
            throw new ConfigurationException(getMessages());
        }
    }

    public void throwProvisionExceptionIfErrorsExist() {
        if (hasErrors()) {
            throw new ProvisionException((Iterable<Message>) getMessages());
        }
    }

    private Message merge(Message message) {
        List<Object> sources = Lists.newArrayList();
        sources.addAll(getSources());
        sources.addAll(message.getSources());
        return new Message(sources, message.getMessage(), message.getCause());
    }

    public Errors merge(Collection<Message> messages) {
        for (Message message : messages) {
            addMessage(merge(message));
        }
        return this;
    }

    public Errors merge(Errors moreErrors) {
        if (!(moreErrors.root == this.root || moreErrors.root.errors == null)) {
            merge((Collection<Message>) moreErrors.root.errors);
        }
        return this;
    }

    public List<Object> getSources() {
        List<Object> sources = Lists.newArrayList();
        for (Errors e = this; e != null; e = e.parent) {
            if (e.source != SourceProvider.UNKNOWN_SOURCE) {
                sources.add(0, e.source);
            }
        }
        return sources;
    }

    public void throwIfNewErrors(int expectedSize) throws ErrorsException {
        if (size() != expectedSize) {
            throw toException();
        }
    }

    public ErrorsException toException() {
        return new ErrorsException(this);
    }

    public boolean hasErrors() {
        return this.root.errors != null;
    }

    public Errors addMessage(String messageFormat, Object... arguments) {
        return addMessage(null, messageFormat, arguments);
    }

    private Errors addMessage(Throwable cause, String messageFormat, Object... arguments) {
        addMessage(new Message(getSources(), format(messageFormat, arguments), cause));
        return this;
    }

    public Errors addMessage(Message message) {
        if (this.root.errors == null) {
            this.root.errors = Lists.newArrayList();
        }
        this.root.errors.add(message);
        return this;
    }

    public static String format(String messageFormat, Object... arguments) {
        for (int i = 0; i < arguments.length; i++) {
            arguments[i] = convert(arguments[i]);
        }
        return String.format(messageFormat, arguments);
    }

    public List<Message> getMessages() {
        if (this.root.errors == null) {
            return ImmutableList.m108of();
        }
        return new Ordering<Message>() {
            public int compare(Message a, Message b) {
                return a.getSource().compareTo(b.getSource());
            }
        }.sortedCopy(this.root.errors);
    }

    public static String format(String heading, Collection<Message> errorMessages) {
        Formatter fmt = new Formatter().format(heading, new Object[0]).format(":%n%n", new Object[0]);
        int index = 1;
        boolean displayCauses = getOnlyCause(errorMessages) == null;
        for (Message errorMessage : errorMessages) {
            int index2 = index + 1;
            fmt.format("%s) %s%n", new Object[]{Integer.valueOf(index), errorMessage.getMessage()});
            List<Object> dependencies = errorMessage.getSources();
            for (int i = dependencies.size() - 1; i >= 0; i--) {
                formatSource(fmt, dependencies.get(i));
            }
            Throwable cause = errorMessage.getCause();
            if (displayCauses && cause != null) {
                StringWriter writer = new StringWriter();
                cause.printStackTrace(new PrintWriter(writer));
                fmt.format("Caused by: %s", new Object[]{writer.getBuffer()});
            }
            fmt.format("%n", new Object[0]);
            index = index2;
        }
        if (errorMessages.size() == 1) {
            fmt.format("1 error", new Object[0]);
        } else {
            fmt.format("%s errors", new Object[]{Integer.valueOf(errorMessages.size())});
        }
        return fmt.toString();
    }

    public <T> T checkForNull(T value, Object source2, Dependency<?> dependency) throws ErrorsException {
        if (value != null || dependency.isNullable()) {
            return value;
        }
        int parameterIndex = dependency.getParameterIndex();
        addMessage("null returned by binding at %s%n but %s%s is not @Nullable", source2, parameterIndex != -1 ? "parameter " + parameterIndex + " of " : "", dependency.getInjectionPoint().getMember());
        throw toException();
    }

    public static Throwable getOnlyCause(Collection<Message> messages) {
        Throwable onlyCause = null;
        for (Message message : messages) {
            Throwable messageCause = message.getCause();
            if (messageCause != null) {
                if (onlyCause != null) {
                    return null;
                }
                onlyCause = messageCause;
            }
        }
        return onlyCause;
    }

    public int size() {
        if (this.root.errors == null) {
            return 0;
        }
        return this.root.errors.size();
    }

    public static Object convert(Object o) {
        ElementSource source2 = null;
        if (o instanceof ElementSource) {
            source2 = (ElementSource) o;
            o = source2.getDeclaringSource();
        }
        return convert(o, source2);
    }

    public static Object convert(Object o, ElementSource source2) {
        for (Converter<?> converter : converters) {
            if (converter.appliesTo(o)) {
                return appendModules(converter.convert(o), source2);
            }
        }
        return appendModules(o, source2);
    }

    private static Object appendModules(Object source2, ElementSource elementSource) {
        String modules = moduleSourceString(elementSource);
        return modules.length() == 0 ? source2 : source2 + modules;
    }

    private static String moduleSourceString(ElementSource elementSource) {
        if (elementSource == null) {
            return "";
        }
        List<String> modules = Lists.newArrayList((Iterable<? extends E>) elementSource.getModuleClassNames());
        while (elementSource.getOriginalElementSource() != null) {
            elementSource = elementSource.getOriginalElementSource();
            modules.addAll(0, elementSource.getModuleClassNames());
        }
        if (modules.size() <= 1) {
            return "";
        }
        StringBuilder builder = new StringBuilder(" (via modules: ");
        for (int i = modules.size() - 1; i >= 0; i--) {
            builder.append((String) modules.get(i));
            if (i != 0) {
                builder.append(" -> ");
            }
        }
        builder.append(")");
        return builder.toString();
    }

    public static void formatSource(Formatter formatter, Object source2) {
        ElementSource elementSource = null;
        if (source2 instanceof ElementSource) {
            elementSource = (ElementSource) source2;
            source2 = elementSource.getDeclaringSource();
        }
        formatSource(formatter, source2, elementSource);
    }

    public static void formatSource(Formatter formatter, Object source2, ElementSource elementSource) {
        String modules = moduleSourceString(elementSource);
        if (source2 instanceof Dependency) {
            Dependency<?> dependency = (Dependency) source2;
            InjectionPoint injectionPoint = dependency.getInjectionPoint();
            if (injectionPoint != null) {
                formatInjectionPoint(formatter, dependency, injectionPoint, elementSource);
            } else {
                formatSource(formatter, dependency.getKey(), elementSource);
            }
        } else if (source2 instanceof InjectionPoint) {
            formatInjectionPoint(formatter, null, (InjectionPoint) source2, elementSource);
        } else if (source2 instanceof Class) {
            formatter.format("  at %s%s%n", new Object[]{StackTraceElements.forType((Class) source2), modules});
        } else if (source2 instanceof Member) {
            formatter.format("  at %s%s%n", new Object[]{StackTraceElements.forMember((Member) source2), modules});
        } else if (source2 instanceof TypeLiteral) {
            formatter.format("  while locating %s%s%n", new Object[]{source2, modules});
        } else if (source2 instanceof Key) {
            formatter.format("  while locating %s%n", new Object[]{convert((Key) source2, elementSource)});
        } else {
            formatter.format("  at %s%s%n", new Object[]{source2, modules});
        }
    }

    public static void formatInjectionPoint(Formatter formatter, Dependency<?> dependency, InjectionPoint injectionPoint, ElementSource elementSource) {
        Member member = injectionPoint.getMember();
        if (Classes.memberType(member) == Field.class) {
            formatter.format("  while locating %s%n", new Object[]{convert(((Dependency) injectionPoint.getDependencies().get(0)).getKey(), elementSource)});
            formatter.format("    for field at %s%n", new Object[]{StackTraceElements.forMember(member)});
        } else if (dependency != null) {
            formatter.format("  while locating %s%n", new Object[]{convert(dependency.getKey(), elementSource)});
            formatter.format("    for parameter %s at %s%n", new Object[]{Integer.valueOf(dependency.getParameterIndex()), StackTraceElements.forMember(member)});
        } else {
            formatSource(formatter, injectionPoint.getMember());
        }
    }
}
