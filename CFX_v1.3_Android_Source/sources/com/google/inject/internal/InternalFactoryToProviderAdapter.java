package com.google.inject.internal;

import com.google.inject.Provider;
import com.google.inject.spi.Dependency;
import org.roboguice.shaded.goole.common.base.Preconditions;

final class InternalFactoryToProviderAdapter<T> implements InternalFactory<T> {
    private final Provider<? extends T> provider;
    private final Object source;

    public InternalFactoryToProviderAdapter(Provider<? extends T> provider2, Object source2) {
        this.provider = (Provider) Preconditions.checkNotNull(provider2, "provider");
        this.source = Preconditions.checkNotNull(source2, "source");
    }

    public T get(Errors errors, InternalContext context, Dependency<?> dependency, boolean linked) throws ErrorsException {
        try {
            return errors.checkForNull(this.provider.get(), this.source, dependency);
        } catch (RuntimeException userException) {
            throw errors.withSource(this.source).errorInProvider(userException).toException();
        }
    }

    public String toString() {
        return this.provider.toString();
    }
}
