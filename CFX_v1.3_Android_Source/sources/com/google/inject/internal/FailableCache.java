package com.google.inject.internal;

import org.roboguice.shaded.goole.common.cache.CacheBuilder;
import org.roboguice.shaded.goole.common.cache.CacheLoader;
import org.roboguice.shaded.goole.common.cache.LoadingCache;

public abstract class FailableCache<K, V> {
    private final LoadingCache<K, Object> delegate = CacheBuilder.newBuilder().build(new CacheLoader<K, Object>() {
        public Object load(K key) {
            Errors errors = new Errors();
            V result = null;
            try {
                result = FailableCache.this.create(key, errors);
            } catch (ErrorsException e) {
                errors.merge(e.getErrors());
            }
            if (errors.hasErrors()) {
                return errors;
            }
            return result;
        }
    });

    /* access modifiers changed from: protected */
    public abstract V create(K k, Errors errors) throws ErrorsException;

    public V get(K key, Errors errors) throws ErrorsException {
        Object resultOrError = this.delegate.getUnchecked(key);
        if (!(resultOrError instanceof Errors)) {
            return resultOrError;
        }
        errors.merge((Errors) resultOrError);
        throw errors.toException();
    }

    /* access modifiers changed from: 0000 */
    public boolean remove(K key) {
        return this.delegate.asMap().remove(key) != null;
    }
}
