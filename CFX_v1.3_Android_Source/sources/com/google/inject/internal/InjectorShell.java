package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.Stage;
import com.google.inject.internal.util.SourceProvider;
import com.google.inject.internal.util.Stopwatch;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.Element;
import com.google.inject.spi.Elements;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.PrivateElements;
import java.util.List;
import java.util.logging.Logger;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.Lists;

final class InjectorShell {
    private final List<Element> elements;
    private final InjectorImpl injector;

    static class Builder {
        private final List<Element> elements = Lists.newArrayList();
        private final List<Module> modules = Lists.newArrayList();
        private InjectorOptions options;
        private InjectorImpl parent;
        private PrivateElementsImpl privateElements;
        private Stage stage;
        private State state;

        Builder() {
        }

        /* access modifiers changed from: 0000 */
        public Builder stage(Stage stage2) {
            this.stage = stage2;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public Builder parent(InjectorImpl parent2) {
            this.parent = parent2;
            this.state = new InheritingState(parent2.state);
            this.options = parent2.options;
            this.stage = this.options.stage;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public Builder privateElements(PrivateElements privateElements2) {
            this.privateElements = (PrivateElementsImpl) privateElements2;
            this.elements.addAll(privateElements2.getElements());
            return this;
        }

        /* access modifiers changed from: 0000 */
        public void addModules(Iterable<? extends Module> modules2) {
            for (Module module : modules2) {
                this.modules.add(module);
            }
        }

        /* access modifiers changed from: 0000 */
        public Stage getStage() {
            return this.options.stage;
        }

        /* access modifiers changed from: 0000 */
        public Object lock() {
            return getState().lock();
        }

        /* access modifiers changed from: 0000 */
        public List<InjectorShell> build(Initializer initializer, ProcessedBindingData bindingData, Stopwatch stopwatch, Errors errors) {
            Preconditions.checkState(this.stage != null, "Stage not initialized");
            Preconditions.checkState(this.privateElements == null || this.parent != null, "PrivateElements with no parent");
            Preconditions.checkState(this.state != null, "no state. Did you remember to lock() ?");
            if (this.parent == null) {
                this.modules.add(0, new RootModule());
            }
            this.elements.addAll(Elements.getElements(this.stage, (Iterable<? extends Module>) this.modules));
            InjectorOptionsProcessor optionsProcessor = new InjectorOptionsProcessor(errors);
            optionsProcessor.process(null, this.elements);
            this.options = optionsProcessor.getOptions(this.stage, this.options);
            InjectorImpl injector = new InjectorImpl(this.parent, this.state, this.options);
            if (this.privateElements != null) {
                this.privateElements.initInjector(injector);
            }
            if (this.parent == null) {
                TypeConverterBindingProcessor.prepareBuiltInConverters(injector);
            }
            stopwatch.resetAndLog("Module execution");
            new MessageProcessor(errors).process(injector, this.elements);
            new ListenerBindingProcessor(errors).process(injector, this.elements);
            injector.membersInjectorStore = new MembersInjectorStore(injector, injector.state.getTypeListenerBindings());
            injector.provisionListenerStore = new ProvisionListenerCallbackStore(injector.state.getProvisionListenerBindings());
            stopwatch.resetAndLog("TypeListeners & ProvisionListener creation");
            new ScopeBindingProcessor(errors).process(injector, this.elements);
            stopwatch.resetAndLog("Scopes creation");
            new TypeConverterBindingProcessor(errors).process(injector, this.elements);
            stopwatch.resetAndLog("Converters creation");
            InjectorShell.bindStage(injector, this.stage);
            InjectorShell.bindInjector(injector);
            InjectorShell.bindLogger(injector);
            new BindingProcessor(errors, initializer, bindingData).process(injector, this.elements);
            new UntargettedBindingProcessor(errors, bindingData).process(injector, this.elements);
            stopwatch.resetAndLog("Binding creation");
            List<InjectorShell> injectorShells = Lists.newArrayList();
            injectorShells.add(new InjectorShell(this, this.elements, injector));
            PrivateElementProcessor processor = new PrivateElementProcessor(errors);
            processor.process(injector, this.elements);
            for (Builder builder : processor.getInjectorShellBuilders()) {
                injectorShells.addAll(builder.build(initializer, bindingData, stopwatch, errors));
            }
            stopwatch.resetAndLog("Private environment creation");
            return injectorShells;
        }

        private State getState() {
            if (this.state == null) {
                this.state = new InheritingState(State.NONE);
            }
            return this.state;
        }
    }

    private static class InjectorFactory implements InternalFactory<Injector>, Provider<Injector> {
        private final Injector injector;

        private InjectorFactory(Injector injector2) {
            this.injector = injector2;
        }

        public Injector get(Errors errors, InternalContext context, Dependency<?> dependency, boolean linked) throws ErrorsException {
            return this.injector;
        }

        public Injector get() {
            return this.injector;
        }

        public String toString() {
            return "Provider<Injector>";
        }
    }

    private static class LoggerFactory implements InternalFactory<Logger>, Provider<Logger> {
        private LoggerFactory() {
        }

        public Logger get(Errors errors, InternalContext context, Dependency<?> dependency, boolean linked) {
            InjectionPoint injectionPoint = dependency.getInjectionPoint();
            return injectionPoint == null ? Logger.getAnonymousLogger() : Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
        }

        public Logger get() {
            return Logger.getAnonymousLogger();
        }

        public String toString() {
            return "Provider<Logger>";
        }
    }

    private static class RootModule implements Module {
        private RootModule() {
        }

        public void configure(Binder binder) {
            Binder binder2 = binder.withSource(SourceProvider.UNKNOWN_SOURCE);
            binder2.bindScope(Singleton.class, Scopes.SINGLETON);
            binder2.bindScope(javax.inject.Singleton.class, Scopes.SINGLETON);
        }
    }

    private InjectorShell(Builder builder, List<Element> elements2, InjectorImpl injector2) {
        this.elements = elements2;
        this.injector = injector2;
    }

    /* access modifiers changed from: 0000 */
    public InjectorImpl getInjector() {
        return this.injector;
    }

    /* access modifiers changed from: 0000 */
    public List<Element> getElements() {
        return this.elements;
    }

    /* access modifiers changed from: private */
    public static void bindInjector(InjectorImpl injector2) {
        Key<Injector> key = Key.get(Injector.class);
        InjectorFactory injectorFactory = new InjectorFactory(injector2);
        injector2.state.putBinding(key, new ProviderInstanceBindingImpl(injector2, key, SourceProvider.UNKNOWN_SOURCE, injectorFactory, Scoping.UNSCOPED, injectorFactory, ImmutableSet.m146of()));
    }

    /* access modifiers changed from: private */
    public static void bindLogger(InjectorImpl injector2) {
        Key<Logger> key = Key.get(Logger.class);
        LoggerFactory loggerFactory = new LoggerFactory();
        injector2.state.putBinding(key, new ProviderInstanceBindingImpl(injector2, key, SourceProvider.UNKNOWN_SOURCE, loggerFactory, Scoping.UNSCOPED, loggerFactory, ImmutableSet.m146of()));
    }

    /* access modifiers changed from: private */
    public static void bindStage(InjectorImpl injector2, Stage stage) {
        Key<Stage> key = Key.get(Stage.class);
        injector2.state.putBinding(key, new InstanceBindingImpl<>(injector2, key, SourceProvider.UNKNOWN_SOURCE, new ConstantFactory(Initializables.m68of(stage)), ImmutableSet.m146of(), stage));
    }
}
