package com.google.inject.internal.util;

import java.util.logging.Logger;

public final class Stopwatch {
    private static final Logger logger = Logger.getLogger(Stopwatch.class.getName());
    private long start = System.currentTimeMillis();

    public long reset() {
        long now = System.currentTimeMillis();
        long l = now - this.start;
        this.start = now;
        return l;
    }

    public void resetAndLog(String label) {
        logger.fine(label + ": " + reset() + "ms");
    }
}
