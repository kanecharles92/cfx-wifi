package com.google.inject.internal.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.util.Map;
import org.roboguice.shaded.goole.common.collect.MapMaker;

public class StackTraceElements {
    private static final InMemoryStackTraceElement[] EMPTY_INMEMORY_STACK_TRACE = new InMemoryStackTraceElement[0];
    private static final StackTraceElement[] EMPTY_STACK_TRACE = new StackTraceElement[0];
    private static final String UNKNOWN_SOURCE = "Unknown Source";
    private static Map<Object, Object> cache = new MapMaker().makeMap();

    public static class InMemoryStackTraceElement {
        private String declaringClass;
        private int lineNumber;
        private String methodName;

        InMemoryStackTraceElement(StackTraceElement ste) {
            this(ste.getClassName(), ste.getMethodName(), ste.getLineNumber());
        }

        InMemoryStackTraceElement(String declaringClass2, String methodName2, int lineNumber2) {
            this.declaringClass = declaringClass2;
            this.methodName = methodName2;
            this.lineNumber = lineNumber2;
        }

        /* access modifiers changed from: 0000 */
        public String getClassName() {
            return this.declaringClass;
        }

        /* access modifiers changed from: 0000 */
        public String getMethodName() {
            return this.methodName;
        }

        /* access modifiers changed from: 0000 */
        public int getLineNumber() {
            return this.lineNumber;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof InMemoryStackTraceElement)) {
                return false;
            }
            InMemoryStackTraceElement e = (InMemoryStackTraceElement) obj;
            if (!e.declaringClass.equals(this.declaringClass) || e.lineNumber != this.lineNumber || !this.methodName.equals(e.methodName)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (((this.declaringClass.hashCode() * 31) + this.methodName.hashCode()) * 31) + this.lineNumber;
        }

        public String toString() {
            return this.declaringClass + "." + this.methodName + "(" + this.lineNumber + ")";
        }
    }

    public static Object forMember(Member member) {
        if (member == null) {
            return SourceProvider.UNKNOWN_SOURCE;
        }
        return new StackTraceElement(member.getDeclaringClass().getName(), Classes.memberType(member) == Constructor.class ? "<init>" : member.getName(), null, -1);
    }

    public static Object forType(Class<?> implementation) {
        return new StackTraceElement(implementation.getName(), "class", null, -1);
    }

    public static void clearCache() {
        cache.clear();
    }

    public static InMemoryStackTraceElement[] convertToInMemoryStackTraceElement(StackTraceElement[] stackTraceElements) {
        if (stackTraceElements.length == 0) {
            return EMPTY_INMEMORY_STACK_TRACE;
        }
        InMemoryStackTraceElement[] inMemoryStackTraceElements = new InMemoryStackTraceElement[stackTraceElements.length];
        for (int i = 0; i < stackTraceElements.length; i++) {
            inMemoryStackTraceElements[i] = weakIntern(new InMemoryStackTraceElement(stackTraceElements[i]));
        }
        return inMemoryStackTraceElements;
    }

    public static StackTraceElement[] convertToStackTraceElement(InMemoryStackTraceElement[] inMemoryStackTraceElements) {
        if (inMemoryStackTraceElements.length == 0) {
            return EMPTY_STACK_TRACE;
        }
        StackTraceElement[] stackTraceElements = new StackTraceElement[inMemoryStackTraceElements.length];
        for (int i = 0; i < inMemoryStackTraceElements.length; i++) {
            stackTraceElements[i] = new StackTraceElement(inMemoryStackTraceElements[i].getClassName(), inMemoryStackTraceElements[i].getMethodName(), UNKNOWN_SOURCE, inMemoryStackTraceElements[i].getLineNumber());
        }
        return stackTraceElements;
    }

    private static InMemoryStackTraceElement weakIntern(InMemoryStackTraceElement inMemoryStackTraceElement) {
        InMemoryStackTraceElement cached = (InMemoryStackTraceElement) cache.get(inMemoryStackTraceElement);
        if (cached != null) {
            return cached;
        }
        InMemoryStackTraceElement inMemoryStackTraceElement2 = new InMemoryStackTraceElement(weakIntern(inMemoryStackTraceElement.getClassName()), weakIntern(inMemoryStackTraceElement.getMethodName()), inMemoryStackTraceElement.getLineNumber());
        cache.put(inMemoryStackTraceElement2, inMemoryStackTraceElement2);
        return inMemoryStackTraceElement2;
    }

    private static String weakIntern(String s) {
        String cached = (String) cache.get(s);
        if (cached != null) {
            return cached;
        }
        cache.put(s, s);
        return s;
    }
}
