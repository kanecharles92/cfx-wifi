package com.google.inject.internal.util;

import java.util.List;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.ImmutableSet.Builder;
import org.roboguice.shaded.goole.common.collect.Lists;

public final class SourceProvider {
    public static final SourceProvider DEFAULT_INSTANCE = new SourceProvider(ImmutableSet.m147of(SourceProvider.class.getName()));
    public static final Object UNKNOWN_SOURCE = "[unknown source]";
    private final ImmutableSet<String> classNamesToSkip;
    private final SourceProvider parent;

    private SourceProvider(Iterable<String> classesToSkip) {
        this(null, classesToSkip);
    }

    private SourceProvider(SourceProvider parent2, Iterable<String> classesToSkip) {
        this.parent = parent2;
        Builder<String> classNamesToSkipBuilder = ImmutableSet.builder();
        for (String classToSkip : classesToSkip) {
            if (parent2 == null || !parent2.shouldBeSkipped(classToSkip)) {
                classNamesToSkipBuilder.add((Object) classToSkip);
            }
        }
        this.classNamesToSkip = classNamesToSkipBuilder.build();
    }

    public SourceProvider plusSkippedClasses(Class... moreClassesToSkip) {
        return new SourceProvider(this, asStrings(moreClassesToSkip));
    }

    private boolean shouldBeSkipped(String className) {
        return (this.parent != null && this.parent.shouldBeSkipped(className)) || this.classNamesToSkip.contains(className);
    }

    private static List<String> asStrings(Class... classes) {
        List<String> strings = Lists.newArrayList();
        for (Class c : classes) {
            strings.add(c.getName());
        }
        return strings;
    }

    public StackTraceElement get(StackTraceElement[] stackTraceElements) {
        StackTraceElement[] arr$;
        Preconditions.checkNotNull(stackTraceElements, "The stack trace elements cannot be null.");
        for (StackTraceElement element : stackTraceElements) {
            if (!shouldBeSkipped(element.getClassName())) {
                return element;
            }
        }
        throw new AssertionError();
    }

    public Object getFromClassNames(List<String> moduleClassNames) {
        Preconditions.checkNotNull(moduleClassNames, "The list of module class names cannot be null.");
        for (String moduleClassName : moduleClassNames) {
            if (!shouldBeSkipped(moduleClassName)) {
                return new StackTraceElement(moduleClassName, "configure", null, -1);
            }
        }
        return UNKNOWN_SOURCE;
    }
}
