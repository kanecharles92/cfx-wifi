package com.google.inject.internal;

import com.google.inject.spi.InjectionPoint;

final class ConstructorInjectorStore {
    private final FailableCache<InjectionPoint, ConstructorInjector<?>> cache = new FailableCache<InjectionPoint, ConstructorInjector<?>>() {
        /* access modifiers changed from: protected */
        public ConstructorInjector<?> create(InjectionPoint constructorInjector, Errors errors) throws ErrorsException {
            return ConstructorInjectorStore.this.createConstructor(constructorInjector, errors);
        }
    };
    private final InjectorImpl injector;

    ConstructorInjectorStore(InjectorImpl injector2) {
        this.injector = injector2;
    }

    public ConstructorInjector<?> get(InjectionPoint constructorInjector, Errors errors) throws ErrorsException {
        return (ConstructorInjector) this.cache.get(constructorInjector, errors);
    }

    /* access modifiers changed from: 0000 */
    public boolean remove(InjectionPoint ip) {
        return this.cache.remove(ip);
    }

    /* access modifiers changed from: private */
    public <T> ConstructorInjector<T> createConstructor(InjectionPoint injectionPoint, Errors errors) throws ErrorsException {
        int numErrorsBefore = errors.size();
        SingleParameterInjector<?>[] constructorParameterInjectors = this.injector.getParametersInjectors(injectionPoint.getDependencies(), errors);
        MembersInjectorImpl<T> membersInjector = this.injector.membersInjectorStore.get(injectionPoint.getDeclaringType(), errors);
        ConstructionProxyFactory<T> factory = new DefaultConstructionProxyFactory<>(injectionPoint);
        errors.throwIfNewErrors(numErrorsBefore);
        return new ConstructorInjector<>(membersInjector.getInjectionPoints(), factory.create(), constructorParameterInjectors, membersInjector);
    }
}
