package com.google.inject.internal;

import com.google.inject.Key;
import com.google.inject.spi.Dependency;
import org.roboguice.shaded.goole.common.base.Objects;

final class FactoryProxy<T> implements InternalFactory<T>, CreationListener {
    private final InjectorImpl injector;
    private final Key<T> key;
    private final Object source;
    private InternalFactory<? extends T> targetFactory;
    private final Key<? extends T> targetKey;

    FactoryProxy(InjectorImpl injector2, Key<T> key2, Key<? extends T> targetKey2, Object source2) {
        this.injector = injector2;
        this.key = key2;
        this.targetKey = targetKey2;
        this.source = source2;
    }

    public void notify(Errors errors) {
        try {
            this.targetFactory = this.injector.getInternalFactory(this.targetKey, errors.withSource(this.source), JitLimitation.NEW_OR_EXISTING_JIT);
        } catch (ErrorsException e) {
            errors.merge(e.getErrors());
        }
    }

    public T get(Errors errors, InternalContext context, Dependency<?> dependency, boolean linked) throws ErrorsException {
        context.pushState(this.targetKey, this.source);
        try {
            return this.targetFactory.get(errors.withSource(this.targetKey), context, dependency, true);
        } finally {
            context.popState();
        }
    }

    public String toString() {
        return Objects.toStringHelper(FactoryProxy.class).add("key", (Object) this.key).add("provider", (Object) this.targetFactory).toString();
    }
}
