package com.google.inject.internal;

import com.google.inject.ConfigurationException;
import com.google.inject.Stage;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.InjectionRequest;
import com.google.inject.spi.StaticInjectionRequest;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.Lists;

final class InjectionRequestProcessor extends AbstractProcessor {
    private final Initializer initializer;
    private final List<StaticInjection> staticInjections = Lists.newArrayList();

    private class StaticInjection {
        final InjectorImpl injector;
        ImmutableList<SingleMemberInjector> memberInjectors;
        final StaticInjectionRequest request;
        final Object source;

        public StaticInjection(InjectorImpl injector2, StaticInjectionRequest request2) {
            this.injector = injector2;
            this.source = request2.getSource();
            this.request = request2;
        }

        /* access modifiers changed from: 0000 */
        public void validate() {
            Set<InjectionPoint> injectionPoints;
            Errors errorsForMember = InjectionRequestProcessor.this.errors.withSource(this.source);
            try {
                injectionPoints = this.request.getInjectionPoints();
            } catch (ConfigurationException e) {
                errorsForMember.merge(e.getErrorMessages());
                injectionPoints = (Set) e.getPartialValue();
            }
            if (injectionPoints != null) {
                this.memberInjectors = this.injector.membersInjectorStore.getInjectors(injectionPoints, errorsForMember);
            } else {
                this.memberInjectors = ImmutableList.m108of();
            }
            InjectionRequestProcessor.this.errors.merge(errorsForMember);
        }

        /* access modifiers changed from: 0000 */
        public void injectMembers() {
            try {
                this.injector.callInContext(new ContextualCallable<Void>() {
                    public Void call(InternalContext context) {
                        Iterator i$ = StaticInjection.this.memberInjectors.iterator();
                        while (i$.hasNext()) {
                            SingleMemberInjector memberInjector = (SingleMemberInjector) i$.next();
                            if (StaticInjection.this.injector.options.stage != Stage.TOOL || memberInjector.getInjectionPoint().isToolable()) {
                                memberInjector.inject(InjectionRequestProcessor.this.errors, context, null);
                            }
                        }
                        return null;
                    }
                });
            } catch (ErrorsException e) {
                throw new AssertionError();
            }
        }
    }

    InjectionRequestProcessor(Errors errors, Initializer initializer2) {
        super(errors);
        this.initializer = initializer2;
    }

    public Boolean visit(StaticInjectionRequest request) {
        this.staticInjections.add(new StaticInjection(this.injector, request));
        return Boolean.valueOf(true);
    }

    public Boolean visit(InjectionRequest<?> request) {
        Set<InjectionPoint> injectionPoints;
        try {
            injectionPoints = request.getInjectionPoints();
        } catch (ConfigurationException e) {
            this.errors.merge(e.getErrorMessages());
            injectionPoints = (Set) e.getPartialValue();
        }
        this.initializer.requestInjection(this.injector, request.getInstance(), null, request.getSource(), injectionPoints);
        return Boolean.valueOf(true);
    }

    /* access modifiers changed from: 0000 */
    public void validate() {
        for (StaticInjection staticInjection : this.staticInjections) {
            staticInjection.validate();
        }
    }

    /* access modifiers changed from: 0000 */
    public void injectMembers() {
        for (StaticInjection staticInjection : this.staticInjections) {
            staticInjection.injectMembers();
        }
    }
}
