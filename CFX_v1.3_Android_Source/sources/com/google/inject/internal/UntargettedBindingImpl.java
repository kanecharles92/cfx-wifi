package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Key;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.UntargettedBinding;
import org.roboguice.shaded.goole.common.base.Objects;

final class UntargettedBindingImpl<T> extends BindingImpl<T> implements UntargettedBinding<T> {
    UntargettedBindingImpl(InjectorImpl injector, Key<T> key, Object source) {
        super(injector, key, source, new InternalFactory<T>() {
            public T get(Errors errors, InternalContext context, Dependency<?> dependency, boolean linked) {
                throw new AssertionError();
            }
        }, Scoping.UNSCOPED);
    }

    public UntargettedBindingImpl(Object source, Key<T> key, Scoping scoping) {
        super(source, key, scoping);
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        return visitor.visit((UntargettedBinding<? extends T>) this);
    }

    public BindingImpl<T> withScoping(Scoping scoping) {
        return new UntargettedBindingImpl(getSource(), getKey(), scoping);
    }

    public BindingImpl<T> withKey(Key<T> key) {
        return new UntargettedBindingImpl(getSource(), key, getScoping());
    }

    public void applyTo(Binder binder) {
        getScoping().applyTo(binder.withSource(getSource()).bind(getKey()));
    }

    public String toString() {
        return Objects.toStringHelper(UntargettedBinding.class).add("key", (Object) getKey()).add("source", getSource()).toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof UntargettedBindingImpl)) {
            return false;
        }
        UntargettedBindingImpl<?> o = (UntargettedBindingImpl) obj;
        if (!getKey().equals(o.getKey()) || !getScoping().equals(o.getScoping())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hashCode(getKey(), getScoping());
    }
}
