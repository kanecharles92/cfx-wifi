package com.google.inject.internal;

import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.ProvisionListenerStackCallback.ProvisionCallback;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.InjectionPoint;
import java.util.Iterator;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.ImmutableSet.Builder;

final class MembersInjectorImpl<T> implements MembersInjector<T> {
    private final ImmutableSet<InjectionListener<? super T>> injectionListeners;
    private final InjectorImpl injector;
    private final ImmutableList<SingleMemberInjector> memberInjectors;
    private final TypeLiteral<T> typeLiteral;
    private final ImmutableSet<MembersInjector<? super T>> userMembersInjectors;

    MembersInjectorImpl(InjectorImpl injector2, TypeLiteral<T> typeLiteral2, EncounterImpl<T> encounter, ImmutableList<SingleMemberInjector> memberInjectors2) {
        this.injector = injector2;
        this.typeLiteral = typeLiteral2;
        this.memberInjectors = memberInjectors2;
        this.userMembersInjectors = encounter.getMembersInjectors();
        this.injectionListeners = encounter.getInjectionListeners();
    }

    public ImmutableList<SingleMemberInjector> getMemberInjectors() {
        return this.memberInjectors;
    }

    public void injectMembers(T instance) {
        Errors errors = new Errors(this.typeLiteral);
        try {
            injectAndNotify(instance, errors, null, null, this.typeLiteral, false);
        } catch (ErrorsException e) {
            errors.merge(e.getErrors());
        }
        errors.throwProvisionExceptionIfErrorsExist();
    }

    /* access modifiers changed from: 0000 */
    public void injectAndNotify(T instance, Errors errors, Key<T> key, ProvisionListenerStackCallback<T> provisionCallback, Object source, boolean toolableOnly) throws ErrorsException {
        if (instance != null) {
            final Key<T> key2 = key;
            final Object obj = source;
            final ProvisionListenerStackCallback<T> provisionListenerStackCallback = provisionCallback;
            final Errors errors2 = errors;
            final T t = instance;
            final boolean z = toolableOnly;
            this.injector.callInContext(new ContextualCallable<Void>() {
                /* JADX INFO: finally extract failed */
                public Void call(final InternalContext context) throws ErrorsException {
                    context.pushState(key2, obj);
                    try {
                        if (provisionListenerStackCallback == null || !provisionListenerStackCallback.hasListeners()) {
                            MembersInjectorImpl.this.injectMembers(t, errors2, context, z);
                        } else {
                            provisionListenerStackCallback.provision(errors2, context, new ProvisionCallback<T>() {
                                public T call() {
                                    MembersInjectorImpl.this.injectMembers(t, errors2, context, z);
                                    return t;
                                }
                            });
                        }
                        context.popState();
                        return null;
                    } catch (Throwable th) {
                        context.popState();
                        throw th;
                    }
                }
            });
            if (!toolableOnly) {
                notifyListeners(instance, errors);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void notifyListeners(T instance, Errors errors) throws ErrorsException {
        int numErrorsBefore = errors.size();
        Iterator i$ = this.injectionListeners.iterator();
        while (i$.hasNext()) {
            InjectionListener<? super T> injectionListener = (InjectionListener) i$.next();
            try {
                injectionListener.afterInjection(instance);
            } catch (RuntimeException e) {
                errors.errorNotifyingInjectionListener(injectionListener, this.typeLiteral, e);
            }
        }
        errors.throwIfNewErrors(numErrorsBefore);
    }

    /* access modifiers changed from: 0000 */
    public void injectMembers(T t, Errors errors, InternalContext context, boolean toolableOnly) {
        int size = this.memberInjectors.size();
        for (int i = 0; i < size; i++) {
            SingleMemberInjector injector2 = (SingleMemberInjector) this.memberInjectors.get(i);
            if (!toolableOnly || injector2.getInjectionPoint().isToolable()) {
                injector2.inject(errors, context, t);
            }
        }
        if (!toolableOnly) {
            Iterator i$ = this.userMembersInjectors.iterator();
            while (i$.hasNext()) {
                MembersInjector<? super T> userMembersInjector = (MembersInjector) i$.next();
                try {
                    userMembersInjector.injectMembers(t);
                } catch (RuntimeException e) {
                    errors.errorInUserInjector(userMembersInjector, this.typeLiteral, e);
                }
            }
        }
    }

    public String toString() {
        return "MembersInjector<" + this.typeLiteral + ">";
    }

    public ImmutableSet<InjectionPoint> getInjectionPoints() {
        Builder<InjectionPoint> builder = ImmutableSet.builder();
        Iterator i$ = this.memberInjectors.iterator();
        while (i$.hasNext()) {
            builder.add((Object) ((SingleMemberInjector) i$.next()).getInjectionPoint());
        }
        return builder.build();
    }
}
