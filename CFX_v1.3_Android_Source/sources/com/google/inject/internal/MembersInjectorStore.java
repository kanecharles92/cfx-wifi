package com.google.inject.internal;

import com.google.inject.ConfigurationException;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.TypeListener;
import com.google.inject.spi.TypeListenerBinding;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Sets;

final class MembersInjectorStore {
    private final FailableCache<TypeLiteral<?>, MembersInjectorImpl<?>> cache = new FailableCache<TypeLiteral<?>, MembersInjectorImpl<?>>() {
        /* access modifiers changed from: protected */
        public MembersInjectorImpl<?> create(TypeLiteral<?> type, Errors errors) throws ErrorsException {
            return MembersInjectorStore.this.createWithListeners(type, errors);
        }
    };
    private final InjectorImpl injector;
    private final ImmutableList<TypeListenerBinding> typeListenerBindings;

    MembersInjectorStore(InjectorImpl injector2, List<TypeListenerBinding> typeListenerBindings2) {
        this.injector = injector2;
        this.typeListenerBindings = ImmutableList.copyOf((Collection<? extends E>) typeListenerBindings2);
    }

    public boolean hasTypeListeners() {
        return !this.typeListenerBindings.isEmpty();
    }

    public <T> MembersInjectorImpl<T> get(TypeLiteral<T> key, Errors errors) throws ErrorsException {
        return (MembersInjectorImpl) this.cache.get(key, errors);
    }

    /* access modifiers changed from: 0000 */
    public boolean remove(TypeLiteral<?> type) {
        return this.cache.remove(type);
    }

    /* access modifiers changed from: private */
    public <T> MembersInjectorImpl<T> createWithListeners(TypeLiteral<T> type, Errors errors) throws ErrorsException {
        Set<InjectionPoint> injectionPoints;
        int numErrorsBefore = errors.size();
        try {
            injectionPoints = InjectionPoint.forInstanceMethodsAndFields(type);
        } catch (ConfigurationException e) {
            errors.merge(e.getErrorMessages());
            injectionPoints = (Set) e.getPartialValue();
        }
        ImmutableList<SingleMemberInjector> injectors = getInjectors(injectionPoints, errors);
        errors.throwIfNewErrors(numErrorsBefore);
        EncounterImpl<T> encounter = new EncounterImpl<>(errors, this.injector.lookups);
        Set<TypeListener> alreadySeenListeners = Sets.newHashSet();
        Iterator i$ = this.typeListenerBindings.iterator();
        while (i$.hasNext()) {
            TypeListenerBinding binding = (TypeListenerBinding) i$.next();
            TypeListener typeListener = binding.getListener();
            if (!alreadySeenListeners.contains(typeListener) && binding.getTypeMatcher().matches(type)) {
                alreadySeenListeners.add(typeListener);
                try {
                    typeListener.hear(type, encounter);
                } catch (RuntimeException e2) {
                    errors.errorNotifyingTypeListener(binding, type, e2);
                }
            }
        }
        encounter.invalidate();
        errors.throwIfNewErrors(numErrorsBefore);
        return new MembersInjectorImpl<>(this.injector, type, encounter, injectors);
    }

    /* access modifiers changed from: 0000 */
    public ImmutableList<SingleMemberInjector> getInjectors(Set<InjectionPoint> injectionPoints, Errors errors) {
        List<SingleMemberInjector> injectors = Lists.newArrayList();
        for (InjectionPoint injectionPoint : injectionPoints) {
            try {
                Errors errorsForMember = injectionPoint.isOptional() ? new Errors(injectionPoint) : errors.withSource(injectionPoint);
                injectors.add(injectionPoint.getMember() instanceof Field ? new SingleFieldInjector(this.injector, injectionPoint, errorsForMember) : new SingleMethodInjector(this.injector, injectionPoint, errorsForMember));
            } catch (ErrorsException e) {
            }
        }
        return ImmutableList.copyOf((Collection<? extends E>) injectors);
    }
}
