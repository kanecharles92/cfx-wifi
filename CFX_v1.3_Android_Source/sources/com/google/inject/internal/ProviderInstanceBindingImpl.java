package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Key;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.HasDependencies;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.ProviderInstanceBinding;
import com.google.inject.spi.ProviderWithExtensionVisitor;
import com.google.inject.util.Providers;
import java.util.Collection;
import java.util.Set;
import javax.inject.Provider;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;

final class ProviderInstanceBindingImpl<T> extends BindingImpl<T> implements ProviderInstanceBinding<T> {
    final ImmutableSet<InjectionPoint> injectionPoints;
    final Provider<? extends T> providerInstance;

    public ProviderInstanceBindingImpl(InjectorImpl injector, Key<T> key, Object source, InternalFactory<? extends T> internalFactory, Scoping scoping, Provider<? extends T> providerInstance2, Set<InjectionPoint> injectionPoints2) {
        super(injector, key, source, internalFactory, scoping);
        this.providerInstance = providerInstance2;
        this.injectionPoints = ImmutableSet.copyOf((Collection<? extends E>) injectionPoints2);
    }

    public ProviderInstanceBindingImpl(Object source, Key<T> key, Scoping scoping, Set<InjectionPoint> injectionPoints2, Provider<? extends T> providerInstance2) {
        super(source, key, scoping);
        this.injectionPoints = ImmutableSet.copyOf((Collection<? extends E>) injectionPoints2);
        this.providerInstance = providerInstance2;
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        if (this.providerInstance instanceof ProviderWithExtensionVisitor) {
            return ((ProviderWithExtensionVisitor) this.providerInstance).acceptExtensionVisitor(visitor, this);
        }
        return visitor.visit((ProviderInstanceBinding<? extends T>) this);
    }

    public com.google.inject.Provider<? extends T> getProviderInstance() {
        return Providers.guicify(this.providerInstance);
    }

    public Provider<? extends T> getUserSuppliedProvider() {
        return this.providerInstance;
    }

    public Set<InjectionPoint> getInjectionPoints() {
        return this.injectionPoints;
    }

    public Set<Dependency<?>> getDependencies() {
        return this.providerInstance instanceof HasDependencies ? ImmutableSet.copyOf((Collection<? extends E>) ((HasDependencies) this.providerInstance).getDependencies()) : Dependency.forInjectionPoints(this.injectionPoints);
    }

    public BindingImpl<T> withScoping(Scoping scoping) {
        return new ProviderInstanceBindingImpl(getSource(), getKey(), scoping, this.injectionPoints, this.providerInstance);
    }

    public BindingImpl<T> withKey(Key<T> key) {
        return new ProviderInstanceBindingImpl(getSource(), key, getScoping(), this.injectionPoints, this.providerInstance);
    }

    public void applyTo(Binder binder) {
        getScoping().applyTo(binder.withSource(getSource()).bind(getKey()).toProvider(getUserSuppliedProvider()));
    }

    public String toString() {
        return Objects.toStringHelper(ProviderInstanceBinding.class).add("key", (Object) getKey()).add("source", getSource()).add("scope", (Object) getScoping()).add("provider", (Object) this.providerInstance).toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ProviderInstanceBindingImpl)) {
            return false;
        }
        ProviderInstanceBindingImpl<?> o = (ProviderInstanceBindingImpl) obj;
        if (!getKey().equals(o.getKey()) || !getScoping().equals(o.getScoping()) || !Objects.equal(this.providerInstance, o.providerInstance)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hashCode(getKey(), getScoping());
    }
}
