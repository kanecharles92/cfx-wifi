package com.google.inject.internal;

import com.google.inject.spi.DefaultElementVisitor;
import com.google.inject.spi.Element;
import java.util.Iterator;
import java.util.List;

abstract class AbstractProcessor extends DefaultElementVisitor<Boolean> {
    protected Errors errors;
    protected InjectorImpl injector;

    protected AbstractProcessor(Errors errors2) {
        this.errors = errors2;
    }

    public void process(Iterable<InjectorShell> isolatedInjectorBuilders) {
        for (InjectorShell injectorShell : isolatedInjectorBuilders) {
            process(injectorShell.getInjector(), injectorShell.getElements());
        }
    }

    public void process(InjectorImpl injector2, List<Element> elements) {
        Errors errorsAnyElement = this.errors;
        this.injector = injector2;
        try {
            Iterator<Element> i = elements.iterator();
            while (i.hasNext()) {
                Element element = (Element) i.next();
                this.errors = errorsAnyElement.withSource(element.getSource());
                if (((Boolean) element.acceptVisitor(this)).booleanValue()) {
                    i.remove();
                }
            }
        } finally {
            this.errors = errorsAnyElement;
            this.injector = null;
        }
    }

    /* access modifiers changed from: protected */
    public Boolean visitOther(Element element) {
        return Boolean.valueOf(false);
    }
}
