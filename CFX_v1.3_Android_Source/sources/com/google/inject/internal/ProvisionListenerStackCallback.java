package com.google.inject.internal;

import com.google.inject.Binding;
import com.google.inject.ProvisionException;
import com.google.inject.spi.DependencyAndSource;
import com.google.inject.spi.Message;
import com.google.inject.spi.ProvisionListener;
import com.google.inject.spi.ProvisionListener.ProvisionInvocation;
import java.util.List;
import java.util.Set;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.Sets;

final class ProvisionListenerStackCallback<T> {
    private static final ProvisionListenerStackCallback<?> EMPTY_CALLBACK = new ProvisionListenerStackCallback<>(null, ImmutableList.m108of());
    private static final ProvisionListener[] EMPTY_LISTENER = new ProvisionListener[0];
    /* access modifiers changed from: private */
    public final Binding<T> binding;
    /* access modifiers changed from: private */
    public final ProvisionListener[] listeners;

    private class Provision extends ProvisionInvocation<T> {
        final ProvisionCallback<T> callable;
        final InternalContext context;
        ProvisionListener erredListener;
        final Errors errors;
        ErrorsException exceptionDuringProvision;
        int index = -1;
        T result;

        public Provision(Errors errors2, InternalContext context2, ProvisionCallback<T> callable2) {
            this.callable = callable2;
            this.context = context2;
            this.errors = errors2;
        }

        public T provision() {
            this.index++;
            if (this.index == ProvisionListenerStackCallback.this.listeners.length) {
                try {
                    this.result = this.callable.call();
                } catch (ErrorsException ee) {
                    this.exceptionDuringProvision = ee;
                    throw new ProvisionException((Iterable<Message>) this.errors.merge(ee.getErrors()).getMessages());
                }
            } else if (this.index < ProvisionListenerStackCallback.this.listeners.length) {
                int currentIdx = this.index;
                try {
                    ProvisionListenerStackCallback.this.listeners[this.index].onProvision(this);
                    if (currentIdx == this.index) {
                        provision();
                    }
                } catch (RuntimeException re) {
                    this.erredListener = ProvisionListenerStackCallback.this.listeners[currentIdx];
                    throw re;
                }
            } else {
                throw new IllegalStateException("Already provisioned in this listener.");
            }
            return this.result;
        }

        public Binding<T> getBinding() {
            return ProvisionListenerStackCallback.this.binding;
        }

        public List<DependencyAndSource> getDependencyChain() {
            return this.context.getDependencyChain();
        }
    }

    public interface ProvisionCallback<T> {
        T call() throws ErrorsException;
    }

    public static <T> ProvisionListenerStackCallback<T> emptyListener() {
        return EMPTY_CALLBACK;
    }

    public ProvisionListenerStackCallback(Binding<T> binding2, List<ProvisionListener> listeners2) {
        this.binding = binding2;
        if (listeners2.isEmpty()) {
            this.listeners = EMPTY_LISTENER;
            return;
        }
        Set<ProvisionListener> deDuplicated = Sets.newLinkedHashSet(listeners2);
        this.listeners = (ProvisionListener[]) deDuplicated.toArray(new ProvisionListener[deDuplicated.size()]);
    }

    public boolean hasListeners() {
        return this.listeners.length > 0;
    }

    public T provision(Errors errors, InternalContext context, ProvisionCallback<T> callable) throws ErrorsException {
        Provision provision = new Provision<>(errors, context, callable);
        RuntimeException caught = null;
        try {
            provision.provision();
        } catch (RuntimeException t) {
            caught = t;
        }
        if (provision.exceptionDuringProvision != null) {
            throw provision.exceptionDuringProvision;
        } else if (caught == null) {
            return provision.result;
        } else {
            throw errors.errorInUserCode(caught, "Error notifying ProvisionListener %s of %s.%n Reason: %s", provision.erredListener != null ? provision.erredListener.getClass() : "(unknown)", this.binding.getKey(), caught).toException();
        }
    }
}
