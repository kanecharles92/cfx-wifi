package com.google.inject.internal;

import com.google.inject.internal.ProvisionListenerStackCallback.ProvisionCallback;
import com.google.inject.spi.InjectionPoint;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Set;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;

final class ConstructorInjector<T> {
    private final ConstructionProxy<T> constructionProxy;
    private final ImmutableSet<InjectionPoint> injectableMembers;
    private final MembersInjectorImpl<T> membersInjector;
    private final SingleParameterInjector<?>[] parameterInjectors;

    ConstructorInjector(Set<InjectionPoint> injectableMembers2, ConstructionProxy<T> constructionProxy2, SingleParameterInjector<?>[] parameterInjectors2, MembersInjectorImpl<T> membersInjector2) {
        this.injectableMembers = ImmutableSet.copyOf((Collection<? extends E>) injectableMembers2);
        this.constructionProxy = constructionProxy2;
        this.parameterInjectors = parameterInjectors2;
        this.membersInjector = membersInjector2;
    }

    public ImmutableSet<InjectionPoint> getInjectableMembers() {
        return this.injectableMembers;
    }

    /* access modifiers changed from: 0000 */
    public ConstructionProxy<T> getConstructionProxy() {
        return this.constructionProxy;
    }

    /* access modifiers changed from: 0000 */
    public Object construct(final Errors errors, final InternalContext context, Class<?> expectedType, boolean allowProxy, ProvisionListenerStackCallback<T> provisionCallback) throws ErrorsException {
        final ConstructionContext<T> constructionContext = context.getConstructionContext(this);
        if (!constructionContext.isConstructing()) {
            T t = constructionContext.getCurrentReference();
            if (t != null) {
                return t;
            }
            constructionContext.startConstruction();
            try {
                if (!provisionCallback.hasListeners()) {
                    return provision(errors, context, constructionContext);
                }
                T t2 = provisionCallback.provision(errors, context, new ProvisionCallback<T>() {
                    public T call() throws ErrorsException {
                        return ConstructorInjector.this.provision(errors, context, constructionContext);
                    }
                });
                constructionContext.finishConstruction();
                return t2;
            } finally {
                constructionContext.finishConstruction();
            }
        } else if (allowProxy) {
            return constructionContext.createProxy(errors, expectedType);
        } else {
            throw errors.circularProxiesDisabled(expectedType).toException();
        }
    }

    /* access modifiers changed from: private */
    public T provision(Errors errors, InternalContext context, ConstructionContext<T> constructionContext) throws ErrorsException {
        Throwable cause;
        try {
            T t = this.constructionProxy.newInstance(SingleParameterInjector.getAll(errors, context, this.parameterInjectors));
            constructionContext.setProxyDelegates(t);
            constructionContext.finishConstruction();
            constructionContext.setCurrentReference(t);
            this.membersInjector.injectMembers(t, errors, context, false);
            this.membersInjector.notifyListeners(t, errors);
            constructionContext.removeCurrentReference();
            return t;
        } catch (InvocationTargetException userException) {
            try {
                if (userException.getCause() != null) {
                    cause = userException.getCause();
                } else {
                    cause = userException;
                }
                throw errors.withSource(this.constructionProxy.getInjectionPoint()).errorInjectingConstructor(cause).toException();
            } catch (Throwable th) {
                constructionContext.removeCurrentReference();
                throw th;
            }
        } catch (Throwable th2) {
            constructionContext.finishConstruction();
            throw th2;
        }
    }
}
