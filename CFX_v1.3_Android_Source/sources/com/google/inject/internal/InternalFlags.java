package com.google.inject.internal;

import java.util.Arrays;
import java.util.logging.Logger;

public class InternalFlags {
    private static final Logger logger = Logger.getLogger(InternalFlags.class.getName());

    public enum IncludeStackTraceOption {
        OFF,
        ONLY_FOR_DECLARING_SOURCE,
        COMPLETE
    }

    public static IncludeStackTraceOption getIncludeStackTraceOption() {
        String flag = System.getProperty("guice_include_stack_traces");
        if (flag != null) {
            try {
                if (flag.length() != 0) {
                    return IncludeStackTraceOption.valueOf(flag);
                }
            } catch (IllegalArgumentException e) {
                logger.warning(flag + " is not a valid flag value for guice_include_stack_traces. " + " Values must be one of " + Arrays.asList(IncludeStackTraceOption.values()));
                return IncludeStackTraceOption.ONLY_FOR_DECLARING_SOURCE;
            }
        }
        return IncludeStackTraceOption.ONLY_FOR_DECLARING_SOURCE;
    }
}
