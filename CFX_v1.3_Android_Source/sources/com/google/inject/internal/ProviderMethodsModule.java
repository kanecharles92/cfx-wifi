package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.HierarchyTraversalFilter;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.Message;
import com.google.inject.util.Modules;
import java.lang.annotation.Annotation;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.HashMultimap;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Multimap;

public final class ProviderMethodsModule implements Module {
    private static final Key<Logger> LOGGER_KEY = Key.get(Logger.class);
    private final Object delegate;
    private HierarchyTraversalFilter filter = Guice.createHierarchyTraversalFilter();
    private final boolean skipFastClassGeneration;
    /* access modifiers changed from: private */
    public final TypeLiteral<?> typeLiteral = TypeLiteral.get(this.delegate.getClass());

    private static final class LogProvider implements Provider<Logger> {
        private final String name;

        public LogProvider(Method method) {
            this.name = method.getDeclaringClass().getName() + "." + method.getName();
        }

        public Logger get() {
            return Logger.getLogger(this.name);
        }
    }

    private final class Signature {
        final int hashCode;
        final String name;
        final Class<?>[] parameters;

        Signature(Method method) {
            this.name = method.getName();
            List<TypeLiteral<?>> resolvedParameterTypes = ProviderMethodsModule.this.typeLiteral.getParameterTypes(method);
            this.parameters = new Class[resolvedParameterTypes.size()];
            for (TypeLiteral<?> type : resolvedParameterTypes) {
                this.parameters[0] = type.getRawType();
            }
            this.hashCode = this.name.hashCode() + (Arrays.hashCode(this.parameters) * 31);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Signature)) {
                return false;
            }
            Signature other = (Signature) obj;
            if (!other.name.equals(this.name) || !Arrays.equals(this.parameters, other.parameters)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.hashCode;
        }
    }

    private ProviderMethodsModule(Object delegate2, boolean skipFastClassGeneration2) {
        this.delegate = Preconditions.checkNotNull(delegate2, "delegate");
        this.skipFastClassGeneration = skipFastClassGeneration2;
    }

    public static Module forModule(Module module) {
        return forObject(module, false);
    }

    public static Module forObject(Object object) {
        return forObject(object, true);
    }

    private static Module forObject(Object object, boolean skipFastClassGeneration2) {
        if (object instanceof ProviderMethodsModule) {
            return Modules.EMPTY_MODULE;
        }
        return new ProviderMethodsModule(object, skipFastClassGeneration2);
    }

    public synchronized void configure(Binder binder) {
        for (ProviderMethod<?> providerMethod : getProviderMethods(binder)) {
            providerMethod.configure(binder);
        }
    }

    public List<ProviderMethod<?>> getProviderMethods(Binder binder) {
        List<ProviderMethod<?>> result = Lists.newArrayList();
        Multimap<Signature, Method> methodsBySignature = HashMultimap.create();
        this.filter.reset();
        for (Class<?> c = this.delegate.getClass(); this.filter.isWorthScanningForMethods(Provides.class.getName(), c); c = c.getSuperclass()) {
            for (Method method : this.filter.getAllMethods(Provides.class.getName(), c)) {
                if ((method.getModifiers() & 10) == 0 && !method.isBridge() && !method.isSynthetic()) {
                    methodsBySignature.put(new Signature(method), method);
                }
                if (isProvider(method)) {
                    result.add(createProviderMethod(binder, method));
                }
            }
        }
        for (ProviderMethod<?> provider : result) {
            Method method2 = provider.getMethod();
            Iterator i$ = methodsBySignature.get(new Signature(method2)).iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Method matchingSignature = (Method) i$.next();
                if (!matchingSignature.getDeclaringClass().isAssignableFrom(method2.getDeclaringClass()) && overrides(matchingSignature, method2)) {
                    binder.addError("Overriding @Provides methods is not allowed.\n\t@Provides method: %s\n\toverridden by: %s", method2, matchingSignature);
                    break;
                }
            }
        }
        return result;
    }

    private static boolean isProvider(Method method) {
        return !method.isBridge() && !method.isSynthetic() && method.isAnnotationPresent(Provides.class);
    }

    private static boolean overrides(Method a, Method b) {
        int modifiers = b.getModifiers();
        if (Modifier.isPublic(modifiers) || Modifier.isProtected(modifiers)) {
            return true;
        }
        if (Modifier.isPrivate(modifiers)) {
            return false;
        }
        return a.getDeclaringClass().getPackage().equals(b.getDeclaringClass().getPackage());
    }

    private <T> ProviderMethod<T> createProviderMethod(Binder binder, Method method) {
        Binder binder2 = binder.withSource(method);
        Errors errors = new Errors(method);
        List<Dependency<?>> dependencies = Lists.newArrayList();
        List<Provider<?>> parameterProviders = Lists.newArrayList();
        List<TypeLiteral<?>> parameterTypes = this.typeLiteral.getParameterTypes(method);
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (int i = 0; i < parameterTypes.size(); i++) {
            Key key = getKey(errors, (TypeLiteral) parameterTypes.get(i), method, parameterAnnotations[i]);
            if (key.equals(LOGGER_KEY)) {
                Key<Logger> loggerKey = Key.get(Logger.class, UniqueAnnotations.create());
                binder2.bind(loggerKey).toProvider((Provider<? extends T>) new LogProvider<Object>(method));
                key = loggerKey;
            }
            dependencies.add(Dependency.get(key));
            parameterProviders.add(binder2.getProvider(key));
        }
        Key<T> key2 = getKey(errors, this.typeLiteral.getReturnType(method), method, method.getAnnotations());
        Class<? extends Annotation> scopeAnnotation = Annotations.findScopeAnnotation(errors, method.getAnnotations());
        for (Message message : errors.getMessages()) {
            binder2.addError(message);
        }
        return ProviderMethod.create(key2, method, this.delegate, ImmutableSet.copyOf((Collection<? extends E>) dependencies), parameterProviders, scopeAnnotation, this.skipFastClassGeneration);
    }

    /* access modifiers changed from: 0000 */
    public <T> Key<T> getKey(Errors errors, TypeLiteral<T> type, Member member, Annotation[] annotations) {
        Annotation bindingAnnotation = Annotations.findBindingAnnotation(errors, member, annotations);
        return bindingAnnotation == null ? Key.get(type) : Key.get(type, bindingAnnotation);
    }

    public boolean equals(Object o) {
        return (o instanceof ProviderMethodsModule) && ((ProviderMethodsModule) o).delegate == this.delegate;
    }

    public int hashCode() {
        return this.delegate.hashCode();
    }
}
