package com.google.inject.internal;

final class Initializables {
    Initializables() {
    }

    /* renamed from: of */
    static <T> Initializable<T> m68of(final T instance) {
        return new Initializable<T>() {
            public T get(Errors errors) throws ErrorsException {
                return instance;
            }

            public String toString() {
                return String.valueOf(instance);
            }
        };
    }
}
