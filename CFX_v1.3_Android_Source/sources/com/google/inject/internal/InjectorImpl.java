package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Binding;
import com.google.inject.ConfigurationException;
import com.google.inject.ImplementedBy;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Module;
import com.google.inject.ProvidedBy;
import com.google.inject.Provider;
import com.google.inject.ProvisionException;
import com.google.inject.Scope;
import com.google.inject.Stage;
import com.google.inject.TypeLiteral;
import com.google.inject.internal.util.SourceProvider;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.ConvertedConstantBinding;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.HasDependencies;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.Message;
import com.google.inject.spi.ProviderBinding;
import com.google.inject.spi.TypeConverterBinding;
import com.google.inject.util.Providers;
import java.lang.annotation.Annotation;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.collect.ImmutableList;
import org.roboguice.shaded.goole.common.collect.ImmutableMap;
import org.roboguice.shaded.goole.common.collect.ImmutableMap.Builder;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.Lists;
import org.roboguice.shaded.goole.common.collect.Maps;
import org.roboguice.shaded.goole.common.collect.Sets;

final class InjectorImpl implements Injector, Lookups {
    public static final TypeLiteral<String> STRING_TYPE = TypeLiteral.get(String.class);
    final BindingsMultimap bindingsMultimap = new BindingsMultimap();
    final ConstructorInjectorStore constructors = new ConstructorInjectorStore(this);
    final Set<Key<?>> failedJitBindings = Sets.newHashSet();
    final Map<Key<?>, BindingImpl<?>> jitBindings = Maps.newHashMap();
    private final ThreadLocal<Object[]> localContext;
    Lookups lookups = new DeferredLookups(this);
    MembersInjectorStore membersInjectorStore;
    final InjectorOptions options;
    final InjectorImpl parent;
    ProvisionListenerCallbackStore provisionListenerStore;
    final State state;

    private static class BindingsMultimap {
        final Map<TypeLiteral<?>, List<Binding<?>>> multimap;

        private BindingsMultimap() {
            this.multimap = Maps.newHashMap();
        }

        /* access modifiers changed from: 0000 */
        public <T> void put(TypeLiteral<T> type, Binding<T> binding) {
            List<Binding<?>> bindingsForType = (List) this.multimap.get(type);
            if (bindingsForType == null) {
                bindingsForType = Lists.newArrayList();
                this.multimap.put(type, bindingsForType);
            }
            bindingsForType.add(binding);
        }

        /* access modifiers changed from: 0000 */
        public <T> List<Binding<T>> getAll(TypeLiteral<T> type) {
            return ((List) this.multimap.get(type)) != null ? Collections.unmodifiableList((List) this.multimap.get(type)) : ImmutableList.m108of();
        }
    }

    private static class ConvertedConstantBindingImpl<T> extends BindingImpl<T> implements ConvertedConstantBinding<T> {
        final Binding<String> originalBinding;
        final Provider<T> provider;
        final TypeConverterBinding typeConverterBinding;
        final T value;

        ConvertedConstantBindingImpl(InjectorImpl injector, Key<T> key, T value2, Binding<String> originalBinding2, TypeConverterBinding typeConverterBinding2) {
            super(injector, key, originalBinding2.getSource(), new ConstantFactory(Initializables.m68of(value2)), Scoping.UNSCOPED);
            this.value = value2;
            this.provider = Providers.m71of(value2);
            this.originalBinding = originalBinding2;
            this.typeConverterBinding = typeConverterBinding2;
        }

        public Provider<T> getProvider() {
            return this.provider;
        }

        public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
            return visitor.visit((ConvertedConstantBinding<? extends T>) this);
        }

        public T getValue() {
            return this.value;
        }

        public TypeConverterBinding getTypeConverterBinding() {
            return this.typeConverterBinding;
        }

        public Key<String> getSourceKey() {
            return this.originalBinding.getKey();
        }

        public Set<Dependency<?>> getDependencies() {
            return ImmutableSet.m147of(Dependency.get(getSourceKey()));
        }

        public void applyTo(Binder binder) {
            throw new UnsupportedOperationException("This element represents a synthetic binding.");
        }

        public String toString() {
            return Objects.toStringHelper(ConvertedConstantBinding.class).add("key", (Object) getKey()).add("sourceKey", (Object) getSourceKey()).add("value", (Object) this.value).toString();
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof ConvertedConstantBindingImpl)) {
                return false;
            }
            ConvertedConstantBindingImpl<?> o = (ConvertedConstantBindingImpl) obj;
            if (!getKey().equals(o.getKey()) || !getScoping().equals(o.getScoping()) || !Objects.equal(this.value, o.value)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return Objects.hashCode(getKey(), getScoping(), this.value);
        }
    }

    static class InjectorOptions {
        final boolean atInjectRequired;
        final boolean disableCircularProxies;
        final boolean exactBindingAnnotationsRequired;
        final boolean jitDisabled;
        final Stage stage;

        InjectorOptions(Stage stage2, boolean jitDisabled2, boolean disableCircularProxies2, boolean atInjectRequired2, boolean exactBindingAnnotationsRequired2) {
            this.stage = stage2;
            this.jitDisabled = jitDisabled2;
            this.disableCircularProxies = disableCircularProxies2;
            this.atInjectRequired = atInjectRequired2;
            this.exactBindingAnnotationsRequired = exactBindingAnnotationsRequired2;
        }

        public String toString() {
            return Objects.toStringHelper(getClass()).add("stage", (Object) this.stage).add("jitDisabled", this.jitDisabled).add("disableCircularProxies", this.disableCircularProxies).add("atInjectRequired", this.atInjectRequired).add("exactBindingAnnotationsRequired", this.exactBindingAnnotationsRequired).toString();
        }
    }

    enum JitLimitation {
        NO_JIT,
        EXISTING_JIT,
        NEW_OR_EXISTING_JIT
    }

    interface MethodInvoker {
        Object invoke(Object obj, Object... objArr) throws IllegalAccessException, InvocationTargetException;
    }

    private static class ProviderBindingImpl<T> extends BindingImpl<Provider<T>> implements ProviderBinding<Provider<T>>, HasDependencies {
        final BindingImpl<T> providedBinding;

        ProviderBindingImpl(InjectorImpl injector, Key<Provider<T>> key, Binding<T> providedBinding2) {
            super(injector, key, providedBinding2.getSource(), createInternalFactory(providedBinding2), Scoping.UNSCOPED);
            this.providedBinding = (BindingImpl) providedBinding2;
        }

        static <T> InternalFactory<Provider<T>> createInternalFactory(Binding<T> providedBinding2) {
            final Provider<T> provider = providedBinding2.getProvider();
            return new InternalFactory<Provider<T>>() {
                public Provider<T> get(Errors errors, InternalContext context, Dependency dependency, boolean linked) {
                    return provider;
                }
            };
        }

        public Key<? extends T> getProvidedKey() {
            return this.providedBinding.getKey();
        }

        public <V> V acceptTargetVisitor(BindingTargetVisitor<? super Provider<T>, V> visitor) {
            return visitor.visit((ProviderBinding<? extends T>) this);
        }

        public void applyTo(Binder binder) {
            throw new UnsupportedOperationException("This element represents a synthetic binding.");
        }

        public String toString() {
            return Objects.toStringHelper(ProviderBinding.class).add("key", (Object) getKey()).add("providedKey", (Object) getProvidedKey()).toString();
        }

        public Set<Dependency<?>> getDependencies() {
            return ImmutableSet.m147of(Dependency.get(getProvidedKey()));
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof ProviderBindingImpl)) {
                return false;
            }
            ProviderBindingImpl<?> o = (ProviderBindingImpl) obj;
            if (!getKey().equals(o.getKey()) || !getScoping().equals(o.getScoping()) || !Objects.equal(this.providedBinding, o.providedBinding)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return Objects.hashCode(getKey(), getScoping(), this.providedBinding);
        }
    }

    InjectorImpl(InjectorImpl parent2, State state2, InjectorOptions injectorOptions) {
        this.parent = parent2;
        this.state = state2;
        this.options = injectorOptions;
        if (parent2 != null) {
            this.localContext = parent2.localContext;
        } else {
            this.localContext = new ThreadLocal<>();
        }
    }

    /* access modifiers changed from: 0000 */
    public void index() {
        for (Binding<?> binding : this.state.getExplicitBindingsThisLevel().values()) {
            index(binding);
        }
    }

    /* access modifiers changed from: 0000 */
    public <T> void index(Binding<T> binding) {
        this.bindingsMultimap.put(binding.getKey().getTypeLiteral(), binding);
    }

    public <T> List<Binding<T>> findBindingsByType(TypeLiteral<T> type) {
        return this.bindingsMultimap.getAll(type);
    }

    public <T> BindingImpl<T> getBinding(Key<T> key) {
        Errors errors = new Errors(key);
        try {
            BindingImpl<T> result = getBindingOrThrow(key, errors, JitLimitation.EXISTING_JIT);
            errors.throwConfigurationExceptionIfErrorsExist();
            return result;
        } catch (ErrorsException e) {
            throw new ConfigurationException(errors.merge(e.getErrors()).getMessages());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        if (isProvider(r8) == false) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0037, code lost:
        if (getExistingBinding((com.google.inject.Key) getProvidedKey(r8, new com.google.inject.internal.Errors())) == null) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0041, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004f, code lost:
        throw new com.google.inject.ConfigurationException(r0.getErrors().getMessages());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return getBinding((com.google.inject.Key) r8);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> com.google.inject.internal.BindingImpl<T> getExistingBinding(com.google.inject.Key<T> r8) {
        /*
            r7 = this;
            com.google.inject.internal.State r5 = r7.state
            com.google.inject.internal.BindingImpl r1 = r5.getExplicitBinding(r8)
            if (r1 == 0) goto L_0x0009
        L_0x0008:
            return r1
        L_0x0009:
            com.google.inject.internal.State r5 = r7.state
            java.lang.Object r6 = r5.lock()
            monitor-enter(r6)
            r2 = r7
        L_0x0011:
            if (r2 == 0) goto L_0x0023
            java.util.Map<com.google.inject.Key<?>, com.google.inject.internal.BindingImpl<?>> r5 = r2.jitBindings     // Catch:{ all -> 0x003e }
            java.lang.Object r3 = r5.get(r8)     // Catch:{ all -> 0x003e }
            com.google.inject.internal.BindingImpl r3 = (com.google.inject.internal.BindingImpl) r3     // Catch:{ all -> 0x003e }
            if (r3 == 0) goto L_0x0020
            monitor-exit(r6)     // Catch:{ all -> 0x003e }
            r1 = r3
            goto L_0x0008
        L_0x0020:
            com.google.inject.internal.InjectorImpl r2 = r2.parent     // Catch:{ all -> 0x003e }
            goto L_0x0011
        L_0x0023:
            monitor-exit(r6)     // Catch:{ all -> 0x003e }
            boolean r5 = isProvider(r8)
            if (r5 == 0) goto L_0x0050
            com.google.inject.internal.Errors r5 = new com.google.inject.internal.Errors     // Catch:{ ErrorsException -> 0x0041 }
            r5.<init>()     // Catch:{ ErrorsException -> 0x0041 }
            com.google.inject.Key r4 = getProvidedKey(r8, r5)     // Catch:{ ErrorsException -> 0x0041 }
            com.google.inject.internal.BindingImpl r5 = r7.getExistingBinding(r4)     // Catch:{ ErrorsException -> 0x0041 }
            if (r5 == 0) goto L_0x0050
            com.google.inject.internal.BindingImpl r1 = r7.getBinding(r8)     // Catch:{ ErrorsException -> 0x0041 }
            goto L_0x0008
        L_0x003e:
            r5 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x003e }
            throw r5
        L_0x0041:
            r0 = move-exception
            com.google.inject.ConfigurationException r5 = new com.google.inject.ConfigurationException
            com.google.inject.internal.Errors r6 = r0.getErrors()
            java.util.List r6 = r6.getMessages()
            r5.<init>(r6)
            throw r5
        L_0x0050:
            r1 = 0
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.InjectorImpl.getExistingBinding(com.google.inject.Key):com.google.inject.internal.BindingImpl");
    }

    /* access modifiers changed from: 0000 */
    public <T> BindingImpl<T> getBindingOrThrow(Key<T> key, Errors errors, JitLimitation jitType) throws ErrorsException {
        BindingImpl<T> binding = this.state.getExplicitBinding(key);
        return binding != null ? binding : getJustInTimeBinding(key, errors, jitType);
    }

    public <T> Binding<T> getBinding(Class<T> type) {
        return getBinding(Key.get(type));
    }

    public Injector getParent() {
        return this.parent;
    }

    public Injector createChildInjector(Iterable<? extends Module> modules) {
        return new InternalInjectorCreator().parentInjector(this).addModules(modules).build();
    }

    public Injector createChildInjector(Module... modules) {
        return createChildInjector((Iterable<? extends Module>) ImmutableList.copyOf((E[]) modules));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r5.options.jitDisabled == false) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        if (r8 != com.google.inject.internal.InjectorImpl.JitLimitation.NO_JIT) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0031, code lost:
        if (r2 != false) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        if ((r0 instanceof com.google.inject.internal.InjectorImpl.ConvertedConstantBindingImpl) != false) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003f, code lost:
        throw r7.jitDisabled(r6).toException();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private <T> com.google.inject.internal.BindingImpl<T> getJustInTimeBinding(com.google.inject.Key<T> r6, com.google.inject.internal.Errors r7, com.google.inject.internal.InjectorImpl.JitLimitation r8) throws com.google.inject.internal.ErrorsException {
        /*
            r5 = this;
            boolean r3 = isProvider(r6)
            if (r3 != 0) goto L_0x0012
            boolean r3 = isTypeLiteral(r6)
            if (r3 != 0) goto L_0x0012
            boolean r3 = isMembersInjector(r6)
            if (r3 == 0) goto L_0x0043
        L_0x0012:
            r2 = 1
        L_0x0013:
            com.google.inject.internal.State r3 = r5.state
            java.lang.Object r4 = r3.lock()
            monitor-enter(r4)
            r1 = r5
        L_0x001b:
            if (r1 == 0) goto L_0x004a
            java.util.Map<com.google.inject.Key<?>, com.google.inject.internal.BindingImpl<?>> r3 = r1.jitBindings     // Catch:{ all -> 0x0040 }
            java.lang.Object r0 = r3.get(r6)     // Catch:{ all -> 0x0040 }
            com.google.inject.internal.BindingImpl r0 = (com.google.inject.internal.BindingImpl) r0     // Catch:{ all -> 0x0040 }
            if (r0 == 0) goto L_0x0047
            com.google.inject.internal.InjectorImpl$InjectorOptions r3 = r5.options     // Catch:{ all -> 0x0040 }
            boolean r3 = r3.jitDisabled     // Catch:{ all -> 0x0040 }
            if (r3 == 0) goto L_0x0045
            com.google.inject.internal.InjectorImpl$JitLimitation r3 = com.google.inject.internal.InjectorImpl.JitLimitation.NO_JIT     // Catch:{ all -> 0x0040 }
            if (r8 != r3) goto L_0x0045
            if (r2 != 0) goto L_0x0045
            boolean r3 = r0 instanceof com.google.inject.internal.InjectorImpl.ConvertedConstantBindingImpl     // Catch:{ all -> 0x0040 }
            if (r3 != 0) goto L_0x0045
            com.google.inject.internal.Errors r3 = r7.jitDisabled(r6)     // Catch:{ all -> 0x0040 }
            com.google.inject.internal.ErrorsException r3 = r3.toException()     // Catch:{ all -> 0x0040 }
            throw r3     // Catch:{ all -> 0x0040 }
        L_0x0040:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0040 }
            throw r3
        L_0x0043:
            r2 = 0
            goto L_0x0013
        L_0x0045:
            monitor-exit(r4)     // Catch:{ all -> 0x0040 }
        L_0x0046:
            return r0
        L_0x0047:
            com.google.inject.internal.InjectorImpl r1 = r1.parent     // Catch:{ all -> 0x0040 }
            goto L_0x001b
        L_0x004a:
            java.util.Set<com.google.inject.Key<?>> r3 = r5.failedJitBindings     // Catch:{ all -> 0x0040 }
            boolean r3 = r3.contains(r6)     // Catch:{ all -> 0x0040 }
            if (r3 == 0) goto L_0x005d
            boolean r3 = r7.hasErrors()     // Catch:{ all -> 0x0040 }
            if (r3 == 0) goto L_0x005d
            com.google.inject.internal.ErrorsException r3 = r7.toException()     // Catch:{ all -> 0x0040 }
            throw r3     // Catch:{ all -> 0x0040 }
        L_0x005d:
            com.google.inject.internal.InjectorImpl$InjectorOptions r3 = r5.options     // Catch:{ all -> 0x0040 }
            boolean r3 = r3.jitDisabled     // Catch:{ all -> 0x0040 }
            com.google.inject.internal.BindingImpl r0 = r5.createJustInTimeBindingRecursive(r6, r7, r3, r8)     // Catch:{ all -> 0x0040 }
            monitor-exit(r4)     // Catch:{ all -> 0x0040 }
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.inject.internal.InjectorImpl.getJustInTimeBinding(com.google.inject.Key, com.google.inject.internal.Errors, com.google.inject.internal.InjectorImpl$JitLimitation):com.google.inject.internal.BindingImpl");
    }

    private static boolean isProvider(Key<?> key) {
        return key.getTypeLiteral().getRawType().equals(Provider.class);
    }

    private static boolean isTypeLiteral(Key<?> key) {
        return key.getTypeLiteral().getRawType().equals(TypeLiteral.class);
    }

    private static <T> Key<T> getProvidedKey(Key<Provider<T>> key, Errors errors) throws ErrorsException {
        Type providerType = key.getTypeLiteral().getType();
        if (providerType instanceof ParameterizedType) {
            return key.ofType(((ParameterizedType) providerType).getActualTypeArguments()[0]);
        }
        throw errors.cannotInjectRawProvider().toException();
    }

    private static boolean isMembersInjector(Key<?> key) {
        return key.getTypeLiteral().getRawType().equals(MembersInjector.class) && key.getAnnotationType() == null;
    }

    private <T> BindingImpl<MembersInjector<T>> createMembersInjectorBinding(Key<MembersInjector<T>> key, Errors errors) throws ErrorsException {
        Type membersInjectorType = key.getTypeLiteral().getType();
        if (!(membersInjectorType instanceof ParameterizedType)) {
            throw errors.cannotInjectRawMembersInjector().toException();
        }
        MembersInjector<T> membersInjector = this.membersInjectorStore.get(TypeLiteral.get(((ParameterizedType) membersInjectorType).getActualTypeArguments()[0]), errors);
        return new InstanceBindingImpl(this, key, SourceProvider.UNKNOWN_SOURCE, new ConstantFactory<>(Initializables.m68of(membersInjector)), ImmutableSet.m146of(), membersInjector);
    }

    private <T> BindingImpl<Provider<T>> createProviderBinding(Key<Provider<T>> key, Errors errors) throws ErrorsException {
        return new ProviderBindingImpl(this, key, getBindingOrThrow(getProvidedKey(key, errors), errors, JitLimitation.NO_JIT));
    }

    private <T> BindingImpl<T> convertConstantStringBinding(Key<T> key, Errors errors) throws ErrorsException {
        BindingImpl<String> stringBinding = this.state.getExplicitBinding(key.ofType(STRING_TYPE));
        if (stringBinding == null || !stringBinding.isConstant()) {
            return null;
        }
        String stringValue = (String) stringBinding.getProvider().get();
        Object source = stringBinding.getSource();
        TypeLiteral<T> type = key.getTypeLiteral();
        TypeConverterBinding typeConverterBinding = this.state.getConverter(stringValue, type, errors, source);
        if (typeConverterBinding == null) {
            return null;
        }
        try {
            T converted = typeConverterBinding.getTypeConverter().convert(stringValue, type);
            if (converted == null) {
                throw errors.converterReturnedNull(stringValue, source, type, typeConverterBinding).toException();
            } else if (type.getRawType().isInstance(converted)) {
                return new ConvertedConstantBindingImpl(this, key, converted, stringBinding, typeConverterBinding);
            } else {
                throw errors.conversionTypeError(stringValue, source, type, typeConverterBinding, converted).toException();
            }
        } catch (ErrorsException e) {
            throw e;
        } catch (RuntimeException e2) {
            throw errors.conversionError(stringValue, source, type, typeConverterBinding, e2).toException();
        }
    }

    /* access modifiers changed from: 0000 */
    public <T> void initializeBinding(BindingImpl<T> binding, Errors errors) throws ErrorsException {
        if (binding instanceof DelayedInitialize) {
            ((DelayedInitialize) binding).initialize(this, errors);
        }
    }

    /* access modifiers changed from: 0000 */
    public <T> void initializeJitBinding(BindingImpl<T> binding, Errors errors) throws ErrorsException {
        if (binding instanceof DelayedInitialize) {
            this.jitBindings.put(binding.getKey(), binding);
            boolean successful = false;
            try {
                ((DelayedInitialize) binding).initialize(this, errors);
                successful = true;
            } finally {
                if (!successful) {
                    removeFailedJitBinding(binding, null);
                    cleanup(binding, new HashSet());
                }
            }
        }
    }

    private boolean cleanup(BindingImpl<?> binding, Set<Key> encountered) {
        boolean bindingFailed = false;
        for (Dependency dep : getInternalDependencies(binding)) {
            Key<?> depKey = dep.getKey();
            InjectionPoint ip = dep.getInjectionPoint();
            if (encountered.add(depKey)) {
                BindingImpl depBinding = (BindingImpl) this.jitBindings.get(depKey);
                if (depBinding != null) {
                    boolean failed = cleanup(depBinding, encountered);
                    if (depBinding instanceof ConstructorBindingImpl) {
                        ConstructorBindingImpl ctorBinding = (ConstructorBindingImpl) depBinding;
                        ip = ctorBinding.getInternalConstructor();
                        if (!ctorBinding.isInitialized()) {
                            failed = true;
                        }
                    }
                    if (failed) {
                        removeFailedJitBinding(depBinding, ip);
                        bindingFailed = true;
                    }
                } else if (this.state.getExplicitBinding(depKey) == null) {
                    bindingFailed = true;
                }
            }
        }
        return bindingFailed;
    }

    private void removeFailedJitBinding(Binding<?> binding, InjectionPoint ip) {
        this.failedJitBindings.add(binding.getKey());
        this.jitBindings.remove(binding.getKey());
        this.membersInjectorStore.remove(binding.getKey().getTypeLiteral());
        this.provisionListenerStore.remove(binding);
        if (ip != null) {
            this.constructors.remove(ip);
        }
    }

    private Set<Dependency<?>> getInternalDependencies(BindingImpl<?> binding) {
        if (binding instanceof ConstructorBindingImpl) {
            return ((ConstructorBindingImpl) binding).getInternalDependencies();
        }
        if (binding instanceof HasDependencies) {
            return ((HasDependencies) binding).getDependencies();
        }
        return ImmutableSet.m146of();
    }

    /* access modifiers changed from: 0000 */
    public <T> BindingImpl<T> createUninitializedBinding(Key<T> key, Scoping scoping, Object source, Errors errors, boolean jitBinding) throws ErrorsException {
        Class<?> rawType = key.getTypeLiteral().getRawType();
        ImplementedBy implementedBy = (ImplementedBy) rawType.getAnnotation(ImplementedBy.class);
        if (rawType.isArray() || (rawType.isEnum() && implementedBy != null)) {
            throw errors.missingImplementation(key).toException();
        } else if (rawType == TypeLiteral.class) {
            return createTypeLiteralBinding(key, errors);
        } else {
            if (implementedBy != null) {
                Annotations.checkForMisplacedScopeAnnotations(rawType, source, errors);
                return createImplementedByBinding(key, scoping, implementedBy, errors);
            }
            ProvidedBy providedBy = (ProvidedBy) rawType.getAnnotation(ProvidedBy.class);
            if (providedBy != null) {
                Annotations.checkForMisplacedScopeAnnotations(rawType, source, errors);
                return createProvidedByBinding(key, scoping, providedBy, errors);
            }
            return ConstructorBindingImpl.create(this, key, null, source, scoping, errors, jitBinding && this.options.jitDisabled, this.options.atInjectRequired);
        }
    }

    private <T> BindingImpl<TypeLiteral<T>> createTypeLiteralBinding(Key<TypeLiteral<T>> key, Errors errors) throws ErrorsException {
        Type typeLiteralType = key.getTypeLiteral().getType();
        if (!(typeLiteralType instanceof ParameterizedType)) {
            throw errors.cannotInjectRawTypeLiteral().toException();
        }
        Type innerType = ((ParameterizedType) typeLiteralType).getActualTypeArguments()[0];
        if ((innerType instanceof Class) || (innerType instanceof GenericArrayType) || (innerType instanceof ParameterizedType)) {
            TypeLiteral<T> value = TypeLiteral.get(innerType);
            return new InstanceBindingImpl(this, key, SourceProvider.UNKNOWN_SOURCE, new ConstantFactory<>(Initializables.m68of(value)), ImmutableSet.m146of(), value);
        }
        throw errors.cannotInjectTypeLiteralOf(innerType).toException();
    }

    /* access modifiers changed from: 0000 */
    public <T> BindingImpl<T> createProvidedByBinding(Key<T> key, Scoping scoping, ProvidedBy providedBy, Errors errors) throws ErrorsException {
        Class<? extends Provider<?>> rawType = key.getTypeLiteral().getRawType();
        Class<? extends Provider<?>> providerType = providedBy.value();
        if (providerType == rawType) {
            throw errors.recursiveProviderType().toException();
        }
        Key<? extends Provider<T>> providerKey = Key.get(providerType);
        ProvidedByInternalFactory<T> internalFactory = new ProvidedByInternalFactory<>(rawType, providerType, providerKey, !this.options.disableCircularProxies);
        Class source = rawType;
        BindingImpl<T> binding = LinkedProviderBindingImpl.createWithInitializer(this, key, source, Scoping.scope(key, this, internalFactory, source, scoping), scoping, providerKey, internalFactory);
        internalFactory.setProvisionListenerCallback(this.provisionListenerStore.get(binding));
        return binding;
    }

    private <T> BindingImpl<T> createImplementedByBinding(Key<T> key, Scoping scoping, ImplementedBy implementedBy, Errors errors) throws ErrorsException {
        Class<?> rawType = key.getTypeLiteral().getRawType();
        Class<?> implementationType = implementedBy.value();
        if (implementationType == rawType) {
            throw errors.recursiveImplementationType().toException();
        } else if (!rawType.isAssignableFrom(implementationType)) {
            throw errors.notASubtype(implementationType, rawType).toException();
        } else {
            final Key<? extends T> targetKey = Key.get(implementationType);
            final BindingImpl<? extends T> targetBinding = getBindingOrThrow(targetKey, errors, JitLimitation.NEW_OR_EXISTING_JIT);
            Class source = rawType;
            return new LinkedBindingImpl(this, key, source, Scoping.scope(key, this, new InternalFactory<T>() {
                public T get(Errors errors, InternalContext context, Dependency<?> dependency, boolean linked) throws ErrorsException {
                    context.pushState(targetKey, targetBinding.getSource());
                    try {
                        return targetBinding.getInternalFactory().get(errors.withSource(targetKey), context, dependency, true);
                    } finally {
                        context.popState();
                    }
                }
            }, source, scoping), scoping, targetKey);
        }
    }

    private <T> BindingImpl<T> createJustInTimeBindingRecursive(Key<T> key, Errors errors, boolean jitDisabled, JitLimitation jitType) throws ErrorsException {
        JitLimitation jitLimitation;
        if (this.parent != null) {
            try {
                InjectorImpl injectorImpl = this.parent;
                Errors errors2 = new Errors();
                if (this.parent.options.jitDisabled) {
                    jitLimitation = JitLimitation.NO_JIT;
                } else {
                    jitLimitation = jitType;
                }
                return injectorImpl.createJustInTimeBindingRecursive(key, errors2, jitDisabled, jitLimitation);
            } catch (ErrorsException e) {
            }
        }
        Set<Object> sources = this.state.getSourcesForBlacklistedKey(key);
        if (this.state.isBlacklisted(key)) {
            throw errors.childBindingAlreadySet(key, sources).toException();
        }
        BindingImpl<T> binding = createJustInTimeBinding(key, errors, jitDisabled, jitType);
        this.state.parent().blacklist(key, this.state, binding.getSource());
        this.jitBindings.put(key, binding);
        return binding;
    }

    private <T> BindingImpl<T> createJustInTimeBinding(Key<T> key, Errors errors, boolean jitDisabled, JitLimitation jitType) throws ErrorsException {
        int numErrorsBefore = errors.size();
        Set<Object> sources = this.state.getSourcesForBlacklistedKey(key);
        if (this.state.isBlacklisted(key)) {
            throw errors.childBindingAlreadySet(key, sources).toException();
        } else if (isProvider(key)) {
            return createProviderBinding(key, errors);
        } else {
            if (isMembersInjector(key)) {
                return createMembersInjectorBinding(key, errors);
            }
            BindingImpl<T> convertedBinding = convertConstantStringBinding(key, errors);
            if (convertedBinding != null) {
                return convertedBinding;
            }
            if (!isTypeLiteral(key) && jitDisabled && jitType != JitLimitation.NEW_OR_EXISTING_JIT) {
                throw errors.jitDisabled(key).toException();
            } else if (key.getAnnotationType() != null) {
                if (key.hasAttributes() && !this.options.exactBindingAnnotationsRequired) {
                    try {
                        return getBindingOrThrow(key.withoutAttributes(), new Errors(), JitLimitation.NO_JIT);
                    } catch (ErrorsException e) {
                    }
                }
                throw errors.missingImplementation(key).toException();
            } else {
                Key<T> key2 = key;
                BindingImpl<T> binding = createUninitializedBinding(key2, Scoping.UNSCOPED, key.getTypeLiteral().getRawType(), errors, true);
                errors.throwIfNewErrors(numErrorsBefore);
                initializeJitBinding(binding, errors);
                return binding;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public <T> InternalFactory<? extends T> getInternalFactory(Key<T> key, Errors errors, JitLimitation jitType) throws ErrorsException {
        return getBindingOrThrow(key, errors, jitType).getInternalFactory();
    }

    public Map<Key<?>, Binding<?>> getBindings() {
        return this.state.getExplicitBindingsThisLevel();
    }

    public Map<Key<?>, Binding<?>> getAllBindings() {
        ImmutableMap build;
        synchronized (this.state.lock()) {
            build = new Builder().putAll(this.state.getExplicitBindingsThisLevel()).putAll(this.jitBindings).build();
        }
        return build;
    }

    public Map<Class<? extends Annotation>, Scope> getScopeBindings() {
        return ImmutableMap.copyOf(this.state.getScopes());
    }

    public Set<TypeConverterBinding> getTypeConverterBindings() {
        return ImmutableSet.copyOf(this.state.getConvertersThisLevel());
    }

    /* access modifiers changed from: 0000 */
    public SingleParameterInjector<?>[] getParametersInjectors(List<Dependency<?>> parameters, Errors errors) throws ErrorsException {
        if (parameters.isEmpty()) {
            return null;
        }
        int numErrorsBefore = errors.size();
        SingleParameterInjector<?>[] result = new SingleParameterInjector[parameters.size()];
        int i = 0;
        for (Dependency<?> parameter : parameters) {
            int i2 = i + 1;
            try {
                result[i] = createParameterInjector(parameter, errors.withSource(parameter));
            } catch (ErrorsException e) {
            }
            i = i2;
        }
        errors.throwIfNewErrors(numErrorsBefore);
        return result;
    }

    /* access modifiers changed from: 0000 */
    public <T> SingleParameterInjector<T> createParameterInjector(Dependency<T> dependency, Errors errors) throws ErrorsException {
        return new SingleParameterInjector<>(dependency, getBindingOrThrow(dependency.getKey(), errors, JitLimitation.NO_JIT));
    }

    public void injectMembers(Object instance) {
        getMembersInjector(instance.getClass()).injectMembers(instance);
    }

    public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> typeLiteral) {
        Errors errors = new Errors(typeLiteral);
        try {
            return this.membersInjectorStore.get(typeLiteral, errors);
        } catch (ErrorsException e) {
            throw new ConfigurationException(errors.merge(e.getErrors()).getMessages());
        }
    }

    public <T> MembersInjector<T> getMembersInjector(Class<T> type) {
        return getMembersInjector(TypeLiteral.get(type));
    }

    public <T> Provider<T> getProvider(Class<T> type) {
        return getProvider(Key.get(type));
    }

    /* access modifiers changed from: 0000 */
    public <T> Provider<T> getProviderOrThrow(Key<T> key, Errors errors) throws ErrorsException {
        final BindingImpl<? extends T> binding = getBindingOrThrow(key, errors, JitLimitation.NO_JIT);
        final Dependency<T> dependency = Dependency.get(key);
        return new Provider<T>() {
            public T get() {
                final Errors errors = new Errors(dependency);
                try {
                    T t = InjectorImpl.this.callInContext(new ContextualCallable<T>() {
                        public T call(InternalContext context) throws ErrorsException {
                            Dependency previous = context.pushDependency(dependency, binding.getSource());
                            try {
                                return binding.getInternalFactory().get(errors, context, dependency, false);
                            } finally {
                                context.popStateAndSetDependency(previous);
                            }
                        }
                    });
                    errors.throwIfNewErrors(0);
                    return t;
                } catch (ErrorsException e) {
                    throw new ProvisionException((Iterable<Message>) errors.merge(e.getErrors()).getMessages());
                }
            }

            public String toString() {
                return binding.getInternalFactory().toString();
            }
        };
    }

    public <T> Provider<T> getProvider(Key<T> key) {
        Errors errors = new Errors(key);
        try {
            Provider<T> result = getProviderOrThrow(key, errors);
            errors.throwIfNewErrors(0);
            return result;
        } catch (ErrorsException e) {
            throw new ConfigurationException(errors.merge(e.getErrors()).getMessages());
        }
    }

    public <T> T getInstance(Key<T> key) {
        return getProvider(key).get();
    }

    public <T> T getInstance(Class<T> type) {
        return getProvider(type).get();
    }

    /* access modifiers changed from: 0000 */
    public <T> T callInContext(ContextualCallable<T> callable) throws ErrorsException {
        Object[] reference = (Object[]) this.localContext.get();
        if (reference == null) {
            reference = new Object[1];
            this.localContext.set(reference);
        }
        if (reference[0] != null) {
            return callable.call((InternalContext) reference[0]);
        }
        reference[0] = new InternalContext();
        try {
            return callable.call((InternalContext) reference[0]);
        } finally {
            reference[0] = null;
        }
    }

    public String toString() {
        return Objects.toStringHelper(Injector.class).add("bindings", (Object) this.state.getExplicitBindingsThisLevel().values()).toString();
    }
}
