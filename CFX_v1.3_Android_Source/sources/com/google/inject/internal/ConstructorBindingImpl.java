package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.ConfigurationException;
import com.google.inject.Inject;
import com.google.inject.Key;
import com.google.inject.internal.util.Classes;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.ConstructorBinding;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.InjectionPoint;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;
import org.roboguice.shaded.goole.common.collect.ImmutableSet.Builder;

final class ConstructorBindingImpl<T> extends BindingImpl<T> implements ConstructorBinding<T>, DelayedInitialize {
    private final InjectionPoint constructorInjectionPoint;
    private final Factory<T> factory;

    private static class Factory<T> implements InternalFactory<T> {
        /* access modifiers changed from: private */
        public boolean allowCircularProxy;
        /* access modifiers changed from: private */
        public ConstructorInjector<T> constructorInjector;
        private final boolean failIfNotLinked;
        private final Key<?> key;
        /* access modifiers changed from: private */
        public ProvisionListenerStackCallback<T> provisionCallback;

        Factory(boolean failIfNotLinked2, Key<?> key2) {
            this.failIfNotLinked = failIfNotLinked2;
            this.key = key2;
        }

        public T get(Errors errors, InternalContext context, Dependency<?> dependency, boolean linked) throws ErrorsException {
            Preconditions.checkState(this.constructorInjector != null, "Constructor not ready");
            if (!this.failIfNotLinked || linked) {
                return this.constructorInjector.construct(errors, context, dependency.getKey().getTypeLiteral().getRawType(), this.allowCircularProxy, this.provisionCallback);
            }
            throw errors.jitDisabled(this.key).toException();
        }
    }

    private ConstructorBindingImpl(InjectorImpl injector, Key<T> key, Object source, InternalFactory<? extends T> scopedFactory, Scoping scoping, Factory<T> factory2, InjectionPoint constructorInjectionPoint2) {
        super(injector, key, source, scopedFactory, scoping);
        this.factory = factory2;
        this.constructorInjectionPoint = constructorInjectionPoint2;
    }

    public ConstructorBindingImpl(Key<T> key, Object source, Scoping scoping, InjectionPoint constructorInjectionPoint2, Set<InjectionPoint> injectionPoints) {
        super(source, key, scoping);
        this.factory = new Factory<>(false, key);
        ConstructionProxy<T> constructionProxy = new DefaultConstructionProxyFactory(constructorInjectionPoint2).create();
        this.constructorInjectionPoint = constructorInjectionPoint2;
        this.factory.constructorInjector = new ConstructorInjector(injectionPoints, constructionProxy, null, null);
    }

    static <T> ConstructorBindingImpl<T> create(InjectorImpl injector, Key<T> key, InjectionPoint constructorInjector, Object source, Scoping scoping, Errors errors, boolean failIfNotLinked, boolean failIfNotExplicit) throws ErrorsException {
        Class<? super T> rawType;
        int numErrors = errors.size();
        if (constructorInjector == null) {
            rawType = key.getTypeLiteral().getRawType();
        } else {
            rawType = constructorInjector.getDeclaringType().getRawType();
        }
        if (Modifier.isAbstract(rawType.getModifiers())) {
            errors.missingImplementation(key);
        }
        if (Classes.isInnerClass(rawType)) {
            errors.cannotInjectInnerClass(rawType);
        }
        errors.throwIfNewErrors(numErrors);
        if (constructorInjector == null) {
            try {
                constructorInjector = InjectionPoint.forConstructorOf(key.getTypeLiteral());
                if (failIfNotExplicit && !hasAtInject((Constructor) constructorInjector.getMember())) {
                    errors.atInjectRequired(rawType);
                }
            } catch (ConfigurationException e) {
                throw errors.merge(e.getErrorMessages()).toException();
            }
        }
        if (!scoping.isExplicitlyScoped()) {
            Class<? extends Annotation> scopeAnnotation = Annotations.findScopeAnnotation(errors, constructorInjector.getMember().getDeclaringClass());
            if (scopeAnnotation != null) {
                scoping = Scoping.makeInjectable(Scoping.forAnnotation(scopeAnnotation), injector, errors.withSource(rawType));
            }
        }
        errors.throwIfNewErrors(numErrors);
        Factory<T> factoryFactory = new Factory<>(failIfNotLinked, key);
        return new ConstructorBindingImpl<>(injector, key, source, Scoping.scope(key, injector, factoryFactory, source, scoping), scoping, factoryFactory, constructorInjector);
    }

    private static boolean hasAtInject(Constructor cxtor) {
        return cxtor.isAnnotationPresent(Inject.class) || cxtor.isAnnotationPresent(javax.inject.Inject.class);
    }

    public void initialize(InjectorImpl injector, Errors errors) throws ErrorsException {
        this.factory.allowCircularProxy = !injector.options.disableCircularProxies;
        this.factory.constructorInjector = injector.constructors.get(this.constructorInjectionPoint, errors);
        this.factory.provisionCallback = injector.provisionListenerStore.get(this);
    }

    /* access modifiers changed from: 0000 */
    public boolean isInitialized() {
        return this.factory.constructorInjector != null;
    }

    /* access modifiers changed from: 0000 */
    public InjectionPoint getInternalConstructor() {
        if (this.factory.constructorInjector != null) {
            return this.factory.constructorInjector.getConstructionProxy().getInjectionPoint();
        }
        return this.constructorInjectionPoint;
    }

    /* access modifiers changed from: 0000 */
    public Set<Dependency<?>> getInternalDependencies() {
        Builder<InjectionPoint> builder = ImmutableSet.builder();
        if (this.factory.constructorInjector == null) {
            builder.add((Object) this.constructorInjectionPoint);
            try {
                builder.addAll((Iterable) InjectionPoint.forInstanceMethodsAndFields(this.constructorInjectionPoint.getDeclaringType()));
            } catch (ConfigurationException e) {
            }
        } else {
            builder.add((Object) getConstructor()).addAll((Iterable) getInjectableMembers());
        }
        return Dependency.forInjectionPoints(builder.build());
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        Preconditions.checkState(this.factory.constructorInjector != null, "not initialized");
        return visitor.visit((ConstructorBinding<? extends T>) this);
    }

    public InjectionPoint getConstructor() {
        Preconditions.checkState(this.factory.constructorInjector != null, "Binding is not ready");
        return this.factory.constructorInjector.getConstructionProxy().getInjectionPoint();
    }

    public Set<InjectionPoint> getInjectableMembers() {
        Preconditions.checkState(this.factory.constructorInjector != null, "Binding is not ready");
        return this.factory.constructorInjector.getInjectableMembers();
    }

    public Set<Dependency<?>> getDependencies() {
        return Dependency.forInjectionPoints(new Builder().add((Object) getConstructor()).addAll((Iterable) getInjectableMembers()).build());
    }

    /* access modifiers changed from: protected */
    public BindingImpl<T> withScoping(Scoping scoping) {
        return new ConstructorBindingImpl(null, getKey(), getSource(), this.factory, scoping, this.factory, this.constructorInjectionPoint);
    }

    /* access modifiers changed from: protected */
    public BindingImpl<T> withKey(Key<T> key) {
        return new ConstructorBindingImpl(null, key, getSource(), this.factory, getScoping(), this.factory, this.constructorInjectionPoint);
    }

    public void applyTo(Binder binder) {
        getScoping().applyTo(binder.withSource(getSource()).bind(getKey()).toConstructor((Constructor) getConstructor().getMember(), getConstructor().getDeclaringType()));
    }

    public String toString() {
        return Objects.toStringHelper(ConstructorBinding.class).add("key", (Object) getKey()).add("source", getSource()).add("scope", (Object) getScoping()).toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ConstructorBindingImpl)) {
            return false;
        }
        ConstructorBindingImpl<?> o = (ConstructorBindingImpl) obj;
        if (!getKey().equals(o.getKey()) || !getScoping().equals(o.getScoping()) || !Objects.equal(this.constructorInjectionPoint, o.constructorInjectionPoint)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hashCode(getKey(), getScoping(), this.constructorInjectionPoint);
    }
}
