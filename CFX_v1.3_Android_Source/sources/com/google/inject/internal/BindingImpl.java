package com.google.inject.internal;

import com.google.inject.Binding;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.spi.BindingScopingVisitor;
import com.google.inject.spi.ElementVisitor;
import com.google.inject.spi.InstanceBinding;
import org.roboguice.shaded.goole.common.base.Objects;

public abstract class BindingImpl<T> implements Binding<T> {
    private final InjectorImpl injector;
    private final InternalFactory<? extends T> internalFactory;
    private final Key<T> key;
    private volatile Provider<T> provider;
    private final Scoping scoping;
    private final Object source;

    public BindingImpl(InjectorImpl injector2, Key<T> key2, Object source2, InternalFactory<? extends T> internalFactory2, Scoping scoping2) {
        this.injector = injector2;
        this.key = key2;
        this.source = source2;
        this.internalFactory = internalFactory2;
        this.scoping = scoping2;
    }

    protected BindingImpl(Object source2, Key<T> key2, Scoping scoping2) {
        this.internalFactory = null;
        this.injector = null;
        this.source = source2;
        this.key = key2;
        this.scoping = scoping2;
    }

    public Key<T> getKey() {
        return this.key;
    }

    public Object getSource() {
        return this.source;
    }

    public Provider<T> getProvider() {
        if (this.provider == null) {
            if (this.injector == null) {
                throw new UnsupportedOperationException("getProvider() not supported for module bindings");
            }
            this.provider = this.injector.getProvider(this.key);
        }
        return this.provider;
    }

    public InternalFactory<? extends T> getInternalFactory() {
        return this.internalFactory;
    }

    public Scoping getScoping() {
        return this.scoping;
    }

    public boolean isConstant() {
        return this instanceof InstanceBinding;
    }

    public <V> V acceptVisitor(ElementVisitor<V> visitor) {
        return visitor.visit((Binding<T>) this);
    }

    public <V> V acceptScopingVisitor(BindingScopingVisitor<V> visitor) {
        return this.scoping.acceptVisitor(visitor);
    }

    /* access modifiers changed from: protected */
    public BindingImpl<T> withScoping(Scoping scoping2) {
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    public BindingImpl<T> withKey(Key<T> key2) {
        throw new AssertionError();
    }

    public String toString() {
        return Objects.toStringHelper(Binding.class).add("key", (Object) this.key).add("scope", (Object) this.scoping).add("source", this.source).toString();
    }

    public InjectorImpl getInjector() {
        return this.injector;
    }
}
