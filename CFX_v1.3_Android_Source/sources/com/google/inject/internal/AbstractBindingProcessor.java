package com.google.inject.internal;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Binding;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Scope;
import com.google.inject.Stage;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.DefaultBindingTargetVisitor;
import java.util.Set;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;

abstract class AbstractBindingProcessor extends AbstractProcessor {
    private static final Set<Class<?>> FORBIDDEN_TYPES = ImmutableSet.m152of(AbstractModule.class, Binder.class, Binding.class, Injector.class, Key.class, MembersInjector.class, Module.class, Provider.class, Scope.class, Stage.class, TypeLiteral.class);
    protected final ProcessedBindingData bindingData;

    abstract class Processor<T, V> extends DefaultBindingTargetVisitor<T, V> {
        final Key<T> key;
        final Class<? super T> rawType = this.key.getTypeLiteral().getRawType();
        Scoping scoping;
        final Object source;

        Processor(BindingImpl<T> binding) {
            this.source = binding.getSource();
            this.key = binding.getKey();
            this.scoping = binding.getScoping();
        }

        /* access modifiers changed from: protected */
        public void prepareBinding() {
            AbstractBindingProcessor.this.validateKey(this.source, this.key);
            this.scoping = Scoping.makeInjectable(this.scoping, AbstractBindingProcessor.this.injector, AbstractBindingProcessor.this.errors);
        }

        /* access modifiers changed from: protected */
        public void scheduleInitialization(final BindingImpl<?> binding) {
            AbstractBindingProcessor.this.bindingData.addUninitializedBinding(new Runnable() {
                public void run() {
                    try {
                        binding.getInjector().initializeBinding(binding, AbstractBindingProcessor.this.errors.withSource(Processor.this.source));
                    } catch (ErrorsException e) {
                        AbstractBindingProcessor.this.errors.merge(e.getErrors());
                    }
                }
            });
        }
    }

    AbstractBindingProcessor(Errors errors, ProcessedBindingData bindingData2) {
        super(errors);
        this.bindingData = bindingData2;
    }

    /* access modifiers changed from: protected */
    public <T> UntargettedBindingImpl<T> invalidBinding(InjectorImpl injector, Key<T> key, Object source) {
        return new UntargettedBindingImpl<>(injector, key, source);
    }

    /* access modifiers changed from: protected */
    public void putBinding(BindingImpl<?> binding) {
        Key<?> key = binding.getKey();
        Class<?> rawType = key.getTypeLiteral().getRawType();
        if (FORBIDDEN_TYPES.contains(rawType)) {
            this.errors.cannotBindToGuiceType(rawType.getSimpleName());
            return;
        }
        BindingImpl<?> original = this.injector.getExistingBinding((Key) key);
        if (original != null) {
            if (this.injector.state.getExplicitBinding(key) != null) {
                try {
                    if (!isOkayDuplicate(original, binding, this.injector.state)) {
                        this.errors.bindingAlreadySet(key, original.getSource());
                        return;
                    }
                } catch (Throwable t) {
                    this.errors.errorCheckingDuplicateBinding(key, original.getSource(), t);
                    return;
                }
            } else {
                this.errors.jitBindingAlreadySet(key);
                return;
            }
        }
        this.injector.state.parent().blacklist(key, this.injector.state, binding.getSource());
        this.injector.state.putBinding(key, binding);
    }

    private boolean isOkayDuplicate(BindingImpl<?> original, BindingImpl<?> binding, State state) {
        if (!(original instanceof ExposedBindingImpl)) {
            BindingImpl<?> original2 = (BindingImpl) state.getExplicitBindingsThisLevel().get(binding.getKey());
            if (original2 != null) {
                return original2.equals(binding);
            }
            return false;
        } else if (((InjectorImpl) ((ExposedBindingImpl) original).getPrivateElements().getInjector()) == binding.getInjector()) {
            return true;
        } else {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public <T> void validateKey(Object source, Key<T> key) {
        Annotations.checkForMisplacedScopeAnnotations(key.getTypeLiteral().getRawType(), source, this.errors);
    }
}
