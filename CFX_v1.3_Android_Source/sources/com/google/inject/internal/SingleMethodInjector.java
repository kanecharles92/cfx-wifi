package com.google.inject.internal;

import com.google.inject.spi.InjectionPoint;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

final class SingleMethodInjector implements SingleMemberInjector {
    private final InjectionPoint injectionPoint;
    private final MethodInvoker methodInvoker;
    private final SingleParameterInjector<?>[] parameterInjectors;

    SingleMethodInjector(InjectorImpl injector, InjectionPoint injectionPoint2, Errors errors) throws ErrorsException {
        this.injectionPoint = injectionPoint2;
        this.methodInvoker = createMethodInvoker((Method) injectionPoint2.getMember());
        this.parameterInjectors = injector.getParametersInjectors(injectionPoint2.getDependencies(), errors);
    }

    private MethodInvoker createMethodInvoker(final Method method) {
        int modifiers = method.getModifiers();
        if (Modifier.isPrivate(modifiers) || !Modifier.isProtected(modifiers)) {
        }
        if (!Modifier.isPublic(modifiers) || !Modifier.isPublic(method.getDeclaringClass().getModifiers())) {
            method.setAccessible(true);
        }
        return new MethodInvoker() {
            public Object invoke(Object target, Object... parameters) throws IllegalAccessException, InvocationTargetException {
                return method.invoke(target, parameters);
            }
        };
    }

    public InjectionPoint getInjectionPoint() {
        return this.injectionPoint;
    }

    public void inject(Errors errors, InternalContext context, Object o) {
        Throwable cause;
        try {
            try {
                this.methodInvoker.invoke(o, SingleParameterInjector.getAll(errors, context, this.parameterInjectors));
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            } catch (InvocationTargetException userException) {
                if (userException.getCause() != null) {
                    cause = userException.getCause();
                } else {
                    cause = userException;
                }
                errors.withSource(this.injectionPoint).errorInjectingMethod(cause);
            }
        } catch (ErrorsException e2) {
            errors.merge(e2.getErrors());
        }
    }
}
