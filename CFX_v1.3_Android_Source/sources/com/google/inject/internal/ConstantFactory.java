package com.google.inject.internal;

import com.google.inject.spi.Dependency;
import org.roboguice.shaded.goole.common.base.Objects;

final class ConstantFactory<T> implements InternalFactory<T> {
    private final Initializable<T> initializable;

    public ConstantFactory(Initializable<T> initializable2) {
        this.initializable = initializable2;
    }

    public T get(Errors errors, InternalContext context, Dependency dependency, boolean linked) throws ErrorsException {
        return this.initializable.get(errors);
    }

    public String toString() {
        return Objects.toStringHelper(ConstantFactory.class).add("value", (Object) this.initializable).toString();
    }
}
