package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.ConfigurationException;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.binder.ScopedBindingBuilder;
import com.google.inject.spi.Element;
import com.google.inject.spi.InjectionPoint;
import com.google.inject.spi.Message;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Set;
import org.roboguice.shaded.goole.common.base.Preconditions;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;

public class BindingBuilder<T> extends AbstractBindingBuilder<T> implements AnnotatedBindingBuilder<T> {
    public BindingBuilder(Binder binder, List<Element> elements, Object source, Key<T> key) {
        super(binder, elements, source, key);
    }

    public BindingBuilder<T> annotatedWith(Class<? extends Annotation> annotationType) {
        annotatedWithInternal(annotationType);
        return this;
    }

    public BindingBuilder<T> annotatedWith(Annotation annotation) {
        annotatedWithInternal(annotation);
        return this;
    }

    /* renamed from: to */
    public BindingBuilder<T> m56to(Class<? extends T> implementation) {
        return mo7040to(Key.get(implementation));
    }

    /* renamed from: to */
    public BindingBuilder<T> m55to(TypeLiteral<? extends T> implementation) {
        return mo7040to(Key.get(implementation));
    }

    /* renamed from: to */
    public BindingBuilder<T> m54to(Key<? extends T> linkedKey) {
        Preconditions.checkNotNull(linkedKey, "linkedKey");
        checkNotTargetted();
        BindingImpl<T> base = getBinding();
        setBinding(new LinkedBindingImpl(base.getSource(), base.getKey(), base.getScoping(), linkedKey));
        return this;
    }

    public void toInstance(T instance) {
        Set<InjectionPoint> injectionPoints;
        checkNotTargetted();
        if (instance != null) {
            try {
                injectionPoints = InjectionPoint.forInstanceMethodsAndFields(instance.getClass());
            } catch (ConfigurationException e) {
                copyErrorsToBinder(e);
                injectionPoints = (Set) e.getPartialValue();
            }
        } else {
            this.binder.addError(AbstractBindingBuilder.BINDING_TO_NULL, new Object[0]);
            injectionPoints = ImmutableSet.m146of();
        }
        BindingImpl<T> base = getBinding();
        setBinding(new InstanceBindingImpl(base.getSource(), base.getKey(), Scoping.EAGER_SINGLETON, injectionPoints, instance));
    }

    public BindingBuilder<T> toProvider(Provider<? extends T> provider) {
        return toProvider((javax.inject.Provider) provider);
    }

    public BindingBuilder<T> toProvider(javax.inject.Provider<? extends T> provider) {
        Set<InjectionPoint> injectionPoints;
        Preconditions.checkNotNull(provider, "provider");
        checkNotTargetted();
        try {
            injectionPoints = InjectionPoint.forInstanceMethodsAndFields(provider.getClass());
        } catch (ConfigurationException e) {
            copyErrorsToBinder(e);
            injectionPoints = (Set) e.getPartialValue();
        }
        BindingImpl<T> base = getBinding();
        setBinding(new ProviderInstanceBindingImpl(base.getSource(), base.getKey(), base.getScoping(), injectionPoints, provider));
        return this;
    }

    public BindingBuilder<T> toProvider(Class<? extends javax.inject.Provider<? extends T>> providerType) {
        return toProvider(Key.get(providerType));
    }

    public BindingBuilder<T> toProvider(TypeLiteral<? extends javax.inject.Provider<? extends T>> providerType) {
        return toProvider(Key.get(providerType));
    }

    public BindingBuilder<T> toProvider(Key<? extends javax.inject.Provider<? extends T>> providerKey) {
        Preconditions.checkNotNull(providerKey, "providerKey");
        checkNotTargetted();
        BindingImpl<T> base = getBinding();
        setBinding(new LinkedProviderBindingImpl(base.getSource(), base.getKey(), base.getScoping(), providerKey));
        return this;
    }

    public <S extends T> ScopedBindingBuilder toConstructor(Constructor<S> constructor) {
        return toConstructor(constructor, TypeLiteral.get(constructor.getDeclaringClass()));
    }

    public <S extends T> ScopedBindingBuilder toConstructor(Constructor<S> constructor, TypeLiteral<? extends S> type) {
        Set<InjectionPoint> injectionPoints;
        Preconditions.checkNotNull(constructor, "constructor");
        Preconditions.checkNotNull(type, "type");
        checkNotTargetted();
        BindingImpl<T> base = getBinding();
        try {
            injectionPoints = InjectionPoint.forInstanceMethodsAndFields(type);
        } catch (ConfigurationException e) {
            copyErrorsToBinder(e);
            injectionPoints = (Set) e.getPartialValue();
        }
        try {
            setBinding(new ConstructorBindingImpl(base.getKey(), base.getSource(), base.getScoping(), InjectionPoint.forConstructor(constructor, type), injectionPoints));
        } catch (ConfigurationException e2) {
            copyErrorsToBinder(e2);
        }
        return this;
    }

    public String toString() {
        return "BindingBuilder<" + getBinding().getKey().getTypeLiteral() + ">";
    }

    private void copyErrorsToBinder(ConfigurationException e) {
        for (Message message : e.getErrorMessages()) {
            this.binder.addError(message);
        }
    }
}
