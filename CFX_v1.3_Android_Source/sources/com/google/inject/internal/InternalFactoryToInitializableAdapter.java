package com.google.inject.internal;

import com.google.inject.spi.Dependency;
import javax.inject.Provider;
import org.roboguice.shaded.goole.common.base.Preconditions;

final class InternalFactoryToInitializableAdapter<T> extends ProviderInternalFactory<T> {
    private final Initializable<? extends Provider<? extends T>> initializable;
    private final ProvisionListenerStackCallback<T> provisionCallback;

    public InternalFactoryToInitializableAdapter(Initializable<? extends Provider<? extends T>> initializable2, Object source, boolean allowProxy, ProvisionListenerStackCallback<T> provisionCallback2) {
        super(source, allowProxy);
        this.provisionCallback = (ProvisionListenerStackCallback) Preconditions.checkNotNull(provisionCallback2, "provisionCallback");
        this.initializable = (Initializable) Preconditions.checkNotNull(initializable2, "provider");
    }

    public T get(Errors errors, InternalContext context, Dependency<?> dependency, boolean linked) throws ErrorsException {
        return circularGet((Provider) this.initializable.get(errors), errors, context, dependency, linked, this.provisionCallback);
    }

    /* access modifiers changed from: protected */
    public T provision(Provider<? extends T> provider, Errors errors, Dependency<?> dependency, ConstructionContext<T> constructionContext) throws ErrorsException {
        try {
            return super.provision(provider, errors, dependency, constructionContext);
        } catch (RuntimeException userException) {
            throw errors.withSource(this.source).errorInProvider(userException).toException();
        }
    }

    public String toString() {
        return this.initializable.toString();
    }
}
