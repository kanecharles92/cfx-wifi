package com.google.inject.internal;

import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.spi.Dependency;
import org.roboguice.shaded.goole.common.base.Preconditions;

class ProvidedByInternalFactory<T> extends ProviderInternalFactory<T> implements DelayedInitialize {
    private BindingImpl<? extends Provider<T>> providerBinding;
    private final Key<? extends Provider<T>> providerKey;
    private final Class<? extends Provider<?>> providerType;
    private ProvisionListenerStackCallback<T> provisionCallback;
    private final Class<?> rawType;

    ProvidedByInternalFactory(Class<?> rawType2, Class<? extends Provider<?>> providerType2, Key<? extends Provider<T>> providerKey2, boolean allowProxy) {
        super(providerKey2, allowProxy);
        this.rawType = rawType2;
        this.providerType = providerType2;
        this.providerKey = providerKey2;
    }

    /* access modifiers changed from: 0000 */
    public void setProvisionListenerCallback(ProvisionListenerStackCallback<T> listener) {
        this.provisionCallback = listener;
    }

    public void initialize(InjectorImpl injector, Errors errors) throws ErrorsException {
        this.providerBinding = injector.getBindingOrThrow(this.providerKey, errors, JitLimitation.NEW_OR_EXISTING_JIT);
    }

    public T get(Errors errors, InternalContext context, Dependency dependency, boolean linked) throws ErrorsException {
        boolean z = true;
        if (this.providerBinding == null) {
            z = false;
        }
        Preconditions.checkState(z, "not initialized");
        context.pushState(this.providerKey, this.providerBinding.getSource());
        try {
            Errors errors2 = errors.withSource(this.providerKey);
            return circularGet((Provider) this.providerBinding.getInternalFactory().get(errors2, context, dependency, true), errors2, context, dependency, linked, this.provisionCallback);
        } finally {
            context.popState();
        }
    }

    /* access modifiers changed from: protected */
    public T provision(javax.inject.Provider<? extends T> provider, Errors errors, Dependency<?> dependency, ConstructionContext<T> constructionContext) throws ErrorsException {
        try {
            Object o = super.provision(provider, errors, dependency, constructionContext);
            if (o == null || this.rawType.isInstance(o)) {
                return o;
            }
            throw errors.subtypeNotProvided(this.providerType, this.rawType).toException();
        } catch (RuntimeException e) {
            throw errors.errorInProvider(e).toException();
        }
    }
}
