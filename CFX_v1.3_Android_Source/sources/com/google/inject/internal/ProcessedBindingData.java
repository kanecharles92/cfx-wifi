package com.google.inject.internal;

import java.util.List;
import org.roboguice.shaded.goole.common.collect.Lists;

class ProcessedBindingData {
    private final List<CreationListener> creationListeners = Lists.newArrayList();
    private final List<Runnable> uninitializedBindings = Lists.newArrayList();

    ProcessedBindingData() {
    }

    /* access modifiers changed from: 0000 */
    public void addCreationListener(CreationListener listener) {
        this.creationListeners.add(listener);
    }

    /* access modifiers changed from: 0000 */
    public void addUninitializedBinding(Runnable runnable) {
        this.uninitializedBindings.add(runnable);
    }

    /* access modifiers changed from: 0000 */
    public void initializeBindings() {
        for (Runnable initializer : this.uninitializedBindings) {
            initializer.run();
        }
    }

    /* access modifiers changed from: 0000 */
    public void runCreationListeners(Errors errors) {
        for (CreationListener creationListener : this.creationListeners) {
            creationListener.notify(errors);
        }
    }
}
