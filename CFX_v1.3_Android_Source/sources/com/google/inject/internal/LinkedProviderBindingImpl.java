package com.google.inject.internal;

import com.google.inject.Binder;
import com.google.inject.Key;
import com.google.inject.spi.BindingTargetVisitor;
import com.google.inject.spi.Dependency;
import com.google.inject.spi.HasDependencies;
import com.google.inject.spi.ProviderKeyBinding;
import java.util.Set;
import javax.inject.Provider;
import org.roboguice.shaded.goole.common.base.Objects;
import org.roboguice.shaded.goole.common.collect.ImmutableSet;

final class LinkedProviderBindingImpl<T> extends BindingImpl<T> implements ProviderKeyBinding<T>, HasDependencies, DelayedInitialize {
    final DelayedInitialize delayedInitializer;
    final Key<? extends Provider<? extends T>> providerKey;

    private LinkedProviderBindingImpl(InjectorImpl injector, Key<T> key, Object source, InternalFactory<? extends T> internalFactory, Scoping scoping, Key<? extends Provider<? extends T>> providerKey2, DelayedInitialize delayedInitializer2) {
        super(injector, key, source, internalFactory, scoping);
        this.providerKey = providerKey2;
        this.delayedInitializer = delayedInitializer2;
    }

    public LinkedProviderBindingImpl(InjectorImpl injector, Key<T> key, Object source, InternalFactory<? extends T> internalFactory, Scoping scoping, Key<? extends Provider<? extends T>> providerKey2) {
        this(injector, key, source, internalFactory, scoping, providerKey2, null);
    }

    LinkedProviderBindingImpl(Object source, Key<T> key, Scoping scoping, Key<? extends Provider<? extends T>> providerKey2) {
        super(source, key, scoping);
        this.providerKey = providerKey2;
        this.delayedInitializer = null;
    }

    static <T> LinkedProviderBindingImpl<T> createWithInitializer(InjectorImpl injector, Key<T> key, Object source, InternalFactory<? extends T> internalFactory, Scoping scoping, Key<? extends Provider<? extends T>> providerKey2, DelayedInitialize delayedInitializer2) {
        return new LinkedProviderBindingImpl<>(injector, key, source, internalFactory, scoping, providerKey2, delayedInitializer2);
    }

    public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
        return visitor.visit((ProviderKeyBinding<? extends T>) this);
    }

    public Key<? extends Provider<? extends T>> getProviderKey() {
        return this.providerKey;
    }

    public void initialize(InjectorImpl injector, Errors errors) throws ErrorsException {
        if (this.delayedInitializer != null) {
            this.delayedInitializer.initialize(injector, errors);
        }
    }

    public Set<Dependency<?>> getDependencies() {
        return ImmutableSet.m147of(Dependency.get(this.providerKey));
    }

    public BindingImpl<T> withScoping(Scoping scoping) {
        return new LinkedProviderBindingImpl(getSource(), getKey(), scoping, this.providerKey);
    }

    public BindingImpl<T> withKey(Key<T> key) {
        return new LinkedProviderBindingImpl(getSource(), key, getScoping(), this.providerKey);
    }

    public void applyTo(Binder binder) {
        getScoping().applyTo(binder.withSource(getSource()).bind(getKey()).toProvider(getProviderKey()));
    }

    public String toString() {
        return Objects.toStringHelper(ProviderKeyBinding.class).add("key", (Object) getKey()).add("source", getSource()).add("scope", (Object) getScoping()).add("provider", (Object) this.providerKey).toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof LinkedProviderBindingImpl)) {
            return false;
        }
        LinkedProviderBindingImpl<?> o = (LinkedProviderBindingImpl) obj;
        if (!getKey().equals(o.getKey()) || !getScoping().equals(o.getScoping()) || !Objects.equal(this.providerKey, o.providerKey)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hashCode(getKey(), getScoping(), this.providerKey);
    }
}
