package com.google.inject;

import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.binder.LinkedBindingBuilder;
import com.google.inject.binder.ScopedBindingBuilder;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import javax.inject.Provider;

public class NoOpAnnotatedBindingBuilder<T> implements AnnotatedBindingBuilder<T> {
    private NoOpLinkedBindingBuilder<T> noOpLinkedBindingBuilder = new NoOpLinkedBindingBuilder<>();
    /* access modifiers changed from: private */
    public ScopedBindingBuilder scopedBindingBuilder = new NoOpScopedBindingBuilder();

    private class NoOpLinkedBindingBuilder<U> implements LinkedBindingBuilder<U> {
        private NoOpLinkedBindingBuilder() {
        }

        /* renamed from: to */
        public ScopedBindingBuilder mo7042to(Class<? extends U> cls) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }

        /* renamed from: to */
        public ScopedBindingBuilder mo7041to(TypeLiteral<? extends U> typeLiteral) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }

        /* renamed from: to */
        public ScopedBindingBuilder mo7040to(Key<? extends U> key) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }

        public void toInstance(U u) {
        }

        public ScopedBindingBuilder toProvider(Provider<? extends U> provider) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }

        public ScopedBindingBuilder toProvider(Class<? extends Provider<? extends U>> cls) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }

        public ScopedBindingBuilder toProvider(TypeLiteral<? extends Provider<? extends U>> typeLiteral) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }

        public ScopedBindingBuilder toProvider(Key<? extends Provider<? extends U>> key) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }

        public <S extends U> ScopedBindingBuilder toConstructor(Constructor<S> constructor) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }

        public <S extends U> ScopedBindingBuilder toConstructor(Constructor<S> constructor, TypeLiteral<? extends S> typeLiteral) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }

        /* renamed from: in */
        public void mo7039in(Class<? extends Annotation> cls) {
        }

        /* renamed from: in */
        public void mo7038in(Scope scope) {
        }

        public void asEagerSingleton() {
        }

        public ScopedBindingBuilder toProvider(Provider<? extends U> provider) {
            return NoOpAnnotatedBindingBuilder.this.scopedBindingBuilder;
        }
    }

    private static class NoOpScopedBindingBuilder implements ScopedBindingBuilder {
        private NoOpScopedBindingBuilder() {
        }

        /* renamed from: in */
        public void mo7039in(Class<? extends Annotation> cls) {
        }

        /* renamed from: in */
        public void mo7038in(Scope scope) {
        }

        public void asEagerSingleton() {
        }
    }

    public LinkedBindingBuilder<T> annotatedWith(Class<? extends Annotation> cls) {
        return this.noOpLinkedBindingBuilder;
    }

    public LinkedBindingBuilder<T> annotatedWith(Annotation annotation) {
        return this;
    }

    /* renamed from: to */
    public ScopedBindingBuilder mo7042to(Class<? extends T> cls) {
        return this.scopedBindingBuilder;
    }

    /* renamed from: to */
    public ScopedBindingBuilder mo7041to(TypeLiteral<? extends T> typeLiteral) {
        return this.scopedBindingBuilder;
    }

    /* renamed from: to */
    public ScopedBindingBuilder mo7040to(Key<? extends T> key) {
        return this.scopedBindingBuilder;
    }

    public void toInstance(T t) {
    }

    public ScopedBindingBuilder toProvider(Provider<? extends T> provider) {
        return this.scopedBindingBuilder;
    }

    public ScopedBindingBuilder toProvider(Class<? extends Provider<? extends T>> cls) {
        return this.scopedBindingBuilder;
    }

    public ScopedBindingBuilder toProvider(TypeLiteral<? extends Provider<? extends T>> typeLiteral) {
        return this.scopedBindingBuilder;
    }

    public ScopedBindingBuilder toProvider(Key<? extends Provider<? extends T>> key) {
        return this.scopedBindingBuilder;
    }

    public <S extends T> ScopedBindingBuilder toConstructor(Constructor<S> constructor) {
        return this.scopedBindingBuilder;
    }

    public <S extends T> ScopedBindingBuilder toConstructor(Constructor<S> constructor, TypeLiteral<? extends S> typeLiteral) {
        return this.scopedBindingBuilder;
    }

    /* renamed from: in */
    public void mo7039in(Class<? extends Annotation> cls) {
    }

    /* renamed from: in */
    public void mo7038in(Scope scope) {
    }

    public void asEagerSingleton() {
    }

    public ScopedBindingBuilder toProvider(Provider<? extends T> provider) {
        return this.scopedBindingBuilder;
    }
}
