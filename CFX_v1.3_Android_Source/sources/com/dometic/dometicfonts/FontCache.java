package com.dometic.dometicfonts;

import android.content.Context;
import android.graphics.Typeface;
import java.util.HashMap;
import java.util.Map;

public class FontCache {
    public static final String TYPEFACE_FOLDER = "fonts";
    private static Map<String, Typeface> fontMap = new HashMap();

    public static Typeface getFont(Context context, String fontFileName) {
        if (fontMap.containsKey(fontFileName)) {
            return (Typeface) fontMap.get(fontFileName);
        }
        Typeface tf = Typeface.createFromAsset(context.getAssets(), new StringBuilder(TYPEFACE_FOLDER).append('/').append(fontFileName).toString());
        fontMap.put(fontFileName, tf);
        return tf;
    }
}
