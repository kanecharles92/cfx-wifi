package com.dometic.dometicfonts;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomFontTextView extends TextView {
    public static final int BOLD = 1;
    public static final int BOLD_ITALIC = 3;
    public static final int ITALIC = 2;
    public static final int LIGHT = 4;
    public static final int LIGHT_ITALIC = 6;
    public static final int NORMAL = 0;
    public static final int SEMIBOLD = 5;
    public static final int SEMIBOLD_ITALIC = 7;
    private int mTextStyleExt;

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context, attrs);
    }

    public int getTextStyleExt() {
        return this.mTextStyleExt;
    }

    public void setTextStyleExt(int textStyleExt) {
        setTextStyleExt(getContext(), textStyleExt);
    }

    public void setTextStyleExt(Context context, int textStyleExt) {
        this.mTextStyleExt = textStyleExt;
        setTypeface(selectTypeface(context, this.mTextStyleExt));
    }

    /* JADX INFO: finally extract failed */
    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, C0338R.styleable.CustomFontTextView, 0, 0);
        try {
            int textStyleExt = a.getInteger(C0338R.styleable.CustomFontTextView_textStyleExt, 0);
            a.recycle();
            setTextStyleExt(context, textStyleExt);
        } catch (Throwable th) {
            a.recycle();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public Typeface selectTypeface(Context context, int textStyleExt) {
        switch (textStyleExt) {
            case 1:
                return FontCache.getFont(context, "Gibson-Bold.otf");
            case 2:
                return FontCache.getFont(context, "Gibson-Italic.otf");
            case 3:
                return FontCache.getFont(context, "Gibson-BoldItalic.otf");
            case 4:
                return FontCache.getFont(context, "Gibson-Light.otf");
            case 5:
                return FontCache.getFont(context, "Gibson-SemiBold.otf");
            case 6:
                return FontCache.getFont(context, "Gibson-LightItalic.otf");
            case 7:
                return FontCache.getFont(context, "Gibson-SemiBoldItalic.otf");
            default:
                return FontCache.getFont(context, "Gibson-Regular.otf");
        }
    }
}
