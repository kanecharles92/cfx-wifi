package com.dometic.dometicui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.p000v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class DometicSeekBar extends SeekBar {
    private static final int ANIMATION_DELAY = 100;
    private static final int ANIMATION_STEP = 5;
    public static final int HORIZONTAL_ORIENTATION = 0;
    private static final float ROTATION_ANGLE = -90.0f;
    public static final int VERTICAL_ORIENTATION = 1;
    public Runnable mAnimate = new Runnable() {
        public void run() {
            if (DometicSeekBar.this.mShowAnimation) {
                if (DometicSeekBar.this.mAnimationProgress + DometicSeekBar.this.mAnimationStep > 100) {
                    DometicSeekBar.this.mAnimationStep = -5;
                } else if (DometicSeekBar.this.mAnimationProgress + DometicSeekBar.this.mAnimationStep < 20) {
                    DometicSeekBar.this.mAnimationStep = 5;
                }
                DometicSeekBar.this.mAnimationProgress = DometicSeekBar.this.mAnimationProgress + DometicSeekBar.this.mAnimationStep;
                DometicSeekBar.this.setBackground(DometicSeekBar.this.getProgress());
                DometicSeekBar.this.mHandler.postDelayed(DometicSeekBar.this.mAnimate, 100);
            }
        }
    };
    /* access modifiers changed from: private */
    public int mAnimationProgress = 20;
    /* access modifiers changed from: private */
    public int mAnimationStep = 5;
    private int mBackgroundColor = -7829368;
    private int mDisabledColor = -12303292;
    private float mHalfStepLength;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    private boolean mIsMoving;
    private int mMaxDisplayableValue = 100;
    private int mMinDisplayableValue;
    private OnSeekBarChangeListener mOnChangeListener;
    private int mOrientation = 0;
    /* access modifiers changed from: private */
    public boolean mShowAnimation;
    private boolean mShowBalloon = true;
    private int mStartProgress;
    private float mStepLength;
    private int[] mTailColors;
    private int mTextSize = 1;
    private int mThumbColor = -7829368;
    private int mTintColor = -16711936;

    public DometicSeekBar(Context context) {
        super(context);
        init(null, 0);
    }

    public DometicSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public DometicSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    /* JADX INFO: finally extract failed */
    private void init(AttributeSet attrs, int defStyle) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, C0342R.styleable.DometicSeekBar, defStyle, 0);
        this.mOrientation = 0;
        setThumb(getResources().getDrawable(C0342R.C0343drawable.thumb_image));
        setBackgroundColor(0);
        try {
            if (a.hasValue(C0342R.styleable.DometicSeekBar_orientation)) {
                this.mOrientation = a.getInteger(C0342R.styleable.DometicSeekBar_orientation, this.mOrientation);
            }
            if (a.hasValue(C0342R.styleable.DometicSeekBar_startProgress)) {
                this.mStartProgress = a.getInteger(C0342R.styleable.DometicSeekBar_startProgress, this.mStartProgress);
            }
            if (a.hasValue(C0342R.styleable.DometicSeekBar_textSize)) {
                this.mTextSize = a.getDimensionPixelSize(C0342R.styleable.DometicSeekBar_textSize, this.mTextSize);
            }
            if (a.hasValue(C0342R.styleable.DometicSeekBar_backgroundColor)) {
                this.mBackgroundColor = a.getColor(C0342R.styleable.DometicSeekBar_backgroundColor, this.mBackgroundColor);
            }
            if (a.hasValue(C0342R.styleable.DometicSeekBar_thumbColor)) {
                this.mThumbColor = a.getColor(C0342R.styleable.DometicSeekBar_thumbColor, this.mThumbColor);
            }
            if (a.hasValue(C0342R.styleable.DometicSeekBar_tintColor)) {
                this.mTintColor = a.getColor(C0342R.styleable.DometicSeekBar_tintColor, this.mTintColor);
            }
            if (a.hasValue(C0342R.styleable.DometicSeekBar_disabledColor)) {
                this.mDisabledColor = a.getColor(C0342R.styleable.DometicSeekBar_disabledColor, this.mDisabledColor);
            }
            if (a.hasValue(C0342R.styleable.DometicSeekBar_minDisplayableValue)) {
                this.mMinDisplayableValue = a.getInteger(C0342R.styleable.DometicSeekBar_minDisplayableValue, this.mMinDisplayableValue);
            }
            if (a.hasValue(C0342R.styleable.DometicSeekBar_maxDisplayableValue)) {
                this.mMaxDisplayableValue = a.getInteger(C0342R.styleable.DometicSeekBar_maxDisplayableValue, this.mMaxDisplayableValue);
            }
            if (a.hasValue(C0342R.styleable.DometicSeekBar_showBalloon)) {
                this.mShowBalloon = a.getBoolean(C0342R.styleable.DometicSeekBar_showBalloon, this.mShowBalloon);
            }
            a.recycle();
            this.mTailColors = new int[]{this.mTintColor, 0};
            setMax(this.mMaxDisplayableValue - this.mMinDisplayableValue);
            setProgress(0);
        } catch (Throwable th) {
            a.recycle();
            throw th;
        }
    }

    public int getOrientation() {
        return this.mOrientation;
    }

    public void setOrientation(int orientation) {
        this.mOrientation = orientation;
    }

    public int getStartProgress() {
        return this.mStartProgress;
    }

    public void setStartProgress(int startProgress) {
        this.mStartProgress = startProgress;
    }

    public int getTextSize() {
        return this.mTextSize;
    }

    public void setTextSize(int textSize) {
        this.mTextSize = textSize;
    }

    public int getBackgroundColor() {
        return this.mBackgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.mBackgroundColor = backgroundColor;
    }

    public int getThumbColor() {
        return this.mThumbColor;
    }

    public void setThumbColor(int thumbColor) {
        this.mThumbColor = thumbColor;
    }

    public int getTintColor() {
        return this.mTintColor;
    }

    public void setTintColor(int tintColor) {
        this.mTintColor = tintColor;
    }

    public int getDisabledColor() {
        return this.mDisabledColor;
    }

    public void setDisabledColor(int disabledColor) {
        this.mDisabledColor = disabledColor;
    }

    public int getMinDisplayableValue() {
        return this.mMinDisplayableValue;
    }

    public void setMinDisplayableValue(int minDisplayableValue) {
        this.mMinDisplayableValue = minDisplayableValue;
    }

    public int getMaxDisplayableValue() {
        return this.mMaxDisplayableValue;
    }

    public void setMaxDisplayableValue(int maxDisplayableValue) {
        this.mMaxDisplayableValue = maxDisplayableValue;
    }

    public boolean getShowBalloon() {
        return this.mShowBalloon;
    }

    public void setShowBalloon(boolean showBalloon) {
        this.mShowBalloon = showBalloon;
    }

    public void setMinMaxStartProgressValues(int minDisplayableValue, int maxDisplayableValue, int startProgress, int progress) {
        this.mMinDisplayableValue = minDisplayableValue;
        this.mMaxDisplayableValue = maxDisplayableValue;
        this.mStartProgress = startProgress;
        setMax(this.mMaxDisplayableValue - this.mMinDisplayableValue);
        setProgress(progress);
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.mOrientation == 1) {
            super.onMeasure(heightMeasureSpec, widthMeasureSpec);
            setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
        }
        calculateSteps();
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        if (this.mOrientation == 1) {
            super.onSizeChanged(height, width, oldHeight, oldWidth);
        } else {
            super.onSizeChanged(width, height, oldWidth, oldHeight);
        }
        calculateSteps();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.mOrientation == 1) {
            canvas.rotate(ROTATION_ANGLE);
            canvas.translate((float) (-getHeight()), 0.0f);
        }
        super.onDraw(canvas);
    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (isEnabled()) {
            float axisPosition = 0.0f;
            int action = event.getAction();
            if (action == 0 || action == 2 || action == 1) {
                axisPosition = this.mOrientation == 1 ? event.getY() : event.getX();
            }
            switch (action) {
                case 0:
                    this.mIsMoving = true;
                    setProgressInternally(translateEventToProgress(axisPosition));
                    fireOnStartTrackingTouch();
                    setPressed(true);
                    setSelected(true);
                    break;
                case 1:
                    this.mIsMoving = false;
                    setProgressInternally(translateEventToProgress(axisPosition));
                    fireOnStopTrackingTouch();
                    setPressed(false);
                    setSelected(false);
                    break;
                case 2:
                    setProgressInternally(translateEventToProgress(axisPosition));
                    setPressed(true);
                    setSelected(true);
                    break;
                case 3:
                    this.mIsMoving = false;
                    super.onTouchEvent(event);
                    fireOnStopTrackingTouch();
                    setPressed(false);
                    setSelected(false);
                    break;
            }
        }
        return true;
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener onChangeListener) {
        this.mOnChangeListener = onChangeListener;
    }

    public OnSeekBarChangeListener getOnSeekBarChangeListener() {
        return this.mOnChangeListener;
    }

    public synchronized void setProgress(int progress) {
        setProgressInternally(progress);
    }

    public void startAnimation() {
        if (this.mShowAnimation) {
            this.mHandler.removeCallbacks(this.mAnimate);
        }
        this.mShowAnimation = true;
        this.mAnimationProgress = 20;
        this.mHandler.postDelayed(this.mAnimate, 100);
    }

    public void stopAnimation() {
        if (this.mShowAnimation) {
            this.mShowAnimation = false;
            this.mAnimationProgress = 20;
            this.mHandler.removeCallbacks(this.mAnimate);
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(21)
    public void setBackground(int progress) {
        Bitmap balloonBmp;
        int[] iArr;
        int progress2 = progress + this.mMinDisplayableValue;
        int startProgress = this.mStartProgress + this.mMinDisplayableValue;
        if (!this.mIsMoving || !this.mShowBalloon) {
            balloonBmp = null;
        } else {
            balloonBmp = BitmapFactory.decodeResource(getResources(), C0342R.mipmap.cfx_android_ikoner_23);
        }
        if (isEnabled()) {
            iArr = this.mTailColors;
        } else {
            iArr = null;
        }
        setBackground(new TailDrawable(progress2, startProgress, iArr, isEnabled() ? this.mBackgroundColor : this.mDisabledColor, this.mOrientation == 1, this.mMinDisplayableValue, this.mMaxDisplayableValue, (int) Utils.pxFromDp(getContext(), 36.0f), (int) Utils.pxFromDp(getContext(), 24.0f), balloonBmp, this.mTextSize, true, this.mAnimationProgress));
    }

    @TargetApi(21)
    private void setSeekBarValue(int progress) {
        setBackground(progress);
        int progress2 = progress + this.mMinDisplayableValue;
        int startProgress = this.mStartProgress + this.mMinDisplayableValue;
        Drawable thumb = ContextCompat.getDrawable(getContext(), isEnabled() ? C0342R.C0343drawable.thumb_ring_progress : C0342R.C0343drawable.thumb_ring_progress_disabled);
        Drawable thumbOverlay = null;
        Drawable newThumb = thumb;
        if ((progress2 == startProgress && !this.mIsMoving) || !isEnabled()) {
            setBackgroundColor(isEnabled() ? this.mBackgroundColor : this.mDisabledColor);
            thumbOverlay = ContextCompat.getDrawable(getContext(), isEnabled() ? C0342R.C0343drawable.thumb_filled_ring_progress : C0342R.C0343drawable.thumb_filled_ring_progress_disabled);
        } else if (!this.mIsMoving || !this.mShowBalloon) {
            thumbOverlay = new TextDrawable(String.valueOf(progress2), (float) this.mTextSize, this.mTailColors[0], this.mOrientation == 1);
        }
        if (thumbOverlay != null) {
            newThumb = createLayerDrawable(thumb, thumbOverlay);
        }
        if (newThumb != null) {
            newThumb.setBounds(0, 0, thumb.getIntrinsicWidth(), thumb.getIntrinsicHeight());
            setThumb(newThumb);
        }
        setThumbOffset(0);
        setPadding(0, 0, 0, 0);
        drawableStateChanged();
    }

    @TargetApi(23)
    private LayerDrawable createLayerDrawable(Drawable thumb, TextDrawable textDrawable) {
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{thumb, textDrawable});
        if (VERSION.SDK_INT >= 23) {
            layerDrawable.setLayerGravity(0, 17);
            layerDrawable.setLayerGravity(1, 17);
            layerDrawable.setLayerWidth(0, thumb.getIntrinsicWidth());
            layerDrawable.setLayerWidth(1, thumb.getIntrinsicWidth());
            layerDrawable.setLayerHeight(0, thumb.getIntrinsicWidth());
            layerDrawable.setLayerHeight(1, thumb.getIntrinsicWidth());
        }
        return layerDrawable;
    }

    @TargetApi(23)
    private LayerDrawable createLayerDrawable(Drawable thumb, Drawable filled) {
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{thumb, filled});
        if (VERSION.SDK_INT >= 23) {
            layerDrawable.setLayerGravity(0, 17);
            layerDrawable.setLayerGravity(1, 17);
        }
        return layerDrawable;
    }

    private int adjustProgressWithinLimits(int progress) {
        if (progress < 0) {
            return 0;
        }
        int max = getMax();
        if (progress <= max) {
            return progress;
        }
        return max;
    }

    private final void setProgressInternally(int progress) {
        int adjustedProgress = adjustProgressWithinLimits(progress);
        setSeekBarValue(adjustedProgress);
        if (adjustedProgress != getProgress()) {
            super.setProgress(adjustedProgress);
            fireOnProgressChanged(adjustedProgress);
        }
        onSizeChanged(getWidth(), getHeight(), 0, 0);
    }

    private void fireOnStartTrackingTouch() {
        if (this.mOnChangeListener != null) {
            this.mOnChangeListener.onStartTrackingTouch(this);
        }
    }

    private void fireOnProgressChanged(int progress) {
        if (this.mOnChangeListener != null) {
            this.mOnChangeListener.onProgressChanged(this, progress, true);
        }
    }

    private void fireOnStopTrackingTouch() {
        if (this.mOnChangeListener != null) {
            this.mOnChangeListener.onStopTrackingTouch(this);
        }
    }

    private int translateEventToProgress(float eventAxisPosition) {
        float newAxisPosition = ((float) (this.mOrientation == 1 ? getHeight() : getWidth())) - eventAxisPosition;
        int progress = (int) (newAxisPosition / this.mStepLength);
        if (((float) ((int) (newAxisPosition % this.mStepLength))) > this.mHalfStepLength) {
            return progress + 1;
        }
        return progress;
    }

    private void calculateSteps() {
        this.mStepLength = (float) ((this.mOrientation == 1 ? getHeight() : getWidth()) / getMax());
        this.mHalfStepLength = this.mStepLength / 2.0f;
    }
}
