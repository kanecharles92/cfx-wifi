package com.dometic.dometicui;

public class Temperature {
    private static final int CELSIUS_TO_FAHRENHEIT_ZERO_CONSTANT = 0;
    private static final String CELSIUS_UNIT = "°C";
    private static final int CONVERSION_CONSTANT = 32;
    private static final double CONVERSION_FACTOR = 1.8d;
    private static final String FAHRENHEIT_UNIT = "°F";

    public static String getTemperatureUnitString(boolean useCelsius) {
        return useCelsius ? "°C" : "°F";
    }

    public static String convertTemperatureValueToString(boolean useCelsius, int value) {
        return convertTemperatureToString(useCelsius, value, 32);
    }

    public static String convertTemperatureUnitToString(boolean useCelsius, int value) {
        return convertTemperatureToString(useCelsius, value, 0);
    }

    public static int convertCelsiusToFahrenheit(int value) {
        return (int) ((CONVERSION_FACTOR * ((double) value)) + 32.0d);
    }

    public static int convertFahrenheitToCelsius(int value) {
        return (int) (((double) (value - 32)) / CONVERSION_FACTOR);
    }

    private static String convertTemperatureToString(boolean useCelsius, int value, int additionalConstant) {
        if (useCelsius) {
            return String.valueOf(value);
        }
        return String.format("%.1f", new Object[]{Double.valueOf((CONVERSION_FACTOR * ((double) value)) + ((double) additionalConstant))});
    }
}
