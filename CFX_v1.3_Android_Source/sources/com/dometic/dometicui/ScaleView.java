package com.dometic.dometicui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

public class ScaleView extends View {
    private int[] mIntValues;
    private int mMax = 100;
    private int mMin = 0;
    private int mScaleColor = -7829368;
    private float mTextHeight;
    private TextPaint mTextPaint;
    private float mTextVerticalOffset;
    private float[] mTextWidths;
    private String mValues;
    private int mZeroValue;

    public ScaleView(Context context) {
        super(context);
        init(null, 0);
    }

    public ScaleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ScaleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, C0342R.styleable.ScaleView, defStyle, 0);
        if (a.hasValue(C0342R.styleable.ScaleView_scaleColor)) {
            this.mScaleColor = a.getColor(C0342R.styleable.ScaleView_scaleColor, this.mScaleColor);
        }
        this.mMin = a.getInteger(C0342R.styleable.ScaleView_min, 0);
        this.mMax = a.getInteger(C0342R.styleable.ScaleView_max, 100);
        this.mValues = a.getString(C0342R.styleable.ScaleView_values);
        initValues(this.mValues);
        a.recycle();
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setStyle(Style.FILL);
        this.mTextPaint.setFlags(1);
        this.mTextPaint.setTextAlign(Align.RIGHT);
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setColor(this.mScaleColor);
        invalidateTextPaintAndMeasurements();
    }

    private void invalidateTextPaintAndMeasurements() {
        this.mTextPaint.setTextSize(60.0f);
        if (this.mIntValues != null) {
            this.mTextWidths = new float[this.mIntValues.length];
            for (int i = 0; i < this.mTextWidths.length; i++) {
                this.mTextWidths[i] = this.mTextPaint.measureText(String.valueOf(this.mIntValues[i]));
            }
        }
        FontMetrics fontMetrics = this.mTextPaint.getFontMetrics();
        this.mTextHeight = -fontMetrics.ascent;
        this.mTextVerticalOffset = (float) Math.abs(Math.round(fontMetrics.top - fontMetrics.ascent));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        String text;
        super.onDraw(canvas);
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int contentHeight = (getHeight() - paddingTop) - getPaddingBottom();
        int range = this.mMax - this.mMin;
        float maxWidth = 0.0f;
        for (int i = 0; i < this.mTextWidths.length; i++) {
            if (maxWidth < this.mTextWidths[i]) {
                maxWidth = this.mTextWidths[i];
            }
        }
        float x = ((float) paddingLeft) + maxWidth;
        for (int i2 = this.mIntValues.length - 1; i2 > -1; i2--) {
            if (this.mIntValues[i2] == this.mZeroValue) {
                text = " ";
            } else {
                text = String.valueOf(this.mIntValues[i2]);
            }
            float y = (((float) ((((this.mMax - this.mIntValues[i2]) * contentHeight) / range) + paddingTop)) + (this.mTextHeight / 2.0f)) - this.mTextVerticalOffset;
            canvas.drawText(text, x, y, this.mTextPaint);
            canvas.drawText("-", 40.0f + x, y, this.mTextPaint);
        }
    }

    public int getScaleColor() {
        return this.mScaleColor;
    }

    public void setScaleColor(int scaleColor) {
        this.mScaleColor = scaleColor;
        if (this.mTextPaint != null) {
            this.mTextPaint.setColor(this.mScaleColor);
        }
    }

    public int getMin() {
        return this.mMin;
    }

    public void setMin(int min) {
        this.mMin = min;
        if (this.mIntValues != null && this.mIntValues.length > 0 && this.mIntValues[0] == this.mMin) {
            invalidateTextPaintAndMeasurements();
        }
    }

    public int getMax() {
        return this.mMin;
    }

    public void setMax(int max) {
        this.mMax = max;
        if (this.mIntValues != null && this.mIntValues.length > 0 && this.mIntValues[this.mIntValues.length - 1] == this.mMax) {
            invalidateTextPaintAndMeasurements();
        }
    }

    public String getValues() {
        return this.mValues;
    }

    public void setValues(String values) {
        initValues(values);
        invalidateTextPaintAndMeasurements();
    }

    public void setMinMaxValues(int min, int max, String values) {
        this.mMin = min;
        this.mMax = max;
        initValues(values);
        invalidateTextPaintAndMeasurements();
    }

    private void initValues(String values) {
        int i = 0;
        this.mValues = values;
        if (this.mValues != null) {
            String vals = this.mValues;
            if (vals.contains(";")) {
                String[] v = vals.split(";");
                if (v == null || v.length <= 1) {
                    vals = "";
                } else {
                    vals = v[1];
                    this.mZeroValue = Integer.parseInt(v[0]);
                }
            }
            String[] strValues = vals.split(",");
            this.mIntValues = new int[strValues.length];
            int length = strValues.length;
            int i2 = 0;
            while (i < length) {
                int i3 = i2 + 1;
                this.mIntValues[i2] = Integer.parseInt(strValues[i]);
                i++;
                i2 = i3;
            }
        }
    }
}
