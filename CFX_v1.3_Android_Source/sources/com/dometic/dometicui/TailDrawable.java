package com.dometic.dometicui;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;

public class TailDrawable extends Drawable {
    private static final int BAR_THICKNESS = 24;
    static final int MIN_ANIMATION_PROGRESS = 20;
    private static final float RADIUS = 0.0f;
    private int mAnimationProgress;
    private int mBackgroundColor;
    private Bitmap mBalloonBmp;
    private int mBarThickness = 24;
    private int[] mColors;
    private boolean mIsVertical;
    private int mMaxProgress;
    private int mMinProgress;
    private Paint mPaint;
    private int mProgress;
    private int mRange;
    private int mStartProgress;
    private int mTextSize;
    private int mThumbLength = 24;

    public TailDrawable(int progress, int startProgress, int[] colors, int backgroundColor, boolean isVertical, int minProgress, int maxProgress, int thumbLength, int barThickness, Bitmap balloonBmp, int textSize, boolean usePulse, int animationProgress) {
        if (startProgress < minProgress) {
            startProgress = minProgress;
        }
        if (startProgress > maxProgress) {
            startProgress = maxProgress;
        }
        this.mProgress = progress;
        this.mStartProgress = startProgress;
        this.mColors = colors;
        this.mBackgroundColor = backgroundColor;
        this.mIsVertical = isVertical;
        this.mMinProgress = Math.min(minProgress, maxProgress);
        this.mMaxProgress = Math.max(minProgress, maxProgress);
        this.mThumbLength = thumbLength;
        this.mBarThickness = barThickness;
        this.mBalloonBmp = balloonBmp;
        this.mTextSize = textSize;
        this.mAnimationProgress = animationProgress;
        this.mRange = this.mMaxProgress - this.mMinProgress;
        this.mPaint = new Paint();
        this.mPaint.setColor(this.mBackgroundColor);
        this.mPaint.setStyle(Style.FILL_AND_STROKE);
        this.mPaint.setStrokeWidth(0.0f);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setDither(true);
        this.mPaint.setFilterBitmap(true);
        if (this.mStartProgress < this.mMinProgress) {
            this.mStartProgress = this.mMinProgress;
        }
        if (this.mStartProgress > this.mMaxProgress) {
            this.mStartProgress = this.mMaxProgress;
        }
        if (this.mProgress < this.mMinProgress) {
            this.mProgress = this.mMinProgress;
        }
        if (this.mProgress > this.mMaxProgress) {
            this.mProgress = this.mMaxProgress;
        }
        if (this.mAnimationProgress > 100) {
            this.mAnimationProgress = 100;
        }
        if (this.mAnimationProgress < 20) {
            this.mAnimationProgress = 20;
        }
    }

    @TargetApi(16)
    public void draw(Canvas canvas) {
        Orientation orientation;
        Rect rect;
        if (this.mRange != 0) {
            Rect bounds = getBounds();
            Rect rect2 = new Rect(bounds.left, bounds.top, bounds.right, bounds.bottom);
            int boundsThickness = this.mIsVertical ? bounds.width() : bounds.height();
            int inset = (boundsThickness > this.mBarThickness ? boundsThickness - this.mBarThickness : 0) / 2;
            RectF rectf = new RectF((float) bounds.left, (float) (bounds.top + inset), (float) bounds.right, (float) (bounds.bottom - inset));
            if (this.mIsVertical) {
                rectf = new RectF((float) (bounds.left + inset), (float) bounds.top, (float) (bounds.right - inset), (float) bounds.bottom);
            }
            float ry = (this.mIsVertical ? rectf.right - rectf.left : rectf.bottom - rectf.top) / 2.0f;
            canvas.drawRoundRect(rectf, ry, ry, this.mPaint);
            int barLength = this.mIsVertical ? rect2.height() : rect2.width();
            int thumbRangeLength = barLength - this.mThumbLength;
            double startValue = ((double) ((this.mMaxProgress - this.mStartProgress) * barLength)) / ((double) this.mRange);
            double targetValue = ((double) ((this.mMaxProgress - this.mProgress) * barLength)) / ((double) this.mRange);
            int balloonPositionX = 0;
            int balloonPositionY = 0;
            if (this.mIsVertical) {
                targetValue = ((double) ((this.mMaxProgress - this.mProgress) * thumbRangeLength)) / ((double) this.mRange);
            }
            if (this.mBalloonBmp != null) {
                balloonPositionX = (boundsThickness - this.mBalloonBmp.getWidth()) / 2;
                balloonPositionY = ((int) Math.round(targetValue)) - this.mBalloonBmp.getHeight();
            }
            if (this.mColors == null || this.mColors.length < 1 || this.mStartProgress <= this.mProgress) {
                drawBalloon(canvas, balloonPositionX, balloonPositionY);
                return;
            }
            if (this.mIsVertical) {
                if (this.mStartProgress > this.mProgress) {
                    targetValue += (double) this.mThumbLength;
                }
                if (targetValue < startValue) {
                    Rect rect3 = new Rect(rect2.left, (int) Math.round(targetValue), rect2.right, (int) Math.round(startValue));
                    orientation = Orientation.TOP_BOTTOM;
                    rect = rect3;
                } else {
                    Rect rect4 = new Rect(rect2.left, (int) Math.round(startValue), rect2.right, (int) Math.round(targetValue));
                    orientation = Orientation.BOTTOM_TOP;
                    rect = rect4;
                }
                rect.left += inset;
                rect.right -= inset;
            } else if (targetValue <= startValue) {
                Rect rect5 = new Rect((int) Math.round(targetValue), rect2.top, (int) Math.round(startValue), rect2.bottom);
                orientation = Orientation.RIGHT_LEFT;
                rect = rect5;
            } else {
                Rect rect6 = new Rect((int) Math.round(startValue), rect2.top, (int) Math.round(targetValue), rect2.bottom);
                orientation = Orientation.LEFT_RIGHT;
                rect = rect6;
            }
            GradientDrawable gd = new GradientDrawable(orientation, this.mColors);
            float r1 = 0.0f;
            float r2 = 0.0f;
            if (this.mStartProgress == this.mMinProgress) {
                r1 = ((float) (this.mIsVertical ? rect.right - rect.left : rect.bottom - rect.top)) / 2.0f;
            } else {
                if (this.mStartProgress == this.mMaxProgress) {
                    r2 = ((float) (this.mIsVertical ? rect.right - rect.left : rect.bottom - rect.top)) / 2.0f;
                }
            }
            if (this.mProgress > this.mStartProgress) {
                r2 = ((float) (this.mIsVertical ? rect.right - rect.left : rect.bottom - rect.top)) / 2.0f;
            } else {
                if (this.mProgress < this.mStartProgress) {
                    r1 = ((float) (this.mIsVertical ? rect.right - rect.left : rect.bottom - rect.top)) / 2.0f;
                }
            }
            gd.setCornerRadii(new float[]{r2, r2, r2, r2, r1, r1, r1, r1});
            gd.setShape(0);
            gd.setAlpha((this.mAnimationProgress * 255) / 100);
            gd.setBounds(rect);
            gd.draw(canvas);
            drawBalloon(canvas, balloonPositionX, balloonPositionY);
        }
    }

    public void setAlpha(int alpha) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public int getOpacity() {
        return -1;
    }

    private void drawBalloon(Canvas canvas, int x, int y) {
        if (this.mBalloonBmp != null) {
            canvas.drawBitmap(this.mBalloonBmp, (float) x, (float) y, this.mPaint);
            TextDrawable textDrawable = new TextDrawable(String.valueOf(this.mProgress), (float) this.mTextSize, -1, false);
            textDrawable.setBounds(x, y, this.mBalloonBmp.getWidth() + x, this.mBalloonBmp.getHeight() + y);
            textDrawable.draw(canvas);
        }
    }
}
