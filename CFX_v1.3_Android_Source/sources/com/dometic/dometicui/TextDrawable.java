package com.dometic.dometicui;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class TextDrawable extends Drawable {
    private static final float TEXT_ROTATION = 270.0f;
    private int mIntrinsicHeight;
    private int mIntrinsicWidth;
    private boolean mIsVertical;
    private final Paint mPaint = new Paint(1);
    private final CharSequence mText;

    public TextDrawable(CharSequence text, float textSize, int textColor, boolean isVertical) {
        this.mText = text;
        this.mPaint.setColor(textColor);
        this.mPaint.setTextAlign(Align.CENTER);
        this.mPaint.setTextSize(textSize);
        this.mIntrinsicWidth = (int) Math.ceil((double) this.mPaint.measureText(this.mText, 0, this.mText.length()));
        this.mIntrinsicHeight = this.mPaint.getFontMetricsInt(null);
        this.mIsVertical = isVertical;
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        if (this.mIsVertical) {
            canvas.rotate(-270.0f);
            Canvas canvas2 = canvas;
            canvas2.drawText(this.mText, 0, this.mText.length(), (float) bounds.centerY(), (float) ((((double) bounds.height()) - (0.75d * ((double) this.mIntrinsicHeight))) - ((double) bounds.centerX())), this.mPaint);
            return;
        }
        canvas.drawText(this.mText, 0, this.mText.length(), (float) bounds.centerX(), (float) bounds.centerY(), this.mPaint);
    }

    public int getOpacity() {
        return this.mPaint.getAlpha();
    }

    public int getIntrinsicWidth() {
        return this.mIntrinsicWidth;
    }

    public int getIntrinsicHeight() {
        return this.mIntrinsicHeight;
    }

    public void setAlpha(int alpha) {
        this.mPaint.setAlpha(alpha);
    }

    public void setColorFilter(ColorFilter filter) {
        this.mPaint.setColorFilter(filter);
    }
}
