package com.dometic.dometicui;

import android.content.Context;

public class Utils {
    public static float pxFromDp(Context context, float dp) {
        return context.getResources().getDisplayMetrics().density * dp;
    }
}
