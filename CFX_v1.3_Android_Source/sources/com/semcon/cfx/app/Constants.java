package com.semcon.cfx.app;

public class Constants {
    public static final String CELSIUS_DEGREES = "°C";
    public static final String FAHRENHEIT_DEGREES = "°F";
    public static final String OFF = "off";

    /* renamed from: ON */
    public static final String f31ON = "on";

    public enum TemperatureUnit {
        Celsius,
        Fahrenheit
    }
}
