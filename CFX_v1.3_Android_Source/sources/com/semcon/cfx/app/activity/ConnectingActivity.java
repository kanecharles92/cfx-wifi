package com.semcon.cfx.app.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.p003v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.google.inject.Inject;
import com.semcon.cfx.app.BuildConfig;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;
import com.zhenbang.project_532.R;
import java.io.IOException;
import java.util.Date;

public class ConnectingActivity extends RoboAppCompatActivity {
    @Inject
    ICfxCommunication mCfxCommunication;
    private ConnectTask mTask;

    class ConnectTask extends AsyncTask<Void, Void, Boolean> {
        ConnectTask() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            Boolean connected = Boolean.valueOf(false);
            do {
                try {
                    ConnectingActivity.this.mCfxCommunication.disconnect();
                    ConnectingActivity.this.mCfxCommunication.connect();
                    Log.d("Connection", "New connect thread: " + getClass().getSimpleName());
                    Thread.sleep(1000);
                    connected = Boolean.valueOf(ConnectingActivity.this.mCfxCommunication.isConnected());
                    Log.d("Connection", "Was connected: " + connected);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            } while (!connected.booleanValue());
            Log.d("Connection", "Next was connected: " + connected);
            Log.d("Connection", "Found network: " + getClass().getSimpleName());
            ConnectingActivity.this.startActivity(new Intent(ConnectingActivity.this.getApplicationContext(), AppMainActivity.class));
            ConnectingActivity.this.finish();
            return Boolean.valueOf(true);
        }

        private void startNextActivity(Class<?> cls) {
            ConnectingActivity.this.startActivity(new Intent(ConnectingActivity.this.getApplicationContext(), cls));
            ConnectingActivity.this.finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Log.d("Connection", "Starting connection activity from " + getClass().getSimpleName());
        setContentView((int) R.layout.activity_connecting);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((Button) findViewById(R.id.connection_guide_btn)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ConnectingActivity.this.startActivity(new Intent(ConnectingActivity.this.getApplicationContext(), WelcomeActivity.class));
                ConnectingActivity.this.finish();
            }
        });
        TextView build = (TextView) findViewById(R.id.connecting_build);
        ((TextView) findViewById(R.id.connecting_version)).append(": 1.3");
        build.append(": " + new Date(BuildConfig.TIMESTAMP));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        tryConnect(false);
    }

    private synchronized boolean tryConnect(boolean endTrying) {
        boolean z = true;
        synchronized (this) {
            Log.d("Connection", "New connect task from: " + getClass().getSimpleName());
            if (endTrying) {
                this.mTask = null;
            } else if (this.mTask == null) {
                this.mTask = new ConnectTask();
                this.mTask.execute(new Void[0]);
            } else {
                z = false;
            }
        }
        return z;
    }
}
