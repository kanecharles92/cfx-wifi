package com.semcon.cfx.app.activity;

import android.os.Bundle;
import android.support.p000v4.app.Fragment;
import android.support.p003v7.widget.Toolbar;
import com.google.inject.Inject;
import com.semcon.cfx.app.fragment.ConnectionFailedFragment;
import com.semcon.cfx.app.fragment.ConnectionSuccessFragment;
import com.semcon.cfx.app.fragment.ConnectionSuccessFragment.OnDoneListener;
import com.semcon.cfx.app.fragment.GuideFragment;
import com.semcon.cfx.app.fragment.GuideFragment.OnConnectionListener;
import com.semcon.cfx.app.persistent.CfxAppSharedPreferencesHelper;
import com.semcon.cfx.app.wifi.WifiHandler;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;
import com.zhenbang.project_532.R;
import roboguice.inject.ContentView;

@ContentView(2131361823)
public class CfxAppGuideActivity extends RoboAppCompatActivity implements OnConnectionListener, OnDoneListener {
    public static final int CONNECTION_FAILED = 1;
    public static final int CONNECTION_SUCCESS = 0;
    @Inject
    private CfxAppSharedPreferencesHelper mSharedPreferencesHelper;
    @Inject
    private WifiHandler mWifiHandler;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GuideFragment()).commit();
        setSupportActionBar((Toolbar) findViewById(R.id.app_toolbar));
    }

    public void onConnectionSuccess() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ConnectionSuccessFragment()).commit();
    }

    public void onConnectionFailed() {
        getSupportFragmentManager().beginTransaction().add((int) R.id.fragment_container, (Fragment) new ConnectionFailedFragment()).addToBackStack(null).commitAllowingStateLoss();
    }

    public void onDone() {
        setResult(0);
        finish();
    }
}
