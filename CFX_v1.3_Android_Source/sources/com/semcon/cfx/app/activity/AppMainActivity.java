package com.semcon.cfx.app.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.p000v4.app.Fragment;
import android.support.p000v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.p000v4.widget.DrawerLayout;
import android.support.p003v7.app.ActionBarDrawerToggle;
import android.support.p003v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.semcon.cfx.app.BuildConfig;
import com.semcon.cfx.app.fragment.DeleteBoxFragment;
import com.semcon.cfx.app.wifi.WifiHandler;
import com.semcon.dometic.cfx.IConnectableActivity;
import com.semcon.dometic.cfx.alarm.ErrorsDialogFragment;
import com.semcon.dometic.cfx.fragment.BoxControlFragment;
import com.semcon.dometic.cfx.fragment.BoxControlFragment.OnToggleConnectionListener;
import com.semcon.dometic.cfx.fragment.BoxSettingsFragment;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;
import com.zhenbang.project_532.R;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import roboguice.event.EventManager;
import roboguice.inject.ContentView;

@ContentView(2131361827)
public class AppMainActivity extends RoboAppCompatActivity implements OnToggleConnectionListener, IConnectableActivity {
    private DrawerLayout mDrawer;
    /* access modifiers changed from: private */
    public ActionBarDrawerToggle mDrawerToggle;
    @Named("GlobalEventManager")
    @Inject
    private EventManager mGlobalEventManager;
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message inputMessage) {
            if (AppMainActivity.this.mLastSelectedMenuItem != null) {
                AppMainActivity.this.mLastSelectedMenuItem.setChecked(true);
                AppMainActivity.this.mNavDrawer.setCheckedItem(AppMainActivity.this.mLastSelectedMenuItem.getItemId());
            }
        }
    };
    /* access modifiers changed from: private */
    public MenuItem mLastSelectedMenuItem;
    /* access modifiers changed from: private */
    public NavigationView mNavDrawer;
    private OnBackStackChangedListener mOnBackStackChangedListener = new OnBackStackChangedListener() {
        public void onBackStackChanged() {
            AppMainActivity.this.syncActionBarArrowState();
        }
    };
    private String mSSID;
    private Toolbar mToolbar;
    @Inject
    private WifiHandler mWifiHandler;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Main app", "Starting main activity");
        this.mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.mDrawerToggle = setupDrawerToggle();
        this.mDrawer.addDrawerListener(this.mDrawerToggle);
        getSupportFragmentManager().addOnBackStackChangedListener(this.mOnBackStackChangedListener);
        this.mNavDrawer = (NavigationView) findViewById(R.id.nvView);
        setupDrawerContent(this.mNavDrawer);
        selectDrawerItem(getBoxControlItem());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        getSupportFragmentManager().removeOnBackStackChangedListener(this.mOnBackStackChangedListener);
        super.onDestroy();
    }

    public void toggleCompartments(View v) {
        if (((Switch) v).isChecked()) {
            Toast.makeText(this, "Compartment is 2", 0).show();
        } else {
            Toast.makeText(this, "Compartment is 1", 0).show();
        }
    }

    public void toggleTempUnit(View v) {
        if (((Switch) v).isChecked()) {
            Toast.makeText(this, "Temp is F", 0).show();
        } else {
            Toast.makeText(this, "Temp is C", 0).show();
        }
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, this.mDrawer, 0, 0) {
            public void onDrawerClosed(View view) {
                AppMainActivity.this.syncActionBarArrowState();
            }

            public void onDrawerOpened(View drawerView) {
                AppMainActivity.this.mDrawerToggle.setDrawerIndicatorEnabled(true);
                TextView build = (TextView) AppMainActivity.this.findViewById(R.id.build_tv);
                ((TextView) AppMainActivity.this.findViewById(R.id.version_tv)).setText(BuildConfig.VERSION_NAME);
                build.setText("" + new Date(BuildConfig.TIMESTAMP));
            }
        };
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.mDrawerToggle.syncState();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.mDrawerToggle.isDrawerIndicatorEnabled() && this.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() != 16908332 || !getSupportFragmentManager().popBackStackImmediate()) {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void onDisconnect() {
        if (this.mSSID != null) {
            this.mWifiHandler.disconnect(this.mSSID);
        }
    }

    public void onConnect() {
        if (this.mSSID != null) {
            this.mWifiHandler.connectToWifi(this, this.mSSID);
        }
    }

    public void onBackPressed() {
        MenuItem selectedItem = getCheckedItem();
        MenuItem boxControlItem = getBoxControlItem();
        if (selectedItem == null || boxControlItem == null) {
            super.onBackPressed();
        } else if (selectedItem.getItemId() == boxControlItem.getItemId()) {
            super.onBackPressed();
        } else if (this.mDrawerToggle.isDrawerIndicatorEnabled()) {
            selectDrawerItem(boxControlItem);
        } else {
            super.onBackPressed();
        }
    }

    public Class<?> getConnectingActivityClass() {
        return ConnectingActivity.class;
    }

    private MenuItem getCheckedItem() {
        if (this.mNavDrawer != null) {
            Menu menu = this.mNavDrawer.getMenu();
            if (menu != null) {
                for (int i = 0; i < menu.size(); i++) {
                    MenuItem item = menu.getItem(i);
                    if (item != null && item.isChecked()) {
                        return item;
                    }
                }
            }
        }
        return null;
    }

    private MenuItem getBoxControlItem() {
        if (this.mNavDrawer != null) {
            Menu menu = this.mNavDrawer.getMenu();
            if (menu != null) {
                for (int i = 0; i < menu.size(); i++) {
                    MenuItem item = menu.getItem(i);
                    if (item != null && item.getItemId() == R.id.nav_first_fragment) {
                        return item;
                    }
                }
            }
        }
        return null;
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new OnNavigationItemSelectedListener() {
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                AppMainActivity.this.selectDrawerItem(menuItem);
                return true;
            }
        });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        if (menuItem != null) {
            if (this.mLastSelectedMenuItem == null || menuItem.getItemId() != this.mLastSelectedMenuItem.getItemId()) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_first_fragment /*2131230876*/:
                        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new BoxControlFragment()).commit();
                        break;
                    case R.id.nav_settings_fragment /*2131230877*/:
                        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new BoxSettingsFragment()).commitAllowingStateLoss();
                        break;
                    default:
                        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new BoxControlFragment()).commitAllowingStateLoss();
                        break;
                }
                if (this.mLastSelectedMenuItem != null) {
                    this.mLastSelectedMenuItem.setChecked(false);
                }
                this.mLastSelectedMenuItem = menuItem;
                menuItem.setChecked(true);
                setTitle(menuItem.getTitle());
                this.mDrawer.closeDrawers();
                return;
            }
            menuItem.setChecked(true);
        }
    }

    public void onWarning(View view) {
        BoxControlFragment boxControlFragment = null;
        Iterator it = getSupportFragmentManager().getFragments().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Fragment fragment = (Fragment) it.next();
            if (fragment instanceof BoxControlFragment) {
                boxControlFragment = (BoxControlFragment) fragment;
                break;
            }
        }
        if (boxControlFragment != null) {
            List<CFXError> errors = boxControlFragment.getCurrentErrors();
            Log.d("ERROR: ", "Size: " + errors.size());
            if (errors != null && errors.size() > 0) {
                ArrayList<String> errs = new ArrayList<>(errors.size());
                ErrorsDialogFragment dialog = new ErrorsDialogFragment();
                Bundle args = new Bundle();
                args.putStringArrayList(ErrorsDialogFragment.ERRORS_ARRAY, errs);
                dialog.setArguments(args);
                dialog.show(getSupportFragmentManager(), "errors_dialog");
            }
        }
    }

    public void confirmDeleteBox() {
        new DeleteBoxFragment().show(getFragmentManager(), "delete");
    }

    /* access modifiers changed from: private */
    public void syncActionBarArrowState() {
        this.mDrawerToggle.setDrawerIndicatorEnabled(getSupportFragmentManager().getBackStackEntryCount() == 0);
    }
}
