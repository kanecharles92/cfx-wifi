package com.semcon.cfx.app.activity;

import android.os.Bundle;
import android.support.p003v7.widget.Toolbar;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;
import com.zhenbang.project_532.R;

public class WaitingForBoxActivity extends RoboAppCompatActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fragment_connecting_error);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
    }
}
