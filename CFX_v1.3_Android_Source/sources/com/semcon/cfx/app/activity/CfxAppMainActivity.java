package com.semcon.cfx.app.activity;

import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.p003v7.app.ActionBar;
import android.support.p003v7.widget.Toolbar;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.semcon.cfx.app.fragment.GuideFragment;
import com.semcon.cfx.app.persistent.CfxAppSharedPreferencesHelper;
import com.semcon.cfx.app.wifi.WifiHandler;
import com.semcon.dometic.cfx.CfxAppSettingsActivity;
import com.semcon.dometic.cfx.FAQActivity;
import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.data.Compartment;
import com.semcon.dometic.cfx.data.Door;
import com.semcon.dometic.cfx.data.Refrigerator;
import com.semcon.dometic.cfx.data.WiFiModule;
import com.semcon.dometic.cfx.event.LostConnectionEvent;
import com.semcon.dometic.cfx.fragment.BoxControlFragment;
import com.semcon.dometic.cfx.fragment.BoxControlFragment.OnToggleConnectionListener;
import com.semcon.dometic.cfx.fragment.MultiboxFragment;
import com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;
import com.zhenbang.project_532.R;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import roboguice.event.EventListener;
import roboguice.event.EventManager;
import roboguice.inject.ContentView;

@ContentView(2131361828)
public class CfxAppMainActivity extends RoboAppCompatActivity implements OnToggleConnectionListener {
    private static final int START_GUIDE_REQUEST = 0;
    List<Refrigerator> deviceTemps = new ArrayList();
    List<Refrigerator> devicesInTheSystem = new ArrayList();
    private EventListener mCfxInfoListener = new EventListener<LostConnectionEvent>() {
        public void onEvent(LostConnectionEvent event) {
            if (CfxAppMainActivity.this.mSSID != null && !CfxAppMainActivity.this.mSSID.equals(CfxAppMainActivity.this.mWifiHandler.getWifiName())) {
                Log.d("WiFi", "Want to reconnect");
                CfxAppMainActivity.this.showReconnectWifiDialog();
            }
        }
    };
    private List<CFXError> mCurrentErrorCodes = new ArrayList();
    @Named("GlobalEventManager")
    @Inject
    private EventManager mGlobalEventManager;
    /* access modifiers changed from: private */
    public String mSSID;
    @Inject
    private CfxAppSharedPreferencesHelper mSharedPreferencesHelper;
    /* access modifiers changed from: private */
    @Inject
    public WifiHandler mWifiHandler;
    @Inject
    private CfxSharedPreferencesHelper_univeral universalSharedPreferencesHelper;

    /* access modifiers changed from: private */
    public void showReconnectWifiDialog() {
        Builder builder = new Builder(this);
        builder.setMessage(R.string.wifi_lost);
        builder.setPositiveButton(R.string.ok, null);
        builder.setNeutralButton(R.string.reconnect, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CfxAppMainActivity.this.mWifiHandler.connectToWifi(CfxAppMainActivity.this, CfxAppMainActivity.this.mSSID);
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Set<String> wifiDevices;
        JsonReader jsonReader;
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }
        findViewById(R.id.main_content).setSystemUiVisibility(1536);
        ArrayList<String> devices = new ArrayList<>();
        devices.add("CFX-95DZ1");
        devices.add("CFX-95DZ2");
        this.mCurrentErrorCodes = getErrorCodes();
        try {
            jsonReader = new JsonReader(new InputStreamReader(getAssets().open("devices.json"), "UTF-8"));
            this.deviceTemps = readDevicesArray(jsonReader);
            for (Refrigerator d : this.deviceTemps) {
                Log.i("Call", "this is the input file" + d.getModel());
                if (devices.contains(d.getModel().toString())) {
                    Refrigerator tempdevice = new Refrigerator(d.getModel(), d.getCompartments(), d.isOn(), d.isAcOn(), d.getTemperatureUnit(), d.getTempAlarmLevel(), d.getDcVoltage(), d.getBatteryProtectionMode(), d.getWiFiModuleList(), this.mCurrentErrorCodes, d.getFeatures());
                    if (this.universalSharedPreferencesHelper != null) {
                        this.universalSharedPreferencesHelper.add_devicesInTheSystem(this, tempdevice);
                    }
                }
            }
            if (this.universalSharedPreferencesHelper != null) {
                this.devicesInTheSystem = this.universalSharedPreferencesHelper.getDevicesInTheSystem();
                Log.i("Call", "These are the devices in the system" + this.devicesInTheSystem.size());
            }
            jsonReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Throwable th) {
            jsonReader.close();
            throw th;
        }
        if (this.mSharedPreferencesHelper == null) {
            wifiDevices = new TreeSet<>();
        } else {
            wifiDevices = this.mSharedPreferencesHelper.getPairedWifiDevices(this);
        }
        Log.i("Dion", "The of wifi list now is" + wifiDevices.size());
        if (wifiDevices.size() == 1) {
            Log.i("Call", "Connected");
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MultiboxFragment()).commit();
        } else if (this.devicesInTheSystem.size() > 1) {
            Log.i("Call", "The wifi size now is " + wifiDevices.size());
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MultiboxFragment()).commit();
        } else if (wifiDevices.isEmpty()) {
            Log.i("Call", "Entering three");
            Intent intent = new Intent(this, CfxAppGuideActivity.class);
            startActivityForResult(intent, 0);
        }
        setSupportActionBar((Toolbar) findViewById(R.id.app_toolbar));
    }

    private void setCurrentFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add((int) R.id.fragment_container, (android.support.p000v4.app.Fragment) new GuideFragment()).commitAllowingStateLoss();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == 0) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new BoxControlFragment()).commitAllowingStateLoss();
        }
    }

    public void onDisconnect() {
        if (this.mSSID != null) {
            this.mWifiHandler.disconnect(this.mSSID);
        }
    }

    public void onConnect() {
        if (this.mSSID != null) {
            this.mWifiHandler.connectToWifi(this, this.mSSID);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.topmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_faq /*2131230730*/:
                Intent intent = new Intent();
                intent.setClass(this, FAQActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_settings /*2131230736*/:
                Intent intent2 = new Intent();
                intent2.setClass(this, CfxAppSettingsActivity.class);
                startActivity(intent2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public List<Refrigerator> readDevicesArray(JsonReader reader) throws IOException {
        List<Refrigerator> deviceTemps2 = new ArrayList<>();
        reader.beginObject();
        while (reader.hasNext()) {
            if (reader.nextName().equals("boxes")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    deviceTemps2.add(readDevice(reader));
                }
                reader.endArray();
            }
        }
        return deviceTemps2;
    }

    public WiFiModule readerWiFiModule(JsonReader reader) throws IOException {
        String type = null;
        int is_on = 0;
        int protocol_version = 0;
        String address = null;
        String port = null;
        String username = null;
        String password = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("type")) {
                type = reader.nextString();
            } else if (name.equals("is_on")) {
                is_on = reader.nextInt();
            } else if (name.equals("protocol_version")) {
                protocol_version = reader.nextInt();
            } else if (name.equals("address")) {
                address = reader.nextString();
            } else if (name.equals("port")) {
                port = reader.nextString();
            } else if (name.equals("username")) {
                username = reader.nextString();
            } else if (name.equals("password")) {
                password = reader.nextString();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new WiFiModule(type, is_on, protocol_version, address, port, username, password);
    }

    public Compartment readerCompartment(JsonReader reader) throws IOException {
        String identifier = null;
        String name = null;
        int is_on = 0;
        int is_compressor_on = 0;
        int mActualTemperature = 0;
        int mTargetTemperature = 0;
        Door mDoor = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String input = reader.nextName();
            if (input.equals("identifier")) {
                identifier = reader.nextString();
            } else if (input.equals("name")) {
                name = reader.nextString();
            } else if (input.equals("is_compressor_on")) {
                is_compressor_on = reader.nextInt();
            } else if (input.equals("is_on")) {
                is_on = reader.nextInt();
            } else if (input.equals("target_temp")) {
                mTargetTemperature = reader.nextInt();
            } else if (input.equals("actual_temp")) {
                mActualTemperature = reader.nextInt();
            } else if (input.equals("door")) {
                mDoor = readDoor(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new Compartment(is_on, is_compressor_on, mActualTemperature, mTargetTemperature, identifier, name, mDoor);
    }

    public Door readDoor(JsonReader reader) throws IOException {
        int is_open = 0;
        int is_alarm_on = 0;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("is_open")) {
                is_open = reader.nextInt();
            } else if (name.equals("is_alarm_on")) {
                is_alarm_on = reader.nextInt();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new Door(is_open, is_alarm_on);
    }

    public Refrigerator readDevice(JsonReader reader) throws IOException {
        String model = null;
        List<Compartment> compartments = new ArrayList<>();
        int is_on = 0;
        int is_ac_on = 0;
        TemperatureUnit temperatureUnit = null;
        BatteryProtectionMode abs_level = null;
        int temp_alarm_level = 0;
        List<WiFiModule> wiFiModuleList = new ArrayList<>();
        List<CFXError> errors = new ArrayList<>();
        List features = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("model")) {
                model = reader.nextString();
            } else {
                if (name.equals("compartments")) {
                    reader.beginArray();
                    while (reader.hasNext()) {
                        compartments.add(readerCompartment(reader));
                    }
                    reader.endArray();
                } else {
                    if (name.equals("is_on")) {
                        is_on = reader.nextInt();
                    } else {
                        if (name.equals("is_ac_on")) {
                            is_ac_on = reader.nextInt();
                        } else {
                            if (name.equals("abs_level")) {
                                switch (reader.nextInt()) {
                                    case 0:
                                        BatteryProtectionMode abs_level2 = BatteryProtectionMode.Low;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                }
                                BatteryProtectionMode abs_level3 = BatteryProtectionMode.Medium;
                                abs_level = BatteryProtectionMode.High;
                                continue;
                            } else {
                                if (!name.equals("temperature_unit")) {
                                    if (name.equals("temp_alarm_level")) {
                                        temp_alarm_level = reader.nextInt();
                                    } else {
                                        if (name.equals("communication")) {
                                            reader.beginArray();
                                            while (reader.hasNext()) {
                                                Log.i("Json", "Running the loop");
                                                WiFiModule wiFiModuletemp = readerWiFiModule(reader);
                                                if (wiFiModuletemp.getType().equals("WIFI")) {
                                                    wiFiModuleList.add(wiFiModuletemp);
                                                }
                                            }
                                            reader.endArray();
                                        } else {
                                            if (name.equals("errors")) {
                                                reader.beginArray();
                                                while (reader.hasNext()) {
                                                    reader.skipValue();
                                                }
                                                reader.endArray();
                                            } else {
                                                if (name.equals("features")) {
                                                    reader.beginArray();
                                                    while (reader.hasNext()) {
                                                        reader.beginObject();
                                                        while (reader.hasNext()) {
                                                            if (name.equals("feature")) {
                                                                features.add(reader.nextString());
                                                            } else {
                                                                reader.skipValue();
                                                            }
                                                        }
                                                        reader.endObject();
                                                    }
                                                    reader.endArray();
                                                } else {
                                                    reader.skipValue();
                                                }
                                            }
                                        }
                                    }
                                } else if (reader.nextString() == "C") {
                                    temperatureUnit = TemperatureUnit.Celsius;
                                } else {
                                    temperatureUnit = TemperatureUnit.Fahrenheit;
                                }
                            }
                        }
                    }
                }
            }
        }
        reader.endObject();
        return new Refrigerator(model, compartments, is_on, is_ac_on, temperatureUnit, temp_alarm_level, 0.0d, abs_level, wiFiModuleList, errors, features);
    }

    public ArrayList<CFXError> getErrorCodes() {
        Log.i("Call", "Calling the Error codes");
        ArrayList<CFXError> codes = new ArrayList<>();
        codes.add(CFXError.ABS_ERROR);
        codes.add(CFXError.COMPRESSOR_FAULTY);
        codes.add(CFXError.THERMOSTAT_FAULTY);
        codes.add(CFXError.SOLENOID_VALVE_FAULTY);
        codes.add(CFXError.DOOR_OPEN);
        codes.add(CFXError.COMPRESSOR_WORKED_FOR_ONE_CYCLE);
        Log.i("Call", "Before returning the error codes" + codes.size() + " " + codes.get(1));
        return codes;
    }
}
