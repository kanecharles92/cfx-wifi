package com.semcon.cfx.app.activity;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.p000v4.app.Fragment;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import com.google.inject.Inject;
import com.semcon.cfx.app.fragment.ConnectingErrorFragment;
import com.semcon.cfx.app.fragment.ConnectionSuccessFragment.OnDoneListener;
import com.semcon.cfx.app.fragment.SelectBoxFragment;
import com.semcon.cfx.app.fragment.SelectBoxFragment.OnClick;
import com.semcon.cfx.app.fragment.WelcomeFragment;
import com.semcon.cfx.app.fragment.WelcomeFragment.OnConnectionListener;
import com.semcon.cfx.app.persistent.CfxAppSharedPreferencesHelper;
import com.semcon.cfx.app.wifi.WifiHandler;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;
import com.zhenbang.project_532.R;
import java.io.IOException;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import roboguice.inject.ContentView;

@ContentView(2131361830)
public class WelcomeActivity extends RoboAppCompatActivity implements OnClick, OnConnectionListener, OnDoneListener {
    public static final int CONNECTION_SUCCESS = 0;
    Bundle args = new Bundle();
    private ConnectingErrorFragment connectingErrorFragment;
    @Inject
    ICfxCommunication mCfxCommunication;
    private String mSSID;
    /* access modifiers changed from: private */
    public SelectBoxFragment mSelectBoxFragment;
    @Inject
    private CfxAppSharedPreferencesHelper mSharedPreferencesHelper;
    @Inject
    private WifiHandler mWifiHandler;

    private class CheckConnectionTask implements Callable<String> {
        ConnectTask connectTask;

        public CheckConnectionTask(ConnectTask connectTask2) {
            this.connectTask = connectTask2;
        }

        public String call() throws Exception {
            try {
                WelcomeActivity.this.mCfxCommunication.connect();
                Log.d("Incoming", "Connected TCP");
                if (WelcomeActivity.this.mCfxCommunication.isConnected()) {
                    this.connectTask.connectionSuccess();
                } else {
                    this.connectTask.connectionFailed();
                }
            } catch (IOException e) {
                e.printStackTrace();
                this.connectTask.connectionFailed();
            }
            return null;
        }
    }

    class ConnectTask extends AsyncTask<Void, Void, Void> {
        ConnectTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<String> future = executor.submit(new CheckConnectionTask(this));
            try {
                System.out.println((String) future.get(1, TimeUnit.SECONDS));
            } catch (TimeoutException e) {
                future.cancel(true);
                WelcomeActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, WelcomeActivity.this.mSelectBoxFragment).commit();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            } catch (ExecutionException e3) {
                e3.printStackTrace();
            }
            executor.shutdownNow();
            return null;
        }

        /* access modifiers changed from: private */
        public void connectionSuccess() {
            WelcomeActivity.this.startActivity(new Intent(WelcomeActivity.this.getApplicationContext(), AppMainActivity.class));
            WelcomeActivity.this.finish();
        }

        /* access modifiers changed from: private */
        public void connectionFailed() {
            WelcomeActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, WelcomeActivity.this.mSelectBoxFragment).commit();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService("connectivity");
        this.mSelectBoxFragment = new SelectBoxFragment();
        this.connectingErrorFragment = new ConnectingErrorFragment();
        NetworkInfo activeNetwork = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting()) {
        }
        if (this.mSharedPreferencesHelper.getPairedWifiDevices(getApplicationContext()).size() > 0) {
            this.mSSID = (String) (this.mSharedPreferencesHelper == null ? new TreeSet<>() : this.mSharedPreferencesHelper.getPairedWifiDevices(this)).iterator().next();
            Log.i("Incoming", "Calling from activity with SSID: " + this.mSSID);
            if (this.mWifiHandler.connectToWifi(this, this.mSSID)) {
                Log.i("Incoming", "About to launch async task: " + this.mSSID);
                new ConnectTask().execute(new Void[0]);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, this.connectingErrorFragment).commit();
                return;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, this.mSelectBoxFragment).commit();
            return;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, this.mSelectBoxFragment).commit();
    }

    public void onSingleCompartment() {
        this.args.putInt(WelcomeFragment.COMPARTMENTS, 1);
        WelcomeFragment newFragment = new WelcomeFragment();
        newFragment.setArguments(this.args);
        setupTransition(true, newFragment);
    }

    public void onDoubleCompartment() {
        this.args.putInt(WelcomeFragment.COMPARTMENTS, 2);
        WelcomeFragment newFragment = new WelcomeFragment();
        newFragment.setArguments(this.args);
        setupTransition(false, newFragment);
    }

    public void onConnectionSuccess() {
        Log.i("Incoming", "Incoming1");
        startActivity(new Intent(getApplicationContext(), AppMainActivity.class));
        finish();
    }

    public void onConnectionFailed() {
        Log.i("Incoming", "Incoming2");
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, this.mSelectBoxFragment).commit();
    }

    public void onDone() {
        setResult(0);
        finish();
    }

    private void setupTransition(boolean isFirstBox, WelcomeFragment endFragment) {
        Fragment startFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (VERSION.SDK_INT >= 21) {
            Transition trans = TransitionInflater.from(getApplicationContext()).inflateTransition(R.transition.box_trans);
            Transition transLeft = TransitionInflater.from(getApplicationContext()).inflateTransition(17760263);
            Transition transRight = TransitionInflater.from(getApplicationContext()).inflateTransition(17760262);
            startFragment.setSharedElementReturnTransition(trans);
            startFragment.setEnterTransition(transLeft);
            startFragment.setExitTransition(transLeft);
            endFragment.setSharedElementEnterTransition(trans);
            endFragment.setEnterTransition(transRight);
            endFragment.setExitTransition(transLeft);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, endFragment).addToBackStack(null).addSharedElement(startFragment.getView().findViewById(isFirstBox ? R.id.imageButton1 : R.id.imageButton2), isFirstBox ? "box1" : "box2").commit();
    }
}
