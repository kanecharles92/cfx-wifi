package com.semcon.cfx.app.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.p003v7.app.ActionBar;
import android.view.View;
import com.google.inject.Inject;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;
import com.zhenbang.project_532.R;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SplashActivity extends RoboAppCompatActivity {
    private static final int UI_ANIMATION_DELAY = 100;
    private static final int UI_SPLASH_DELAY = 2000;
    @Inject
    ICfxCommunication mCfxCommunication;
    /* access modifiers changed from: private */
    public View mContentView;
    private final Handler mHideHandler = new Handler();
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint({"InlinedApi"})
        public void run() {
            SplashActivity.this.mContentView.setSystemUiVisibility(4871);
            SplashActivity.this.delayedShow(SplashActivity.UI_SPLASH_DELAY);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        public void run() {
            ActionBar actionBar = SplashActivity.this.getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    private final Runnable mShowRunnable = new Runnable() {
        public void run() {
            SplashActivity.this.show();
        }
    };

    class ConnectTask extends AsyncTask<Void, Void, Boolean> {
        ConnectTask() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            try {
                SplashActivity.this.mCfxCommunication.connect();
                return Boolean.valueOf(SplashActivity.this.mCfxCommunication.isConnected());
            } catch (IOException e) {
                e.printStackTrace();
                return Boolean.valueOf(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_splash);
        this.mContentView = findViewById(R.id.fullscreen_content);
        hide();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private void hide() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        this.mHideHandler.removeCallbacks(this.mShowPart2Runnable);
        this.mHideHandler.postDelayed(this.mHidePart2Runnable, 100);
    }

    /* access modifiers changed from: private */
    @SuppressLint({"InlinedApi"})
    public void show() {
        this.mHideHandler.removeCallbacks(this.mHidePart2Runnable);
        Boolean connected = Boolean.valueOf(false);
        try {
            connected = (Boolean) new ConnectTask().execute(new Void[0]).get(1300, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
        }
        startNextActivity(connected.booleanValue() ? AppMainActivity.class : ConnectingActivity.class);
        if (connected.booleanValue()) {
            showSystemBar();
        }
    }

    private void showSystemBar() {
        this.mContentView.setSystemUiVisibility(1536);
    }

    /* access modifiers changed from: private */
    public void delayedShow(int delayMillis) {
        this.mHideHandler.removeCallbacks(this.mShowRunnable);
        this.mHideHandler.postDelayed(this.mShowRunnable, (long) delayMillis);
    }

    private void startNextActivity(Class<?> cls) {
        startActivity(new Intent(getApplicationContext(), cls));
        finish();
    }
}
