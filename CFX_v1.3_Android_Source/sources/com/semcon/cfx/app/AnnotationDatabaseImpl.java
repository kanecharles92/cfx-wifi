package com.semcon.cfx.app;

import com.google.inject.AnnotationDatabase;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import roboguice.fragment.FragmentUtil;

public class AnnotationDatabaseImpl extends AnnotationDatabase {
    public void fillAnnotationClassesAndFieldsNames(HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToFieldSet) {
        String annotationClassName = "com.google.inject.Inject";
        Map<String, Set<String>> mapClassWithInjectionNameToFieldSet = (Map) mapAnnotationToMapClassWithInjectionNameToFieldSet.get(annotationClassName);
        if (mapClassWithInjectionNameToFieldSet == null) {
            mapClassWithInjectionNameToFieldSet = new HashMap<>();
            mapAnnotationToMapClassWithInjectionNameToFieldSet.put(annotationClassName, mapClassWithInjectionNameToFieldSet);
        }
        Set<String> fieldNameSet = new HashSet<>();
        fieldNameSet.add("mAlarmViews");
        fieldNameSet.add("mCfxAppSharedPreferencesHelper");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.CfxApplication", fieldNameSet);
        Set<String> fieldNameSet2 = new HashSet<>();
        fieldNameSet2.add("mSharedPreferencesHelper");
        fieldNameSet2.add("mGlobalEventManager");
        fieldNameSet2.add("mWifiHandler");
        fieldNameSet2.add("universalSharedPreferencesHelper");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.activity.CfxAppMainActivity", fieldNameSet2);
        Set<String> fieldNameSet3 = new HashSet<>();
        fieldNameSet3.add("mWifiManager");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.wifi.WifiHandler", fieldNameSet3);
        Set<String> fieldNameSet4 = new HashSet<>();
        fieldNameSet4.add("mGlobalEventManager");
        fieldNameSet4.add("mWifiHandler");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.activity.AppMainActivity", fieldNameSet4);
        Set<String> fieldNameSet5 = new HashSet<>();
        fieldNameSet5.add("mSharedPreferencesHelper");
        fieldNameSet5.add("mCfxCommunication");
        fieldNameSet5.add("mWifiHandler");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.fragment.GuideFragment", fieldNameSet5);
        Set<String> fieldNameSet6 = new HashSet<>();
        fieldNameSet6.add("mCfxCommunication");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.activity.ConnectingActivity", fieldNameSet6);
        Set<String> fieldNameSet7 = new HashSet<>();
        fieldNameSet7.add("mSharedPreferencesHelper");
        fieldNameSet7.add("mCfxCommunication");
        fieldNameSet7.add("mWifiHandler");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.activity.WelcomeActivity", fieldNameSet7);
        Set<String> fieldNameSet8 = new HashSet<>();
        fieldNameSet8.add("mSharedPreferencesHelper");
        fieldNameSet8.add("mWifiHandler");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.activity.CfxAppGuideActivity", fieldNameSet8);
        Set<String> fieldNameSet9 = new HashSet<>();
        fieldNameSet9.add("mCfxCommunication");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.activity.SplashActivity", fieldNameSet9);
        Set<String> fieldNameSet10 = new HashSet<>();
        fieldNameSet10.add("mSharedPreferencesHelper");
        fieldNameSet10.add("mCfxCommunication");
        fieldNameSet10.add("mWifiHandler");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.cfx.app.fragment.WelcomeFragment", fieldNameSet10);
        String annotationClassName2 = "roboguice.inject.InjectView";
        Map<String, Set<String>> mapClassWithInjectionNameToFieldSet2 = (Map) mapAnnotationToMapClassWithInjectionNameToFieldSet.get(annotationClassName2);
        if (mapClassWithInjectionNameToFieldSet2 == null) {
            mapClassWithInjectionNameToFieldSet2 = new HashMap<>();
            mapAnnotationToMapClassWithInjectionNameToFieldSet.put(annotationClassName2, mapClassWithInjectionNameToFieldSet2);
        }
        Set<String> fieldNameSet11 = new HashSet<>();
        fieldNameSet11.add("mProgressBar");
        mapClassWithInjectionNameToFieldSet2.put("com.semcon.cfx.app.fragment.GuideFragment", fieldNameSet11);
    }

    public void fillAnnotationClassesAndMethods(HashMap<String, Map<String, Set<String>>> hashMap) {
    }

    public void fillAnnotationClassesAndConstructors(HashMap<String, Map<String, Set<String>>> hashMap) {
    }

    public void fillClassesContainingInjectionPointSet(HashSet<String> classesContainingInjectionPointsSet) {
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.CfxApplication");
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.activity.CfxAppMainActivity");
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.wifi.WifiHandler");
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.activity.AppMainActivity");
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.fragment.GuideFragment");
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.activity.ConnectingActivity");
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.activity.WelcomeActivity");
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.activity.CfxAppGuideActivity");
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.activity.SplashActivity");
        classesContainingInjectionPointsSet.add("com.semcon.cfx.app.fragment.WelcomeFragment");
    }

    public void fillBindableClasses(HashSet<String> injectedClasses) {
        injectedClasses.add("roboguice.event.EventManager");
        injectedClasses.add("android.net.wifi.WifiManager");
        injectedClasses.add("com.semcon.cfx.app.persistent.CfxAppSharedPreferencesHelper");
        injectedClasses.add("com.semcon.cfx.app.wifi.WifiHandler");
        injectedClasses.add("com.semcon.dometic.cfx.communication.ICfxCommunication");
        injectedClasses.add("android.widget.ProgressBar");
        injectedClasses.add("com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral");
        injectedClasses.add("com.semcon.dometic.cfx.alarm.AlarmViews");
        if (FragmentUtil.hasNative) {
            injectedClasses.add("android.app.FragmentManager");
        }
        if (FragmentUtil.hasSupport) {
            injectedClasses.add("android.support.v4.app.FragmentManager");
        }
    }
}
