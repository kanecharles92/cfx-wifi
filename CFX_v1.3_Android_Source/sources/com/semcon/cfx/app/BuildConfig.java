package com.semcon.cfx.app;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.zhenbang.project_532";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final long TIMESTAMP = 1553683199604L;
    public static final int VERSION_CODE = 24;
    public static final String VERSION_NAME = "1.3";
}
