package com.semcon.cfx.app.roboguice.module;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.semcon.dometic.cfx.communication.CfxTcpCommunicationWrapper;
import com.semcon.dometic.cfx.communication.ICfxCommunication;

public class CfxRoboModule implements Module {
    public void configure(Binder binder) {
        binder.bind(ICfxCommunication.class).mo7042to(CfxTcpCommunicationWrapper.class);
    }
}
