package com.semcon.cfx.app.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;
import com.google.inject.Inject;

public class WifiHandler {
    @Inject
    private WifiManager mWifiManager;
    /* access modifiers changed from: private */
    public WifiStateListener mWifiStateListener;

    private class WifiStateListener extends BroadcastReceiver {
        private final String mSsid;

        WifiStateListener(String ssid) {
            this.mSsid = ssid;
        }

        public void onReceive(Context context, Intent intent) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            if (networkInfo.getType() == 1 && networkInfo.getState() == State.CONNECTED) {
                WifiHandler.this.connectToSSID(this.mSsid);
                context.unregisterReceiver(WifiHandler.this.mWifiStateListener);
                WifiHandler.this.mWifiStateListener = null;
            }
        }
    }

    public String getWifiName() {
        return this.mWifiManager.getConnectionInfo().getSSID();
    }

    public boolean connectToWifi(Context context, String ssid) {
        if (this.mWifiManager.isWifiEnabled()) {
            return connectToSSID(ssid);
        }
        enableWifi(context, ssid);
        return true;
    }

    private void enableWifi(Context context, String ssid) {
        if (this.mWifiStateListener != null) {
            context.unregisterReceiver(this.mWifiStateListener);
        }
        this.mWifiStateListener = new WifiStateListener(ssid);
        context.registerReceiver(this.mWifiStateListener, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        this.mWifiManager.setWifiEnabled(true);
    }

    /* access modifiers changed from: private */
    public boolean connectToSSID(String ssid) {
        Log.i("Incoming", "Incoming Calling the connection with ssid" + ssid);
        if (this.mWifiManager.getConfiguredNetworks() != null) {
            for (WifiConfiguration wifiConfiguration : this.mWifiManager.getConfiguredNetworks()) {
                if (wifiConfiguration.SSID.equals(ssid)) {
                    this.mWifiManager.enableNetwork(wifiConfiguration.networkId, true);
                    Log.i("Incoming", "Connected to: " + ssid);
                    return true;
                }
            }
        }
        return false;
    }

    public void disconnect(String ssid) {
        if (this.mWifiManager.getConfiguredNetworks() != null) {
            for (WifiConfiguration wifiConfiguration : this.mWifiManager.getConfiguredNetworks()) {
                if (wifiConfiguration.SSID.equals(ssid)) {
                    this.mWifiManager.disableNetwork(wifiConfiguration.networkId);
                }
            }
        }
    }
}
