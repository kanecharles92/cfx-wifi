package com.semcon.cfx.app;

import android.app.Application;
import com.google.inject.Inject;
import com.semcon.cfx.app.persistent.CfxAppSharedPreferencesHelper;
import com.semcon.cfx.app.roboguice.module.CfxRoboModule;
import com.semcon.dometic.cfx.alarm.AlarmViews;
import com.semcon.dometic.cfx.locale.LocaleHelper;
import roboguice.RoboGuice;

public class CfxApplication extends Application {
    @Inject
    private AlarmViews mAlarmViews;
    @Inject
    private CfxAppSharedPreferencesHelper mCfxAppSharedPreferencesHelper;

    public void onCreate() {
        super.onCreate();
        RoboGuice.getOrCreateBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE, RoboGuice.newDefaultRoboModule(this), new CfxRoboModule());
        String language = this.mCfxAppSharedPreferencesHelper == null ? null : this.mCfxAppSharedPreferencesHelper.getLanguage(this);
        if (language != null) {
            LocaleHelper.setLocale(this, language);
        }
    }
}
