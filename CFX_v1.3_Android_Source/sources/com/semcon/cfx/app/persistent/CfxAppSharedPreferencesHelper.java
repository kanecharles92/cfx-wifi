package com.semcon.cfx.app.persistent;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.semcon.dometic.cfx.data.Device;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CfxAppSharedPreferencesHelper {
    private static final String SHARED_PREFS_CFX = "shared_prefs_cfx_app";
    private static final String SHARED_PREFS_CFX_CONNECTED_BLUETOOTH_DEVICES_KEY = "connected_bluetooth_devices_key";
    private static final String SHARED_PREFS_CFX_CONNECTED_WIFI_DEVICES_KEY = "connected_wifi_devices_key";
    private static final String SHARED_PREFS_CFX_LANGUAGE_KEY = "language_key";
    private static final List<Device> devicesInTheSystem = new ArrayList();

    public Set<String> getPairedWifiDevices(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_CFX, 0).getStringSet(SHARED_PREFS_CFX_CONNECTED_WIFI_DEVICES_KEY, new HashSet());
    }

    public Set<String> getPairedBluetoothDevices(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_CFX, 0).getStringSet(SHARED_PREFS_CFX_CONNECTED_BLUETOOTH_DEVICES_KEY, new HashSet());
    }

    public void addPairedWifiDevice(Context context, String ssid, String password) {
        if (context != null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(SHARED_PREFS_CFX, 0);
            if (sharedPrefs != null) {
                HashSet<String> allDevices = new HashSet<>(sharedPrefs.getStringSet(SHARED_PREFS_CFX_CONNECTED_WIFI_DEVICES_KEY, new HashSet()));
                Editor editor = sharedPrefs.edit();
                allDevices.add(ssid);
                editor.putStringSet(SHARED_PREFS_CFX_CONNECTED_WIFI_DEVICES_KEY, allDevices);
                editor.apply();
            }
        }
    }

    public void addPairedBluehtoothDevice(Context context, String ssid, String password) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(SHARED_PREFS_CFX, 0);
        HashSet<String> allDevices = new HashSet<>();
        allDevices.addAll(sharedPrefs.getStringSet(SHARED_PREFS_CFX_CONNECTED_BLUETOOTH_DEVICES_KEY, new HashSet()));
        Editor editor = sharedPrefs.edit();
        allDevices.add(ssid);
        editor.putStringSet(SHARED_PREFS_CFX_CONNECTED_BLUETOOTH_DEVICES_KEY, allDevices);
        editor.apply();
    }

    public void setLanguage(Context context, String language) {
        Editor editor = context.getSharedPreferences(SHARED_PREFS_CFX, 0).edit();
        editor.putString(SHARED_PREFS_CFX_LANGUAGE_KEY, language);
        editor.apply();
    }

    public String getLanguage(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_CFX, 0).getString(SHARED_PREFS_CFX_LANGUAGE_KEY, null);
    }

    public void adddevicesInTheSystem(Context context, Device device) {
        devicesInTheSystem.add(device);
    }

    public List<Device> getDevicesInTheSystem(Context context) {
        return devicesInTheSystem;
    }

    public List<Device> emptyPairedDevices(Context context) {
        devicesInTheSystem.clear();
        return devicesInTheSystem;
    }
}
