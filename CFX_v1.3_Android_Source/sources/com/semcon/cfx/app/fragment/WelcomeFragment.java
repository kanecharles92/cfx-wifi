package com.semcon.cfx.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.google.inject.Inject;
import com.semcon.cfx.app.persistent.CfxAppSharedPreferencesHelper;
import com.semcon.cfx.app.wifi.WifiHandler;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.zhenbang.project_532.R;
import java.io.IOException;
import java.text.MessageFormat;
import roboguice.fragment.RoboFragment;

public class WelcomeFragment extends RoboFragment {
    public static String COMPARTMENTS = "COMPARTMENTS";
    /* access modifiers changed from: private */
    public static int PICK_WIFI = 0;
    public static String STEP2 = "STEP2";
    @Inject
    ICfxCommunication mCfxCommunication;
    /* access modifiers changed from: private */
    public int mCompartments;
    /* access modifiers changed from: private */
    public OnConnectionListener mConnectionListener;
    /* access modifiers changed from: private */
    public boolean mIsStep2;
    /* access modifiers changed from: private */
    @Inject
    public CfxAppSharedPreferencesHelper mSharedPreferencesHelper;
    private TextView mStep;
    private TextView mStepA;
    private TextView mStepB;
    private Button mStepButton;
    private TextView mStepC;
    /* access modifiers changed from: private */
    @Inject
    public WifiHandler mWifiHandler;

    class ConnectTask extends AsyncTask<Void, Void, Void> {
        ConnectTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                WelcomeFragment.this.mCfxCommunication.connect();
                if (WelcomeFragment.this.mCfxCommunication.isConnected()) {
                    connectionSuccess();
                } else {
                    connectionFailed();
                }
            } catch (IOException e) {
                connectionFailed();
                e.printStackTrace();
            }
            return null;
        }

        private void connectionSuccess() {
            WelcomeFragment.this.mSharedPreferencesHelper.emptyPairedDevices(WelcomeFragment.this.getContext());
            WelcomeFragment.this.mSharedPreferencesHelper.addPairedWifiDevice(WelcomeFragment.this.getContext(), WelcomeFragment.this.mWifiHandler.getWifiName(), null);
            if (WelcomeFragment.this.mConnectionListener != null) {
                WelcomeFragment.this.mConnectionListener.onConnectionSuccess();
            }
        }

        private void connectionFailed() {
        }
    }

    class DisconnectTask extends AsyncTask<Void, Void, Void> {
        DisconnectTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                if (WelcomeFragment.this.mCfxCommunication != null) {
                    WelcomeFragment.this.mCfxCommunication.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public interface OnConnectionListener {
        void onConnectionFailed();

        void onConnectionSuccess();
    }

    class StepButtonListener implements OnClickListener {
        StepButtonListener() {
        }

        public void onClick(View v) {
            if (WelcomeFragment.this.mIsStep2) {
                new DisconnectTask().execute(new Void[0]);
                WelcomeFragment.this.startActivityForResult(new Intent("android.net.wifi.PICK_WIFI_NETWORK"), WelcomeFragment.PICK_WIFI);
                return;
            }
            WelcomeFragment fragment = new WelcomeFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(WelcomeFragment.COMPARTMENTS, WelcomeFragment.this.mCompartments);
            bundle.putBoolean(WelcomeFragment.STEP2, true);
            fragment.setArguments(bundle);
            if (VERSION.SDK_INT >= 21) {
                Transition transLeft = TransitionInflater.from(WelcomeFragment.this.getContext()).inflateTransition(17760263);
                fragment.setEnterTransition(TransitionInflater.from(WelcomeFragment.this.getContext()).inflateTransition(17760262));
                fragment.setExitTransition(transLeft);
            }
            WelcomeFragment.this.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        this.mStep = (TextView) view.findViewById(R.id.step_tv);
        this.mStepA = (TextView) view.findViewById(R.id.step_a_tv);
        this.mStepB = (TextView) view.findViewById(R.id.step_b_tv);
        this.mStepC = (TextView) view.findViewById(R.id.step_c_tv);
        this.mStepButton = (Button) view.findViewById(R.id.step_btn);
        if (this.mStepButton != null) {
            this.mStepButton.setOnClickListener(new StepButtonListener());
        }
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(COMPARTMENTS)) {
                this.mCompartments = bundle.getInt(COMPARTMENTS, 1);
            }
            if (bundle.containsKey(STEP2)) {
                this.mIsStep2 = bundle.getBoolean(STEP2);
            }
            ImageButton image = (ImageButton) getActivity().findViewById(this.mCompartments == 1 ? R.id.imageButton1 : R.id.imageButton2);
            if (image != null) {
                if (this.mIsStep2) {
                    image.setImageResource(R.mipmap.cfx_android_ikoner_24);
                } else {
                    image.setImageResource(this.mCompartments == 1 ? R.mipmap.cfx_android_ikoner_19 : R.mipmap.cfx_android_ikoner_18);
                }
            }
            if (this.mIsStep2) {
                this.mStep.setText(getString(R.string.step2));
                this.mStepA.setText(getString(R.string.step2a));
                this.mStepB.setText(getString(R.string.step2b));
                this.mStepC.setText(getString(R.string.step2c));
                this.mStepButton.setText(getString(R.string.connect_wifi));
                return;
            }
            this.mStep.setText(getString(R.string.step1));
            this.mStepA.setText(getString(R.string.step1a));
            TextView textView = this.mStepB;
            String string = getString(R.string.step1b);
            Object[] objArr = new Object[1];
            objArr[0] = getString(this.mCompartments == 1 ? R.string.compartment1_set_presses : R.string.compartment2_set_presses);
            textView.setText(MessageFormat.format(string, objArr));
            this.mStepC.setText(getString(R.string.step1d));
            this.mStepButton.setText(getString(R.string.next));
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_WIFI) {
            new ConnectTask().execute(new Void[0]);
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnConnectionListener) {
            this.mConnectionListener = (OnConnectionListener) context;
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mConnectionListener = null;
    }
}
