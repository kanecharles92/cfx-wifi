package com.semcon.cfx.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.google.inject.Inject;
import com.semcon.cfx.app.persistent.CfxAppSharedPreferencesHelper;
import com.semcon.cfx.app.wifi.WifiHandler;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.zhenbang.project_532.R;
import java.io.IOException;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

public class GuideFragment extends RoboFragment {
    /* access modifiers changed from: private */
    public static int PICK_WIFI = 0;
    @Inject
    ICfxCommunication mCfxCommunication;
    /* access modifiers changed from: private */
    public OnConnectionListener mConnectionListener;
    /* access modifiers changed from: private */
    @InjectView(2131230800)
    public ProgressBar mProgressBar;
    /* access modifiers changed from: private */
    @Inject
    public CfxAppSharedPreferencesHelper mSharedPreferencesHelper;
    /* access modifiers changed from: private */
    @Inject
    public WifiHandler mWifiHandler;

    class ConnectTask extends AsyncTask<Void, Void, Void> {
        ConnectTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                GuideFragment.this.mCfxCommunication.connect();
                if (GuideFragment.this.mCfxCommunication.isConnected()) {
                    connectionSuccess();
                } else {
                    connectionFailed();
                }
            } catch (IOException e) {
                connectionFailed();
                e.printStackTrace();
            }
            return null;
        }

        private void connectionSuccess() {
            GuideFragment.this.mSharedPreferencesHelper.addPairedWifiDevice(GuideFragment.this.getContext(), GuideFragment.this.mWifiHandler.getWifiName(), null);
            if (GuideFragment.this.mConnectionListener != null) {
                GuideFragment.this.mConnectionListener.onConnectionSuccess();
            }
        }

        private void connectionFailed() {
            if (GuideFragment.this.mConnectionListener != null) {
                GuideFragment.this.mConnectionListener.onConnectionFailed();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void aVoid) {
            GuideFragment.this.mProgressBar.setVisibility(8);
        }
    }

    class DisconnectTask extends AsyncTask<Void, Void, Void> {
        DisconnectTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                GuideFragment.this.mCfxCommunication.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public interface OnConnectionListener {
        void onConnectionFailed();

        void onConnectionSuccess();
    }

    class WifiConnectButtonListener implements OnClickListener {
        WifiConnectButtonListener() {
        }

        public void onClick(View v) {
            new DisconnectTask().execute(new Void[0]);
            GuideFragment.this.startActivityForResult(new Intent("android.net.wifi.PICK_WIFI_NETWORK"), GuideFragment.PICK_WIFI);
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cfx_fragment_connect_guide, container, false);
        view.findViewById(R.id.wifi_connect_button).setOnClickListener(new WifiConnectButtonListener());
        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_WIFI) {
            this.mProgressBar.setVisibility(0);
            new ConnectTask().execute(new Void[0]);
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnConnectionListener) {
            this.mConnectionListener = (OnConnectionListener) context;
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mConnectionListener = null;
    }
}
