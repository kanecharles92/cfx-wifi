package com.semcon.cfx.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.zhenbang.project_532.R;
import roboguice.fragment.RoboFragment;

public class ConnectionFailedFragment extends RoboFragment {
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cfx_fragment_connection_failed, container, false);
        view.findViewById(R.id.connection_failed_ok_button).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ConnectionFailedFragment.this.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SelectBoxFragment()).addToBackStack(null).commit();
            }
        });
        return view;
    }
}
