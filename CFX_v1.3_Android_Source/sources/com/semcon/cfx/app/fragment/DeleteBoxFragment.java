package com.semcon.cfx.app.fragment;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import com.semcon.cfx.app.activity.WelcomeActivity;
import com.zhenbang.project_532.R;

public class DeleteBoxFragment extends DialogFragment {
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Builder builder = new Builder(getActivity());
        builder.setMessage(R.string.dialog_delete_box).setPositiveButton(R.string.delete, new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Activity activity = DeleteBoxFragment.this.getActivity();
                DeleteBoxFragment.this.startActivity(new Intent(DeleteBoxFragment.this.getActivity(), WelcomeActivity.class));
                activity.finish();
            }
        }).setNegativeButton(R.string.cancel, null);
        return builder.create();
    }
}
