package com.semcon.cfx.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.semcon.cfx.app.activity.AppMainActivity;
import com.zhenbang.project_532.R;
import roboguice.fragment.RoboFragment;

public class ConnectionSuccessFragment extends RoboFragment {
    /* access modifiers changed from: private */
    public OnDoneListener mDoneListener;

    public interface OnDoneListener {
        void onDone();
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cfx_fragment_connection_success, container, false);
        view.findViewById(R.id.connection_success_ok_button).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (ConnectionSuccessFragment.this.mDoneListener != null) {
                    ConnectionSuccessFragment.this.mDoneListener.onDone();
                }
                ConnectionSuccessFragment.this.startActivity(new Intent(ConnectionSuccessFragment.this.getContext(), AppMainActivity.class));
            }
        });
        return view;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDoneListener) {
            this.mDoneListener = (OnDoneListener) context;
        }
    }
}
