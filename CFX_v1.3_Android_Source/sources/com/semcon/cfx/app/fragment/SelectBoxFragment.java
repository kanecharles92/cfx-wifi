package com.semcon.cfx.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.zhenbang.project_532.R;
import roboguice.fragment.RoboFragment;

public class SelectBoxFragment extends RoboFragment {
    private OnClick mOnClick;

    public interface OnClick {
        void onDoubleCompartment();

        void onSingleCompartment();
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.select_box_layout, container, false);
        ((ImageButton) view.findViewById(R.id.imageButton1)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                SelectBoxFragment.this.onSingleCompartment();
            }
        });
        ((ImageButton) view.findViewById(R.id.imageButton2)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                SelectBoxFragment.this.onDoubleCompartment();
            }
        });
        return view;
    }

    /* access modifiers changed from: private */
    public void onSingleCompartment() {
        if (this.mOnClick != null) {
            this.mOnClick.onSingleCompartment();
        }
    }

    /* access modifiers changed from: private */
    public void onDoubleCompartment() {
        if (this.mOnClick != null) {
            this.mOnClick.onDoubleCompartment();
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClick) {
            this.mOnClick = (OnClick) context;
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mOnClick = null;
    }
}
