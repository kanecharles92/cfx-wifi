package com.semcon.dometic.cfx.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.semcon.dometic.cfx.C0434R;

public class ErrorView extends LinearLayout {
    TextView error_message;

    public ErrorView(Context context) {
        super(context);
        init(context);
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        View.inflate(context, C0434R.layout.error_view_layout, this);
        setDescendantFocusability(393216);
        this.error_message = (TextView) findViewById(C0434R.C0436id.error_text);
    }

    public void setError_message(String text) {
        this.error_message.setText(text);
    }
}
