package com.semcon.dometic.cfx.views;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;
import com.dometic.dometicui.Utils;
import com.semcon.dometic.cfx.C0434R;

public class CoolBoxView extends ImageView implements AnimatorUpdateListener {
    public static final int ACTIVE_COMPARTMENT_BOTH = 3;
    public static final int ACTIVE_COMPARTMENT_LEFT = 1;
    public static final int ACTIVE_COMPARTMENT_NONE = 0;
    public static final int ACTIVE_COMPARTMENT_RIGHT = 2;
    public static final int DOUBLE_COMPARTMENT_TYPE = 2;
    private static final int MAX_DOOR_LEAD_OPEN_INSET = 30;
    public static final int OPEN_DOOR_COMPARTMENT_BOTH = 3;
    public static final int OPEN_DOOR_COMPARTMENT_LEFT = 1;
    public static final int OPEN_DOOR_COMPARTMENT_NONE = 0;
    public static final int OPEN_DOOR_COMPARTMENT_RIGHT = 2;
    public static final int SINGLE_COMPARTMENT_TYPE = 1;
    private int mActiveCompartment = 0;
    private int mBoxType = 1;
    private ValueAnimator mLeftLeadAnimator = null;
    private int mLeftLeadOpenInset;
    private OnTouchCompartmentListener mOnTouchCompartmentListener;
    private int mOpenDoorCompartment = 0;
    private ValueAnimator mRightLeadAnimator = null;
    private int mRightLeadOpenInset;

    public interface OnTouchCompartmentListener {
        void onCompartmentTouched(CoolBoxView coolBoxView, int i, boolean z);
    }

    public CoolBoxView(Context context) {
        super(context);
        init(null, 0);
    }

    public CoolBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public CoolBoxView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, C0434R.styleable.CoolBoxView, defStyle, 0);
        this.mBoxType = a.getInt(C0434R.styleable.CoolBoxView_boxType, this.mBoxType);
        this.mActiveCompartment = a.getInt(C0434R.styleable.CoolBoxView_activeCompartment, this.mActiveCompartment);
        this.mOpenDoorCompartment = a.getInt(C0434R.styleable.CoolBoxView_openDoorCompartment, this.mOpenDoorCompartment);
        a.recycle();
        updateView();
    }

    public void setEnabled(boolean enabled) {
        if (isEnabled() != enabled) {
            super.setEnabled(enabled);
            updateView();
        }
    }

    public int getBoxType() {
        return this.mBoxType;
    }

    public void setBoxType(int boxType) {
        if (boxType != this.mBoxType) {
            this.mBoxType = boxType;
            this.mRightLeadOpenInset = 0;
            this.mLeftLeadOpenInset = 0;
            this.mOpenDoorCompartment = 0;
            updateView();
        }
    }

    public int getActiveCompartment() {
        return this.mActiveCompartment;
    }

    public void setActiveCompartment(int activeCompartment) {
        if (activeCompartment != this.mActiveCompartment) {
            this.mActiveCompartment = activeCompartment;
            updateView();
        }
    }

    public int getOpenDoorCompartment() {
        return this.mOpenDoorCompartment;
    }

    public void setOpenDoorCompartment(int openDoorCompartment) {
        int leftLeadOpenInset;
        int rightLeadOpenInset;
        ValueAnimator animator;
        int leadOpenInset = 0;
        this.mOpenDoorCompartment = openDoorCompartment;
        int doorOpenInset = getDoorOpenMaxInset();
        if (getBoxType() == 1) {
            if (getOpenDoorCompartment() != 0) {
                leadOpenInset = doorOpenInset;
            }
            animator = setupAnimation(this.mLeftLeadOpenInset, leadOpenInset, this.mLeftLeadAnimator);
            this.mLeftLeadAnimator = animator;
        } else {
            if ((getOpenDoorCompartment() & 1) != 0) {
                leftLeadOpenInset = doorOpenInset;
            } else {
                leftLeadOpenInset = 0;
            }
            if ((getOpenDoorCompartment() & 2) != 0) {
                rightLeadOpenInset = doorOpenInset;
            } else {
                rightLeadOpenInset = 0;
            }
            animator = setupAnimation(this.mLeftLeadOpenInset, leftLeadOpenInset, this.mLeftLeadAnimator);
            this.mLeftLeadAnimator = animator;
            this.mRightLeadAnimator = setupAnimation(this.mRightLeadOpenInset, rightLeadOpenInset, this.mRightLeadAnimator);
            if (this.mRightLeadAnimator != null) {
                animator = this.mRightLeadAnimator;
            }
            if (!(this.mLeftLeadAnimator == null || this.mRightLeadAnimator == null)) {
                animator = null;
                this.mLeftLeadAnimator.start();
                this.mRightLeadAnimator.start();
            }
        }
        if (animator != null) {
            animator.start();
        }
    }

    public void setOnTouchCompartmentListener(OnTouchCompartmentListener l) {
        this.mOnTouchCompartmentListener = l;
    }

    public OnTouchCompartmentListener getOnTouchCompartmentListener() {
        return this.mOnTouchCompartmentListener;
    }

    public void onAnimationUpdate(ValueAnimator animation) {
        if (animation == this.mLeftLeadAnimator) {
            this.mLeftLeadOpenInset = ((Integer) animation.getAnimatedValue()).intValue();
        }
        if (animation == this.mRightLeadAnimator) {
            this.mRightLeadOpenInset = ((Integer) animation.getAnimatedValue()).intValue();
        }
        updateView();
    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        int compartmentIndex;
        boolean z = true;
        if (!isEnabled() || getBoxType() == 1 || event.getAction() != 1) {
            return true;
        }
        int index = event.getActionIndex();
        if (index <= -1) {
            return true;
        }
        if (2.0f * event.getX(index) < ((float) getWidth())) {
            compartmentIndex = 0;
        } else {
            compartmentIndex = 1;
        }
        int newActiveCompartment = (compartmentIndex == 0 ? 1 : 2) ^ getActiveCompartment();
        if (this.mOnTouchCompartmentListener != null) {
            OnTouchCompartmentListener onTouchCompartmentListener = this.mOnTouchCompartmentListener;
            if (newActiveCompartment - getActiveCompartment() <= 0) {
                z = false;
            }
            onTouchCompartmentListener.onCompartmentTouched(this, compartmentIndex, z);
        }
        return false;
    }

    private ValueAnimator setupAnimation(int oldValue, int newValue, ValueAnimator oldAnimator) {
        if (oldAnimator != null) {
            oldAnimator.removeAllUpdateListeners();
            oldAnimator.cancel();
        }
        if (oldValue == newValue) {
            return null;
        }
        ValueAnimator animator = ValueAnimator.ofInt(new int[]{oldValue, newValue});
        animator.addUpdateListener(this);
        animator.setRepeatCount(0);
        animator.setDuration(400);
        return animator;
    }

    @TargetApi(23)
    private LayerDrawable createLayerDrawable(Drawable[] drawables) {
        int count;
        int i = 0;
        int len = drawables.length;
        int count2 = 0;
        for (Drawable d : drawables) {
            if (d == null) {
                count2++;
            }
        }
        if (count2 != len) {
            Drawable[] drws = new Drawable[(len - count2)];
            int length = drawables.length;
            int count3 = 0;
            while (i < length) {
                Drawable d2 = drawables[i];
                if (d2 != null) {
                    count = count3 + 1;
                    drws[count3] = d2;
                } else {
                    count = count3;
                }
                i++;
                count3 = count;
            }
            drawables = drws;
            int i2 = count3;
        }
        LayerDrawable layerDrawable = new LayerDrawable(drawables);
        if (VERSION.SDK_INT >= 23) {
            for (int i3 = 0; i3 < drawables.length; i3++) {
                layerDrawable.setLayerGravity(i3, 17);
            }
        }
        return layerDrawable;
    }

    private void updateView() {
        Drawable[] drawables;
        int left = 0;
        int right = 0;
        if (this.mActiveCompartment == 0) {
            int i = this.mBoxType == 1 ? isEnabled() ? C0434R.mipmap.cfx_android_ikoner_16 : C0434R.mipmap.cfx_android_ikoner_16_off : isEnabled() ? C0434R.mipmap.cfx_android_ikoner_13 : C0434R.mipmap.cfx_android_ikoner_13_off;
            setImageResource(i);
            if (this.mBoxType == 1) {
                BitmapDrawable bdBackground = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), isEnabled() ? C0434R.mipmap.cfx_android_ikoner_16 : C0434R.mipmap.cfx_android_ikoner_16_off));
                BitmapDrawable bdArrowsLead = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.single_arrows));
                BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_16t));
                if (this.mLeftLeadOpenInset > getDoorOpenMaxInset() / 2) {
                    bdArrowsLead = null;
                    left = 1;
                }
                drawables = new Drawable[]{bdBackground, bdArrowsLead, bitmapDrawable};
            } else {
                BitmapDrawable bdBackground2 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), isEnabled() ? C0434R.mipmap.cfx_android_ikoner_13 : C0434R.mipmap.cfx_android_ikoner_13_off));
                BitmapDrawable bdArrowsLeftLead = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.dual_arrows_left));
                BitmapDrawable bdArrowsRightLead = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.dual_arrows_right));
                BitmapDrawable bdLeftLead = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_13_l));
                BitmapDrawable bitmapDrawable2 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_13_r));
                if (this.mLeftLeadOpenInset > getDoorOpenMaxInset() / 2) {
                    bdArrowsLeftLead = null;
                    left = 1;
                }
                if (this.mRightLeadOpenInset > getDoorOpenMaxInset() / 2) {
                    bdArrowsRightLead = null;
                    right = 1;
                }
                drawables = new Drawable[]{bdBackground2, bdArrowsLeftLead, bdArrowsRightLead, bdLeftLead, bitmapDrawable2};
            }
        } else if (this.mBoxType == 1) {
            BitmapDrawable bdBackground3 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), isEnabled() ? C0434R.mipmap.cfx_android_ikoner_16 : C0434R.mipmap.cfx_android_ikoner_16_off));
            BitmapDrawable bdArrowsLead2 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.single_arrows));
            BitmapDrawable bitmapDrawable3 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_16t));
            BitmapDrawable bdActive = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_17));
            if (this.mLeftLeadOpenInset > getDoorOpenMaxInset() / 2) {
                bdArrowsLead2 = null;
                left = 1;
            }
            drawables = new Drawable[]{bdBackground3, bdArrowsLead2, bitmapDrawable3, bdActive};
        } else {
            BitmapDrawable bdBackground4 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), isEnabled() ? C0434R.mipmap.cfx_android_ikoner_13 : C0434R.mipmap.cfx_android_ikoner_13_off));
            BitmapDrawable bdArrowsLeftLead2 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.dual_arrows_left));
            BitmapDrawable bdArrowsRightLead2 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.dual_arrows_right));
            BitmapDrawable bdLeftLead2 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_13_l));
            BitmapDrawable bitmapDrawable4 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_13_r));
            BitmapDrawable bdActive2 = null;
            BitmapDrawable bdActiveLeft = null;
            BitmapDrawable bdActiveRight = null;
            if (this.mLeftLeadOpenInset > getDoorOpenMaxInset() / 2) {
                bdArrowsLeftLead2 = null;
                left = 1;
            }
            if (this.mRightLeadOpenInset > getDoorOpenMaxInset() / 2) {
                bdArrowsRightLead2 = null;
                right = 1;
            }
            switch (this.mActiveCompartment) {
                case 1:
                    bdActive2 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_12));
                    break;
                case 2:
                    bdActive2 = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_15));
                    break;
                case 3:
                    bdActiveLeft = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_12));
                    bdActiveRight = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), C0434R.mipmap.cfx_android_ikoner_15));
                    break;
            }
            drawables = bdActive2 == null ? new Drawable[]{bdBackground4, bdArrowsLeftLead2, bdArrowsRightLead2, bdLeftLead2, bitmapDrawable4, bdActiveLeft, bdActiveRight} : new Drawable[]{bdBackground4, bdArrowsLeftLead2, bdArrowsRightLead2, bdLeftLead2, bitmapDrawable4, bdActive2};
        }
        if (drawables != null) {
            LayerDrawable layerDrawable = createLayerDrawable(drawables);
            if (getBoxType() != 1) {
                if (this.mLeftLeadOpenInset != 0) {
                    layerDrawable.setLayerInset((3 - left) - right, 0, this.mLeftLeadOpenInset, 0, 0);
                }
                if (this.mRightLeadOpenInset != 0) {
                    layerDrawable.setLayerInset((4 - left) - right, 0, this.mRightLeadOpenInset, 0, 0);
                }
            } else if (this.mLeftLeadOpenInset != 0) {
                layerDrawable.setLayerInset(2 - left, 0, this.mLeftLeadOpenInset, 0, 0);
            }
            setImageDrawable(layerDrawable);
        }
    }

    private final int getDoorOpenMaxInset() {
        return -((int) Math.ceil((double) Utils.pxFromDp(getContext(), 30.0f)));
    }
}
