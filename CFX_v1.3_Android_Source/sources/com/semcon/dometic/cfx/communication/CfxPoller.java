package com.semcon.dometic.cfx.communication;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.semcon.dometic.cfx.data.Compartment;
import com.semcon.dometic.cfx.data.Door;
import com.semcon.dometic.cfx.data.Refrigerator;
import com.semcon.dometic.cfx.event.LostConnectionEvent;
import com.semcon.dometic.cfx.event.OnNewCfxInfo;
import com.semcon.dometic.cfx.persistent.CfxBoxSettingsItem;
import com.semcon.dometic.cfx.persistent.CfxBoxSettingsItem.AlarmType;
import com.semcon.dometic.cfx.persistent.CfxBoxSettingsItem.CompartmentAlarmInfo;
import com.semcon.dometic.cfx.persistent.CfxBoxSettingsItem.CompartmentAlarmInfo.TargetTemperatureBoundsOperator;
import com.semcon.dometic.cfx.persistent.CfxSettingsPersister;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import com.semcon.dometic.cfx.tcp.CfxProtocolException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import roboguice.event.EventManager;

@Singleton
public class CfxPoller {
    public static final int POLLING_DELAY_MILLIS = 500;
    /* access modifiers changed from: private */
    @Inject
    public ICfxCommunication mCfxCommunication;
    /* access modifiers changed from: private */
    public boolean mConnected;
    private AsyncTask<Void, Void, Refrigerator> mFetchData;
    /* access modifiers changed from: private */
    @Named("GlobalEventManager")
    @Inject
    public EventManager mGlobalEventManager;
    /* access modifiers changed from: private */
    public Refrigerator mRefrigerator;
    /* access modifiers changed from: private */
    @Inject
    public Handler mServiceHandler;

    class FetchCfxData extends AsyncTask<Void, Void, Refrigerator> {
        /* access modifiers changed from: private */
        public final Context mContext;

        public FetchCfxData(Context context) {
            this.mContext = context;
        }

        /* access modifiers changed from: protected */
        public Refrigerator doInBackground(Void... params) {
            Refrigerator refrigerator = null;
            if (CfxPoller.this.mCfxCommunication == null) {
                return null;
            }
            try {
                if (!CfxPoller.this.mCfxCommunication.isConnected()) {
                    CfxPoller.this.mCfxCommunication.connect();
                }
                refrigerator = CfxPoller.this.getRefrigeratorInfo();
                if (refrigerator != null) {
                    CfxPoller.this.mRefrigerator = refrigerator;
                }
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    CfxPoller.this.mCfxCommunication.disconnect();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            return refrigerator;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Refrigerator refrigerator) {
            synchronized (CfxPoller.this) {
                if (CfxPoller.this.isPolling()) {
                    if (refrigerator != null) {
                        CfxPoller.this.mConnected = true;
                        checkAlarm(refrigerator);
                        CfxPoller.this.mGlobalEventManager.fire(new OnNewCfxInfo(this.mContext, refrigerator));
                    } else {
                        if (CfxPoller.this.mConnected) {
                            Log.d("Event", "Lost connection");
                            CfxPoller.this.mGlobalEventManager.fire(new LostConnectionEvent(this.mContext));
                        }
                        CfxPoller.this.mConnected = false;
                    }
                    if (CfxPoller.this.mServiceHandler == null) {
                        CfxPoller.this.mServiceHandler = new Handler();
                    }
                    CfxPoller.this.mServiceHandler.postDelayed(new Runnable() {
                        public void run() {
                            new FetchCfxData(FetchCfxData.this.mContext).execute(new Void[0]);
                        }
                    }, 500);
                }
            }
        }

        private void checkAlarm(Refrigerator refrigerator) {
            if (refrigerator != null && refrigerator.getCompartments().size() > 0) {
                Context context = this.mContext;
                if (context != null) {
                    WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                    WifiInfo info = wifiManager == null ? null : wifiManager.getConnectionInfo();
                    CfxBoxSettingsItem boxSettingsItem = CfxSettingsPersister.getBoxSettings(context, info == null ? "" : info.getSSID());
                    if (boxSettingsItem != null && boxSettingsItem.getAlarmType() != AlarmType.None) {
                        List<Compartment> compartments = refrigerator.getCompartments();
                        boolean isDirty = false;
                        if (refrigerator.getTemperatureUnit() != boxSettingsItem.getTemperatureUnit()) {
                            boxSettingsItem.setTemperatureUnit(refrigerator.getTemperatureUnit());
                            boxSettingsItem.setCompartmentAlarms(new CompartmentAlarmInfo[0]);
                            isDirty = true;
                        }
                        if (boxSettingsItem.getCompartmentAlarms().length == 0) {
                            List<CompartmentAlarmInfo> newAlarms = new ArrayList<>();
                            for (int i = 0; i < compartments.size(); i++) {
                                Compartment comp = (Compartment) compartments.get(i);
                                if (comp != null && comp.getActualTemperature() > -45) {
                                    newAlarms.add(new CompartmentAlarmInfo(i + 1, comp.getActualTemperature(), comp.getTargetTemperature()));
                                }
                            }
                            if (newAlarms.size() > 0) {
                                CompartmentAlarmInfo[] alarms = new CompartmentAlarmInfo[newAlarms.size()];
                                newAlarms.toArray(alarms);
                                boxSettingsItem.setCompartmentAlarms(alarms);
                                isDirty = true;
                            }
                        }
                        CompartmentAlarmInfo[] compartmentAlarms = boxSettingsItem.getCompartmentAlarms();
                        int length = compartmentAlarms.length;
                        for (int i2 = 0; i2 < length; i2++) {
                            CompartmentAlarmInfo compartmentAlarm = compartmentAlarms[i2];
                            if (compartments.size() >= compartmentAlarm.getCompartmentNumber()) {
                                Compartment compartment = (Compartment) compartments.get(compartmentAlarm.getCompartmentNumber() - 1);
                                if (compartment != null) {
                                    if (compartmentAlarm.getTargetTemperature() != compartment.getTargetTemperature()) {
                                        compartmentAlarm.setTargetTemperature(compartment.getTargetTemperature());
                                        compartmentAlarm.setInitialActualTemperature(compartment.getActualTemperature());
                                        compartmentAlarm.mo7980xccef1b3d(false);
                                        compartmentAlarm.setOperator(TargetTemperatureBoundsOperator.In);
                                        isDirty = true;
                                    }
                                    if (compartmentAlarm.getOperator().check(compartment.getActualTemperature(), compartment.getTargetTemperature(), boxSettingsItem.getAlarmType())) {
                                        if (compartmentAlarm.mo7976x88957605()) {
                                            CFXError err = compartmentAlarm.getCompartmentNumber() == 1 ? CFXError.TEMPERATURE_ALARM_1_ERROR : compartmentAlarm.getCompartmentNumber() == 2 ? CFXError.TEMPERATURE_ALARM_2_ERROR : CFXError.TEMPERATURE_ALARM_ERROR;
                                            refrigerator.getErrorCodes().add(err);
                                            Log.i("CFX_TEMPERATURE_ALARM", "Actual: " + compartment.getActualTemperature() + ", Target: " + compartment.getTargetTemperature());
                                        } else {
                                            compartmentAlarm.mo7980xccef1b3d(true);
                                            compartmentAlarm.setOperator(compartmentAlarm.getOperator().next());
                                            isDirty = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (isDirty) {
                            CfxSettingsPersister.setBoxSettings(context, boxSettingsItem);
                        }
                    }
                }
            }
        }
    }

    public synchronized void startPolling(Context context) {
        if (!isPolling()) {
            this.mFetchData = new FetchCfxData(context).execute(new Void[0]);
        }
    }

    public synchronized void stopPolling() {
        if (isPolling()) {
            if (this.mServiceHandler != null) {
                this.mServiceHandler.removeCallbacks(null);
            }
            this.mFetchData.cancel(true);
            this.mFetchData = null;
        }
    }

    /* access modifiers changed from: private */
    public boolean isPolling() {
        return this.mFetchData != null;
    }

    @NonNull
    public Refrigerator getRefrigeratorInfo() throws IOException, CfxProtocolException {
        Refrigerator tempRefrigerator;
        List<Compartment> compartments = new ArrayList<>();
        try {
            boolean isRefrigeratorOn = this.mCfxCommunication.isRefrigeratorOn() != 0;
            for (int i = 0; i < this.mCfxCommunication.getCompartmentCount(); i++) {
                compartments.add(new Compartment((!isRefrigeratorOn || !this.mCfxCommunication.isCompartmentOn(i + 1)) ? 0 : 1, 0, this.mCfxCommunication.getTempOfCompartment(i + 1), this.mCfxCommunication.getSetTempOfCompartment(i + 1), null, null, new Door(this.mCfxCommunication.isDoorOpen(i + 1) ? 1 : 0, this.mCfxCommunication.isDoorAlarming(i + 1) ? 1 : 0)));
            }
            tempRefrigerator = new Refrigerator(compartments, this.mCfxCommunication.isRefrigeratorOn(), this.mCfxCommunication.isAcAvailable() ? 1 : 0, this.mCfxCommunication.getBatteryProtectionMode(), this.mCfxCommunication.getTempUnit(), 0, (double) this.mCfxCommunication.getDcVoltage());
            try {
                tempRefrigerator.setErrorCodes(this.mCfxCommunication.getErrorCodes());
            } catch (IOException e) {
                e = e;
                e.printStackTrace();
                return tempRefrigerator;
            } catch (CfxProtocolException e2) {
                e = e2;
                e.printStackTrace();
                return tempRefrigerator;
            }
        } catch (IOException e3) {
            e = e3;
            tempRefrigerator = null;
            e.printStackTrace();
            return tempRefrigerator;
        } catch (CfxProtocolException e4) {
            e = e4;
            tempRefrigerator = null;
            e.printStackTrace();
            return tempRefrigerator;
        }
        return tempRefrigerator;
    }

    public Refrigerator getLatestRefrigeratorInfo() {
        return this.mRefrigerator;
    }
}
