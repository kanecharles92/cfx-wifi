package com.semcon.dometic.cfx.communication;

import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import com.semcon.dometic.cfx.tcp.CfxProtocolException;
import java.io.IOException;
import java.util.List;

public interface ICfxCommunication {
    void connect() throws IOException;

    void disconnect() throws IOException;

    BatteryProtectionMode getBatteryProtectionMode() throws IOException, CfxProtocolException;

    int getCompartmentCount() throws IOException;

    float getDcVoltage() throws IOException, CfxProtocolException;

    List<CFXError> getErrorCodes() throws IOException, CfxProtocolException;

    int getSetTempOfCompartment(int i) throws IOException, CfxProtocolException;

    int getTempOfCompartment(int i) throws IOException, CfxProtocolException;

    TemperatureUnit getTempUnit() throws IOException, CfxProtocolException;

    boolean isAcAvailable() throws IOException, CfxProtocolException;

    boolean isClosed() throws IOException;

    boolean isCompartmentOn(int i) throws IOException, CfxProtocolException;

    boolean isConnected() throws IOException;

    boolean isDoorAlarming(int i) throws IOException, CfxProtocolException;

    boolean isDoorOpen(int i) throws IOException, CfxProtocolException;

    int isRefrigeratorOn() throws IOException, CfxProtocolException;

    void reOpenSocket() throws IOException;

    void setBatteryProtectionMode(BatteryProtectionMode batteryProtectionMode) throws IOException, CfxProtocolException;

    void setCompartmentOn(int i, boolean z) throws IOException, CfxProtocolException;

    String setNameAndPassword(String str, String str2) throws IOException, CfxProtocolException;

    int setRefrigeratorOn(int i) throws IOException, CfxProtocolException;

    void setTempCompartment(int i, int i2) throws IOException, CfxProtocolException;

    void setTempUnit(TemperatureUnit temperatureUnit) throws IOException, CfxProtocolException;
}
