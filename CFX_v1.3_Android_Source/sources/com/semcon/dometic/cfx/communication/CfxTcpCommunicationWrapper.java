package com.semcon.dometic.cfx.communication;

import android.os.AsyncTask;
import com.google.inject.Singleton;
import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import com.semcon.dometic.cfx.tcp.CfxProtocolException;
import com.semcon.dometic.cfx.tcp.CfxTcpCommunication;
import java.io.IOException;
import java.util.List;

@Singleton
public class CfxTcpCommunicationWrapper implements ICfxCommunication {
    private CfxTcpCommunication mCfxCommunication;

    private class DisconnectTask extends AsyncTask<Void, Void, Void> {
        private DisconnectTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                CfxTcpCommunicationWrapper.this.getTcpConnection().disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /* access modifiers changed from: private */
    public CfxTcpCommunication getTcpConnection() throws IOException {
        if (this.mCfxCommunication == null || !this.mCfxCommunication.isConnected()) {
            System.out.println("Incoming: Creating new socket.");
            this.mCfxCommunication = new CfxTcpCommunication("192.168.1.1", 6378);
        }
        return this.mCfxCommunication;
    }

    public int isRefrigeratorOn() throws IOException, CfxProtocolException {
        return getTcpConnection().getRefrigeratorOn();
    }

    public int setRefrigeratorOn(int i) throws IOException, CfxProtocolException {
        return getTcpConnection().setRefrigeratorOn(i);
    }

    public boolean isAcAvailable() throws IOException, CfxProtocolException {
        return getTcpConnection().isAcAvailable();
    }

    public TemperatureUnit getTempUnit() throws IOException, CfxProtocolException {
        return getTcpConnection().getTempUnit() == CfxTcpCommunication.CELSIUS ? TemperatureUnit.Celsius : TemperatureUnit.Fahrenheit;
    }

    public void setTempUnit(TemperatureUnit unit) throws IOException, CfxProtocolException {
        getTcpConnection().changeTempUnit(unit == TemperatureUnit.Celsius ? (byte) CfxTcpCommunication.CELSIUS : (byte) CfxTcpCommunication.FARENHEIT);
    }

    public boolean isCompartmentOn(int compartment) throws IOException, CfxProtocolException {
        return getTcpConnection().getCompartmentOn(compartment);
    }

    public float getDcVoltage() throws IOException, CfxProtocolException {
        return getTcpConnection().getDCSupplyVoltage();
    }

    public int getSetTempOfCompartment(int compartment) throws IOException, CfxProtocolException {
        return getTcpConnection().getSetTempOfCompartment(compartment);
    }

    public boolean isDoorAlarming(int compartmentNumber) throws IOException, CfxProtocolException {
        return getTcpConnection().isDoorAlarming(compartmentNumber);
    }

    public int getTempOfCompartment(int compartment) throws IOException, CfxProtocolException {
        return getTcpConnection().getTempOfCompartment(compartment);
    }

    public int getCompartmentCount() throws IOException {
        return getTcpConnection().getCompartmentCount();
    }

    public boolean isConnected() throws IOException {
        try {
            return (this.mCfxCommunication == null || !this.mCfxCommunication.isConnected() || this.mCfxCommunication.getCfxInfo() == null) ? false : true;
        } catch (CfxProtocolException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setTempCompartment(int temperature, int compartment) throws IOException, CfxProtocolException {
        getTcpConnection().setTempCompartment(temperature, compartment);
    }

    public boolean isClosed() throws IOException {
        return getTcpConnection().isClosed();
    }

    public void connect() throws IOException {
        getTcpConnection();
    }

    public void reOpenSocket() throws IOException {
        this.mCfxCommunication.reOpenSocket();
    }

    public void disconnect() throws IOException {
        new DisconnectTask().execute(new Void[0]);
    }

    public void setCompartmentOn(int compartment, boolean on) throws IOException, CfxProtocolException {
        getTcpConnection().setCompartmentOn(compartment, on);
    }

    public List<CFXError> getErrorCodes() throws IOException, CfxProtocolException {
        return getTcpConnection().getErrorCodes();
    }

    public boolean isDoorOpen(int doorNumber) throws IOException, CfxProtocolException {
        return getTcpConnection().isDoorOpen(doorNumber);
    }

    public BatteryProtectionMode getBatteryProtectionMode() throws IOException, CfxProtocolException {
        switch (getTcpConnection().getAbsSetting()) {
            case 0:
                return BatteryProtectionMode.Low;
            case 1:
                return BatteryProtectionMode.Medium;
            case 2:
                return BatteryProtectionMode.High;
            default:
                return null;
        }
    }

    public void setBatteryProtectionMode(BatteryProtectionMode mode) throws IOException, CfxProtocolException {
        if (mode == BatteryProtectionMode.Low) {
            getTcpConnection().setABS(0);
        } else if (mode == BatteryProtectionMode.Medium) {
            getTcpConnection().setABS(1);
        } else if (mode == BatteryProtectionMode.High) {
            getTcpConnection().setABS(2);
        }
    }

    public String setNameAndPassword(String name, String password) throws IOException, CfxProtocolException {
        return getTcpConnection().change_Wifi_Credentials(name, password);
    }
}
