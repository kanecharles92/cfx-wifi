package com.semcon.dometic.cfx.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.data.Refrigerator;
import java.util.List;

public class DevicesAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    Context context;
    private int counter;
    List<Refrigerator> data;

    public static class ViewHolder {
        public TextView device_name;
        public ImageView type_of_connection;
    }

    public DevicesAdapter(Context context2, List<Refrigerator> data2) {
        this.context = context2;
        this.data = data2;
        inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.data.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(this.data.indexOf(Integer.valueOf(position)));
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;
        if (convertView == null) {
            try {
                vi = inflater.inflate(C0434R.layout.device_simple_layout, null);
                holder = new ViewHolder();
                holder.device_name = (TextView) vi.findViewById(C0434R.C0436id.device_name);
                holder.type_of_connection = (ImageView) vi.findViewById(C0434R.C0436id.type_of_connection);
                vi.setTag(holder);
            } catch (Exception e) {
            }
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        holder.device_name.setText(((Refrigerator) this.data.get(position)).getModel());
        holder.type_of_connection.setImageResource(C0434R.C0435drawable.wifi_icon);
        return vi;
    }
}
