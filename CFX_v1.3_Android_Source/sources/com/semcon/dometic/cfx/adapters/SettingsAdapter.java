package com.semcon.dometic.cfx.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.data.SettingsItem;
import java.util.List;

public class SettingsAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    Context context;
    private int counter;
    List<SettingsItem> data;

    public static class SettingsItemViewHolder {
        public TextView mName;
        public TextView mSelectedValue;
    }

    public SettingsAdapter(Context context2, List<SettingsItem> data2) {
        this.context = context2;
        this.data = data2;
        inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.data.size();
    }

    public SettingsItem getItem(int position) {
        return (SettingsItem) this.data.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        SettingsItemViewHolder holder;
        View vi = convertView;
        if (convertView == null) {
            try {
                vi = inflater.inflate(C0434R.layout.cfx_layout_single_setting, null);
                holder = new SettingsItemViewHolder();
                holder.mName = (TextView) vi.findViewById(C0434R.C0436id.setting_name);
                holder.mSelectedValue = (TextView) vi.findViewById(C0434R.C0436id.setting_selected_value);
                vi.setTag(holder);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder = (SettingsItemViewHolder) vi.getTag();
        }
        holder.mName.setText(((SettingsItem) this.data.get(position)).getName());
        holder.mSelectedValue.setText(((SettingsItem) this.data.get(position)).getSelectedValue());
        return vi;
    }
}
