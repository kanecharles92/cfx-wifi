package com.semcon.dometic.cfx.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.data.DeviceTemp;
import java.util.List;

public class Simple_device_adapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    Context context;
    private int counter;
    List<DeviceTemp> data;

    public static class ViewHolder {
        public TextView device_name;
    }

    public Simple_device_adapter(Context context2, List<DeviceTemp> data2) {
        this.context = context2;
        this.data = data2;
        inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.data.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(this.data.indexOf(Integer.valueOf(position)));
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;
        if (convertView == null) {
            try {
                vi = inflater.inflate(C0434R.layout.simple_device_layout, null);
                holder = new ViewHolder();
                holder.device_name = (TextView) vi.findViewById(C0434R.C0436id.device_name);
                vi.setTag(holder);
            } catch (Exception e) {
            }
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        holder.device_name.setText(((DeviceTemp) this.data.get(position)).getDevice_name());
        return vi;
    }
}
