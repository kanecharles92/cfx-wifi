package com.semcon.dometic.cfx.utils;

import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;

public class CfxStringUtil {
    public static String getStringTemperatureUnitRepresentation(TemperatureUnit unit) {
        return unit == TemperatureUnit.Celsius ? "°C" : "°F";
    }
}
