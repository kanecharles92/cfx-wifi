package com.semcon.dometic.cfx.event;

import android.content.Context;

public class LostConnectionEvent {
    private final Context mContext;

    public LostConnectionEvent(Context context) {
        this.mContext = context;
    }
}
