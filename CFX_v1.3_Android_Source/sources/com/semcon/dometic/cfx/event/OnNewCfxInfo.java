package com.semcon.dometic.cfx.event;

import android.content.Context;
import com.semcon.dometic.cfx.data.Refrigerator;

public class OnNewCfxInfo {
    private final Context mContext;
    private final Refrigerator mRefrigerator;

    public OnNewCfxInfo(Context context, Refrigerator refrigerator) {
        this.mRefrigerator = refrigerator;
        this.mContext = context;
    }

    public Refrigerator getRefrigerator() {
        return this.mRefrigerator;
    }

    public Context getContext() {
        return this.mContext;
    }
}
