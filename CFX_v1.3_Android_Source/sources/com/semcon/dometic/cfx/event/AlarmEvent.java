package com.semcon.dometic.cfx.event;

import android.content.Context;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import java.util.List;

public class AlarmEvent {
    private final Context mContext;
    private final List<CFXError> mErrors;

    public AlarmEvent(List<CFXError> errors, Context context) {
        this.mErrors = errors;
        this.mContext = context;
    }

    public List<CFXError> getErrors() {
        return this.mErrors;
    }

    public Context getContext() {
        return this.mContext;
    }
}
