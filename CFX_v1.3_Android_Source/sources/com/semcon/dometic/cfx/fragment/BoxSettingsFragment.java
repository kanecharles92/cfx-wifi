package com.semcon.dometic.cfx.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.p003v7.app.AlertDialog.Builder;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.inject.Inject;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.adapters.SettingsAdapter;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.data.SettingsItem;
import com.semcon.dometic.cfx.persistent.CfxBoxSettingsItem;
import com.semcon.dometic.cfx.persistent.CfxBoxSettingsItem.AlarmType;
import com.semcon.dometic.cfx.persistent.CfxBoxSettingsItem.CompartmentAlarmInfo;
import com.semcon.dometic.cfx.persistent.CfxSettingsPersister;
import com.semcon.dometic.cfx.tcp.CfxProtocolException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import roboguice.fragment.RoboFragment;

public class BoxSettingsFragment extends RoboFragment {
    /* access modifiers changed from: private */
    @Inject
    public ICfxCommunication mCfxCommunication;
    /* access modifiers changed from: private */
    public ListView mListView;
    /* access modifiers changed from: private */
    public int mSelectedSettingsItem;

    private class GetBoxInfo extends AsyncTask<Void, Void, int[]> {
        private GetBoxInfo() {
        }

        /* access modifiers changed from: protected */
        public int[] doInBackground(Void... params) {
            int[] ret = new int[2];
            try {
                if (!BoxSettingsFragment.this.mCfxCommunication.isConnected()) {
                    BoxSettingsFragment.this.mCfxCommunication.connect();
                }
                BatteryProtectionMode mode = BoxSettingsFragment.this.mCfxCommunication.getBatteryProtectionMode();
                TemperatureUnit tempUnit = BoxSettingsFragment.this.mCfxCommunication.getTempUnit();
                ret[0] = mode.ordinal();
                ret[1] = tempUnit.ordinal();
                return ret;
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    BoxSettingsFragment.this.mCfxCommunication.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }
        }
    }

    private class SetBatteryProtectionModeTask extends AsyncTask<Void, Void, Void> {
        private final BatteryProtectionMode mMode;

        SetBatteryProtectionModeTask(BatteryProtectionMode mode) {
            this.mMode = mode;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                if (!BoxSettingsFragment.this.mCfxCommunication.isConnected()) {
                    BoxSettingsFragment.this.mCfxCommunication.connect();
                }
                BoxSettingsFragment.this.mCfxCommunication.setBatteryProtectionMode(this.mMode);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            return null;
        }
    }

    private class SetUnitTask extends AsyncTask<Void, Void, Void> {
        private final TemperatureUnit mUnit;

        SetUnitTask(TemperatureUnit unit) {
            this.mUnit = unit;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                if (!BoxSettingsFragment.this.mCfxCommunication.isConnected()) {
                    BoxSettingsFragment.this.mCfxCommunication.connect();
                }
                BoxSettingsFragment.this.mCfxCommunication.setTempUnit(this.mUnit);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            return null;
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(C0434R.layout.cfx_layout_box_settings, container, false);
        Context context = getActivity();
        String[] settingNames = context.getResources().getStringArray(C0434R.array.settings_array);
        ArrayList<SettingsItem> settings = new ArrayList<>(settingNames.length);
        for (int i = 0; i < settingNames.length; i++) {
            String settingName = settingNames[i];
            List<String> values = new ArrayList<>();
            if (i < 3) {
                values = Arrays.asList(context.getResources().getStringArray(getResources().getIdentifier("settings_" + i, "array", context.getPackageName())));
            } else if (i == 3) {
                values.add(getWifiName());
            } else {
                values.add("");
            }
            SettingsItem settingsItem = new SettingsItem(settingName, 0, values);
            settings.add(settingsItem);
        }
        try {
            if (settings.size() > 1) {
                GetBoxInfo getBoxInfo = new GetBoxInfo();
                int[] result = (int[]) getBoxInfo.execute(new Void[0]).get(2000, TimeUnit.MILLISECONDS);
                if (result != null && result.length > 1) {
                    ((SettingsItem) settings.get(0)).setSelectedValueIndex(result[0]);
                    ((SettingsItem) settings.get(1)).setSelectedValueIndex(result[1]);
                }
            }
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        } catch (ExecutionException e3) {
            e3.printStackTrace();
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        if (settings.size() > 2) {
            int selectedValueIndex = 0;
            CfxBoxSettingsItem boxSettingsItem = CfxSettingsPersister.getBoxSettings(getContext(), getWifiName());
            if (boxSettingsItem != null) {
                selectedValueIndex = boxSettingsItem.getAlarmType().ordinal();
                if (selectedValueIndex > 2) {
                    selectedValueIndex -= 2;
                }
            }
            ((SettingsItem) settings.get(2)).setSelectedValueIndex(selectedValueIndex);
            int tempUnitIndex = ((SettingsItem) settings.get(1)).getSelectedValueIndex();
            ((SettingsItem) settings.get(2)).setValues(Arrays.asList(getContext().getResources().getStringArray(getResources().getIdentifier("settings_2" + (tempUnitIndex == 0 ? "" : "b"), "array", getContext().getPackageName()))));
        }
        this.mListView = (ListView) view.findViewById(C0434R.C0436id.settings_listview);
        ListView listView = this.mListView;
        SettingsAdapter settingsAdapter = new SettingsAdapter(context, settings);
        listView.setAdapter(settingsAdapter);
        ListView listView2 = this.mListView;
        C04521 r0 = new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int pos, long id) {
                BoxSettingsFragment.this.mSelectedSettingsItem = pos;
                if (BoxSettingsFragment.this.mSelectedSettingsItem >= 0 && BoxSettingsFragment.this.mSelectedSettingsItem < BoxSettingsFragment.this.mListView.getCount()) {
                    if (BoxSettingsFragment.this.mSelectedSettingsItem <= 2) {
                        Builder builder = new Builder(BoxSettingsFragment.this.getActivity());
                        ArrayAdapter<SpannableString> arrayAdapter = new ArrayAdapter<>(BoxSettingsFragment.this.getActivity(), 17367058);
                        SettingsItem settingsItem = ((SettingsAdapter) BoxSettingsFragment.this.mListView.getAdapter()).getItem(BoxSettingsFragment.this.mSelectedSettingsItem);
                        View dialogTitleView = LayoutInflater.from(BoxSettingsFragment.this.getContext()).inflate(C0434R.layout.cfx_fragment_box_settings_dialog_title, null);
                        ((TextView) dialogTitleView.findViewById(C0434R.C0436id.title_tv)).setText(settingsItem.getName());
                        TextView tvTitle = (TextView) dialogTitleView.findViewById(C0434R.C0436id.title_detail_tv);
                        if (BoxSettingsFragment.this.mSelectedSettingsItem == 2) {
                            tvTitle.setText(C0434R.string.temp_alarm_detail);
                        } else {
                            tvTitle.setVisibility(8);
                        }
                        builder.setCustomTitle(dialogTitleView);
                        List access$400 = BoxSettingsFragment.this.getSelectedSettingOptions();
                        arrayAdapter.addAll(BoxSettingsFragment.this.getSelectedSettingOptions());
                        builder.setNegativeButton(17039360, (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.setSingleChoiceItems((ListAdapter) arrayAdapter, settingsItem.getSelectedValueIndex(), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                SettingsAdapter adapter = (SettingsAdapter) BoxSettingsFragment.this.mListView.getAdapter();
                                adapter.getItem(BoxSettingsFragment.this.mSelectedSettingsItem).setSelectedValueIndex(which);
                                switch (BoxSettingsFragment.this.mSelectedSettingsItem) {
                                    case 0:
                                        new SetBatteryProtectionModeTask(BoxSettingsFragment.this.getBatteryProtectionMode(which)).execute(new Void[0]);
                                        break;
                                    case 1:
                                        new SetUnitTask(BoxSettingsFragment.this.getTemperatureUnit(which)).execute(new Void[0]);
                                        break;
                                    case 2:
                                        break;
                                }
                                int tempUnitIndex = adapter.getItem(1).getSelectedValueIndex();
                                int alarmIndex = adapter.getItem(2).getSelectedValueIndex();
                                int alarmTypeIndex = alarmIndex + (alarmIndex == 0 ? 0 : tempUnitIndex * 2);
                                adapter.getItem(2).setValues(Arrays.asList(BoxSettingsFragment.this.getContext().getResources().getStringArray(BoxSettingsFragment.this.getResources().getIdentifier("settings_2" + (tempUnitIndex == 0 ? "" : "b"), "array", BoxSettingsFragment.this.getContext().getPackageName()))));
                                String wifiName = BoxSettingsFragment.this.getWifiName();
                                CfxBoxSettingsItem boxSettingsItem = CfxSettingsPersister.getBoxSettings(BoxSettingsFragment.this.getContext(), wifiName);
                                if (boxSettingsItem == null) {
                                    boxSettingsItem = new CfxBoxSettingsItem(wifiName);
                                }
                                boxSettingsItem.setCompartmentAlarms(new CompartmentAlarmInfo[0]);
                                boxSettingsItem.setAlarmType(AlarmType.values()[alarmTypeIndex]);
                                CfxSettingsPersister.setBoxSettings(BoxSettingsFragment.this.getContext(), boxSettingsItem);
                                adapter.notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        });
                        builder.show().setCanceledOnTouchOutside(false);
                    } else if (BoxSettingsFragment.this.mSelectedSettingsItem == 3) {
                        BoxSettingsFragment.this.getFragmentManager().beginTransaction().replace(BoxSettingsFragment.this.getId(), new BoxConnectionSettingsFragment(), null).addToBackStack(null).commitAllowingStateLoss();
                    } else if (BoxSettingsFragment.this.mSelectedSettingsItem == 4) {
                        BoxSettingsFragment.this.getFragmentManager().beginTransaction().replace(BoxSettingsFragment.this.getId(), new LegalFragment(), null).addToBackStack(null).commitAllowingStateLoss();
                    }
                }
            }
        };
        listView2.setOnItemClickListener(r0);
        this.mListView.setLongClickable(false);
        view.setLongClickable(false);
        return view;
    }

    /* access modifiers changed from: private */
    public List<SpannableString> getSelectedSettingOptions() {
        if (this.mSelectedSettingsItem < 0 || this.mSelectedSettingsItem >= this.mListView.getCount()) {
            return new ArrayList();
        }
        SettingsItem settingsItem = ((SettingsAdapter) this.mListView.getAdapter()).getItem(this.mSelectedSettingsItem);
        int resourceID = getResources().getIdentifier("settings_" + this.mSelectedSettingsItem + "_0", "array", getContext().getPackageName());
        String[] textDetails = resourceID == 0 ? null : getContext().getResources().getStringArray(resourceID);
        ArrayList<SpannableString> options = new ArrayList<>();
        for (int i = 0; i < settingsItem.getValues().size(); i++) {
            String optionName = (String) settingsItem.getValues().get(i);
            SpannableString s = new SpannableString(optionName);
            if (textDetails != null && textDetails.length > i && !textDetails[i].isEmpty()) {
                String optionTitle = "\n" + optionName + "\n";
                String optionString = optionTitle + textDetails[i];
                s = new SpannableString(optionString);
                s.setSpan(new RelativeSizeSpan(0.5f), optionTitle.length(), optionString.length(), 0);
            }
            options.add(s);
        }
        return options;
    }

    /* access modifiers changed from: private */
    public BatteryProtectionMode getBatteryProtectionMode(int menuItemPosition) {
        return BatteryProtectionMode.values()[menuItemPosition];
    }

    /* access modifiers changed from: private */
    public TemperatureUnit getTemperatureUnit(int menuItemPosition) {
        return TemperatureUnit.values()[menuItemPosition];
    }

    /* access modifiers changed from: private */
    public String getWifiName() {
        WifiManager wifiManager = (WifiManager) getContext().getSystemService("wifi");
        WifiInfo info = wifiManager == null ? null : wifiManager.getConnectionInfo();
        return info == null ? "" : info.getSSID();
    }
}
