package com.semcon.dometic.cfx.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.inject.Inject;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.communication.CfxPoller;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral;
import java.io.IOException;
import java.io.InputStream;
import roboguice.fragment.RoboFragment;

public class LegalFragment extends RoboFragment {
    @Inject
    private ICfxCommunication mCfxCommunication;
    @Inject
    private CfxPoller mCfxPoller;
    @Inject
    private CfxSharedPreferencesHelper_univeral mCfxSharedPreferencesHelperUniveral;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(C0434R.layout.fragment_cfx_legal, container, false);
        TextView tv = (TextView) view.findViewById(C0434R.C0436id.legal_tv);
        tv.setMovementMethod(ScrollingMovementMethod.getInstance());
        tv.setText(loadLegalText());
        return view;
    }

    private String loadLegalText() {
        String text = "";
        InputStream input = null;
        try {
            input = getContext().getAssets().open("legal.txt");
            byte[] buffer = new byte[input.available()];
            input.read(buffer);
            String text2 = new String(buffer, "UTF-8");
            if (input == null) {
                return text2;
            }
            try {
                input.close();
                return text2;
            } catch (IOException e) {
                e.printStackTrace();
                return text2;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            if (input == null) {
                return text;
            }
            try {
                input.close();
                return text;
            } catch (IOException e3) {
                e3.printStackTrace();
                return text;
            }
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
        }
    }
}
