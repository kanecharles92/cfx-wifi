package com.semcon.dometic.cfx.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import com.google.inject.Inject;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.communication.CfxPoller;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.data.Refrigerator;
import com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral;
import com.semcon.dometic.cfx.tcp.CfxProtocolException;
import java.io.IOException;
import roboguice.fragment.RoboFragment;

public class BoxBatterySettingsFragment extends RoboFragment {
    OnCheckedChangeListener mBatteryProtectionGroupListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            BatteryProtectionMode batteryProtectionMode = null;
            if (checkedId == C0434R.C0436id.button_protection_high) {
                batteryProtectionMode = BatteryProtectionMode.High;
            } else if (checkedId == C0434R.C0436id.button_protection_medium) {
                batteryProtectionMode = BatteryProtectionMode.Medium;
            } else if (checkedId == C0434R.C0436id.button_protection_low) {
                batteryProtectionMode = BatteryProtectionMode.Low;
            }
            new SetBatteryProtectionModeTask(batteryProtectionMode).execute(new Void[0]);
        }
    };
    /* access modifiers changed from: private */
    @Inject
    public ICfxCommunication mCfxCommunication;
    @Inject
    private CfxPoller mCfxPoller;
    @Inject
    private CfxSharedPreferencesHelper_univeral mCfxSharedPreferencesHelperUniveral;
    OnCheckedChangeListener mUnitGroupListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            TemperatureUnit temperatureUnit = null;
            if (checkedId == C0434R.C0436id.button_unit_celsius) {
                temperatureUnit = TemperatureUnit.Celsius;
            } else if (checkedId == C0434R.C0436id.button_unit_fahrenheit) {
                temperatureUnit = TemperatureUnit.Fahrenheit;
            }
            new SetUnitTask(temperatureUnit).execute(new Void[0]);
        }
    };

    private class SetBatteryProtectionModeTask extends AsyncTask<Void, Void, Void> {
        private final BatteryProtectionMode mMode;

        SetBatteryProtectionModeTask(BatteryProtectionMode mode) {
            this.mMode = mode;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                BoxBatterySettingsFragment.this.mCfxCommunication.setBatteryProtectionMode(this.mMode);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            return null;
        }
    }

    private class SetUnitTask extends AsyncTask<Void, Void, Void> {
        private final TemperatureUnit mUnit;

        SetUnitTask(TemperatureUnit unit) {
            this.mUnit = unit;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                BoxBatterySettingsFragment.this.mCfxCommunication.setTempUnit(this.mUnit);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            return null;
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(C0434R.layout.cfx_layout_battery_protection, container, false);
        ((RadioGroup) view.findViewById(C0434R.C0436id.radio_group_battery_protection)).setOnCheckedChangeListener(this.mBatteryProtectionGroupListener);
        setBatteryProtectionMode(view, this.mCfxPoller.getLatestRefrigeratorInfo());
        return view;
    }

    private void setBatteryProtectionMode(View view, Refrigerator refrigeratorTemp) {
        if (refrigeratorTemp != null) {
            RadioButton button = null;
            BatteryProtectionMode batteryProtection = refrigeratorTemp.getBatteryProtectionMode();
            if (batteryProtection == BatteryProtectionMode.High) {
                button = (RadioButton) view.findViewById(C0434R.C0436id.button_protection_high);
            } else if (batteryProtection == BatteryProtectionMode.Medium) {
                button = (RadioButton) view.findViewById(C0434R.C0436id.button_protection_medium);
            } else if (batteryProtection == BatteryProtectionMode.Low) {
                button = (RadioButton) view.findViewById(C0434R.C0436id.button_protection_low);
            }
            if (button != null) {
                button.setChecked(true);
            }
        }
    }
}
