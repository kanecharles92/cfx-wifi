package com.semcon.dometic.cfx.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.p000v4.app.FragmentActivity;
import android.support.p003v7.app.AlertDialog.Builder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.dometic.dometicui.DometicSeekBar;
import com.dometic.dometicui.ScaleView;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.CfxBoxSettingsActivity;
import com.semcon.dometic.cfx.ErrorDetailsFragment;
import com.semcon.dometic.cfx.IConnectableActivity;
import com.semcon.dometic.cfx.alarm.AlarmHandler;
import com.semcon.dometic.cfx.communication.CfxPoller;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.data.Compartment;
import com.semcon.dometic.cfx.data.Door;
import com.semcon.dometic.cfx.data.Refrigerator;
import com.semcon.dometic.cfx.event.OnNewCfxInfo;
import com.semcon.dometic.cfx.persistent.CfxBoxSettingsItem;
import com.semcon.dometic.cfx.persistent.CfxDevicesPersister;
import com.semcon.dometic.cfx.persistent.CfxSettingsPersister;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import com.semcon.dometic.cfx.tcp.CfxProtocolException;
import com.semcon.dometic.cfx.utils.CfxStringUtil;
import com.semcon.dometic.cfx.views.CoolBoxView;
import com.semcon.dometic.cfx.views.CoolBoxView.OnTouchCompartmentListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import roboguice.event.EventListener;
import roboguice.event.EventManager;
import roboguice.fragment.RoboFragment;

public class BoxControlFragment extends RoboFragment {
    private static final int AFTER_USER_TRACKING_SKIPPED_UPDATES = 3;
    public static final int CELSIUS_RANGE_MAX = 10;
    public static final int CELSIUS_RANGE_MIN = -22;
    private static final int CONNECTION_LOST_TIMEOUT_MILLIS = 5000;
    public static final int FAHRENHEIT_RANGE_MAX = 50;
    public static final int FAHRENHEIT_RANGE_MIN = -8;
    public static final int PICK_WIFI = 0;
    /* access modifiers changed from: private */
    public static final String TAG = BoxControlFragment.class.getName();
    /* access modifiers changed from: private */
    public boolean checkForTempDeviation = false;
    /* access modifiers changed from: private */
    public int mAfterPowerButtonPressedSkippedUpdates;
    /* access modifiers changed from: private */
    public int[] mAfterTemperatureTrackingSkippedUpdates = new int[2];
    @Inject
    private AlarmHandler mAlarmHandler;
    /* access modifiers changed from: private */
    @Inject
    public ICfxCommunication mCfxCommunication;
    private EventListener mCfxInfoListener = new EventListener<OnNewCfxInfo>() {
        public void onEvent(OnNewCfxInfo event) {
            BoxControlFragment.this.showRefrigeratorInfo(event.getRefrigerator());
            BoxControlFragment.this.showErrors(event.getRefrigerator());
        }
    };
    /* access modifiers changed from: private */
    @Inject
    public CfxPoller mCfxPoller;
    private DometicSeekBar mCompartmentSeekBar1;
    private DometicSeekBar mCompartmentSeekBar2;
    private final Handler mConnectionLostHandler = new Handler();
    private final Runnable mConnectionLostRunnable = new Runnable() {
        public void run() {
            FragmentActivity activity = BoxControlFragment.this.getActivity();
            if (BoxControlFragment.this.isResumed() && (activity instanceof IConnectableActivity)) {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) activity.getSystemService("connectivity")).getActiveNetworkInfo();
                BoxControlFragment.this.startActivity(new Intent(BoxControlFragment.this.getContext(), ((IConnectableActivity) activity).getConnectingActivityClass()));
                activity.finish();
            }
        }
    };
    private List<CFXError> mCurrentErrors = new ArrayList();
    private List<Refrigerator> mDevicesInTheSystem;
    @Named("GlobalEventManager")
    @Inject
    private EventManager mGlobalEventManager;
    /* access modifiers changed from: private */
    public boolean mIsCoolerOn;
    private OnToggleConnectionListener mOnToggleConnectionListener;
    /* access modifiers changed from: private */
    public Refrigerator mRefrigerator;
    /* access modifiers changed from: private */
    public boolean[] mTemperatureTracking = new boolean[2];

    private class BoxSettingsButtonClickListener implements OnClickListener {
        private BoxSettingsButtonClickListener() {
        }

        public void onClick(View v) {
            BoxControlFragment.this.startActivity(new Intent(BoxControlFragment.this.getContext(), CfxBoxSettingsActivity.class));
        }
    }

    private abstract class CfxTask extends AsyncTask<Void, Void, Refrigerator> {
        private CfxTask() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Refrigerator refrigerator) {
        }
    }

    private class ChangeTemperatureClickListener implements OnClickListener {
        private final DometicSeekBar mCompartmentSeekBar;
        private final int mDiff;

        ChangeTemperatureClickListener(DometicSeekBar compartmentSeekBar, int diff) {
            this.mCompartmentSeekBar = compartmentSeekBar;
            this.mDiff = diff;
        }

        public void onClick(View v) {
            int compartment = ((SeekBarChangeListener) this.mCompartmentSeekBar.getOnSeekBarChangeListener()).getCompartment();
            BoxControlFragment.this.mTemperatureTracking[compartment - 1] = true;
            BoxControlFragment.this.mAfterTemperatureTrackingSkippedUpdates[compartment - 1] = 3;
            if (this.mCompartmentSeekBar != null && this.mCompartmentSeekBar.getProgress() + this.mDiff >= 0 && this.mCompartmentSeekBar.getProgress() + this.mDiff <= this.mCompartmentSeekBar.getMax()) {
                int newProgress = this.mCompartmentSeekBar.getProgress() + this.mDiff;
                this.mCompartmentSeekBar.setProgress(newProgress);
                new SetTemperatureTask(compartment, this.mCompartmentSeekBar.getMinDisplayableValue() + newProgress).execute(new Void[0]);
            }
        }
    }

    private class CreateNewFridgeTask extends CfxTask {
        CreateNewFridgeTask(Refrigerator refrigerator) {
            super();
            BoxControlFragment.this.mRefrigerator = refrigerator;
        }

        /* access modifiers changed from: protected */
        public Refrigerator doInBackground(Void... params) {
            TemperatureUnit temperatureUnit;
            TemperatureUnit temperatureUnit2 = TemperatureUnit.Celsius;
            ArrayList arrayList = new ArrayList();
            int compartmentCount = BoxControlFragment.this.mRefrigerator.getCompartments().size();
            for (int i = 0; i < compartmentCount; i++) {
                try {
                    if (!BoxControlFragment.this.mCfxCommunication.isConnected()) {
                        BoxControlFragment.this.mCfxCommunication.connect();
                    }
                    arrayList.add(new Compartment(BoxControlFragment.this.mCfxCommunication.isCompartmentOn(i + 1) ? 1 : 0, 0, BoxControlFragment.this.mCfxCommunication.getTempOfCompartment(i + 1), BoxControlFragment.this.mCfxCommunication.getSetTempOfCompartment(i + 1), null, null, new Door(BoxControlFragment.this.mCfxCommunication.isDoorOpen(i + 1) ? 1 : 0, BoxControlFragment.this.mCfxCommunication.isDoorAlarming(i + 1) ? 1 : 0)));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (CfxProtocolException e2) {
                    e2.printStackTrace();
                }
            }
            try {
                if (!BoxControlFragment.this.mCfxCommunication.isConnected()) {
                    BoxControlFragment.this.mCfxCommunication.connect();
                }
                temperatureUnit = BoxControlFragment.this.mCfxCommunication.getTempUnit();
            } catch (IOException e3) {
                e3.printStackTrace();
                temperatureUnit = temperatureUnit2;
            } catch (CfxProtocolException e4) {
                e4.printStackTrace();
                temperatureUnit = temperatureUnit2;
            }
            BoxControlFragment.this.mRefrigerator = new Refrigerator(BoxControlFragment.this.mRefrigerator.getModel(), arrayList, BoxControlFragment.this.mRefrigerator.isOn(), BoxControlFragment.this.mRefrigerator.isAcOn(), temperatureUnit, BoxControlFragment.this.mRefrigerator.getTempAlarmLevel(), BoxControlFragment.this.mRefrigerator.getDcVoltage(), BoxControlFragment.this.mRefrigerator.getBatteryProtectionMode(), BoxControlFragment.this.mRefrigerator.getWiFiModuleList(), BoxControlFragment.this.mRefrigerator.getErrorCodes(), BoxControlFragment.this.mRefrigerator.getFeatures());
            return BoxControlFragment.this.mRefrigerator;
        }
    }

    private class ErrorViewClickListener implements OnClickListener {
        ArrayList<CFXError> errors;

        public ErrorViewClickListener(ArrayList<CFXError> errors2) {
            this.errors = errors2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(BoxControlFragment.this.getContext(), ErrorDetailsFragment.class);
            intent.putExtra("errors", this.errors.toString());
            BoxControlFragment.this.startActivity(intent);
        }
    }

    private class OnCompartmentTouchedListener implements OnTouchCompartmentListener {
        /* access modifiers changed from: private */
        public int mCompartment;
        /* access modifiers changed from: private */
        public boolean mWillTurnOn;

        private OnCompartmentTouchedListener() {
        }

        public void onCompartmentTouched(CoolBoxView coolBoxView, int compartmentIndex, boolean willTurnOn) {
            if (coolBoxView.getBoxType() != 1) {
                this.mCompartment = compartmentIndex + 1;
                this.mWillTurnOn = willTurnOn;
                new Builder(BoxControlFragment.this.getActivity()).setMessage((CharSequence) BoxControlFragment.this.getActivity().getString(this.mWillTurnOn ? C0434R.string.turn_compartment_on : C0434R.string.turn_comparment_off)).setPositiveButton(17039379, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new RefrigeratorCompartmentOnOffTask(OnCompartmentTouchedListener.this.mCompartment, OnCompartmentTouchedListener.this.mWillTurnOn).execute(new Void[0]);
                    }
                }).setNegativeButton(17039369, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
            }
        }
    }

    public interface OnToggleConnectionListener {
        void onConnect();

        void onDisconnect();
    }

    private class RefigeratorOnOffClickListener implements OnClickListener {
        private RefigeratorOnOffClickListener() {
        }

        public void onClick(View v) {
            new Builder(BoxControlFragment.this.getActivity()).setMessage((CharSequence) BoxControlFragment.this.getActivity().getString(BoxControlFragment.this.mIsCoolerOn ? C0434R.string.turn_cfx_off : C0434R.string.turn_cfx_on)).setPositiveButton(17039379, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    boolean z;
                    BoxControlFragment.this.mAfterPowerButtonPressedSkippedUpdates = 2;
                    ImageButton powerBtn = (ImageButton) BoxControlFragment.this.getView().findViewById(C0434R.C0436id.power_btn);
                    BoxControlFragment boxControlFragment = BoxControlFragment.this;
                    if (!BoxControlFragment.this.mIsCoolerOn) {
                        z = true;
                    } else {
                        z = false;
                    }
                    boxControlFragment.mIsCoolerOn = z;
                    if (powerBtn != null) {
                        powerBtn.setImageResource(BoxControlFragment.this.mIsCoolerOn ? C0434R.mipmap.cfx_android_ikoner_08 : C0434R.mipmap.cfx_android_ikoner_20);
                    }
                    CoolBoxView coolBoxView = (CoolBoxView) BoxControlFragment.this.getView().findViewById(C0434R.C0436id.compartments_iv);
                    if (coolBoxView != null) {
                        coolBoxView.setEnabled(BoxControlFragment.this.mIsCoolerOn);
                    }
                    new RefrigeratorOnOffTask().execute(new Void[0]);
                }
            }).setNegativeButton(17039369, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();
        }
    }

    private class RefrigeratorCompartmentOnOffTask extends AsyncTask<Void, Void, Void> {
        private final int mCompartment;
        private final boolean mTurnOn;

        RefrigeratorCompartmentOnOffTask(int compartment, boolean turnOn) {
            this.mCompartment = compartment;
            this.mTurnOn = turnOn;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                if (!BoxControlFragment.this.mCfxCommunication.isConnected()) {
                    BoxControlFragment.this.mCfxCommunication.connect();
                }
                BoxControlFragment.this.mCfxCommunication.setCompartmentOn(this.mCompartment, this.mTurnOn);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            return null;
        }
    }

    private class RefrigeratorOnOffTask extends CfxTask {
        private RefrigeratorOnOffTask() {
            super();
        }

        /* access modifiers changed from: protected */
        public Refrigerator doInBackground(Void... params) {
            Refrigerator refrigerator = BoxControlFragment.this.mRefrigerator;
            try {
                if (!BoxControlFragment.this.mCfxCommunication.isConnected()) {
                    BoxControlFragment.this.mCfxCommunication.connect();
                }
                if (refrigerator == null) {
                    refrigerator = BoxControlFragment.this.mCfxPoller.getRefrigeratorInfo();
                }
                BoxControlFragment.this.mCfxCommunication.setRefrigeratorOn(BoxControlFragment.this.mIsCoolerOn ? 1 : 0);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            return refrigerator;
        }
    }

    private class SeekBarChangeListener implements OnSeekBarChangeListener {
        private int mCompartment;

        SeekBarChangeListener(int compartment) {
            this.mCompartment = compartment;
        }

        /* access modifiers changed from: 0000 */
        public int getCompartment() {
            return this.mCompartment;
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            BoxControlFragment.this.checkForTempDeviation = false;
            BoxControlFragment.this.mTemperatureTracking[this.mCompartment - 1] = true;
            BoxControlFragment.this.mAfterTemperatureTrackingSkippedUpdates[this.mCompartment - 1] = 3;
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            BoxControlFragment.this.checkForTempDeviation = false;
            new SetTemperatureTask(this.mCompartment, seekBar.getProgress() + ((DometicSeekBar) seekBar).getMinDisplayableValue()).execute(new Void[0]);
        }
    }

    private class SetTemperatureTask extends CfxTask {
        private final int mCompartment;
        private final int mNewTemperature;

        SetTemperatureTask(int compartment, int newTemperature) {
            super();
            this.mNewTemperature = newTemperature;
            this.mCompartment = compartment;
        }

        /* access modifiers changed from: protected */
        public Refrigerator doInBackground(Void... params) {
            Refrigerator tempRefrigerator = BoxControlFragment.this.mRefrigerator;
            if (tempRefrigerator == null) {
                return null;
            }
            try {
                if (!BoxControlFragment.this.mCfxCommunication.isConnected()) {
                    BoxControlFragment.this.mCfxCommunication.connect();
                }
                Log.d(BoxControlFragment.TAG, "Set target temperature " + this.mNewTemperature + " for compartment " + this.mCompartment);
                BoxControlFragment.this.mCfxCommunication.setTempCompartment(this.mNewTemperature, this.mCompartment);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            BoxControlFragment.this.mTemperatureTracking[this.mCompartment - 1] = false;
            return tempRefrigerator;
        }
    }

    private class WifiConnectButtonListener implements OnClickListener {
        private WifiConnectButtonListener() {
        }

        public void onClick(View v) {
            BoxControlFragment.this.startActivityForResult(new Intent("android.net.wifi.PICK_WIFI_NETWORK"), 0);
        }
    }

    /* access modifiers changed from: private */
    public void showErrors(Refrigerator mRefrigerator2) {
        List<CFXError> newErrors = this.mAlarmHandler.getCurrentErrorCodes();
        newErrors.remove(CFXError.DOOR_OPEN_OVER_3_MIN);
        newErrors.remove(CFXError.DOOR_OPEN);
        WifiManager wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService("wifi");
        CfxBoxSettingsItem boxSettingsItem = CfxSettingsPersister.getBoxSettings(getContext(), (wifiManager == null ? null : wifiManager.getConnectionInfo()).getSSID());
        int rangeMax = 0;
        if (boxSettingsItem != null) {
            rangeMax = boxSettingsItem.getAlarmType().getRangeMax();
        }
        for (Compartment c : mRefrigerator2.getCompartments()) {
            if (c.getTargetTemperature() != -50 && Math.abs(c.getTargetTemperature() - c.getActualTemperature()) < Math.abs(rangeMax)) {
                this.checkForTempDeviation = true;
            }
        }
        if (this.checkForTempDeviation && rangeMax > 0) {
            for (Compartment c2 : mRefrigerator2.getCompartments()) {
                if (Math.abs(c2.getTargetTemperature() - c2.getActualTemperature()) > Math.abs(boxSettingsItem.getAlarmType().getRangeMax())) {
                    newErrors.add(CFXError.TEMPERATURE_ALARM_ERROR);
                }
            }
        }
        View view = getView();
        for (Compartment c3 : mRefrigerator2.getCompartments()) {
            if (c3.getDoor().isAlarmOn() == 1) {
                newErrors.add(CFXError.DOOR_OPEN_OVER_3_MIN);
            }
        }
        Log.d("BoxControl CODES: ", "Errors: " + this.mAlarmHandler.getCurrentErrorCodes());
        if (view != null) {
            ViewGroup errorsLayout = (ViewGroup) view.findViewById(C0434R.C0436id.errors_layout);
            ArrayList<CFXError> errList = new ArrayList<>();
            if (errorsLayout != null) {
                if (newErrors.size() > 0) {
                    for (CFXError err : newErrors) {
                        if (!err.name().toUpperCase().endsWith("CYCLE")) {
                            errList.add(err);
                            Log.d("Error", "Showing for error: " + err);
                        }
                    }
                    if (errList.size() > 0) {
                        errorsLayout.setVisibility(0);
                        errorsLayout.setOnClickListener(new ErrorViewClickListener(errList));
                        return;
                    }
                    return;
                }
                errorsLayout.setVisibility(4);
            }
        }
        this.mCurrentErrors = newErrors;
    }

    public List<CFXError> getCurrentErrors() {
        return this.mCurrentErrors;
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.mDevicesInTheSystem = new CfxDevicesPersister(getContext().getAssets()).createDevices();
        String deviceName1 = "CFX-95DZ1";
        try {
            for (Refrigerator refrigeratorDevice : this.mDevicesInTheSystem) {
                if (refrigeratorDevice.getModel().equals(deviceName1)) {
                    new CreateNewFridgeTask(refrigeratorDevice).execute(new Void[0]);
                } else {
                    this.mDevicesInTheSystem.listIterator().next();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        View view = inflater.inflate(C0434R.layout.fragment_box_control, container, false);
        CoolBoxView boxView = (CoolBoxView) view.findViewById(C0434R.C0436id.compartments_iv);
        if (boxView != null) {
            boxView.setEnabled(false);
            boxView.setBoxType(1 == 2 ? 2 : 1);
            boxView.setOnTouchCompartmentListener(new OnCompartmentTouchedListener());
        }
        View compartment1 = view.findViewById(C0434R.C0436id.compartment1);
        View compartment2 = view.findViewById(C0434R.C0436id.compartment2);
        if (compartment1 != null) {
            this.mCompartmentSeekBar1 = (DometicSeekBar) compartment1.findViewById(C0434R.C0436id.compartment_sb);
            this.mCompartmentSeekBar1.setOnSeekBarChangeListener(new SeekBarChangeListener(1));
            ((Button) compartment1.findViewById(C0434R.C0436id.minus_btn)).setOnClickListener(new ChangeTemperatureClickListener(this.mCompartmentSeekBar1, -1));
            ((Button) compartment1.findViewById(C0434R.C0436id.plus_btn)).setOnClickListener(new ChangeTemperatureClickListener(this.mCompartmentSeekBar1, 1));
            this.mCompartmentSeekBar1.startAnimation();
        }
        if (compartment2 != null) {
            this.mCompartmentSeekBar2 = (DometicSeekBar) compartment2.findViewById(C0434R.C0436id.compartment_sb);
            this.mCompartmentSeekBar2.setOnSeekBarChangeListener(new SeekBarChangeListener(2));
            ((Button) compartment2.findViewById(C0434R.C0436id.minus_btn)).setOnClickListener(new ChangeTemperatureClickListener(this.mCompartmentSeekBar2, -1));
            ((Button) compartment2.findViewById(C0434R.C0436id.plus_btn)).setOnClickListener(new ChangeTemperatureClickListener(this.mCompartmentSeekBar2, 1));
            compartment2.setVisibility(1 == 2 ? 0 : 8);
            this.mCompartmentSeekBar2.startAnimation();
        }
        view.findViewById(C0434R.C0436id.wifi_btn).setOnClickListener(new WifiConnectButtonListener());
        view.findViewById(C0434R.C0436id.power_btn).setOnClickListener(new RefigeratorOnOffClickListener());
        return view;
    }

    public void onStart() {
        super.onStart();
        this.mConnectionLostHandler.postDelayed(this.mConnectionLostRunnable, 5000);
        this.mCfxPoller.startPolling(getContext());
        this.mGlobalEventManager.registerObserver(OnNewCfxInfo.class, this.mCfxInfoListener);
    }

    public void onStop() {
        super.onStop();
        this.mConnectionLostHandler.removeCallbacks(this.mConnectionLostRunnable);
        this.mCfxPoller.stopPolling();
        this.mGlobalEventManager.unregisterObserver(OnNewCfxInfo.class, this.mCfxInfoListener);
        try {
            this.mCfxCommunication.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnToggleConnectionListener) {
            this.mOnToggleConnectionListener = (OnToggleConnectionListener) context;
        }
    }

    /* access modifiers changed from: private */
    public void showRefrigeratorInfo(Refrigerator refrigerator) {
        this.mConnectionLostHandler.removeCallbacks(this.mConnectionLostRunnable);
        this.mConnectionLostHandler.postDelayed(this.mConnectionLostRunnable, 5000);
        View rootView = getView();
        Refrigerator tempRefrigerator = this.mCfxPoller.getLatestRefrigeratorInfo();
        if (!(tempRefrigerator == null || tempRefrigerator == refrigerator)) {
            refrigerator.setCompartments(tempRefrigerator.getCompartments());
            refrigerator.setIsAcOn(tempRefrigerator.isAcOn());
            refrigerator.setTemperatureUnit(tempRefrigerator.getTemperatureUnit());
        }
        if (this.mCfxCommunication != null && refrigerator != null && rootView != null) {
            View compSep = rootView.findViewById(C0434R.C0436id.compartment_separator);
            View compartment2 = rootView.findViewById(C0434R.C0436id.compartment2);
            int compNumber = refrigerator.getCompartments().size();
            if (compNumber > 1 && ((Compartment) refrigerator.getCompartments().get(1)).getActualTemperature() <= -45) {
                compNumber = 1;
            }
            int vis = compNumber > 1 ? 0 : 8;
            if (!(compSep == null || compSep.getVisibility() == vis)) {
                compSep.setVisibility(vis);
            }
            if (!(compartment2 == null || compartment2.getVisibility() == vis)) {
                compartment2.setVisibility(vis);
            }
            View boxLayout = rootView.findViewById(C0434R.C0436id.box_control_layout);
            if (boxLayout != null) {
                int vis2 = compNumber > 0 ? 0 : 4;
                if (boxLayout.getVisibility() != vis2) {
                    boxLayout.setVisibility(vis2);
                }
            }
            ImageView acdc = (ImageView) rootView.findViewById(C0434R.C0436id.power_source_iv);
            if (acdc != null) {
                acdc.setImageResource(refrigerator.isAcOn() == 0 ? C0434R.mipmap.cfx_android_ikoner_21 : C0434R.mipmap.cfx_android_ikoner_07);
            }
            TextView powerSource = (TextView) rootView.findViewById(C0434R.C0436id.power_source_tv);
            if (powerSource != null) {
                if (refrigerator.isAcOn() == 0) {
                    powerSource.setText(getString(C0434R.string.f42dc) + " " + String.format("%.1f", new Object[]{Double.valueOf(refrigerator.getDcVoltage())}) + "V");
                } else {
                    powerSource.setText(getString(C0434R.string.f41ac));
                }
            }
            int skippedPowerButtonUpdates = this.mAfterPowerButtonPressedSkippedUpdates - 1;
            if (skippedPowerButtonUpdates < 0) {
                skippedPowerButtonUpdates = 0;
            }
            this.mAfterPowerButtonPressedSkippedUpdates = skippedPowerButtonUpdates;
            if (skippedPowerButtonUpdates == 0) {
                ImageButton powerBtn = (ImageButton) rootView.findViewById(C0434R.C0436id.power_btn);
                this.mIsCoolerOn = refrigerator.isOn() != 0;
                if (powerBtn != null) {
                    powerBtn.setImageResource(this.mIsCoolerOn ? C0434R.mipmap.cfx_android_ikoner_08 : C0434R.mipmap.cfx_android_ikoner_20);
                }
            }
            CoolBoxView boxView = (CoolBoxView) rootView.findViewById(C0434R.C0436id.compartments_iv);
            if (boxView != null) {
                boxView.setEnabled(this.mIsCoolerOn);
                if (compNumber < 1) {
                    boxView.setBoxType(2);
                    boxView.setActiveCompartment(0);
                } else if (compNumber == 1) {
                    boxView.setBoxType(1);
                    boxView.setActiveCompartment(((Compartment) refrigerator.getCompartments().get(0)).isOn() == 1 ? 3 : 0);
                    try {
                        boxView.setOpenDoorCompartment(this.mCfxCommunication.isDoorOpen(1) ? 3 : 0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    boxView.setBoxType(2);
                    boxView.setActiveCompartment((((Compartment) refrigerator.getCompartments().get(0)).isOn() == 1 ? 1 : 0) | (((Compartment) refrigerator.getCompartments().get(1)).isOn() == 1 ? 2 : 0));
                    try {
                        boxView.setOpenDoorCompartment((this.mCfxCommunication.isDoorOpen(1) ? 1 : 0) | (this.mCfxCommunication.isDoorOpen(2) ? 2 : 0));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
            for (int i = 1; i < compNumber + 1; i++) {
                View compartmentView = null;
                if (i == 1) {
                    compartmentView = rootView.findViewById(C0434R.C0436id.compartment1);
                } else if (i == 2) {
                    compartmentView = rootView.findViewById(C0434R.C0436id.compartment2);
                }
                if (compartmentView != null) {
                    boolean isCompartmentOn = ((Compartment) refrigerator.getCompartments().get(i + -1)).isOn() != 0;
                    TextView textView = (TextView) compartmentView.findViewById(C0434R.C0436id.temperature_tv);
                    int actualTemperature = ((Compartment) refrigerator.getCompartments().get(i - 1)).getActualTemperature();
                    if (textView != null) {
                        textView.setText(actualTemperature + "");
                    }
                    TextView textView2 = (TextView) compartmentView.findViewById(C0434R.C0436id.temperature_unit_tv);
                    if (textView2 != null) {
                        textView2.setText(CfxStringUtil.getStringTemperatureUnitRepresentation(refrigerator.getTemperatureUnit()));
                    }
                    Button button = (Button) compartmentView.findViewById(C0434R.C0436id.minus_btn);
                    if (button != null) {
                        button.setEnabled(isCompartmentOn);
                    }
                    Button button2 = (Button) compartmentView.findViewById(C0434R.C0436id.plus_btn);
                    if (button2 != null) {
                        button2.setEnabled(isCompartmentOn);
                    }
                    DometicSeekBar sb = (DometicSeekBar) compartmentView.findViewById(C0434R.C0436id.compartment_sb);
                    if (sb != null) {
                        sb.setEnabled(isCompartmentOn);
                        if (!this.mTemperatureTracking[i - 1]) {
                            int skippedUpdates = this.mAfterTemperatureTrackingSkippedUpdates[i - 1] - 1;
                            if (skippedUpdates < 0) {
                                skippedUpdates = 0;
                            }
                            this.mAfterTemperatureTrackingSkippedUpdates[i - 1] = skippedUpdates;
                            if (skippedUpdates <= 0) {
                                int targetTemp = ((Compartment) refrigerator.getCompartments().get(i - 1)).getTargetTemperature();
                                int min = -22;
                                int max = 10;
                                String scaleValues = "0;-18,0,3";
                                if (refrigerator.getTemperatureUnit() == TemperatureUnit.Fahrenheit) {
                                    min = -8;
                                    max = 50;
                                    scaleValues = "32;0,32,37";
                                }
                                sb.setMinMaxStartProgressValues(min, max, actualTemperature - sb.getMinDisplayableValue(), targetTemp - sb.getMinDisplayableValue());
                                ScaleView sv = (ScaleView) compartmentView.findViewById(C0434R.C0436id.compartment_sv);
                                if (sv != null) {
                                    sv.setMinMaxValues(min, max, scaleValues);
                                    sv.invalidate();
                                }
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
    }
}
