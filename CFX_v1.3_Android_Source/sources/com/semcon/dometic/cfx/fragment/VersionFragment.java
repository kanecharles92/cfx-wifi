package com.semcon.dometic.cfx.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.inject.Inject;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.communication.CfxPoller;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral;
import roboguice.fragment.RoboFragment;

public class VersionFragment extends RoboFragment {
    @Inject
    private ICfxCommunication mCfxCommunication;
    @Inject
    private CfxPoller mCfxPoller;
    @Inject
    private CfxSharedPreferencesHelper_univeral mCfxSharedPreferencesHelperUniveral;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(C0434R.layout.fragment_cfx_version, container, false);
    }
}
