package com.semcon.dometic.cfx.fragment;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import com.google.inject.Inject;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.communication.CfxPoller;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.data.Refrigerator;
import com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral;
import com.semcon.dometic.cfx.tcp.CfxProtocolException;
import java.io.IOException;
import roboguice.fragment.RoboFragment;

public class BoxAlarmSettingsFragment extends RoboFragment {
    private static int PICK_WIFI = 0;
    OnCheckedChangeListener mBatteryProtectionGroupListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            BatteryProtectionMode batteryProtectionMode = null;
            if (checkedId == C0434R.C0436id.button_protection_high) {
                batteryProtectionMode = BatteryProtectionMode.High;
            } else if (checkedId == C0434R.C0436id.button_protection_medium) {
                batteryProtectionMode = BatteryProtectionMode.Medium;
            } else if (checkedId == C0434R.C0436id.button_protection_low) {
                batteryProtectionMode = BatteryProtectionMode.Low;
            }
            new SetBatteryProtectionModeTask(batteryProtectionMode).execute(new Void[0]);
        }
    };
    /* access modifiers changed from: private */
    @Inject
    public ICfxCommunication mCfxCommunication;
    @Inject
    private CfxPoller mCfxPoller;
    @Inject
    private CfxSharedPreferencesHelper_univeral mCfxSharedPreferencesHelperUniveral;
    OnCheckedChangeListener mUnitGroupListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            TemperatureUnit temperatureUnit = null;
            if (checkedId == C0434R.C0436id.button_unit_celsius) {
                temperatureUnit = TemperatureUnit.Celsius;
            } else if (checkedId == C0434R.C0436id.button_unit_fahrenheit) {
                temperatureUnit = TemperatureUnit.Fahrenheit;
            }
            new SetUnitTask(temperatureUnit).execute(new Void[0]);
        }
    };

    private class SetBatteryProtectionModeTask extends AsyncTask<Void, Void, Void> {
        private final BatteryProtectionMode mMode;

        SetBatteryProtectionModeTask(BatteryProtectionMode mode) {
            this.mMode = mode;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                BoxAlarmSettingsFragment.this.mCfxCommunication.setBatteryProtectionMode(this.mMode);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            return null;
        }
    }

    private class SetNameTask extends AsyncTask<Void, String, String> {
        private final String mName;
        private final String mPassword;

        SetNameTask(String name, String password) {
            this.mName = name;
            this.mPassword = password;
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            String result = null;
            try {
                return BoxAlarmSettingsFragment.this.mCfxCommunication.setNameAndPassword(this.mName, this.mPassword);
            } catch (IOException e) {
                e.printStackTrace();
                return result;
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
                return result;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (result.equals("Success in Updating Wifi")) {
                Toast.makeText(BoxAlarmSettingsFragment.this.getContext(), "Success in updating wifi", 1).show();
                BoxAlarmSettingsFragment.this.connectToNewNetwork(this.mName, this.mPassword);
                return;
            }
            Toast.makeText(BoxAlarmSettingsFragment.this.getContext(), "Problem in updating wifi", 1).show();
        }
    }

    private class SetUnitTask extends AsyncTask<Void, Void, Void> {
        private final TemperatureUnit mUnit;

        SetUnitTask(TemperatureUnit unit) {
            this.mUnit = unit;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                BoxAlarmSettingsFragment.this.mCfxCommunication.setTempUnit(this.mUnit);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                e2.printStackTrace();
            }
            return null;
        }
    }

    class WifiConnectButtonListener implements OnClickListener {
        WifiConnectButtonListener() {
        }

        public void onClick(View v) {
            View view = BoxAlarmSettingsFragment.this.getView();
            if (view != null) {
                String cfx_wifi_name = ((TextView) view.findViewById(C0434R.C0436id.cfx_name_edit_text)).getText().toString();
                String cfx_wifi_password = ((TextView) view.findViewById(C0434R.C0436id.cfx_password_edit_text)).getText().toString();
                if (cfx_wifi_name == null || cfx_wifi_password == null) {
                    Toast.makeText(BoxAlarmSettingsFragment.this.getContext(), "Write again the wifi name and password", 1).show();
                } else {
                    new SetNameTask(cfx_wifi_name, cfx_wifi_password).execute(new Void[0]);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void connectToNewNetwork(String name, String password) {
        String networkSSID = name;
        String networkPass = password;
        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + networkSSID + "\"";
        conf.preSharedKey = "\"" + networkPass + "\"";
        WifiManager wifiManager = (WifiManager) getContext().getSystemService("wifi");
        wifiManager.addNetwork(conf);
        for (WifiConfiguration i : wifiManager.getConfiguredNetworks()) {
            if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                Log.i("Call", "New wifi found" + i.SSID);
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                wifiManager.reconnect();
                Toast.makeText(getContext(), "Connecting to new wifi", 1).show();
                return;
            }
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(C0434R.layout.notification_temp_deviates, container, false);
        ((RadioGroup) view.findViewById(C0434R.C0436id.alarm_deviates_settings)).setOnCheckedChangeListener(this.mBatteryProtectionGroupListener);
        setBatteryProtectionMode(view, this.mCfxPoller.getLatestRefrigeratorInfo());
        return view;
    }

    private void setBatteryProtectionMode(View view, Refrigerator refrigeratorTemp) {
        if (refrigeratorTemp != null) {
            RadioButton button = null;
            BatteryProtectionMode batteryProtection = refrigeratorTemp.getBatteryProtectionMode();
            if (batteryProtection == BatteryProtectionMode.High) {
                button = (RadioButton) view.findViewById(C0434R.C0436id.button_protection_high);
            } else if (batteryProtection == BatteryProtectionMode.Medium) {
                button = (RadioButton) view.findViewById(C0434R.C0436id.button_protection_medium);
            } else if (batteryProtection == BatteryProtectionMode.Low) {
                button = (RadioButton) view.findViewById(C0434R.C0436id.button_protection_low);
            }
            if (button != null) {
                button.setChecked(true);
            }
        }
    }

    private void setTemperatureUnit(View view, Refrigerator refrigeratorTemp) {
        if (refrigeratorTemp != null) {
            RadioButton button = null;
            TemperatureUnit unit = refrigeratorTemp.getTemperatureUnit();
            if (unit == TemperatureUnit.Celsius) {
                button = (RadioButton) view.findViewById(C0434R.C0436id.button_unit_celsius);
            } else if (unit == TemperatureUnit.Fahrenheit) {
                button = (RadioButton) view.findViewById(C0434R.C0436id.button_unit_fahrenheit);
            }
            if (button != null) {
                button.setChecked(true);
            }
        }
    }
}
