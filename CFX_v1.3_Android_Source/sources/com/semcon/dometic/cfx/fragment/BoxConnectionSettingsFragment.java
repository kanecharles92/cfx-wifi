package com.semcon.dometic.cfx.fragment;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.inject.Inject;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.communication.CfxPoller;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.cfx.persistent.CfxBoxSettingsItem;
import com.semcon.dometic.cfx.persistent.CfxSettingsPersister;
import com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral;
import com.semcon.dometic.cfx.tcp.CfxProtocolException;
import com.semcon.dometic.cfx.views.ErrorView;
import java.io.IOException;
import roboguice.fragment.RoboFragment;

public class BoxConnectionSettingsFragment extends RoboFragment {
    private static int PICK_WIFI = 0;
    private String cfx_wifi_name;
    /* access modifiers changed from: private */
    public String cfx_wifi_password;
    private ErrorView error_view;
    /* access modifiers changed from: private */
    @Inject
    public ICfxCommunication mCfxCommunication;
    @Inject
    private CfxPoller mCfxPoller;
    @Inject
    private CfxSharedPreferencesHelper_univeral mCfxSharedPreferencesHelperUniveral;

    private class SetNameTask extends AsyncTask<Void, String, String> {
        private final String mName;
        private final String mOldName;
        private final String mPassword;

        SetNameTask(String oldName, String name, String password) {
            this.mOldName = oldName;
            this.mName = name;
            this.mPassword = password;
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            String result;
            try {
                result = BoxConnectionSettingsFragment.this.mCfxCommunication.setNameAndPassword(this.mName, this.mPassword);
                CfxBoxSettingsItem boxSettingsItem = CfxSettingsPersister.getBoxSettings(BoxConnectionSettingsFragment.this.getContext(), this.mOldName);
                if (boxSettingsItem != null) {
                    boxSettingsItem.setWifiName(this.mName);
                    CfxSettingsPersister.removeBoxSettings(BoxConnectionSettingsFragment.this.getContext(), this.mOldName);
                    CfxSettingsPersister.setBoxSettings(BoxConnectionSettingsFragment.this.getContext(), boxSettingsItem);
                }
            } catch (IOException e) {
                result = "Error Connect";
                e.printStackTrace();
            } catch (CfxProtocolException e2) {
                result = "Error Connect";
                e2.printStackTrace();
            }
            Log.i("Incoming", "The result now is" + result);
            return result;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (result.equals("Error Connect")) {
                BoxConnectionSettingsFragment.this.show_Error();
            } else if (result.equals("Success in Updating Wifi")) {
                BoxConnectionSettingsFragment.this.connectToNewNetwork(this.mName, this.mPassword);
            } else {
                BoxConnectionSettingsFragment.this.show_Error();
            }
        }
    }

    class WifiConnectButtonListener implements OnClickListener {
        WifiConnectButtonListener() {
        }

        public void onClick(View v) {
            View view = BoxConnectionSettingsFragment.this.getView();
            if (view != null) {
                String cfx_wifi_name = ((TextView) view.findViewById(C0434R.C0436id.cfx_name_edit_text)).getText().toString();
                BoxConnectionSettingsFragment.this.cfx_wifi_password = ((TextView) view.findViewById(C0434R.C0436id.cfx_password_edit_text)).getText().toString();
                if (cfx_wifi_name == null || BoxConnectionSettingsFragment.this.cfx_wifi_password == null) {
                    Toast.makeText(BoxConnectionSettingsFragment.this.getContext(), "Write again the wifi name and password", 1).show();
                    return;
                }
                Log.i("Incoming", "Calling the task");
                BoxConnectionSettingsFragment.this.hide_Error();
                WifiManager wifiManager = (WifiManager) BoxConnectionSettingsFragment.this.getContext().getSystemService("wifi");
                WifiInfo info = wifiManager == null ? null : wifiManager.getConnectionInfo();
                new SetNameTask(info == null ? "" : info.getSSID(), cfx_wifi_name, BoxConnectionSettingsFragment.this.cfx_wifi_password).execute(new Void[0]);
            }
        }
    }

    /* access modifiers changed from: private */
    public void connectToNewNetwork(String name, String password) {
        String networkSSID = name;
        String networkPass = password;
        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + networkSSID + "\"";
        conf.preSharedKey = "\"" + networkPass + "\"";
        WifiManager wifiManager = (WifiManager) getContext().getSystemService("wifi");
        wifiManager.addNetwork(conf);
        for (WifiConfiguration i : wifiManager.getConfiguredNetworks()) {
            if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                Log.i("Call", "New wifi found" + i.SSID);
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                wifiManager.reconnect();
                Toast.makeText(getContext(), "Connecting to new wifi", 1).show();
                return;
            }
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(C0434R.layout.cfx_layout_wifi_dialog, container, false);
        EditText editText = (EditText) view.findViewById(C0434R.C0436id.cfx_password_edit_text);
        this.error_view = (ErrorView) view.findViewById(C0434R.C0436id.error_View);
        hide_Error();
        view.findViewById(C0434R.C0436id.submit_wifi_change).setOnClickListener(new WifiConnectButtonListener());
        return view;
    }

    public void show_Error() {
        this.error_view.setError_message("Error connecting on Wifi");
        this.error_view.setVisibility(0);
    }

    public void hide_Error() {
        this.error_view.setVisibility(4);
    }
}
