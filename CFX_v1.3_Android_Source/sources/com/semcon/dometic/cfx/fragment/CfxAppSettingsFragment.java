package com.semcon.dometic.cfx.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import com.google.inject.Inject;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.CfxBoxSettingsActivity;
import com.semcon.dometic.cfx.adapters.Simple_device_adapter;
import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.data.Compartment;
import com.semcon.dometic.cfx.data.DeviceTemp;
import com.semcon.dometic.cfx.data.RefrigeratorTemp;
import com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import java.util.ArrayList;
import java.util.List;
import roboguice.fragment.RoboFragment;

public class CfxAppSettingsFragment extends RoboFragment {
    /* access modifiers changed from: private */
    @Inject
    public CfxSharedPreferencesHelper_univeral CfxSharedPreferencesHelper_univeral;
    ListView listview;
    OnClickListener mAddBoxClickListner = new OnClickListener() {
        public void onClick(View v) {
        }
    };
    private OnCheckedChangeListener mAlarmsCheckListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            CfxAppSettingsFragment.this.CfxSharedPreferencesHelper_univeral.enableAlarms(CfxAppSettingsFragment.this.getContext(), isChecked);
        }
    };

    class SelectedBoxClickListener implements OnClickListener {
        String device_name;

        public SelectedBoxClickListener(String device_name2) {
            this.device_name = device_name2;
        }

        public void onClick(View v) {
            Intent myIntent = new Intent(CfxAppSettingsFragment.this.getContext(), CfxBoxSettingsActivity.class);
            Log.i("textview1", "Calling the listener");
            Log.i("call", "this is the device name" + this.device_name);
            myIntent.putExtra("Device_name", this.device_name);
            CfxAppSettingsFragment.this.startActivity(myIntent);
        }
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(C0434R.layout.fragment_cfx_app_settings, container, false);
        List<DeviceTemp> deviceTemps = new ArrayList<>();
        ArrayList<CFXError> errors = new ArrayList<>();
        errors.add(CFXError.ABS_ERROR);
        RefrigeratorTemp refrigeratorTemp = new RefrigeratorTemp(true, true, TemperatureUnit.Celsius, BatteryProtectionMode.Medium, new Compartment[1], errors);
        DeviceTemp deviceTemp = new DeviceTemp("CFX1", "wireless", "power_supply_type", refrigeratorTemp);
        DeviceTemp deviceTemp1 = new DeviceTemp("CFX2", "wireless", "power_supply_type", refrigeratorTemp);
        DeviceTemp deviceTemp2 = new DeviceTemp("CFX3", "wireless", "power_supply_type", refrigeratorTemp);
        deviceTemps.add(deviceTemp);
        deviceTemps.add(deviceTemp1);
        deviceTemps.add(deviceTemp2);
        Context context = getActivity();
        this.listview = (ListView) view.findViewById(C0434R.C0436id.listview_settings_devices);
        this.listview.setAdapter(new Simple_device_adapter(context, deviceTemps));
        this.listview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int pos, long id) {
                String device_name = ((TextView) v.findViewById(C0434R.C0436id.device_name)).getText().toString();
                Log.i("call", "this is the deviceTemp name" + device_name);
                v.setOnClickListener(new SelectedBoxClickListener(device_name));
            }
        });
        String[] stringArray = getResources().getStringArray(C0434R.array.language_names);
        return view;
    }

    private void reloadTexts() {
        ((TextView) getView().findViewById(C0434R.C0436id.settings_title)).setText(C0434R.string.settings_title);
    }
}
