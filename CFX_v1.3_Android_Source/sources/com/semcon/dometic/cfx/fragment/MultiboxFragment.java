package com.semcon.dometic.cfx.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.CFxBoxControlActivity;
import com.semcon.dometic.cfx.adapters.DevicesAdapter;
import com.semcon.dometic.cfx.alarm.AlarmHandler;
import com.semcon.dometic.cfx.communication.CfxPoller;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.cfx.data.Refrigerator;
import com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral;
import java.io.IOException;
import java.util.List;
import roboguice.event.EventManager;
import roboguice.fragment.RoboFragment;

public class MultiboxFragment extends RoboFragment {
    private static final String COMPARTMENT = "compartment";
    private static final String TAG = BoxControlFragment.class.getName();
    private List<Refrigerator> devicesInTheSystem;
    private ListView listview;
    @Inject
    private AlarmHandler mAlarmHandler;
    @Inject
    private ICfxCommunication mCfxCommunication;
    @Inject
    private CfxPoller mCfxPoller;
    @Named("GlobalEventManager")
    @Inject
    private EventManager mGlobalEventManager;
    private OnToggleConnectionListener mOnToggleConnectionListener;
    @Inject
    private Handler mServiceHandler;
    @Inject
    private CfxSharedPreferencesHelper_univeral mSharedPreferencesHelper;

    public interface OnToggleConnectionListener {
        void onConnect();

        void onDisconnect();
    }

    class SelectedBoxClickListener implements OnClickListener {
        String device_name;

        public SelectedBoxClickListener(String device_name2) {
            this.device_name = device_name2;
        }

        public void onClick(View v) {
            Intent myIntent = new Intent(MultiboxFragment.this.getContext(), CFxBoxControlActivity.class);
            myIntent.putExtra("Device_name", this.device_name);
            MultiboxFragment.this.startActivity(myIntent);
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.devicesInTheSystem = this.mSharedPreferencesHelper.getDevicesInTheSystem();
        View view = inflater.inflate(C0434R.layout.fragment_multiple_device, container, false);
        Context context = getActivity();
        this.listview = (ListView) view.findViewById(C0434R.C0436id.devices_listview);
        this.listview.setAdapter(new DevicesAdapter(context, this.devicesInTheSystem));
        this.listview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int pos, long id) {
                v.setOnClickListener(new SelectedBoxClickListener(((TextView) v.findViewById(C0434R.C0436id.device_name)).getText().toString()));
            }
        });
        return view;
    }

    public void onStart() {
        super.onStart();
    }

    public void onStop() {
        super.onStop();
        try {
            this.mCfxCommunication.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnToggleConnectionListener) {
            this.mOnToggleConnectionListener = (OnToggleConnectionListener) context;
        }
    }
}
