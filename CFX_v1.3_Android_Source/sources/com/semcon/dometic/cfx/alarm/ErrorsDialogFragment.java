package com.semcon.dometic.cfx.alarm;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.p000v4.app.DialogFragment;
import android.support.p003v7.app.AlertDialog.Builder;
import com.semcon.dometic.cfx.C0434R;
import java.util.ArrayList;
import java.util.List;

public class ErrorsDialogFragment extends DialogFragment {
    public static final String ERRORS_ARRAY = "ERRORS_ARRAY";
    private List<String> _errors = new ArrayList();

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey(ERRORS_ARRAY)) {
            this._errors = args.getStringArrayList(ERRORS_ARRAY);
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String[] errors = new String[this._errors.size()];
        this._errors.toArray(errors);
        Builder builder = new Builder(getActivity());
        builder.setTitle(C0434R.string.errors).setNegativeButton(C0434R.string.cancel, (OnClickListener) null).setCancelable(true).setItems((CharSequence[]) errors, (OnClickListener) null);
        return builder.create();
    }
}
