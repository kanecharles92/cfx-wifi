package com.semcon.dometic.cfx.alarm;

import android.app.AlertDialog.Builder;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.semcon.dometic.cfx.C0434R;
import com.semcon.dometic.cfx.event.AlarmEvent;
import com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral;
import roboguice.event.EventListener;
import roboguice.event.EventManager;

public class AlarmViews {
    private EventListener mAlarmListener = new EventListener<AlarmEvent>() {
        public void onEvent(AlarmEvent event) {
            if (AlarmViews.this.mCfxSharedPreferencesHelperUniveral.isAlarmsEnabled(event.getContext())) {
                AlarmViews.this.showAlertDialog(event);
            }
        }
    };
    @Inject
    CfxSharedPreferencesHelper_univeral mCfxSharedPreferencesHelperUniveral;
    @Named("GlobalEventManager")
    @Inject
    private EventManager mGlobalEventManager;

    public void start() {
        this.mGlobalEventManager.registerObserver(AlarmEvent.class, this.mAlarmListener);
    }

    /* access modifiers changed from: private */
    public void showAlertDialog(AlarmEvent event) {
        Builder builder = new Builder(event.getContext());
        builder.setMessage(event.getErrors().toString());
        builder.setPositiveButton(C0434R.string.f44ok, null);
        builder.create().show();
    }
}
