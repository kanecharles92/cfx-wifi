package com.semcon.dometic.cfx.alarm;

import android.util.Log;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.semcon.dometic.cfx.event.AlarmEvent;
import com.semcon.dometic.cfx.event.OnNewCfxInfo;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import java.util.ArrayList;
import java.util.List;
import roboguice.activity.event.OnStopEvent;
import roboguice.context.event.OnStartEvent;
import roboguice.event.EventListener;
import roboguice.event.EventManager;
import roboguice.event.Observes;

public class AlarmHandler {
    private EventListener mCfxInfoListener = new EventListener<OnNewCfxInfo>() {
        public void onEvent(OnNewCfxInfo event) {
            AlarmHandler.this.mCurrentErrorCodes = event.getRefrigerator().getErrorCodes();
            Log.d("AlarmHandler CODES: ", "Errors: " + AlarmHandler.this.mCurrentErrorCodes);
            AlarmHandler.this.mGlobalEventManager.fire(new AlarmEvent(AlarmHandler.this.mCurrentErrorCodes, event.getContext()));
        }
    };
    /* access modifiers changed from: private */
    public List<CFXError> mCurrentErrorCodes = new ArrayList();
    /* access modifiers changed from: private */
    @Named("GlobalEventManager")
    @Inject
    public EventManager mGlobalEventManager;

    public void startListen(@Observes OnStartEvent event) {
        this.mGlobalEventManager.registerObserver(OnNewCfxInfo.class, this.mCfxInfoListener);
    }

    public void stopListen(@Observes OnStopEvent event) {
        this.mGlobalEventManager.unregisterObserver(OnNewCfxInfo.class, this.mCfxInfoListener);
    }

    public List<CFXError> getCurrentErrorCodes() {
        return this.mCurrentErrorCodes;
    }
}
