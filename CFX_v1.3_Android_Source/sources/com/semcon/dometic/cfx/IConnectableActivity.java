package com.semcon.dometic.cfx;

public interface IConnectableActivity {
    Class<?> getConnectingActivityClass();
}
