package com.semcon.dometic.cfx;

import com.google.inject.AnnotationDatabase;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import roboguice.fragment.FragmentUtil;

public class AnnotationDatabaseImpl extends AnnotationDatabase {
    public void fillAnnotationClassesAndFieldsNames(HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToFieldSet) {
        String annotationClassName = "com.google.inject.Inject";
        Map<String, Set<String>> mapClassWithInjectionNameToFieldSet = (Map) mapAnnotationToMapClassWithInjectionNameToFieldSet.get(annotationClassName);
        if (mapClassWithInjectionNameToFieldSet == null) {
            mapClassWithInjectionNameToFieldSet = new HashMap<>();
            mapAnnotationToMapClassWithInjectionNameToFieldSet.put(annotationClassName, mapClassWithInjectionNameToFieldSet);
        }
        Set<String> fieldNameSet = new HashSet<>();
        fieldNameSet.add("mGlobalEventManager");
        fieldNameSet.add("mCfxCommunication");
        fieldNameSet.add("mServiceHandler");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.communication.CfxPoller", fieldNameSet);
        Set<String> fieldNameSet2 = new HashSet<>();
        fieldNameSet2.add("mCfxPoller");
        fieldNameSet2.add("mCfxCommunication");
        fieldNameSet2.add("mCfxSharedPreferencesHelperUniveral");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.fragment.BoxAlarmSettingsFragment", fieldNameSet2);
        Set<String> fieldNameSet3 = new HashSet<>();
        fieldNameSet3.add("mCfxPoller");
        fieldNameSet3.add("mSharedPreferencesHelper");
        fieldNameSet3.add("mCfxCommunication");
        fieldNameSet3.add("mGlobalEventManager");
        fieldNameSet3.add("mAlarmHandler");
        fieldNameSet3.add("mServiceHandler");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.fragment.MultiboxFragment", fieldNameSet3);
        Set<String> fieldNameSet4 = new HashSet<>();
        fieldNameSet4.add("CfxSharedPreferencesHelper_univeral");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.fragment.CfxAppSettingsFragment", fieldNameSet4);
        Set<String> fieldNameSet5 = new HashSet<>();
        fieldNameSet5.add("mCfxPoller");
        fieldNameSet5.add("mCfxCommunication");
        fieldNameSet5.add("mCfxSharedPreferencesHelperUniveral");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.fragment.BoxConnectionSettingsFragment", fieldNameSet5);
        Set<String> fieldNameSet6 = new HashSet<>();
        fieldNameSet6.add("mCfxPoller");
        fieldNameSet6.add("mCfxCommunication");
        fieldNameSet6.add("mGlobalEventManager");
        fieldNameSet6.add("mAlarmHandler");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.ErrorDetailsFragment", fieldNameSet6);
        Set<String> fieldNameSet7 = new HashSet<>();
        fieldNameSet7.add("mCfxCommunication");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.fragment.BoxSettingsFragment", fieldNameSet7);
        Set<String> fieldNameSet8 = new HashSet<>();
        fieldNameSet8.add("mCfxPoller");
        fieldNameSet8.add("mCfxCommunication");
        fieldNameSet8.add("mCfxSharedPreferencesHelperUniveral");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.fragment.LegalFragment", fieldNameSet8);
        Set<String> fieldNameSet9 = new HashSet<>();
        fieldNameSet9.add("mGlobalEventManager");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.alarm.AlarmHandler", fieldNameSet9);
        Set<String> fieldNameSet10 = new HashSet<>();
        fieldNameSet10.add("mCfxPoller");
        fieldNameSet10.add("mCfxCommunication");
        fieldNameSet10.add("mCfxSharedPreferencesHelperUniveral");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.fragment.BoxBatterySettingsFragment", fieldNameSet10);
        Set<String> fieldNameSet11 = new HashSet<>();
        fieldNameSet11.add("mCfxPoller");
        fieldNameSet11.add("mCfxCommunication");
        fieldNameSet11.add("mCfxSharedPreferencesHelperUniveral");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.fragment.VersionFragment", fieldNameSet11);
        Set<String> fieldNameSet12 = new HashSet<>();
        fieldNameSet12.add("mGlobalEventManager");
        fieldNameSet12.add("mCfxSharedPreferencesHelperUniveral");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.alarm.AlarmViews", fieldNameSet12);
        Set<String> fieldNameSet13 = new HashSet<>();
        fieldNameSet13.add("mCfxPoller");
        fieldNameSet13.add("mCfxCommunication");
        fieldNameSet13.add("mGlobalEventManager");
        fieldNameSet13.add("mAlarmHandler");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.cfx.fragment.BoxControlFragment", fieldNameSet13);
    }

    public void fillAnnotationClassesAndMethods(HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToMethodsSet) {
        String annotationClassName = "roboguice.event.Observes";
        Map<String, Set<String>> mapClassWithInjectionNameToMethodSet = (Map) mapAnnotationToMapClassWithInjectionNameToMethodsSet.get(annotationClassName);
        if (mapClassWithInjectionNameToMethodSet == null) {
            mapClassWithInjectionNameToMethodSet = new HashMap<>();
            mapAnnotationToMapClassWithInjectionNameToMethodsSet.put(annotationClassName, mapClassWithInjectionNameToMethodSet);
        }
        Set<String> methodSet = new HashSet<>();
        methodSet.add("startListen:roboguice.context.event.OnStartEvent");
        methodSet.add("stopListen:roboguice.activity.event.OnStopEvent");
        mapClassWithInjectionNameToMethodSet.put("com.semcon.dometic.cfx.alarm.AlarmHandler", methodSet);
    }

    public void fillAnnotationClassesAndConstructors(HashMap<String, Map<String, Set<String>>> hashMap) {
    }

    public void fillClassesContainingInjectionPointSet(HashSet<String> classesContainingInjectionPointsSet) {
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.communication.CfxPoller");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.fragment.BoxAlarmSettingsFragment");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.fragment.MultiboxFragment");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.fragment.CfxAppSettingsFragment");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.fragment.BoxConnectionSettingsFragment");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.ErrorDetailsFragment");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.fragment.BoxSettingsFragment");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.fragment.LegalFragment");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.alarm.AlarmHandler");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.fragment.BoxBatterySettingsFragment");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.fragment.VersionFragment");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.alarm.AlarmViews");
        classesContainingInjectionPointsSet.add("com.semcon.dometic.cfx.fragment.BoxControlFragment");
    }

    public void fillBindableClasses(HashSet<String> injectedClasses) {
        injectedClasses.add("roboguice.event.EventManager");
        injectedClasses.add("com.semcon.dometic.cfx.communication.CfxPoller");
        injectedClasses.add("com.semcon.dometic.cfx.alarm.AlarmHandler");
        injectedClasses.add("com.semcon.dometic.cfx.communication.ICfxCommunication");
        injectedClasses.add("roboguice.activity.event.OnStopEvent");
        injectedClasses.add("android.os.Handler");
        injectedClasses.add("roboguice.context.event.OnStartEvent");
        injectedClasses.add("com.semcon.dometic.cfx.persistent.CfxSharedPreferencesHelper_univeral");
        if (FragmentUtil.hasNative) {
            injectedClasses.add("android.app.FragmentManager");
        }
        if (FragmentUtil.hasSupport) {
            injectedClasses.add("android.support.v4.app.FragmentManager");
        }
    }
}
