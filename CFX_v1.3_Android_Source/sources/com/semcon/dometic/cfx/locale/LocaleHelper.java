package com.semcon.dometic.cfx.locale;

import android.content.Context;
import android.content.res.Configuration;
import java.util.Locale;

public class LocaleHelper {
    public static void setLocale(Context context, String language) {
        Locale locale = new Locale(language);
        Context context2 = context.getApplicationContext();
        Locale.setDefault(locale);
        Configuration config = new Configuration(context2.getResources().getConfiguration());
        config.locale = locale;
        context2.getResources().updateConfiguration(config, context2.getResources().getDisplayMetrics());
    }
}
