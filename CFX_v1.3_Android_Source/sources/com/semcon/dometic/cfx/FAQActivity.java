package com.semcon.dometic.cfx;

import android.os.Bundle;
import android.support.p003v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;

public class FAQActivity extends RoboAppCompatActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0434R.layout.activity_cfx_app_faq);
        setSupportActionBar((Toolbar) findViewById(C0434R.C0436id.toolbar));
        ((TextView) findViewById(C0434R.C0436id.faq_link_textview)).setMovementMethod(LinkMovementMethod.getInstance());
    }
}
