package com.semcon.dometic.cfx;

import android.os.Bundle;
import android.support.p000v4.app.Fragment;
import com.semcon.dometic.cfx.fragment.BoxControlFragment;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;

public class CfxActivity extends RoboAppCompatActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0434R.layout.activity_cfx);
        getSupportFragmentManager().beginTransaction().add(C0434R.C0436id.fragment_container, (Fragment) new BoxControlFragment()).commit();
    }
}
