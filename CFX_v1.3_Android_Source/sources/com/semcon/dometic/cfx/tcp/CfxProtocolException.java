package com.semcon.dometic.cfx.tcp;

public class CfxProtocolException extends Exception {
    public CfxProtocolException() {
    }

    public CfxProtocolException(String detailMessage) {
        super(detailMessage);
    }

    public CfxProtocolException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CfxProtocolException(Throwable throwable) {
        super(throwable);
    }

    public String toString() {
        return super.toString();
    }
}
