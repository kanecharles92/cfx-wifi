package com.semcon.dometic.cfx.tcp;

public interface CfxComInterface {

    public enum CFXError {
        ABS_ERROR,
        COMPRESSOR_FAULTY,
        THERMOSTAT_FAULTY,
        SOLENOID_VALVE_FAULTY,
        DOOR_OPEN,
        COMPRESSOR_WORKED_FOR_ONE_CYCLE,
        TEMPERATURE_ALARM_ERROR,
        TEMPERATURE_ALARM_1_ERROR,
        TEMPERATURE_ALARM_2_ERROR,
        DOOR_OPEN_OVER_3_MIN
    }
}
