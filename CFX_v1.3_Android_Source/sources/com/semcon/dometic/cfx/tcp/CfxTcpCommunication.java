package com.semcon.dometic.cfx.tcp;

import android.util.Log;
import com.semcon.dometic.cfx.communication.CfxPoller;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import org.roboguice.shaded.goole.common.base.Ascii;

public class CfxTcpCommunication extends CfxTcpBase implements CfxComInterface {
    private static final byte ABS_ERROR_MASK = 1;
    private static byte AC_AVAILABLE_MASK = 4;
    private static byte BOTH_DOORS_MASK = 3;
    private static final byte BOX_OFF = 0;
    private static final byte BOX_ON = 1;
    public static int CELSIUS = 0;
    private static String CFX_IP = "192.168.1.1";
    private static int CFX_PORT = 6378;
    private static byte COMMAND_CHECK_INFO = 0;
    private static byte COMMAND_RESET_WIFI = 8;
    private static byte COMMAND_SET_ABS = 3;
    private static byte COMMAND_SET_ALARM = 7;
    private static byte COMMAND_SET_CF = 4;
    private static byte COMMAND_SET_TEMP_CALIB = 6;
    private static byte COMMAND_SET_TEMP_COMP1 = 1;
    private static byte COMMAND_SET_TEMP_COMP2 = 2;
    private static byte COMMAND_TURN_ON_OFF = 5;
    private static byte COMPARTMENT_1_MASK = 2;
    private static byte COMPARTMENT_2_MASK = 4;
    private static final byte COMPRESSOR_ERROR_MASK = 2;
    private static final byte COMPRESSOR_ONE_CYCLE_ERROR_MASK = 32;
    private static byte DOOR_1_ALARM_MASK = 16;
    private static byte DOOR_1_MASK = 1;
    private static byte DOOR_2_ALARM_MASK = 32;
    private static byte DOOR_2_MASK = 2;
    private static final byte DOOR_OPEN_ERROR_MASK = 16;
    public static int FARENHEIT = 1;
    private static int POS_ABS = 5;
    private static int POS_ALARM_SETTING = 9;
    private static int POS_CF_COMPONOFF = 6;
    private static int POS_COMMAND = 1;
    private static int POS_COMPRESSOR_WORKS = 13;
    private static int POS_DC_SUPPLY_VOLTAGE = 10;
    private static int POS_ERROR_CODES = 15;
    private static int POS_INITIATLIZATION = 0;
    private static int POS_PAYLOAD_SIZE = 2;
    private static int POS_REFRIGERATOR_ONOFF = 7;
    private static int POS_SET_TEMP_COMPARTMENT_1 = 3;
    private static int POS_SET_TEMP_COMPARTMENT_2 = 4;
    private static int POS_STATUS_OF_DOORS_AND_AC = 14;
    private static int POS_TEMP_CALIBRATION = 8;
    private static int POS_TEMP_OF_COMPARTMENT1 = 11;
    private static int POS_TEMP_OF_COMPARTMENT2 = 12;
    private static final byte SOLENOID_VALVE_ERROR_MASK = 8;
    private static final int TEMPOFFSET = -50;
    private static byte TEMP_UNIT_MASK = 1;
    private static final byte THERMOSTAT_ERROR_MASK = 4;
    private byte COMPRESSOR_ON_MASK = 2;
    private byte COMPRESSOR_WORKING_FOR_COMPARTMENT_MASK = 1;

    /* renamed from: IP */
    private String f49IP = CFX_IP;
    private boolean boxCacheOn = true;
    private int boxStateCacheTime = CfxPoller.POLLING_DELAY_MILLIS;
    private byte[] cachedBoxState;
    private byte[] cfxSendDatStructure = {51, 0, Ascii.f56FF, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0};

    // /* renamed from: in */
    InputStream f50in = null;
    OutputStream out = null;
    private int port = CFX_PORT;
    private Socket socket;
    private long timeCached = 0;

    public CfxTcpCommunication(String IP, int port2) throws IOException {
        this.f49IP = IP;
        this.port = port2;
        this.socket = new Socket(IP, port2);
        this.out = this.socket.getOutputStream();
        this.f50in = this.socket.getInputStream();
    }

    public void reOpenSocket() throws IOException {
        if (this.socket == null) {
            throw new IOException("Socket is null");
        }
        this.socket = new Socket(this.f49IP, this.port);
        if (this.out != null) {
            this.out.close();
        }
        if (this.f50in != null) {
            this.f50in.close();
        }
        this.out = this.socket.getOutputStream();
        this.f50in = this.socket.getInputStream();
    }

    public byte[] getCfxInfo() throws IOException, CfxProtocolException {

        if (System.currentTimeMillis() - this.timeCached > ((long) this.boxStateCacheTime) || !this.boxCacheOn) {
            this.cfxSendDatStructure[POS_COMMAND] = COMMAND_CHECK_INFO;
            sendTCPCommand(this.cfxSendDatStructure);
            byte[] response = readCommandResponse();
            if (!verifyCommand(response)) {
                throw new CfxProtocolException("Unknown command sent from CFX");
            } else if (!verifyChecksum(response)) {
                throw new CfxProtocolException("Checksum invalid");
            } else if (!verifyPayloadLength(response)) {
                throw new CfxProtocolException("Payload length faulty");
            } else {
                this.cachedBoxState = response;
                this.timeCached = System.currentTimeMillis();
            }
        }
        return this.cachedBoxState;
    }

    public int getCompartmentCount() {
        return 2;
    }

    public byte setABS(int absLevel) throws IOException, CfxProtocolException {
        if (absLevel < 0 || absLevel > 2) {
            throw new CfxProtocolException("ABS level not within range. Tried to set: " + absLevel);
        }
        this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_ABS;
        this.cfxSendDatStructure[POS_ABS] = (byte) absLevel;
        sendTCPCommand(this.cfxSendDatStructure);
        return extractCfxReturnDataValue(readCommandResponse(), POS_ABS);
    }

    public byte changeTempUnit(byte tempUnit) throws IOException, CfxProtocolException {
        this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_CF;
        if (tempUnit == CELSIUS) {
            this.cfxSendDatStructure[POS_CF_COMPONOFF] = 0;
        } else {
            this.cfxSendDatStructure[POS_CF_COMPONOFF] = 1;
        }
        sendTCPCommand(this.cfxSendDatStructure);
        if ((extractCfxReturnDataValue(readCommandResponse(), POS_CF_COMPONOFF) & 1) == 1) {
            return 1;
        }
        return 0;
    }

    public boolean setCompartmentOn(int compartmentNumber, boolean on) throws IOException, CfxProtocolException {
        byte[] tmp = getCfxInfo();
        this.cfxSendDatStructure[POS_SET_TEMP_COMPARTMENT_1] = tmp[POS_SET_TEMP_COMPARTMENT_1];
        this.cfxSendDatStructure[POS_SET_TEMP_COMPARTMENT_2] = tmp[POS_SET_TEMP_COMPARTMENT_2];
        if (compartmentNumber == 1) {
            this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_TEMP_COMP1;
            if (on) {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 0;
            } else {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 2;
            }
        } else if (compartmentNumber == 2) {
            this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_TEMP_COMP2;
            if (on) {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 0;
            } else {
                this.cfxSendDatStructure[POS_CF_COMPONOFF] = 4;
            }
        } else {
            throw new CfxProtocolException("Compartment " + compartmentNumber + " does not exist in this box");
        }
        sendTCPCommand(this.cfxSendDatStructure);
        byte b = extractCfxReturnDataValue(readCommandResponse(), POS_CF_COMPONOFF);
        if ((COMPARTMENT_1_MASK & b) == COMPARTMENT_1_MASK && compartmentNumber == 1) {
            return false;
        }
        if ((COMPARTMENT_2_MASK & b) == COMPARTMENT_2_MASK && compartmentNumber == 2) {
            return false;
        }
        return true;
    }

    public int setRefrigeratorOn(int i) throws IOException, CfxProtocolException {
        this.cfxSendDatStructure[POS_COMMAND] = COMMAND_TURN_ON_OFF;
        if (i == 1) {
            this.cfxSendDatStructure[POS_REFRIGERATOR_ONOFF] = 1;
        } else {
            this.cfxSendDatStructure[POS_REFRIGERATOR_ONOFF] = 0;
        }
        sendTCPCommand(this.cfxSendDatStructure);
        if (extractCfxReturnDataValue(readCommandResponse(), POS_REFRIGERATOR_ONOFF) == 1) {
            return 1;
        }
        return 0;
    }

    public byte setAlarm(int alarm) throws IOException, CfxProtocolException {
        boolean z = true;
        boolean z2 = alarm < 0;
        if (alarm <= 2) {
            z = false;
        }
        if (z || z2) {
            throw new CfxProtocolException("Alarm level not within allowed limits (0,1,2). Tried to set: " + alarm);
        }
        this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_ALARM;
        this.cfxSendDatStructure[POS_ALARM_SETTING] = (byte) alarm;
        sendTCPCommand(this.cfxSendDatStructure);
        return extractCfxReturnDataValue(readCommandResponse(), POS_ALARM_SETTING);
    }

    public byte setTempCompartment(int temp, int compartment) throws IOException, CfxProtocolException {
        if (temp < -127 || temp > 127) {
            throw new CfxProtocolException("Temperature out of range");
        }
        this.cfxSendDatStructure[POS_CF_COMPONOFF] = getCfxInfo()[POS_CF_COMPONOFF];
        if (compartment == 1) {
            this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_TEMP_COMP1;
            this.cfxSendDatStructure[POS_SET_TEMP_COMPARTMENT_1] = (byte) (temp + 50);
        }
        if (compartment == 2) {
            this.cfxSendDatStructure[POS_COMMAND] = COMMAND_SET_TEMP_COMP2;
            this.cfxSendDatStructure[POS_SET_TEMP_COMPARTMENT_2] = (byte) (temp + 50);
        }
        sendTCPCommand(this.cfxSendDatStructure);
        byte[] response = readCommandResponse();
        byte b = 0;
        if (compartment == 1) {
            b = (byte) (extractCfxReturnDataValue(response, POS_SET_TEMP_COMPARTMENT_1) - 50);
        }
        if (compartment == 2) {
            return (byte) (extractCfxReturnDataValue(response, POS_SET_TEMP_COMPARTMENT_2) - 50);
        }
        return b;
    }

    public int getTempOfCompartment(int compartmentNumber) throws IOException, CfxProtocolException {
        if (compartmentNumber == 1) {
            return extractCfxReturnDataValue(getCfxInfo(), POS_TEMP_OF_COMPARTMENT1) - 50;
        }
        if (compartmentNumber == 2) {
            return extractCfxReturnDataValue(getCfxInfo(), POS_TEMP_OF_COMPARTMENT2) - 50;
        }
        throw new CfxProtocolException("Compartment: " + compartmentNumber + " not available in this model");
    }

    public int getSetTempOfCompartment(int compartmentNumber) throws IOException, CfxProtocolException {
        if (compartmentNumber == 1) {
            return extractCfxReturnDataValue(getCfxInfo(), POS_SET_TEMP_COMPARTMENT_1) - 50;
        }
        if (compartmentNumber == 2) {
            return extractCfxReturnDataValue(getCfxInfo(), POS_SET_TEMP_COMPARTMENT_2) - 50;
        }
        throw new CfxProtocolException("Compartment: " + compartmentNumber + " not available in this model");
    }

    public int getAbsSetting() throws IOException, CfxProtocolException {
        return extractCfxReturnDataValue(getCfxInfo(), POS_ABS);
    }

    public int getTempUnit() throws IOException, CfxProtocolException {
        return TEMP_UNIT_MASK & extractCfxReturnDataValue(getCfxInfo(), POS_CF_COMPONOFF);
    }

    public boolean getCompartmentOn(int compartmentNumber) throws IOException, CfxProtocolException {
        byte ret = extractCfxReturnDataValue(getCfxInfo(), POS_CF_COMPONOFF);
        if (compartmentNumber == 1 && (COMPARTMENT_1_MASK & ret) == COMPARTMENT_1_MASK) {
            return false;
        }
        if (compartmentNumber == 2 && (COMPARTMENT_2_MASK & ret) == COMPARTMENT_2_MASK) {
            return false;
        }
        return true;
    }

    public int getRefrigeratorOn() throws IOException, CfxProtocolException {
        if (extractCfxReturnDataValue(getCfxInfo(), POS_REFRIGERATOR_ONOFF) == 0) {
            return 0;
        }
        return 1;
    }

    public float getDCSupplyVoltage() throws IOException, CfxProtocolException {
        byte reading = extractCfxReturnDataValue(getCfxInfo(), POS_DC_SUPPLY_VOLTAGE);
        if (reading < 40) {
            return ((float) (reading + 256)) / 10.0f;
        }
        return (float) (((double) reading) / 10.0d);
    }

    public boolean isCompressorWorking() throws IOException, CfxProtocolException {
        if ((this.COMPRESSOR_ON_MASK & extractCfxReturnDataValue(getCfxInfo(), POS_COMPRESSOR_WORKS)) == 2) {
            return true;
        }
        return false;
    }

    public int compressorWorkingForCompartment() throws IOException, CfxProtocolException {
        if ((this.COMPRESSOR_WORKING_FOR_COMPARTMENT_MASK & extractCfxReturnDataValue(getCfxInfo(), POS_COMPRESSOR_WORKS)) == 1) {
            return 1;
        }
        return 2;
    }

    public boolean isDoorOpen(int doorNumber) throws IOException, CfxProtocolException {
        byte reading = extractCfxReturnDataValue(getCfxInfo(), POS_STATUS_OF_DOORS_AND_AC);
        if ((BOTH_DOORS_MASK & reading) == 0) {
            return false;
        }
        if ((BOTH_DOORS_MASK & reading) == 3) {
            return true;
        }
        if ((DOOR_1_MASK & reading) == DOOR_1_MASK && doorNumber == 1) {
            return true;
        }
        if ((DOOR_1_MASK & reading) == 0 && doorNumber == 1) {
            return false;
        }
        if ((DOOR_2_MASK & reading) == DOOR_2_MASK && doorNumber == 2) {
            return true;
        }
        if ((DOOR_2_MASK & reading) != 0 || doorNumber == 2) {
        }
        return false;
    }

    public boolean isAcAvailable() throws IOException, CfxProtocolException {
        if ((AC_AVAILABLE_MASK & extractCfxReturnDataValue(getCfxInfo(), POS_STATUS_OF_DOORS_AND_AC)) == AC_AVAILABLE_MASK) {
            return true;
        }
        return false;
    }

    public boolean isDoorAlarming(int doorNumber) throws IOException, CfxProtocolException {
        byte reading = extractCfxReturnDataValue(getCfxInfo(), POS_STATUS_OF_DOORS_AND_AC);
        if ((DOOR_1_ALARM_MASK & reading) == DOOR_1_ALARM_MASK && doorNumber == 1) {
            return true;
        }
        if ((DOOR_2_ALARM_MASK & reading) == DOOR_2_ALARM_MASK && doorNumber == 2) {
            return true;
        }
        return false;
    }

    public ArrayList<CFXError> getErrorCodes() throws IOException, CfxProtocolException {
        ArrayList<CFXError> codes = new ArrayList<>();
        byte reading = extractCfxReturnDataValue(getCfxInfo(), POS_ERROR_CODES);
        if ((reading & 1) != 0) {
            codes.add(CFXError.ABS_ERROR);
        }
        if ((reading & 2) != 0) {
            codes.add(CFXError.COMPRESSOR_FAULTY);
        }
        if ((reading & 4) != 0) {
            codes.add(CFXError.THERMOSTAT_FAULTY);
        }
        if ((reading & 8) != 0) {
            codes.add(CFXError.SOLENOID_VALVE_FAULTY);
        }
        if ((reading & 16) != 0) {
            codes.add(CFXError.DOOR_OPEN);
        }
        if ((reading & 32) != 0) {
            codes.add(CFXError.COMPRESSOR_WORKED_FOR_ONE_CYCLE);
        }
        return codes;
    }

    private byte[] readCommandResponse() throws IOException {
        if (this.socket.isClosed()) {
            System.out.println("Reopening socket");
            reOpenSocket();
        }
        ByteArrayOutputStream inBuffer = new ByteArrayOutputStream();
        System.out.print("Reading stream ");
        do {
            inBuffer.write(this.f50in.read());
            System.out.print(".");
        } while (this.f50in.available() != 0);
        System.out.println("");
        System.out.print("IN:");
        printByteArray(inBuffer.toByteArray());
        return inBuffer.toByteArray();
    }

    private void sendTCPCommand(byte[] buffer) throws IOException, SocketException {
        boolean written = false;
        int retries = 50;
        do {
            retries--;
            if (this.socket.isClosed()) {
                reOpenSocket();
            }
            try {
                buffer[buffer.length - 1] = calculateChecksum(buffer);
                printByteArray(buffer);
                this.out.write(buffer);
                this.out.flush();
                written = true;
            } catch (SocketException e) {
                e.printStackTrace();
                reOpenSocket();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (written) {
                return;
            }
        } while (retries > 0);
    }

    private byte extractCfxReturnDataValue(byte[] buffer, int value) throws CfxProtocolException {
        if (buffer.length == 20) {
            return buffer[value];
        }
        throw new CfxProtocolException("Returned message not of expected length. Expected 20 bytes, got " + buffer.length);
    }

    private void printByteArray(byte[] buffer) {
        int length = buffer.length;
        for (int i = 0; i < length; i++) {
            System.out.print(buffer[i] + " ");
        }
        System.out.println();
    }

    public String change_Wifi_Credentials(String wifi_name, String wifi_pass) {
        Log.i("Call", "Calling the changing credentials method");
        byte[] name_bytes = wifi_name.getBytes();
        byte[] pass_bytes = wifi_pass.getBytes();
        int length = name_bytes.length + 2 + pass_bytes.length;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(51);
        bout.write(-18);
        bout.write((byte) name_bytes.length);
        bout.write((byte) pass_bytes.length);
        for (byte write : name_bytes) {
            bout.write(write);
        }
        for (byte write2 : pass_bytes) {
            bout.write(write2);
        }
        byte[] cfxChangeWiFiCredentials = bout.toByteArray();
        int length2 = cfxChangeWiFiCredentials.length;
        for (int i = 0; i < length2; i++) {
            System.out.print(cfxChangeWiFiCredentials[i] + " ");
        }
        try {
            this.socket.getOutputStream().write(cfxChangeWiFiCredentials);
            return readCommandResponseWifi();
        } catch (IOException e) {
            e.printStackTrace();
            return e.toString();
        }
    }

    public String readCommandResponseWifi() throws IOException {
        byte[] byteArray = {-52, -18, 2, 1, -67};
        InputStream in = this.socket.getInputStream();
        ByteArrayOutputStream inBuffer = new ByteArrayOutputStream();
        do {
            inBuffer.write(in.read());
        } while (in.available() != 0);
        if (Arrays.equals(inBuffer.toByteArray(), byteArray)) {
            return "Success in Updating Wifi";
        }
        return "Problem in Updating Wifi";
    }

    public boolean isConnected() {
        return this.socket.isConnected();
    }

    public boolean isClosed() {
        return this.socket.isClosed();
    }

    public void disconnect() {
        try {
            this.socket.close();
            Log.d("Socket", "Closing socket");
            if (this.socket.isClosed()) {
                Log.d("Socket", "Socket closed successfully");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getBoxStateCacheTime() {
        return this.boxStateCacheTime;
    }

    public void setBoxStateCacheTime(int boxStateCacheTime2) {
        this.boxStateCacheTime = boxStateCacheTime2;
    }

    public boolean isBoxCacheOn() {
        return this.boxCacheOn;
    }

    public void setBoxCacheOn(boolean boxCacheOn2) {
        this.boxCacheOn = boxCacheOn2;
    }
}
