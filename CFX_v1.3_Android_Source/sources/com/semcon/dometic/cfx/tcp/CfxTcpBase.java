package com.semcon.dometic.cfx.tcp;

public class CfxTcpBase {
    public byte calculateChecksum(byte[] buffer) {
        byte checksum = 0;
        for (int i = 0; i < buffer.length - 1; i++) {
            checksum = (byte) (checksum + buffer[i]);
        }
        return checksum;
    }

    public boolean verifyChecksum(byte[] buffer) {
        if (buffer[buffer.length - 1] == calculateChecksum(buffer)) {
            return true;
        }
        return false;
    }

    public boolean verifyPayloadLength(byte[] buffer) {
        if (buffer[2] != buffer.length - 3) {
            return false;
        }
        return true;
    }

    public boolean verifyCommand(byte[] buffer) {
        if (buffer.length > 1 && buffer[1] == 0 && buffer[1] == 238) {
            return false;
        }
        return true;
    }
}
