package com.semcon.dometic.cfx.persistent;

import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CfxBoxSettingsItem {
    private static final String ALARM_TYPE_FIELD_NAME = "alarm_type";
    private static final String COMPARTMENT_ALARMS_FIELD_NAME = "compartment_alarms";
    private static final String COMPARTMENT_NUMBER_FIELD_NAME = "compartment_number";
    private static final String INITIAL_ACTUAL_TEMPERATURE_FIELD_NAME = "initial_actual_temperature";
    private static final String OPERATOR_FIELD_NAME = "operator";

    /* renamed from: SHOULD_FIRE_ALARM_NEXT_TIME_ACTUAL_TEMPERATURE_IS_OUT_OF_TARGET_TEMPERATURE_BOUNDS_FIELD_NAME */
    private static final String f46xdd1aaaae = "should_fire_alarm_next_time_actual_temperature_is_out_of_target_temperature_bounds";
    private static final String TARGET_TEMPERATURE_FIELD_NAME = "target_temperature";
    private static final String TEMPERATURE_UNIT_FIELD_NAME = "temperature_unit";
    private static final String WIFI_FIELD_NAME = "wifi_name";
    private AlarmType mAlarmType;
    private CompartmentAlarmInfo[] mCompartmentAlarms;
    private TemperatureUnit mTemperatureUnit;
    private String mWifiName;

    public enum AlarmType {
        None(-1000, 1000),
        PlusMinus3(-3, 3),
        PlusMinus5(-5, 5),
        PlusMinus6(-6, 6),
        PlusMinus9(-9, 9);
        
        private int mRangeMax;
        private int mRangeMin;

        private AlarmType(int rangeMin, int rangeMax) {
            this.mRangeMin = rangeMin;
            this.mRangeMax = rangeMax;
        }

        public int getRangeMin() {
            return this.mRangeMin;
        }

        public int getRangeMax() {
            return this.mRangeMax;
        }
    }

    public static class CompartmentAlarmInfo {
        private int mCompartmentNumber;
        private int mInitialActualTemperature;
        private TargetTemperatureBoundsOperator mOperator = TargetTemperatureBoundsOperator.In;

        /* renamed from: mShouldFireAlarmNextTimeActualTemperatureIsOutOfTargetTemperatureBounds */
        private boolean f47xc4eb1308;
        private int mTargetTemperature;

        public enum TargetTemperatureBoundsOperator {
            In,
            Out;

            public boolean check(int actualTemperature, int targetTemperature, AlarmType alarmType) {
                return (actualTemperature < alarmType.getRangeMin() + targetTemperature || actualTemperature > alarmType.getRangeMax() + targetTemperature) ? this == Out : this == In;
            }

            public TargetTemperatureBoundsOperator next() {
                return values()[(ordinal() + 1) % values().length];
            }
        }

        public int getCompartmentNumber() {
            return this.mCompartmentNumber;
        }

        public void setCompartmentNumber(int compartmentNumber) {
            this.mCompartmentNumber = compartmentNumber;
        }

        public int getInitialActualTemperature() {
            return this.mInitialActualTemperature;
        }

        public void setInitialActualTemperature(int initialActualTemperature) {
            this.mInitialActualTemperature = initialActualTemperature;
        }

        public int getTargetTemperature() {
            return this.mTargetTemperature;
        }

        public void setTargetTemperature(int targetTemperature) {
            this.mTargetTemperature = targetTemperature;
        }

        /* renamed from: isShouldFireAlarmNextTimeActualTemperatureIsOutOfTargetTemperatureBounds */
        public boolean mo7976x88957605() {
            return this.f47xc4eb1308;
        }

        /* renamed from: setShouldFireAlarmNextTimeActualTemperatureIsOutOfTargetTemperatureBounds */
        public void mo7980xccef1b3d(boolean shouldFireAlarmNextTimeActualTemperatureIsOutOfTargetTemperatureBounds) {
            this.f47xc4eb1308 = shouldFireAlarmNextTimeActualTemperatureIsOutOfTargetTemperatureBounds;
        }

        public TargetTemperatureBoundsOperator getOperator() {
            return this.mOperator;
        }

        public void setOperator(TargetTemperatureBoundsOperator operator) {
            this.mOperator = operator;
        }

        public CompartmentAlarmInfo() {
        }

        public CompartmentAlarmInfo(int compartmentNumber, int initialActualTemperature, int targetTemperature) {
            this.mCompartmentNumber = compartmentNumber;
            this.mInitialActualTemperature = initialActualTemperature;
            this.mTargetTemperature = targetTemperature;
        }
    }

    public String getWifiName() {
        return this.mWifiName;
    }

    public void setWifiName(String wifiName) {
        this.mWifiName = wifiName;
    }

    public AlarmType getAlarmType() {
        return this.mAlarmType;
    }

    public void setAlarmType(AlarmType alarmType) {
        this.mAlarmType = alarmType;
    }

    public TemperatureUnit getTemperatureUnit() {
        return this.mTemperatureUnit;
    }

    public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
        this.mTemperatureUnit = temperatureUnit;
    }

    public CompartmentAlarmInfo[] getCompartmentAlarms() {
        return this.mCompartmentAlarms;
    }

    public void setCompartmentAlarms(CompartmentAlarmInfo[] compartmentAlarms) {
        this.mCompartmentAlarms = compartmentAlarms;
    }

    public CfxBoxSettingsItem() {
        this("");
    }

    public CfxBoxSettingsItem(String wifiName) {
        this.mAlarmType = AlarmType.None;
        this.mTemperatureUnit = TemperatureUnit.Celsius;
        this.mCompartmentAlarms = new CompartmentAlarmInfo[0];
        this.mWifiName = wifiName;
    }

    public String serialize() {
        JSONObject obj = new JSONObject();
        try {
            obj.put(WIFI_FIELD_NAME, getWifiName());
            obj.put(ALARM_TYPE_FIELD_NAME, getAlarmType().ordinal());
            obj.put(TEMPERATURE_UNIT_FIELD_NAME, getTemperatureUnit().ordinal());
            JSONArray compartmentAlarms = new JSONArray();
            int LENGTH = getCompartmentAlarms().length;
            for (int i = 0; i < LENGTH; i++) {
                JSONObject o = new JSONObject();
                CompartmentAlarmInfo compAlarm = this.mCompartmentAlarms[i];
                o.put(COMPARTMENT_NUMBER_FIELD_NAME, compAlarm.getCompartmentNumber());
                o.put(INITIAL_ACTUAL_TEMPERATURE_FIELD_NAME, compAlarm.getInitialActualTemperature());
                o.put(TARGET_TEMPERATURE_FIELD_NAME, compAlarm.getTargetTemperature());
                o.put(f46xdd1aaaae, compAlarm.mo7976x88957605());
                o.put(OPERATOR_FIELD_NAME, compAlarm.getOperator().ordinal());
                compartmentAlarms.put(o);
            }
            obj.put(COMPARTMENT_ALARMS_FIELD_NAME, compartmentAlarms);
            return obj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static CfxBoxSettingsItem deserialize(String serializedData) {
        if (serializedData == null || serializedData.isEmpty()) {
            return null;
        }
        CfxBoxSettingsItem item = new CfxBoxSettingsItem();
        try {
            JSONObject obj = new JSONObject(serializedData);
            if (obj == null) {
                return item;
            }
            if (obj.has(WIFI_FIELD_NAME)) {
                item.setWifiName(obj.getString(WIFI_FIELD_NAME));
            }
            if (obj.has(ALARM_TYPE_FIELD_NAME)) {
                item.setAlarmType(AlarmType.values()[obj.getInt(ALARM_TYPE_FIELD_NAME)]);
            }
            if (obj.has(TEMPERATURE_UNIT_FIELD_NAME)) {
                item.setTemperatureUnit(TemperatureUnit.values()[obj.getInt(TEMPERATURE_UNIT_FIELD_NAME)]);
            }
            if (!obj.has(COMPARTMENT_ALARMS_FIELD_NAME)) {
                return item;
            }
            JSONArray compartmentAlarms = obj.getJSONArray(COMPARTMENT_ALARMS_FIELD_NAME);
            if (compartmentAlarms == null || compartmentAlarms.length() <= 0) {
                return item;
            }
            List<CompartmentAlarmInfo> compAlarms = new ArrayList<>();
            for (int i = 0; i < compartmentAlarms.length(); i++) {
                JSONObject o = compartmentAlarms.getJSONObject(i);
                if (o != null) {
                    CompartmentAlarmInfo compAlarm = new CompartmentAlarmInfo();
                    if (o.has(COMPARTMENT_NUMBER_FIELD_NAME)) {
                        compAlarm.setCompartmentNumber(o.getInt(COMPARTMENT_NUMBER_FIELD_NAME));
                    }
                    if (o.has(INITIAL_ACTUAL_TEMPERATURE_FIELD_NAME)) {
                        compAlarm.setInitialActualTemperature(o.getInt(INITIAL_ACTUAL_TEMPERATURE_FIELD_NAME));
                    }
                    if (o.has(TARGET_TEMPERATURE_FIELD_NAME)) {
                        compAlarm.setTargetTemperature(o.getInt(TARGET_TEMPERATURE_FIELD_NAME));
                    }
                    if (o.has(f46xdd1aaaae)) {
                        compAlarm.mo7980xccef1b3d(o.getBoolean(f46xdd1aaaae));
                    }
                    if (o.has(OPERATOR_FIELD_NAME)) {
                        compAlarm.setOperator(TargetTemperatureBoundsOperator.values()[o.getInt(OPERATOR_FIELD_NAME)]);
                    }
                    compAlarms.add(compAlarm);
                }
            }
            CompartmentAlarmInfo[] ca = new CompartmentAlarmInfo[compAlarms.size()];
            compAlarms.toArray(ca);
            item.setCompartmentAlarms(ca);
            return item;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
