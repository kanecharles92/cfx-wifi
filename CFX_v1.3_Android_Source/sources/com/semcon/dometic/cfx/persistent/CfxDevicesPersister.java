package com.semcon.dometic.cfx.persistent;

import android.content.res.AssetManager;
import android.util.JsonReader;
import android.util.Log;
import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.data.Compartment;
import com.semcon.dometic.cfx.data.Door;
import com.semcon.dometic.cfx.data.Refrigerator;
import com.semcon.dometic.cfx.data.WiFiModule;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CfxDevicesPersister {
    List<Refrigerator> deviceTemps = new ArrayList();
    List<Refrigerator> devicesInTheSystem = new ArrayList();
    private AssetManager mAssetManager;
    private List<CFXError> mCurrentErrorCodes = new ArrayList();

    public void setAssetManager(AssetManager assetManager) {
        this.mAssetManager = assetManager;
    }

    public CfxDevicesPersister(AssetManager assetManager) {
        this.mAssetManager = assetManager;
    }

    public WiFiModule readerWiFiModule(JsonReader reader) throws IOException {
        String type = null;
        int is_on = 0;
        int protocol_version = 0;
        String address = null;
        String port = null;
        String username = null;
        String password = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("type")) {
                type = reader.nextString();
            } else if (name.equals("is_on")) {
                is_on = reader.nextInt();
            } else if (name.equals("protocol_version")) {
                protocol_version = reader.nextInt();
            } else if (name.equals("address")) {
                address = reader.nextString();
            } else if (name.equals("port")) {
                port = reader.nextString();
            } else if (name.equals("username")) {
                username = reader.nextString();
            } else if (name.equals("password")) {
                password = reader.nextString();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new WiFiModule(type, is_on, protocol_version, address, port, username, password);
    }

    public Compartment readerCompartment(JsonReader reader) throws IOException {
        String identifier = null;
        String name = null;
        int is_on = 0;
        int is_compressor_on = 0;
        int mActualTemperature = 0;
        int mTargetTemperature = 0;
        Door mDoor = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String input = reader.nextName();
            if (input.equals("identifier")) {
                identifier = reader.nextString();
            } else if (input.equals("name")) {
                name = reader.nextString();
            } else if (input.equals("is_compressor_on")) {
                is_compressor_on = reader.nextInt();
            } else if (input.equals("is_on")) {
                is_on = reader.nextInt();
            } else if (input.equals("target_temp")) {
                mTargetTemperature = reader.nextInt();
            } else if (input.equals("actual_temp")) {
                mActualTemperature = reader.nextInt();
            } else if (input.equals("door")) {
                mDoor = readDoor(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new Compartment(is_on, is_compressor_on, mActualTemperature, mTargetTemperature, identifier, name, mDoor);
    }

    public Door readDoor(JsonReader reader) throws IOException {
        int is_open = 0;
        int is_alarm_on = 0;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("is_open")) {
                is_open = reader.nextInt();
            } else if (name.equals("is_alarm_on")) {
                is_alarm_on = reader.nextInt();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new Door(is_open, is_alarm_on);
    }

    public Refrigerator readDevice(JsonReader reader) throws IOException {
        String model = null;
        List<Compartment> compartments = new ArrayList<>();
        int is_on = 0;
        int is_ac_on = 0;
        TemperatureUnit temperatureUnit = null;
        BatteryProtectionMode abs_level = null;
        int temp_alarm_level = 0;
        List<WiFiModule> wiFiModuleList = new ArrayList<>();
        List<CFXError> errors = new ArrayList<>();
        List features = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("model")) {
                model = reader.nextString();
            } else {
                if (name.equals("compartments")) {
                    reader.beginArray();
                    while (reader.hasNext()) {
                        compartments.add(readerCompartment(reader));
                    }
                    reader.endArray();
                } else {
                    if (name.equals("is_on")) {
                        is_on = reader.nextInt();
                    } else {
                        if (name.equals("is_ac_on")) {
                            is_ac_on = reader.nextInt();
                        } else {
                            if (name.equals("abs_level")) {
                                switch (reader.nextInt()) {
                                    case 0:
                                        BatteryProtectionMode abs_level2 = BatteryProtectionMode.Low;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                }
                                BatteryProtectionMode abs_level3 = BatteryProtectionMode.Medium;
                                abs_level = BatteryProtectionMode.High;
                                continue;
                            } else {
                                if (!name.equals("temperature_unit")) {
                                    if (name.equals("temp_alarm_level")) {
                                        temp_alarm_level = reader.nextInt();
                                    } else {
                                        if (name.equals("communication")) {
                                            reader.beginArray();
                                            while (reader.hasNext()) {
                                                Log.i("Json", "Running the loop");
                                                WiFiModule wiFiModuletemp = readerWiFiModule(reader);
                                                if (wiFiModuletemp.getType().equals("WIFI")) {
                                                    wiFiModuleList.add(wiFiModuletemp);
                                                }
                                            }
                                            reader.endArray();
                                        } else {
                                            if (name.equals("errors")) {
                                                reader.beginArray();
                                                while (reader.hasNext()) {
                                                    reader.skipValue();
                                                }
                                                reader.endArray();
                                            } else {
                                                if (name.equals("features")) {
                                                    reader.beginArray();
                                                    while (reader.hasNext()) {
                                                        reader.beginObject();
                                                        while (reader.hasNext()) {
                                                            if (name.equals("feature")) {
                                                                features.add(reader.nextString());
                                                            } else {
                                                                reader.skipValue();
                                                            }
                                                        }
                                                        reader.endObject();
                                                    }
                                                    reader.endArray();
                                                } else {
                                                    reader.skipValue();
                                                }
                                            }
                                        }
                                    }
                                } else if (reader.nextString() == "C") {
                                    temperatureUnit = TemperatureUnit.Celsius;
                                } else {
                                    temperatureUnit = TemperatureUnit.Fahrenheit;
                                }
                            }
                        }
                    }
                }
            }
        }
        reader.endObject();
        return new Refrigerator(model, compartments, is_on, is_ac_on, temperatureUnit, temp_alarm_level, 0.0d, abs_level, wiFiModuleList, errors, features);
    }

    public List<Refrigerator> readDevicesArray(JsonReader reader) throws IOException {
        List<Refrigerator> deviceTemps2 = new ArrayList<>();
        reader.beginObject();
        while (reader.hasNext()) {
            if (reader.nextName().equals("boxes")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    deviceTemps2.add(readDevice(reader));
                }
                reader.endArray();
            }
        }
        return deviceTemps2;
    }

    public ArrayList<CFXError> getErrorCodes() {
        Log.i("Call", "Calling the Error codes");
        ArrayList<CFXError> codes = new ArrayList<>();
        codes.add(CFXError.ABS_ERROR);
        codes.add(CFXError.COMPRESSOR_FAULTY);
        codes.add(CFXError.THERMOSTAT_FAULTY);
        codes.add(CFXError.SOLENOID_VALVE_FAULTY);
        codes.add(CFXError.DOOR_OPEN);
        codes.add(CFXError.COMPRESSOR_WORKED_FOR_ONE_CYCLE);
        Log.i("Call", "Before returning the error codes" + codes.size() + " " + codes.get(1));
        return codes;
    }

    public List<Refrigerator> createDevices() {
        JsonReader jsonReader;
        ArrayList<String> devices = new ArrayList<>();
        devices.add("CFX-95DZ1");
        devices.add("CFX-95DZ2");
        this.mCurrentErrorCodes = getErrorCodes();
        try {
            jsonReader = new JsonReader(new InputStreamReader(this.mAssetManager.open("devices.json"), "UTF-8"));
            this.deviceTemps = readDevicesArray(jsonReader);
            for (Refrigerator d : this.deviceTemps) {
                Log.i("Call", "this is the input file" + d.getModel());
                if (devices.contains(d.getModel().toString())) {
                    Refrigerator tempdevice = new Refrigerator(d.getModel(), d.getCompartments(), d.isOn(), d.isAcOn(), d.getTemperatureUnit(), d.getTempAlarmLevel(), d.getDcVoltage(), d.getBatteryProtectionMode(), d.getWiFiModuleList(), this.mCurrentErrorCodes, d.getFeatures());
                    if (this.devicesInTheSystem != null) {
                        this.devicesInTheSystem.add(tempdevice);
                    }
                }
            }
            List<Refrigerator> list = this.devicesInTheSystem;
            jsonReader.close();
            return list;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Throwable th) {
            jsonReader.close();
            throw th;
        }
    }
}
