package com.semcon.dometic.cfx.persistent;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.semcon.dometic.cfx.data.Refrigerator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CfxSharedPreferencesHelper_univeral {
    private static final String SHARED_PREFS_CFX = "shared_prefs_cfx";
    public static final String SHARED_PREFS_CFX_ENABLE_ALARMS_KEY = "enable_alarms_key";
    public static final String SHARED_PREFS_CFX_ERRORS_KEY = "errors_key";
    private static final List<Refrigerator> devicesInTheSystem = new ArrayList();

    public Set<String> getErrors(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_CFX, 0).getStringSet(SHARED_PREFS_CFX_ERRORS_KEY, new HashSet());
    }

    public void setErrors(Context context, Set<String> errors) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(SHARED_PREFS_CFX, 0);
        HashSet<String> allErrors = new HashSet<>();
        allErrors.addAll(errors);
        Editor editor = sharedPrefs.edit();
        editor.putStringSet(SHARED_PREFS_CFX_ERRORS_KEY, allErrors);
        editor.apply();
    }

    public boolean isAlarmsEnabled(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_CFX, 0).getBoolean(SHARED_PREFS_CFX_ENABLE_ALARMS_KEY, true);
    }

    public void enableAlarms(Context context, boolean enable) {
        Editor editor = context.getSharedPreferences(SHARED_PREFS_CFX, 0).edit();
        editor.putBoolean(SHARED_PREFS_CFX_ENABLE_ALARMS_KEY, enable);
        editor.apply();
    }

    public void add_devicesInTheSystem(Context context, Refrigerator device) {
        devicesInTheSystem.add(device);
    }

    public List<Refrigerator> getDevicesInTheSystem() {
        return devicesInTheSystem;
    }
}
