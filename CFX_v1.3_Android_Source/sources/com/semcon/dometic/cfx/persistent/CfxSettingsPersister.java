package com.semcon.dometic.cfx.persistent;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Pair;
import java.util.Set;
import java.util.TreeSet;

public class CfxSettingsPersister {
    private static final String SHARED_PREFS_CFX = "shared_prefs_cfx";
    private static final String SHARED_PREFS_CFX_BOXES_KEY = "boxes_key";

    public static CfxBoxSettingsItem getBoxSettings(Context context, String wifiName) {
        Pair<CfxBoxSettingsItem, String> pair = findBox(getStringSet(context, SHARED_PREFS_CFX_BOXES_KEY), wifiName);
        if (pair == null) {
            return null;
        }
        return (CfxBoxSettingsItem) pair.first;
    }

    public static void setBoxSettings(Context context, CfxBoxSettingsItem item) {
        if (item != null && item.getWifiName() != null && !item.getWifiName().isEmpty()) {
            Set<String> strings = removeBoxSettings(context, item.getWifiName());
            if (strings == null) {
                strings = new TreeSet<>();
            }
            strings.add(item.serialize());
            setStringSet(context, SHARED_PREFS_CFX_BOXES_KEY, strings);
        }
    }

    public static Set<String> removeBoxSettings(Context context, String wifiName) {
        Set<String> strings = getStringSet(context, SHARED_PREFS_CFX_BOXES_KEY);
        Pair<CfxBoxSettingsItem, String> pair = findBox(strings, wifiName);
        if (pair != null) {
            strings.remove(pair.second);
            setStringSet(context, SHARED_PREFS_CFX_BOXES_KEY, strings);
        }
        return strings;
    }

    private static Pair<CfxBoxSettingsItem, String> findBox(Set<String> strings, String wifiName) {
        if (strings == null || wifiName == null || wifiName.isEmpty()) {
            return null;
        }
        for (String s : strings) {
            CfxBoxSettingsItem item = CfxBoxSettingsItem.deserialize(s);
            if (item != null && item.getWifiName().equalsIgnoreCase(wifiName)) {
                return new Pair<>(item, s);
            }
        }
        return null;
    }

    private static Set<String> getStringSet(Context context, String key) {
        SharedPreferences sharedPrefs = getPrefs(context);
        return sharedPrefs == null ? new TreeSet() : sharedPrefs.getStringSet(key, new TreeSet());
    }

    private static void setStringSet(Context context, String key, Set<String> strings) {
        Editor editor = getPrefsEditor(context);
        if (editor != null && strings != null) {
            editor.putStringSet(key, strings);
            editor.commit();
        }
    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_CFX, 0);
    }

    private static Editor getPrefsEditor(Context context) {
        SharedPreferences sharedPrefs = getPrefs(context);
        if (sharedPrefs != null) {
            return sharedPrefs.edit();
        }
        return null;
    }
}
