package com.semcon.dometic.cfx;

import android.content.Intent;
import android.os.Bundle;
import android.support.p003v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.semcon.dometic.cfx.fragment.BoxControlFragment;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;

public class CFxBoxControlActivity extends RoboAppCompatActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0434R.layout.activity_cfx_box_settings);
        String device_name = getIntent().getStringExtra("Device_name");
        Bundle bundle = new Bundle();
        bundle.putString("Device_name", device_name);
        BoxControlFragment boxControlFragment = new BoxControlFragment();
        boxControlFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(C0434R.C0436id.fragment_container, boxControlFragment).commit();
        setSupportActionBar((Toolbar) findViewById(C0434R.C0436id.app_toolbar));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0434R.C0437menu.topmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0434R.C0436id.action_settings) {
            Intent intent = new Intent();
            intent.setClass(this, CfxAppSettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (item.getItemId() != C0434R.C0436id.action_faq) {
            return super.onOptionsItemSelected(item);
        } else {
            Intent intent2 = new Intent();
            intent2.setClass(this, FAQActivity.class);
            startActivity(intent2);
            return true;
        }
    }
}
