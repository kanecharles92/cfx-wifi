package com.semcon.dometic.cfx.constants;

public class Constants {
    public static final String CELSIUS_DEGREES = "°C";
    public static final String FAHRENHEIT_DEGREES = "°F";
    public static final String OFF = "off";

    /* renamed from: ON */
    public static final String f45ON = "on";

    public enum BatteryProtectionMode {
        High,
        Medium,
        Low
    }

    public enum TemperatureUnit {
        Celsius,
        Fahrenheit
    }
}
