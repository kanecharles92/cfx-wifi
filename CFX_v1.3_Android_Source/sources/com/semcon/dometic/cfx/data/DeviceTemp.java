package com.semcon.dometic.cfx.data;

public class DeviceTemp {
    private final String device_name;
    private final String power_supply_type;
    private final RefrigeratorTemp refrigeratorTemp;
    private final String type_of_connection;

    public String getType_of_connection() {
        return this.type_of_connection;
    }

    public String getPower_supply_type() {
        return this.power_supply_type;
    }

    public RefrigeratorTemp getRefrigeratorTemp() {
        return this.refrigeratorTemp;
    }

    public String getDevice_name() {
        return this.device_name;
    }

    public DeviceTemp(String device_name2, String type_of_connection2, String power_supply_type2, RefrigeratorTemp refrigeratorTemp2) {
        this.device_name = device_name2;
        this.type_of_connection = type_of_connection2;
        this.power_supply_type = power_supply_type2;
        this.refrigeratorTemp = refrigeratorTemp2;
    }
}
