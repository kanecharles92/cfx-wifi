package com.semcon.dometic.cfx.data;

public class Compartment {
    private int mActualTemperature;
    private Door mDoor;
    private String mIdentifier;
    private int mIsCompressorOn;
    private int mIsOn;
    private String mName;
    private int mTargetTemperature;

    public int getActualTemperature() {
        return this.mActualTemperature;
    }

    public void setActualTemperature(int actualTemperature) {
        this.mActualTemperature = actualTemperature;
    }

    public int getTargetTemperature() {
        return this.mTargetTemperature;
    }

    public void setTargetTemperature(int targetTemperature) {
        this.mTargetTemperature = targetTemperature;
    }

    public String getIdentifier() {
        return this.mIdentifier;
    }

    public void setIdentifier(String identifier) {
        this.mIdentifier = identifier;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int isCompressorOn() {
        return this.mIsCompressorOn;
    }

    public void setIsCompressorOn(int isCompressorOn) {
        this.mIsCompressorOn = isCompressorOn;
    }

    public Door getDoor() {
        return this.mDoor;
    }

    public void setDoor(Door door) {
        this.mDoor = door;
    }

    public int isOn() {
        return this.mIsOn;
    }

    public void setIsOn(int isOn) {
        this.mIsOn = isOn;
    }

    public Compartment(int isOn, int isCompressorOn, int actualTemperature, int targetTemperature, String name, String identifier, Door door) {
        this.mIsOn = isOn;
        this.mIsCompressorOn = isCompressorOn;
        this.mActualTemperature = actualTemperature;
        this.mTargetTemperature = targetTemperature;
        this.mName = name;
        this.mIdentifier = identifier;
        this.mDoor = door;
    }
}
