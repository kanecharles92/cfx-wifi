package com.semcon.dometic.cfx.data;

public class Error {
    private String mDescription;
    private byte mErrorCode;

    public byte getErrorCode() {
        return this.mErrorCode;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public Error(byte errorCode, String description) {
        this.mErrorCode = errorCode;
        this.mDescription = description;
    }
}
