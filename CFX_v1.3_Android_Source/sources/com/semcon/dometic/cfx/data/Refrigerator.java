package com.semcon.dometic.cfx.data;

import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import java.util.ArrayList;
import java.util.List;

public class Refrigerator {
    private BatteryProtectionMode mBatteryProtectionMode;
    private List<Compartment> mCompartments = new ArrayList();
    private double mDcVoltage;
    private List<CFXError> mErrorCodes = new ArrayList();
    private List mFeatures = new ArrayList();
    private int mIsAcOn;
    private int mIsDoorAlarming;
    private int mIsOn;
    private String mModel;
    private int mTempAlarmLevel;
    private TemperatureUnit mTemperatureUnit;
    private List<WiFiModule> mWiFiModuleList = new ArrayList();

    public String getModel() {
        return this.mModel;
    }

    public void setModel(String model) {
        this.mModel = model;
    }

    public List<Compartment> getCompartments() {
        return this.mCompartments;
    }

    public void setCompartments(List<Compartment> compartments) {
        this.mCompartments = compartments;
    }

    public int isOn() {
        return this.mIsOn;
    }

    public void setIsOn(int isOn) {
        this.mIsOn = isOn;
    }

    public int isAcOn() {
        return this.mIsAcOn;
    }

    public void setIsAcOn(int isAcOn) {
        this.mIsAcOn = isAcOn;
    }

    public BatteryProtectionMode getBatteryProtectionMode() {
        return this.mBatteryProtectionMode;
    }

    public void setBatteryProtectionMode(BatteryProtectionMode batteryProtectionMode) {
        this.mBatteryProtectionMode = batteryProtectionMode;
    }

    public TemperatureUnit getTemperatureUnit() {
        return this.mTemperatureUnit;
    }

    public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
        this.mTemperatureUnit = temperatureUnit;
    }

    public double getDcVoltage() {
        return this.mDcVoltage;
    }

    public void setDcVoltage(double dcVoltage) {
        this.mDcVoltage = dcVoltage;
    }

    public int getTempAlarmLevel() {
        return this.mTempAlarmLevel;
    }

    public void setTempAlarmLevel(int tempAlarmLevel) {
        this.mTempAlarmLevel = tempAlarmLevel;
    }

    public List<WiFiModule> getWiFiModuleList() {
        return this.mWiFiModuleList;
    }

    public void setWiFiModuleList(List<WiFiModule> wiFiModuleList) {
        this.mWiFiModuleList = wiFiModuleList;
    }

    public List<CFXError> getErrorCodes() {
        return this.mErrorCodes;
    }

    public void setErrorCodes(List<CFXError> errorCodes) {
        this.mErrorCodes = errorCodes;
    }

    public List getFeatures() {
        return this.mFeatures;
    }

    public void setFeatures(List features) {
        this.mFeatures = features;
    }

    public Refrigerator(String model, List<Compartment> compartments, int isOn, int isAcOn, TemperatureUnit temperatureUnit, int tempAlarmLevel, double dcVoltage, BatteryProtectionMode batteryProtectionMode, List<WiFiModule> wiFiModulesList, List<CFXError> errorList, List features) {
        this.mModel = model;
        this.mCompartments.addAll(compartments);
        this.mIsOn = isOn;
        this.mIsAcOn = isAcOn;
        this.mTemperatureUnit = temperatureUnit;
        this.mTempAlarmLevel = tempAlarmLevel;
        this.mDcVoltage = dcVoltage;
        this.mBatteryProtectionMode = batteryProtectionMode;
        this.mWiFiModuleList.addAll(wiFiModulesList);
        this.mErrorCodes.addAll(errorList);
        this.mFeatures.add(features);
    }

    public Refrigerator(List<Compartment> compartments, int isOn, int isAcOn, BatteryProtectionMode batteryProtectionMode, TemperatureUnit temperatureUnit, int tempAlarmLevel, double dcVoltage) {
        this.mCompartments = compartments;
        this.mIsOn = isOn;
        this.mIsAcOn = isAcOn;
        this.mBatteryProtectionMode = batteryProtectionMode;
        this.mTemperatureUnit = temperatureUnit;
        this.mTempAlarmLevel = tempAlarmLevel;
        this.mDcVoltage = dcVoltage;
    }
}
