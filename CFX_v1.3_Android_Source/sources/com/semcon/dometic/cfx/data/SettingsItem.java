package com.semcon.dometic.cfx.data;

import java.util.List;

public class SettingsItem {
    private String mName;
    private int mSelectedValueIndex;
    private List<String> mValues;

    public SettingsItem(String name, int selectedValueIndex, List<String> values) {
        this.mName = name;
        this.mSelectedValueIndex = selectedValueIndex;
        this.mValues = values;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getSelectedValueIndex() {
        return this.mSelectedValueIndex;
    }

    public void setSelectedValueIndex(int selectedValueIndex) {
        this.mSelectedValueIndex = selectedValueIndex;
    }

    public String getSelectedValue() {
        return this.mSelectedValueIndex < 0 ? "" : (String) this.mValues.get(this.mSelectedValueIndex);
    }

    public List<String> getValues() {
        return this.mValues;
    }

    public void setValues(List<String> values) {
        this.mValues = values;
    }
}
