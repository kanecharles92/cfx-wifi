package com.semcon.dometic.cfx.data;

public class Door {
    public int mIsAlarmOn;
    public int mIsOpen;

    public int isOpen() {
        return this.mIsOpen;
    }

    public int isAlarmOn() {
        return this.mIsAlarmOn;
    }

    public Door(int isOpen, int isAlarmOn) {
        this.mIsOpen = isOpen;
        this.mIsAlarmOn = isAlarmOn;
    }
}
