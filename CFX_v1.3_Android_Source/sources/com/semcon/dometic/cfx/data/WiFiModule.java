package com.semcon.dometic.cfx.data;

public class WiFiModule {
    private String mAddress;
    private int mIsOn;
    private String mPassword;
    private String mPort;
    private int mProtocolVersion;
    private String mType;
    private String mUsername;

    public String getType() {
        return this.mType;
    }

    public int isOn() {
        return this.mIsOn;
    }

    public int getProtocolVersion() {
        return this.mProtocolVersion;
    }

    public String getAddress() {
        return this.mAddress;
    }

    public String getUsername() {
        return this.mUsername;
    }

    public String getPort() {
        return this.mPort;
    }

    public String getPassword() {
        return this.mPassword;
    }

    public WiFiModule(String type, int isOn, int protocolVersion, String address, String port, String username, String password) {
        this.mType = type;
        this.mIsOn = isOn;
        this.mProtocolVersion = protocolVersion;
        this.mAddress = address;
        this.mPort = port;
        this.mUsername = username;
        this.mPassword = password;
    }
}
