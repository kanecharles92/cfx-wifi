package com.semcon.dometic.cfx.data;

import java.util.List;

public class Device {
    private final int mAbsLevel;
    private final List mCommunication;
    private final int mCompartments;
    private final double mDcVoltage;
    private final List mErrors;
    private final List mFeatures;
    private final int mIsAcOn;
    private final int mIsOn;
    private final String mModel;
    private final int mTempAlarmLevel;
    private final String mTemperatureUnit;

    public String getModel() {
        return this.mModel;
    }

    public int getCompartments() {
        return this.mCompartments;
    }

    public String getTemperatureUnit() {
        return this.mTemperatureUnit;
    }

    public int isOn() {
        return this.mIsOn;
    }

    public int isAcOn() {
        return this.mIsAcOn;
    }

    public int getAbsLevel() {
        return this.mAbsLevel;
    }

    public int getTempAlarmLevel() {
        return this.mTempAlarmLevel;
    }

    public double getDcVoltage() {
        return this.mDcVoltage;
    }

    public List getCommunication() {
        return this.mCommunication;
    }

    public List getErrors() {
        return this.mErrors;
    }

    public List getFeatures() {
        return this.mFeatures;
    }

    public Device(String model, int compartments, int isOn, int isAcOn, int absLevel, String temperatureUnit, int tempAlarmLevel, double dcVoltage, List communication, List errors, List features) {
        this.mModel = model;
        this.mCompartments = compartments;
        this.mIsOn = isOn;
        this.mIsAcOn = isAcOn;
        this.mAbsLevel = absLevel;
        this.mTemperatureUnit = temperatureUnit;
        this.mTempAlarmLevel = tempAlarmLevel;
        this.mDcVoltage = dcVoltage;
        this.mCommunication = communication;
        this.mErrors = errors;
        this.mFeatures = features;
    }
}
