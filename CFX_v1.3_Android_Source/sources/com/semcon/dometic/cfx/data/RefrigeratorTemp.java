package com.semcon.dometic.cfx.data;

import com.semcon.dometic.cfx.constants.Constants.BatteryProtectionMode;
import com.semcon.dometic.cfx.constants.Constants.TemperatureUnit;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import java.util.ArrayList;
import java.util.List;

public class RefrigeratorTemp {
    private final boolean mAcSupply;
    private final BatteryProtectionMode mBatteryProtectionMode;
    private final Compartment[] mCompartments;
    private final List<CFXError> mErrorCodes = new ArrayList();
    private final boolean mOn;
    private final TemperatureUnit mTemperatureUnit;

    public BatteryProtectionMode getBatteryProtectionMode() {
        return this.mBatteryProtectionMode;
    }

    public Compartment[] getCompartments() {
        return this.mCompartments;
    }

    public boolean isOn() {
        return this.mOn;
    }

    public boolean isAcSupply() {
        return this.mAcSupply;
    }

    public TemperatureUnit getTemperatureUnit() {
        return this.mTemperatureUnit;
    }

    public List<CFXError> getErrorCodes() {
        return this.mErrorCodes;
    }

    public RefrigeratorTemp(boolean on, boolean acSupply, TemperatureUnit temperatureUnit, BatteryProtectionMode batteryProtectionMode, Compartment[] compartments, List<CFXError> errorList) {
        this.mOn = on;
        this.mAcSupply = acSupply;
        this.mTemperatureUnit = temperatureUnit;
        this.mCompartments = compartments;
        this.mBatteryProtectionMode = batteryProtectionMode;
        this.mErrorCodes.addAll(errorList);
    }
}
