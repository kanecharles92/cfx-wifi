package com.semcon.dometic.cfx;

import android.os.Bundle;
import android.support.p003v7.widget.Toolbar;
import com.semcon.dometic.cfx.fragment.BoxSettingsFragment;
import com.semcon.dometic.roboguicehelper.RoboAppCompatActivity;

public class CfxBoxSettingsActivity extends RoboAppCompatActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0434R.layout.activity_cfx_box_settings);
        new Bundle().putString("Device_name", getIntent().getStringExtra("Device_name"));
        getSupportFragmentManager().beginTransaction().replace(C0434R.C0436id.fragment_container, new BoxSettingsFragment()).commit();
        setSupportActionBar((Toolbar) findViewById(C0434R.C0436id.app_toolbar));
    }
}
