package com.semcon.dometic.cfx;

import android.os.Bundle;
import android.widget.TextView;
import com.dometic.dometicui.DometicSeekBar;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.semcon.dometic.cfx.alarm.AlarmHandler;
import com.semcon.dometic.cfx.communication.CfxPoller;
import com.semcon.dometic.cfx.communication.ICfxCommunication;
import com.semcon.dometic.cfx.data.Refrigerator;
import com.semcon.dometic.cfx.fragment.BoxControlFragment.OnToggleConnectionListener;
import com.semcon.dometic.cfx.tcp.CfxComInterface.CFXError;
import java.util.List;
import roboguice.activity.RoboActivity;
import roboguice.event.EventManager;

public class ErrorDetailsFragment extends RoboActivity {
    @Inject
    private AlarmHandler mAlarmHandler;
    @Inject
    private ICfxCommunication mCfxCommunication;
    @Inject
    private CfxPoller mCfxPoller;
    private DometicSeekBar mCompartmentSeekBar1;
    private DometicSeekBar mCompartmentSeekBar2;
    private List<Refrigerator> mDevicesInTheSystem;
    @Named("GlobalEventManager")
    @Inject
    private EventManager mGlobalEventManager;
    private boolean mIsCoolerOn;
    private OnToggleConnectionListener mOnToggleConnectionListener;
    private Refrigerator mRefrigerator;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String[] errorList;
        super.onCreate(savedInstanceState);
        setContentView(C0434R.layout.fragment_error_details);
        TextView errors = (TextView) findViewById(C0434R.C0436id.error_list);
        for (String e : getIntent().getExtras().getString("errors").split("]")) {
            if (e.contains(CFXError.ABS_ERROR.toString())) {
                errors.append(getResources().getString(C0434R.string.error_text_abs));
            }
            if (e.contains(CFXError.COMPRESSOR_FAULTY.toString())) {
                errors.append(getResources().getString(C0434R.string.error_text_compressor_faulty));
            }
            if (e.contains(CFXError.THERMOSTAT_FAULTY.toString())) {
                errors.append(getResources().getString(C0434R.string.error_text_thermostat_faulty));
            }
            if (e.contains(CFXError.COMPRESSOR_WORKED_FOR_ONE_CYCLE.toString())) {
                errors.append(getResources().getString(C0434R.string.error_text_compressor_worked_for_one_cycle));
            }
            if (e.contains(CFXError.DOOR_OPEN_OVER_3_MIN.toString())) {
                errors.append(getResources().getString(C0434R.string.error_text_door_open_3_minutes));
            }
            if (e.contains(CFXError.TEMPERATURE_ALARM_ERROR.toString())) {
                errors.append(getResources().getString(C0434R.string.error_temperature_alarm));
            }
            if (e.contains(CFXError.SOLENOID_VALVE_FAULTY.toString())) {
                errors.append(getResources().getString(C0434R.string.error_solenoid_faulty));
            }
        }
    }

    public void onStart() {
        super.onStart();
    }

    public void onStop() {
        super.onStop();
    }
}
