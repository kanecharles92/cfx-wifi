package com.semcon.dometic.roboguicehelper;

import com.google.inject.AnnotationDatabase;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import roboguice.fragment.FragmentUtil;

public class AnnotationDatabaseImpl extends AnnotationDatabase {
    public void fillAnnotationClassesAndFieldsNames(HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToFieldSet) {
        String annotationClassName = "com.google.inject.Inject";
        Map<String, Set<String>> mapClassWithInjectionNameToFieldSet = (Map) mapAnnotationToMapClassWithInjectionNameToFieldSet.get(annotationClassName);
        if (mapClassWithInjectionNameToFieldSet == null) {
            mapClassWithInjectionNameToFieldSet = new HashMap<>();
            mapAnnotationToMapClassWithInjectionNameToFieldSet.put(annotationClassName, mapClassWithInjectionNameToFieldSet);
        }
        Set<String> fieldNameSet = new HashSet<>();
        fieldNameSet.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("com.semcon.dometic.roboguicehelper.RoboAppCompatActivity", fieldNameSet);
    }

    public void fillAnnotationClassesAndMethods(HashMap<String, Map<String, Set<String>>> hashMap) {
    }

    public void fillAnnotationClassesAndConstructors(HashMap<String, Map<String, Set<String>>> hashMap) {
    }

    public void fillClassesContainingInjectionPointSet(HashSet<String> classesContainingInjectionPointsSet) {
        classesContainingInjectionPointsSet.add("com.semcon.dometic.roboguicehelper.RoboAppCompatActivity");
    }

    public void fillBindableClasses(HashSet<String> injectedClasses) {
        injectedClasses.add("roboguice.inject.ContentViewListener");
        if (FragmentUtil.hasNative) {
            injectedClasses.add("android.app.FragmentManager");
        }
        if (FragmentUtil.hasSupport) {
            injectedClasses.add("android.support.v4.app.FragmentManager");
        }
    }
}
