package roboguice;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import com.google.inject.Guice;
import com.google.inject.HierarchyTraversalFilter;
import com.google.inject.HierarchyTraversalFilterFactory;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Stage;
import com.google.inject.internal.util.Stopwatch;
import com.google.inject.util.Modules;
import edu.umd.p004cs.findbugs.annotations.SuppressWarnings;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;
import roboguice.config.DefaultRoboModule;
import roboguice.config.RoboGuiceHierarchyTraversalFilter;
import roboguice.event.EventManager;
import roboguice.inject.ContextScope;
import roboguice.inject.ContextScopedRoboInjector;
import roboguice.inject.ResourceListener;
import roboguice.inject.RoboInjector;
import roboguice.inject.ViewListener;
import roboguice.util.Strings;

public final class RoboGuice {
    @SuppressWarnings({"MS_SHOULD_BE_FINAL"})
    public static Stage DEFAULT_STAGE = Stage.PRODUCTION;
    @SuppressWarnings({"MS_SHOULD_BE_FINAL"})
    protected static WeakHashMap<Application, Injector> injectors = new WeakHashMap<>();
    @SuppressWarnings({"MS_SHOULD_BE_FINAL"})
    protected static WeakHashMap<Application, ResourceListener> resourceListeners = new WeakHashMap<>();
    private static boolean useAnnotationDatabases;
    @SuppressWarnings({"MS_SHOULD_BE_FINAL"})
    protected static WeakHashMap<Application, ViewListener> viewListeners = new WeakHashMap<>();

    public static final class Util {
        private Util() {
        }

        public static void reset() {
            RoboGuice.injectors.clear();
            RoboGuice.resourceListeners.clear();
            RoboGuice.viewListeners.clear();
            Guice.setAnnotationDatabasePackageNames(null);
            Guice.setHierarchyTraversalFilterFactory(new HierarchyTraversalFilterFactory());
        }
    }

    static {
        useAnnotationDatabases = true;
        String useAnnotationsEnvVar = System.getenv("roboguice.useAnnotationDatabases");
        if (useAnnotationsEnvVar != null) {
            useAnnotationDatabases = Boolean.parseBoolean(useAnnotationsEnvVar);
        }
    }

    private RoboGuice() {
    }

    public static Injector getOrCreateBaseApplicationInjector(Application application) {
        Injector rtrn = (Injector) injectors.get(application);
        if (rtrn != null) {
            return rtrn;
        }
        synchronized (RoboGuice.class) {
            Injector rtrn2 = (Injector) injectors.get(application);
            if (rtrn2 != null) {
                return rtrn2;
            }
            Injector orCreateBaseApplicationInjector = getOrCreateBaseApplicationInjector(application, DEFAULT_STAGE);
            return orCreateBaseApplicationInjector;
        }
    }

    public static Injector getOrCreateBaseApplicationInjector(Application application, Stage stage, Module... modules) {
        Injector createGuiceInjector;
        Stopwatch stopwatch = new Stopwatch();
        synchronized (RoboGuice.class) {
            m183xc33de072(application);
            createGuiceInjector = createGuiceInjector(application, stage, stopwatch, modules);
        }
        return createGuiceInjector;
    }

    public static Injector overrideApplicationInjector(Application application, Module... overrideModules) {
        Injector rtrn;
        synchronized (RoboGuice.class) {
            List<Module> baseModules = extractModulesFromManifest(application);
            rtrn = Guice.createInjector(DEFAULT_STAGE, Modules.override((Iterable<? extends Module>) baseModules).with(overrideModules));
            injectors.put(application, rtrn);
        }
        return rtrn;
    }

    public static Injector getOrCreateBaseApplicationInjector(Application application, Stage stage) {
        Injector createGuiceInjector;
        Stopwatch stopwatch = new Stopwatch();
        synchronized (RoboGuice.class) {
            m183xc33de072(application);
            List<Module> modules = extractModulesFromManifest(application);
            createGuiceInjector = createGuiceInjector(application, stage, stopwatch, (Module[]) modules.toArray(new Module[modules.size()]));
        }
        return createGuiceInjector;
    }

    private static List<Module> extractModulesFromManifest(Application application) {
        String[] arr$;
        try {
            ArrayList<Module> modules = new ArrayList<>();
            Bundle bundle = application.getPackageManager().getApplicationInfo(application.getPackageName(), 128).metaData;
            String roboguiceModules = bundle != null ? bundle.getString("roboguice.modules") : null;
            DefaultRoboModule defaultRoboModule = newDefaultRoboModule(application);
            String[] moduleNames = roboguiceModules != null ? roboguiceModules.split("[\\s,]") : new String[0];
            modules.add(defaultRoboModule);
            for (String name : moduleNames) {
                if (Strings.notEmpty(name)) {
                    Class<? extends Module> clazz = Class.forName(name).asSubclass(Module.class);
                    try {
                        modules.add(clazz.getDeclaredConstructor(new Class[]{Application.class}).newInstance(new Object[]{application}));
                    } catch (NoSuchMethodException e) {
                        modules.add(clazz.newInstance());
                    }
                }
            }
            return modules;
        } catch (Exception e2) {
            throw new RuntimeException("Unable to instantiate your Module.  Check your roboguice.modules metadata in your AndroidManifest.xml", e2);
        }
    }

    private static Injector createGuiceInjector(Application application, Stage stage, Stopwatch stopwatch, Module... modules) {
        Injector rtrn;
        try {
            synchronized (RoboGuice.class) {
                rtrn = Guice.createInjector(stage, modules);
                injectors.put(application, rtrn);
            }
            return rtrn;
        } finally {
            stopwatch.resetAndLog("BaseApplicationInjector creation");
        }
    }

    public static RoboInjector getInjector(Context context) {
        return new ContextScopedRoboInjector(context, getOrCreateBaseApplicationInjector((Application) context.getApplicationContext()));
    }

    public static <T> T injectMembers(Context context, T t) {
        getInjector(context).injectMembers(t);
        return t;
    }

    public static DefaultRoboModule newDefaultRoboModule(Application application) {
        return new DefaultRoboModule(application, new ContextScope(application), getViewListener(application), getResourceListener(application));
    }

    public static void setUseAnnotationDatabases(boolean useAnnotationDatabases2) {
        useAnnotationDatabases = useAnnotationDatabases2;
    }

    @SuppressWarnings(justification = "Double check lock", value = {"NP_LOAD_OF_KNOWN_NULL_VALUE"})
    protected static ResourceListener getResourceListener(Application application) {
        ResourceListener resourceListener;
        ResourceListener resourceListener2 = (ResourceListener) resourceListeners.get(application);
        if (resourceListener2 == null) {
            synchronized (RoboGuice.class) {
                if (resourceListener2 == null) {
                    try {
                        resourceListener = new ResourceListener(application);
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                    try {
                        resourceListeners.put(application, resourceListener);
                        resourceListener2 = resourceListener;
                    } catch (Throwable th2) {
                        th = th2;
                        ResourceListener resourceListener3 = resourceListener;
                        throw th;
                    }
                }
            }
        }
        return resourceListener2;
    }

    @SuppressWarnings(justification = "Double check lock", value = {"NP_LOAD_OF_KNOWN_NULL_VALUE"})
    protected static ViewListener getViewListener(Application application) {
        ViewListener viewListener;
        ViewListener viewListener2 = (ViewListener) viewListeners.get(application);
        if (viewListener2 == null) {
            synchronized (RoboGuice.class) {
                if (viewListener2 == null) {
                    try {
                        viewListener = new ViewListener();
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                    try {
                        viewListeners.put(application, viewListener);
                        viewListener2 = viewListener;
                    } catch (Throwable th2) {
                        th = th2;
                        ViewListener viewListener3 = viewListener;
                        throw th;
                    }
                }
            }
        }
        return viewListener2;
    }

    public static void destroyInjector(Context context) {
        ((EventManager) getInjector(context).getInstance(EventManager.class)).destroy();
        injectors.remove(context);
    }

    /* renamed from: initializeAnnotationDatabaseFinderAndHierarchyTraversalFilterFactory */
    private static void m183xc33de072(Application application) {
        if (useAnnotationDatabases) {
            Log.d(RoboGuice.class.getName(), "Using annotation database(s).");
            long start = SystemClock.currentThreadTimeMillis();
            try {
                Set<String> packageNameList = new HashSet<>();
                try {
                    Bundle bundle = application.getPackageManager().getApplicationInfo(application.getPackageName(), 128).metaData;
                    String roboguicePackages = bundle != null ? bundle.getString("roboguice.annotations.packages") : null;
                    if (roboguicePackages != null) {
                        for (String packageName : roboguicePackages.split("[\\s,]")) {
                            packageNameList.add(packageName);
                        }
                    }
                } catch (NameNotFoundException e) {
                    Log.d(RoboGuice.class.getName(), "Failed to read manifest properly.");
                    e.printStackTrace();
                }
                if (packageNameList.isEmpty()) {
                    packageNameList.add("");
                }
                packageNameList.add("roboguice");
                Log.d(RoboGuice.class.getName(), "Using annotation database(s) : " + packageNameList.toString());
                String[] packageNames = new String[packageNameList.size()];
                packageNameList.toArray(packageNames);
                Guice.setAnnotationDatabasePackageNames(packageNames);
                Log.d(RoboGuice.class.getName(), "Time spent loading annotation databases : " + (SystemClock.currentThreadTimeMillis() - start));
            } catch (Exception ex) {
                IllegalStateException illegalStateException = new IllegalStateException("Unable to use annotation database(s)", ex);
                throw illegalStateException;
            }
        } else {
            Log.d(RoboGuice.class.getName(), "Using full reflection. Try using RoboGuice annotation processor for better performance.");
            Guice.setHierarchyTraversalFilterFactory(new HierarchyTraversalFilterFactory() {
                public HierarchyTraversalFilter createHierarchyTraversalFilter() {
                    return new RoboGuiceHierarchyTraversalFilter();
                }
            });
        }
    }
}
