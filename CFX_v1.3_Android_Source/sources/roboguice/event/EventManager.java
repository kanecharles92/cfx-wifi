package roboguice.event;

import android.content.Context;
import com.google.inject.Inject;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import roboguice.event.eventListener.ObserverMethodListener;
import roboguice.inject.ContextSingleton;

@ContextSingleton
public class EventManager {
    @Inject
    protected Context context;
    protected Map<Class<?>, Set<EventListener<?>>> registrations = new HashMap();

    public <T> void registerObserver(Class<T> event, EventListener<?> listener) {
        Set<EventListener<?>> observers = (Set) this.registrations.get(event);
        if (observers == null) {
            observers = new CopyOnWriteArraySet<>();
            this.registrations.put(event, observers);
        }
        observers.add(listener);
    }

    public <T> void registerObserver(Object instance, Method method, Class<T> event) {
        registerObserver(event, new ObserverMethodListener(instance, method));
    }

    public <T> void unregisterObserver(Class<T> event, EventListener<T> listener) {
        Set<EventListener<?>> observers = (Set) this.registrations.get(event);
        if (observers != null) {
            observers.remove(listener);
        }
    }

    public <T> void unregisterObserver(Object instance, Class<T> event) {
        Set<EventListener<?>> observers = (Set) this.registrations.get(event);
        if (observers != null) {
            ObserverMethodListener<?> toRemove = null;
            Iterator<EventListener<?>> iterator = observers.iterator();
            while (true) {
                if (!iterator.hasNext()) {
                    break;
                }
                EventListener<?> listener = (EventListener) iterator.next();
                if (listener instanceof ObserverMethodListener) {
                    ObserverMethodListener<?> observer = (ObserverMethodListener) listener;
                    if (observer.getInstance() == instance) {
                        toRemove = observer;
                        break;
                    }
                }
            }
            if (toRemove != null) {
                observers.remove(toRemove);
            }
        }
    }

    public void fire(Object event) {
        Set<EventListener<?>> observers = (Set) this.registrations.get(event.getClass());
        if (observers != null) {
            for (EventListener observer : observers) {
                observer.onEvent(event);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Set<EventListener<?>> copyObservers(Set<EventListener<?>> observers) {
        LinkedHashSet linkedHashSet;
        synchronized (observers) {
            linkedHashSet = new LinkedHashSet(observers);
        }
        return linkedHashSet;
    }

    public void destroy() {
        for (Entry<Class<?>, Set<EventListener<?>>> e : this.registrations.entrySet()) {
            ((Set) e.getValue()).clear();
        }
        this.registrations.clear();
    }
}
