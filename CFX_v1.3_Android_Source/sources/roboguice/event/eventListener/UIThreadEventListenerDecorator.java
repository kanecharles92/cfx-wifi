package roboguice.event.eventListener;

import android.os.Handler;
import roboguice.event.EventListener;

public class UIThreadEventListenerDecorator<T> implements EventListener<T> {
    protected EventListener<T> eventListener;
    protected Handler handler;

    public UIThreadEventListenerDecorator(EventListener<T> eventListener2, Handler handler2) {
        this.eventListener = eventListener2;
        this.handler = handler2;
    }

    public void onEvent(T event) {
        this.handler.post(new EventListenerRunnable(event, this.eventListener));
    }
}
