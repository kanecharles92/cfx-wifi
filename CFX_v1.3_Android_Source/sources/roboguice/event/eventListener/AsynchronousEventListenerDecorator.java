package roboguice.event.eventListener;

import android.os.Handler;
import roboguice.event.EventListener;

public class AsynchronousEventListenerDecorator<T> implements EventListener<T> {
    protected EventListener<T> eventListener;
    protected Handler handler;

    public AsynchronousEventListenerDecorator(EventListener<T> eventListener2) {
        this.eventListener = eventListener2;
    }

    public AsynchronousEventListenerDecorator(Handler handler2, EventListener<T> eventListener2) {
        this.handler = handler2;
        this.eventListener = eventListener2;
    }

    public void onEvent(T event) {
        new RunnableAsyncTaskAdaptor(this.handler, new EventListenerRunnable(event, this.eventListener)).execute();
    }
}
