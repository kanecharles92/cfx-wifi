package roboguice.event.eventListener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import roboguice.event.EventListener;
import roboguice.event.eventListener.javaassist.RuntimeSupport;
import roboguice.util.C0729Ln;

public class ObserverMethodListener<T> implements EventListener<T> {
    protected String descriptor;
    protected Object instance;
    protected Method method;

    public ObserverMethodListener(Object instance2, Method method2) {
        this.instance = instance2;
        this.method = method2;
        this.descriptor = method2.getName() + ':' + RuntimeSupport.makeDescriptor(method2);
        method2.setAccessible(true);
    }

    public void onEvent(Object event) {
        try {
            this.method.invoke(this.instance, new Object[]{event});
        } catch (InvocationTargetException e) {
            C0729Ln.m188e(e);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException(e2);
        }
    }

    public Object getInstance() {
        return this.instance;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ObserverMethodListener<?> that = (ObserverMethodListener) o;
        if (this.descriptor == null ? that.descriptor != null : !this.descriptor.equals(that.descriptor)) {
            return false;
        }
        if (this.instance != null) {
            if (this.instance.equals(that.instance)) {
                return true;
            }
        } else if (that.instance == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.descriptor != null) {
            result = this.descriptor.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.instance != null) {
            i = this.instance.hashCode();
        }
        return i2 + i;
    }
}
