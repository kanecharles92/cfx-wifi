package roboguice.event.eventListener;

import roboguice.event.EventListener;

public class EventListenerRunnable<T> implements Runnable {
    protected T event;
    protected EventListener<T> eventListener;

    public EventListenerRunnable(T event2, EventListener<T> eventListener2) {
        this.event = event2;
        this.eventListener = eventListener2;
    }

    public void run() {
        this.eventListener.onEvent(this.event);
    }
}
