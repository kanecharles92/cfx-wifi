package roboguice.event.eventListener.javaassist;

import java.lang.reflect.Method;

public final class RuntimeSupport {
    private RuntimeSupport() {
    }

    public static String makeDescriptor(Method m) {
        return makeDescriptor(m.getParameterTypes(), m.getReturnType());
    }

    public static String makeDescriptor(Class<?>[] params, Class<?> retType) {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append('(');
        for (Class<?> makeDesc : params) {
            makeDesc(sbuf, makeDesc);
        }
        sbuf.append(')');
        if (retType != null) {
            makeDesc(sbuf, retType);
        }
        return sbuf.toString();
    }

    private static void makeDesc(StringBuffer sbuf, Class<?> type) {
        if (type.isArray()) {
            sbuf.append('[');
            makeDesc(sbuf, type.getComponentType());
        } else if (!type.isPrimitive()) {
            sbuf.append('L').append(type.getName().replace('.', '/')).append(';');
        } else if (type == Void.TYPE) {
            sbuf.append('V');
        } else if (type == Integer.TYPE) {
            sbuf.append('I');
        } else if (type == Byte.TYPE) {
            sbuf.append('B');
        } else if (type == Long.TYPE) {
            sbuf.append('J');
        } else if (type == Double.TYPE) {
            sbuf.append('D');
        } else if (type == Float.TYPE) {
            sbuf.append('F');
        } else if (type == Character.TYPE) {
            sbuf.append('C');
        } else if (type == Short.TYPE) {
            sbuf.append('S');
        } else if (type == Boolean.TYPE) {
            sbuf.append('Z');
        } else {
            throw new RuntimeException("bad type: " + type.getName());
        }
    }
}
