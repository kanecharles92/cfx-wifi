package roboguice.event.eventListener;

import android.os.Handler;
import roboguice.util.SafeAsyncTask;

public class RunnableAsyncTaskAdaptor extends SafeAsyncTask<Void> {
    protected Runnable runnable;

    public RunnableAsyncTaskAdaptor(Runnable runnable2) {
        this.runnable = runnable2;
    }

    public RunnableAsyncTaskAdaptor(Handler handler, Runnable runnable2) {
        super(handler);
        this.runnable = runnable2;
    }

    public Void call() throws Exception {
        this.runnable.run();
        return null;
    }
}
