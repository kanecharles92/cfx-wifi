package roboguice.event;

import com.google.inject.Guice;
import com.google.inject.HierarchyTraversalFilter;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import roboguice.event.eventListener.ObserverMethodListener;
import roboguice.event.eventListener.factory.EventListenerThreadingDecorator;

public class ObservesTypeListener implements TypeListener {
    protected Provider<EventManager> eventManagerProvider;
    private HierarchyTraversalFilter filter;
    protected EventListenerThreadingDecorator observerThreadingDecorator;

    public static class ContextObserverMethodInjector<I, T> implements InjectionListener<I> {
        protected Class<T> event;
        protected Provider<EventManager> eventManagerProvider;
        protected Method method;
        protected EventListenerThreadingDecorator observerThreadingDecorator;
        protected EventThread threadType;

        public ContextObserverMethodInjector(Provider<EventManager> eventManagerProvider2, EventListenerThreadingDecorator observerThreadingDecorator2, Method method2, Class<T> event2, EventThread threadType2) {
            this.observerThreadingDecorator = observerThreadingDecorator2;
            this.eventManagerProvider = eventManagerProvider2;
            this.method = method2;
            this.event = event2;
            this.threadType = threadType2;
        }

        public void afterInjection(I i) {
            ((EventManager) this.eventManagerProvider.get()).registerObserver(this.event, this.observerThreadingDecorator.decorate(this.threadType, new ObserverMethodListener(i, this.method)));
        }
    }

    public ObservesTypeListener(Provider<EventManager> eventManagerProvider2, EventListenerThreadingDecorator observerThreadingDecorator2) {
        this.eventManagerProvider = eventManagerProvider2;
        this.observerThreadingDecorator = observerThreadingDecorator2;
    }

    public <I> void hear(TypeLiteral<I> iTypeLiteral, TypeEncounter<I> iTypeEncounter) {
        if (this.filter == null) {
            this.filter = Guice.createHierarchyTraversalFilter();
        } else {
            this.filter.reset();
        }
        for (Class<?> c = iTypeLiteral.getRawType(); isWorthScanning(c); c = c.getSuperclass()) {
            for (Method method : this.filter.getAllMethods(Observes.class.getName(), c)) {
                findContextObserver(method, iTypeEncounter);
            }
            for (Class<?> interfaceClass : c.getInterfaces()) {
                for (Method method2 : this.filter.getAllMethods(Observes.class.getName(), interfaceClass)) {
                    findContextObserver(method2, iTypeEncounter);
                }
            }
        }
    }

    private boolean isWorthScanning(Class<?> c) {
        return this.filter.isWorthScanningForMethods(Observes.class.getName(), c);
    }

    /* access modifiers changed from: protected */
    public <I> void findContextObserver(Method method, TypeEncounter<I> iTypeEncounter) {
        Annotation[] arr$;
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (int i = 0; i < parameterAnnotations.length; i++) {
            Annotation[] annotationArray = parameterAnnotations[i];
            Class<?> parameterType = method.getParameterTypes()[i];
            for (Annotation annotation : annotationArray) {
                if (annotation.annotationType().equals(Observes.class)) {
                    registerContextObserver(iTypeEncounter, method, parameterType, ((Observes) annotation).value());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public <I, T> void registerContextObserver(TypeEncounter<I> iTypeEncounter, Method method, Class<T> parameterType, EventThread threadType) {
        checkMethodParameters(method);
        iTypeEncounter.register((InjectionListener<? super I>) new ContextObserverMethodInjector<Object>(this.eventManagerProvider, this.observerThreadingDecorator, method, parameterType, threadType));
    }

    /* access modifiers changed from: protected */
    public void checkMethodParameters(Method method) {
        if (method.getParameterTypes().length > 1) {
            throw new RuntimeException("Annotation @Observes must only annotate one parameter, which must be the only parameter in the listener method.");
        }
    }
}
