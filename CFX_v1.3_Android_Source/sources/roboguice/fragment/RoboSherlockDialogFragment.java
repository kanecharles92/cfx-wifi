package roboguice.fragment;

import android.os.Bundle;
import android.view.View;
import com.actionbarsherlock.app.SherlockDialogFragment;
import roboguice.RoboGuice;

public abstract class RoboSherlockDialogFragment extends SherlockDialogFragment {
    public void onCreate(Bundle savedInstanceState) {
        RoboSherlockDialogFragment.super.onCreate(savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        RoboSherlockDialogFragment.super.onViewCreated(view, savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectViewMembers(this);
    }
}
