package roboguice.fragment;

import android.os.Bundle;
import android.view.View;
import com.actionbarsherlock.app.SherlockFragment;
import roboguice.RoboGuice;

public abstract class RoboSherlockFragment extends SherlockFragment {
    public void onCreate(Bundle savedInstanceState) {
        RoboSherlockFragment.super.onCreate(savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        RoboSherlockFragment.super.onViewCreated(view, savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectViewMembers(this);
    }
}
