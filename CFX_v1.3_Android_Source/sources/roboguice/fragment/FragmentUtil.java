package roboguice.fragment;

import android.app.Activity;
import android.view.View;
import com.google.inject.Provider;
import edu.umd.p004cs.findbugs.annotations.SuppressWarnings;

public final class FragmentUtil {
    public static final String NATIVE_PACKAGE = "android.app.";
    public static final String SUPPORT_PACKAGE = "android.support.v4.app.";
    @SuppressWarnings({"MS_SHOULD_BE_REFACTORED_TO_BE_FINAL"})
    public static boolean hasNative;
    @SuppressWarnings({"MS_SHOULD_BE_REFACTORED_TO_BE_FINAL"})
    public static boolean hasSupport;
    @SuppressWarnings({"MS_SHOULD_BE_REFACTORED_TO_BE_FINAL"})
    public static C0724f nativeFrag;
    @SuppressWarnings({"MS_SHOULD_BE_REFACTORED_TO_BE_FINAL"})
    public static Class<? extends Activity> supportActivity;
    @SuppressWarnings({"MS_SHOULD_BE_REFACTORED_TO_BE_FINAL"})
    public static C0724f supportFrag;

    /* renamed from: roboguice.fragment.FragmentUtil$f */
    public interface C0724f<fragType, fragManagerType> {
        fragType findFragmentById(fragManagerType fragmanagertype, int i);

        fragType findFragmentByTag(fragManagerType fragmanagertype, String str);

        Class<Provider<fragManagerType>> fragmentManagerProviderType();

        Class<fragManagerType> fragmentManagerType();

        Class<fragType> fragmentType();

        View getView(fragType fragtype);
    }

    static {
        boolean z;
        nativeFrag = null;
        supportFrag = null;
        supportActivity = null;
        hasNative = false;
        hasSupport = false;
        try {
            nativeFrag = (C0724f) Class.forName("roboguice.fragment.provided.NativeFragmentUtil").newInstance();
            hasNative = nativeFrag != null;
        } catch (Throwable th) {
        }
        try {
            supportFrag = (C0724f) Class.forName("roboguice.fragment.support.SupportFragmentUtil").newInstance();
            supportActivity = Class.forName("android.support.v4.app.FragmentActivity");
            if (supportFrag == null || supportActivity == null) {
                z = false;
            } else {
                z = true;
            }
            hasSupport = z;
        } catch (Throwable th2) {
        }
    }

    private FragmentUtil() {
    }
}
