package roboguice.fragment;

import android.os.Bundle;
import android.view.View;
import com.actionbarsherlock.app.SherlockListFragment;
import roboguice.RoboGuice;

public abstract class RoboSherlockListFragment extends SherlockListFragment {
    public void onCreate(Bundle savedInstanceState) {
        RoboSherlockListFragment.super.onCreate(savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        RoboSherlockListFragment.super.onViewCreated(view, savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectViewMembers(this);
    }
}
