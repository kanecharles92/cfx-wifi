package roboguice.fragment;

import android.os.Bundle;
import android.support.p000v4.app.DialogFragment;
import android.view.View;
import roboguice.RoboGuice;

public abstract class RoboDialogFragment extends DialogFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectViewMembers(this);
    }
}
