package roboguice.fragment.support;

import android.app.Activity;
import android.support.p000v4.app.Fragment;
import android.support.p000v4.app.FragmentActivity;
import android.support.p000v4.app.FragmentManager;
import android.view.View;
import com.google.inject.Inject;
import com.google.inject.Provider;
import roboguice.fragment.FragmentUtil.C0724f;
import roboguice.inject.ContextSingleton;

public class SupportFragmentUtil implements C0724f<Fragment, FragmentManager> {

    @ContextSingleton
    public static class FragmentManagerProvider implements Provider<FragmentManager> {
        @Inject
        protected Activity activity;

        public FragmentManager get() {
            return ((FragmentActivity) this.activity).getSupportFragmentManager();
        }
    }

    public SupportFragmentUtil() throws ClassNotFoundException {
        Class.forName(Fragment.class.getName());
        Class.forName(FragmentManager.class.getName());
    }

    public View getView(Fragment frag) {
        return frag.getView();
    }

    public Fragment findFragmentById(FragmentManager fm, int id) {
        return fm.findFragmentById(id);
    }

    public Fragment findFragmentByTag(FragmentManager fm, String tag) {
        return fm.findFragmentByTag(tag);
    }

    public Class<Fragment> fragmentType() {
        return Fragment.class;
    }

    public Class<FragmentManager> fragmentManagerType() {
        return FragmentManager.class;
    }

    public Class fragmentManagerProviderType() {
        return FragmentManagerProvider.class;
    }
}
