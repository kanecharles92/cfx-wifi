package roboguice.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import com.google.android.maps.MapActivity;
import com.google.inject.Inject;
import com.google.inject.Key;
import java.util.HashMap;
import java.util.Map;
import roboguice.RoboGuice;
import roboguice.activity.event.OnActivityResultEvent;
import roboguice.activity.event.OnContentChangedEvent;
import roboguice.activity.event.OnNewIntentEvent;
import roboguice.activity.event.OnPauseEvent;
import roboguice.activity.event.OnRestartEvent;
import roboguice.activity.event.OnResumeEvent;
import roboguice.activity.event.OnSaveInstanceStateEvent;
import roboguice.activity.event.OnStopEvent;
import roboguice.context.event.OnConfigurationChangedEvent;
import roboguice.context.event.OnCreateEvent;
import roboguice.context.event.OnDestroyEvent;
import roboguice.context.event.OnStartEvent;
import roboguice.event.EventManager;
import roboguice.inject.ContentViewListener;
import roboguice.inject.RoboInjector;
import roboguice.util.RoboContext;

public abstract class RoboMapActivity extends MapActivity implements RoboContext {
    protected EventManager eventManager;
    @Inject
    ContentViewListener ignored;
    protected HashMap<Key<?>, Object> scopedObjects = new HashMap<>();

    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, roboguice.activity.RoboMapActivity, java.lang.Object, com.google.android.maps.MapActivity] */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        RoboInjector injector = RoboGuice.getInjector(this);
        this.eventManager = (EventManager) injector.getInstance(EventManager.class);
        injector.injectMembersWithoutViews(this);
        RoboMapActivity.super.onCreate(savedInstanceState);
        this.eventManager.fire(new OnCreateEvent(this, savedInstanceState));
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity, android.app.Activity] */
    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        RoboMapActivity.super.onSaveInstanceState(outState);
        this.eventManager.fire(new OnSaveInstanceStateEvent(this, outState));
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity, android.app.Activity] */
    /* access modifiers changed from: protected */
    public void onRestart() {
        RoboMapActivity.super.onRestart();
        this.eventManager.fire(new OnRestartEvent(this));
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity] */
    /* access modifiers changed from: protected */
    public void onStart() {
        RoboMapActivity.super.onStart();
        this.eventManager.fire(new OnStartEvent(this));
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity, android.app.Activity] */
    /* access modifiers changed from: protected */
    public void onResume() {
        RoboMapActivity.super.onResume();
        this.eventManager.fire(new OnResumeEvent(this));
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity, android.app.Activity] */
    /* access modifiers changed from: protected */
    public void onPause() {
        RoboMapActivity.super.onPause();
        this.eventManager.fire(new OnPauseEvent(this));
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity, android.app.Activity] */
    public void onNewIntent(Intent intent) {
        RoboMapActivity.super.onNewIntent(intent);
        this.eventManager.fire(new OnNewIntentEvent(this));
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity, android.app.Activity] */
    /* access modifiers changed from: protected */
    public void onStop() {
        try {
            this.eventManager.fire(new OnStopEvent(this));
        } finally {
            RoboMapActivity.super.onStop();
        }
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity] */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            this.eventManager.fire(new OnDestroyEvent(this));
            try {
                RoboGuice.destroyInjector(this);
            } finally {
                RoboMapActivity.super.onDestroy();
            }
        } catch (Throwable th) {
            RoboGuice.destroyInjector(this);
            throw th;
        } finally {
            RoboMapActivity.super.onDestroy();
        }
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity] */
    public void onConfigurationChanged(Configuration newConfig) {
        Configuration currentConfig = getResources().getConfiguration();
        RoboMapActivity.super.onConfigurationChanged(newConfig);
        this.eventManager.fire(new OnConfigurationChangedEvent(this, currentConfig, newConfig));
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, roboguice.activity.RoboMapActivity, java.lang.Object, com.google.android.maps.MapActivity, android.app.Activity] */
    public void onContentChanged() {
        RoboMapActivity.super.onContentChanged();
        RoboGuice.getInjector(this).injectViewMembers(this);
        this.eventManager.fire(new OnContentChangedEvent(this));
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [roboguice.activity.RoboMapActivity, com.google.android.maps.MapActivity, android.app.Activity] */
    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        RoboMapActivity.super.onActivityResult(requestCode, resultCode, data);
        this.eventManager.fire(new OnActivityResultEvent(this, requestCode, resultCode, data));
    }

    public Map<Key<?>, Object> getScopedObjectMap() {
        return this.scopedObjects;
    }

    public View onCreateView(String name, Context context, AttributeSet attrs) {
        if (RoboActivity.shouldInjectOnCreateView(name)) {
            return RoboActivity.injectOnCreateView(name, context, attrs);
        }
        return RoboMapActivity.super.onCreateView(name, context, attrs);
    }

    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        if (RoboActivity.shouldInjectOnCreateView(name)) {
            return RoboActivity.injectOnCreateView(name, context, attrs);
        }
        return RoboMapActivity.super.onCreateView(parent, name, context, attrs);
    }
}
