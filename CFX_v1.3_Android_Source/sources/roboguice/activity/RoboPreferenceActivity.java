package roboguice.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.util.AttributeSet;
import android.view.View;
import com.google.inject.Inject;
import com.google.inject.Key;
import java.util.HashMap;
import java.util.Map;
import roboguice.RoboGuice;
import roboguice.activity.event.OnActivityResultEvent;
import roboguice.activity.event.OnContentChangedEvent;
import roboguice.activity.event.OnNewIntentEvent;
import roboguice.activity.event.OnPauseEvent;
import roboguice.activity.event.OnRestartEvent;
import roboguice.activity.event.OnResumeEvent;
import roboguice.activity.event.OnSaveInstanceStateEvent;
import roboguice.activity.event.OnStopEvent;
import roboguice.context.event.OnConfigurationChangedEvent;
import roboguice.context.event.OnCreateEvent;
import roboguice.context.event.OnDestroyEvent;
import roboguice.context.event.OnStartEvent;
import roboguice.event.EventManager;
import roboguice.inject.ContentViewListener;
import roboguice.inject.ContextScope;
import roboguice.inject.PreferenceListener;
import roboguice.inject.RoboInjector;
import roboguice.util.RoboContext;

public abstract class RoboPreferenceActivity extends PreferenceActivity implements RoboContext {
    protected EventManager eventManager;
    @Inject
    ContentViewListener ignored;
    protected PreferenceListener preferenceListener;
    protected HashMap<Key<?>, Object> scopedObjects = new HashMap<>();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        RoboInjector injector = RoboGuice.getInjector(this);
        this.eventManager = (EventManager) injector.getInstance(EventManager.class);
        this.preferenceListener = (PreferenceListener) injector.getInstance(PreferenceListener.class);
        injector.injectMembersWithoutViews(this);
        super.onCreate(savedInstanceState);
        this.eventManager.fire(new OnCreateEvent(this, savedInstanceState));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.eventManager.fire(new OnSaveInstanceStateEvent(this, outState));
    }

    @Deprecated
    public void setPreferenceScreen(PreferenceScreen preferenceScreen) {
        super.setPreferenceScreen(preferenceScreen);
        ContextScope scope = (ContextScope) RoboGuice.getInjector(this).getInstance(ContextScope.class);
        synchronized (ContextScope.class) {
            scope.enter(this);
            try {
                this.preferenceListener.injectPreferenceViews();
                scope.exit(this);
            } catch (Throwable th) {
                scope.exit(this);
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.eventManager.fire(new OnRestartEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.eventManager.fire(new OnStartEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.eventManager.fire(new OnResumeEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.eventManager.fire(new OnPauseEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.eventManager.fire(new OnNewIntentEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        try {
            this.eventManager.fire(new OnStopEvent(this));
        } finally {
            super.onStop();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            this.eventManager.fire(new OnDestroyEvent(this));
            try {
                RoboGuice.destroyInjector(this);
            } finally {
                super.onDestroy();
            }
        } catch (Throwable th) {
            RoboGuice.destroyInjector(this);
            throw th;
        } finally {
            super.onDestroy();
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        Configuration currentConfig = getResources().getConfiguration();
        super.onConfigurationChanged(newConfig);
        this.eventManager.fire(new OnConfigurationChangedEvent(this, currentConfig, newConfig));
    }

    public void onContentChanged() {
        super.onContentChanged();
        RoboGuice.getInjector(this).injectViewMembers(this);
        this.eventManager.fire(new OnContentChangedEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.eventManager.fire(new OnActivityResultEvent(this, requestCode, resultCode, data));
    }

    public Map<Key<?>, Object> getScopedObjectMap() {
        return this.scopedObjects;
    }

    public View onCreateView(String name, Context context, AttributeSet attrs) {
        if (RoboActivity.shouldInjectOnCreateView(name)) {
            return RoboActivity.injectOnCreateView(name, context, attrs);
        }
        return super.onCreateView(name, context, attrs);
    }

    @TargetApi(11)
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        if (RoboActivity.shouldInjectOnCreateView(name)) {
            return RoboActivity.injectOnCreateView(name, context, attrs);
        }
        return super.onCreateView(parent, name, context, attrs);
    }
}
