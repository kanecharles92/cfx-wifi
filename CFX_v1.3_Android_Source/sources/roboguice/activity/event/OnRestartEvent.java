package roboguice.activity.event;

import android.app.Activity;

public class OnRestartEvent {
    protected Activity activity;

    public OnRestartEvent(Activity activity2) {
        this.activity = activity2;
    }

    public Activity getActivity() {
        return this.activity;
    }
}
