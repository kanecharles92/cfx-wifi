package roboguice.activity.event;

import android.app.Activity;
import android.content.Intent;

public class OnActivityResultEvent {
    protected Activity activity;
    protected Intent data;
    protected int requestCode;
    protected int resultCode;

    public OnActivityResultEvent(Activity activity2, int requestCode2, int resultCode2, Intent data2) {
        this.activity = activity2;
        this.requestCode = requestCode2;
        this.resultCode = resultCode2;
        this.data = data2;
    }

    public int getRequestCode() {
        return this.requestCode;
    }

    public int getResultCode() {
        return this.resultCode;
    }

    public Intent getData() {
        return this.data;
    }

    public Activity getActivity() {
        return this.activity;
    }
}
