package roboguice.activity.event;

import android.app.Activity;

public class OnPauseEvent {
    protected Activity activity;

    public OnPauseEvent(Activity activity2) {
        this.activity = activity2;
    }

    public Activity getActivity() {
        return this.activity;
    }
}
