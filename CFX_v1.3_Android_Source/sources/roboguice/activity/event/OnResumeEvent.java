package roboguice.activity.event;

import android.app.Activity;

public class OnResumeEvent {
    protected Activity activity;

    public OnResumeEvent(Activity activity2) {
        this.activity = activity2;
    }

    public Activity getActivity() {
        return this.activity;
    }
}
