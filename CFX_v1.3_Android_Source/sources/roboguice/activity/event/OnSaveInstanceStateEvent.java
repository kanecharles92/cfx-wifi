package roboguice.activity.event;

import android.app.Activity;
import android.os.Bundle;

public class OnSaveInstanceStateEvent {
    protected Activity activity;
    protected Bundle savedInstanceState;

    public OnSaveInstanceStateEvent(Activity activity2, Bundle savedInstanceState2) {
        this.activity = activity2;
        this.savedInstanceState = savedInstanceState2;
    }

    public Bundle getSavedInstanceState() {
        return this.savedInstanceState;
    }

    public Activity getActivity() {
        return this.activity;
    }
}
