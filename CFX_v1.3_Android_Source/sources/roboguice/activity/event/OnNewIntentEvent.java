package roboguice.activity.event;

import android.app.Activity;

public class OnNewIntentEvent {
    protected Activity activity;

    public OnNewIntentEvent(Activity activity2) {
        this.activity = activity2;
    }

    public Activity getActivity() {
        return this.activity;
    }
}
