package roboguice.activity.event;

import android.app.Activity;

public class OnContentChangedEvent {
    protected Activity activity;

    public OnContentChangedEvent(Activity activity2) {
        this.activity = activity2;
    }

    public Activity getActivity() {
        return this.activity;
    }
}
