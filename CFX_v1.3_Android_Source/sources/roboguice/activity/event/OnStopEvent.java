package roboguice.activity.event;

import android.app.Activity;

public class OnStopEvent {
    protected Activity activity;

    public OnStopEvent(Activity activity2) {
        this.activity = activity2;
    }

    public Activity getActivity() {
        return this.activity;
    }
}
