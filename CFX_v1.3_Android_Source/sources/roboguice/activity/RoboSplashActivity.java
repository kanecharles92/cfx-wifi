package roboguice.activity;

import android.app.Activity;
import android.app.Application;
import roboguice.RoboGuice;

public abstract class RoboSplashActivity extends Activity {
    private static final double DEFAULT_SPLASH_DELAY_MS = 2500.0d;
    protected int minDisplayMs = 2500;

    /* access modifiers changed from: protected */
    public abstract void startNextActivity();

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        final long start = System.currentTimeMillis();
        new Thread(new Runnable() {
            public void run() {
                Application app = RoboSplashActivity.this.getApplication();
                RoboGuice.getOrCreateBaseApplicationInjector(RoboSplashActivity.this.getApplication());
                RoboSplashActivity.this.doStuffInBackground(app);
                long duration = System.currentTimeMillis() - start;
                if (duration < ((long) RoboSplashActivity.this.minDisplayMs)) {
                    try {
                        Thread.sleep(((long) RoboSplashActivity.this.minDisplayMs) - duration);
                    } catch (InterruptedException e) {
                        Thread.interrupted();
                    }
                }
                RoboSplashActivity.this.startNextActivity();
                RoboSplashActivity.this.andFinishThisOne();
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void doStuffInBackground(Application app) {
    }

    /* access modifiers changed from: protected */
    public void andFinishThisOne() {
        finish();
    }
}
