package roboguice.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import com.google.inject.Inject;
import com.google.inject.Key;
import com.google.inject.internal.util.Stopwatch;
import java.util.HashMap;
import java.util.Map;
import roboguice.RoboGuice;
import roboguice.activity.event.OnActivityResultEvent;
import roboguice.activity.event.OnContentChangedEvent;
import roboguice.activity.event.OnNewIntentEvent;
import roboguice.activity.event.OnPauseEvent;
import roboguice.activity.event.OnRestartEvent;
import roboguice.activity.event.OnResumeEvent;
import roboguice.activity.event.OnSaveInstanceStateEvent;
import roboguice.activity.event.OnStopEvent;
import roboguice.context.event.OnConfigurationChangedEvent;
import roboguice.context.event.OnCreateEvent;
import roboguice.context.event.OnDestroyEvent;
import roboguice.context.event.OnStartEvent;
import roboguice.event.EventManager;
import roboguice.inject.ContentViewListener;
import roboguice.inject.RoboInjector;
import roboguice.util.RoboContext;

public class RoboActivity extends Activity implements RoboContext {
    protected EventManager eventManager;
    @Inject
    ContentViewListener ignored;
    protected HashMap<Key<?>, Object> scopedObjects = new HashMap<>();
    private Stopwatch stopwatch;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        this.stopwatch = new Stopwatch();
        RoboInjector injector = RoboGuice.getInjector(this);
        this.stopwatch.resetAndLog("RoboActivity creation of injector");
        this.eventManager = (EventManager) injector.getInstance(EventManager.class);
        this.stopwatch.resetAndLog("RoboActivity creation of eventmanager");
        injector.injectMembersWithoutViews(this);
        this.stopwatch.resetAndLog("RoboActivity inject members without views");
        super.onCreate(savedInstanceState);
        this.stopwatch.resetAndLog("RoboActivity super onCreate");
        this.eventManager.fire(new OnCreateEvent(this, savedInstanceState));
        this.stopwatch.resetAndLog("RoboActivity fire event");
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.eventManager.fire(new OnSaveInstanceStateEvent(this, outState));
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.eventManager.fire(new OnRestartEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.eventManager.fire(new OnStartEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.eventManager.fire(new OnResumeEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.eventManager.fire(new OnPauseEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.eventManager.fire(new OnNewIntentEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        try {
            this.eventManager.fire(new OnStopEvent(this));
        } finally {
            super.onStop();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            this.eventManager.fire(new OnDestroyEvent(this));
            try {
                RoboGuice.destroyInjector(this);
            } finally {
                super.onDestroy();
            }
        } catch (Throwable th) {
            RoboGuice.destroyInjector(this);
            throw th;
        } finally {
            super.onDestroy();
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        Configuration currentConfig = getResources().getConfiguration();
        super.onConfigurationChanged(newConfig);
        this.eventManager.fire(new OnConfigurationChangedEvent(this, currentConfig, newConfig));
    }

    public void onContentChanged() {
        super.onContentChanged();
        RoboGuice.getInjector(this).injectViewMembers(this);
        this.eventManager.fire(new OnContentChangedEvent(this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.eventManager.fire(new OnActivityResultEvent(this, requestCode, resultCode, data));
    }

    public Map<Key<?>, Object> getScopedObjectMap() {
        return this.scopedObjects;
    }

    public View onCreateView(String name, Context context, AttributeSet attrs) {
        if (shouldInjectOnCreateView(name)) {
            return injectOnCreateView(name, context, attrs);
        }
        return super.onCreateView(name, context, attrs);
    }

    @TargetApi(11)
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        if (shouldInjectOnCreateView(name)) {
            return injectOnCreateView(name, context, attrs);
        }
        return super.onCreateView(parent, name, context, attrs);
    }

    protected static boolean shouldInjectOnCreateView(String name) {
        return false;
    }

    protected static View injectOnCreateView(String name, Context context, AttributeSet attrs) {
        try {
            View view = (View) Class.forName(name).getConstructor(new Class[]{Context.class, AttributeSet.class}).newInstance(new Object[]{context, attrs});
            RoboGuice.getInjector(context).injectMembers(view);
            RoboGuice.getInjector(context).injectViewMembers(view);
            return view;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
