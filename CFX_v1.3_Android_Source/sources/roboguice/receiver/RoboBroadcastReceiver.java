package roboguice.receiver;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import roboguice.RoboGuice;

public abstract class RoboBroadcastReceiver extends BroadcastReceiver {
    public final void onReceive(Context context, Intent intent) {
        RoboGuice.getOrCreateBaseApplicationInjector((Application) context.getApplicationContext()).injectMembers(this);
        handleReceive(context, intent);
    }

    /* access modifiers changed from: protected */
    public void handleReceive(Context context, Intent intent) {
    }
}
