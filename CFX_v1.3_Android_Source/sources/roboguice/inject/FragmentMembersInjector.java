package roboguice.inject;

import android.app.Activity;
import android.content.Context;
import com.google.inject.MembersInjector;
import com.google.inject.Provider;
import com.google.inject.spi.TypeEncounter;
import java.lang.annotation.Annotation;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.WeakHashMap;
import roboguice.fragment.FragmentUtil.C0724f;

public class FragmentMembersInjector<T> implements MembersInjector<T> {
    protected static final WeakHashMap<Object, ArrayList<FragmentMembersInjector<?>>> VIEW_MEMBERS_INJECTORS = new WeakHashMap<>();
    protected Provider<Activity> activityProvider;
    protected Annotation annotation;
    protected Field field;
    protected Provider fragManager;
    protected C0724f fragUtils;
    protected WeakReference<T> instanceRef;

    public FragmentMembersInjector(Field field2, Annotation annotation2, TypeEncounter<T> typeEncounter, C0724f<?, ?> utils) {
        this.field = field2;
        this.annotation = annotation2;
        this.activityProvider = typeEncounter.getProvider(Activity.class);
        if (utils != null) {
            this.fragUtils = utils;
            this.fragManager = typeEncounter.getProvider(utils.fragmentManagerType());
        }
    }

    public void injectMembers(T instance) {
        synchronized (FragmentMembersInjector.class) {
            Object key = this.fragUtils != null && this.fragUtils.fragmentType().isInstance(instance) ? instance : this.activityProvider.get();
            if (key != null) {
                ArrayList<FragmentMembersInjector<?>> injectors = (ArrayList) VIEW_MEMBERS_INJECTORS.get(key);
                if (injectors == null) {
                    injectors = new ArrayList<>();
                    VIEW_MEMBERS_INJECTORS.put(key, injectors);
                }
                injectors.add(this);
                this.instanceRef = new WeakReference<>(instance);
            }
        }
    }

    public void reallyInjectMembers(Object activityOrFragment) {
        Object fragment;
        T instance = this.instanceRef.get();
        if (instance != null) {
            if (!(activityOrFragment instanceof Context) || (activityOrFragment instanceof Activity)) {
                Object obj = null;
                try {
                    InjectFragment injectFragment = (InjectFragment) this.annotation;
                    int id = injectFragment.value();
                    if (id >= 0) {
                        fragment = this.fragUtils.findFragmentById(this.fragManager.get(), id);
                    } else {
                        fragment = this.fragUtils.findFragmentByTag(this.fragManager.get(), injectFragment.tag());
                    }
                    if (fragment != null || !Nullable.notNullable(this.field)) {
                        this.field.setAccessible(true);
                        this.field.set(instance, fragment);
                        return;
                    }
                    throw new NullPointerException(String.format("Can't inject null value into %s.%s when field is not @Nullable", new Object[]{this.field.getDeclaringClass(), this.field.getName()}));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (IllegalArgumentException f) {
                    String str = "Can't assign %s value %s to %s field %s";
                    Object[] objArr = new Object[4];
                    objArr[0] = obj != null ? obj.getClass() : "(null)";
                    objArr[1] = obj;
                    objArr[2] = this.field.getType();
                    objArr[3] = this.field.getName();
                    throw new IllegalArgumentException(String.format(str, objArr), f);
                }
            } else {
                throw new UnsupportedOperationException("Can't inject fragment into a non-Activity context");
            }
        }
    }

    protected static void injectViews(Object activityOrFragment) {
        synchronized (FragmentMembersInjector.class) {
            ArrayList<FragmentMembersInjector<?>> injectors = (ArrayList) VIEW_MEMBERS_INJECTORS.get(activityOrFragment);
            if (injectors != null) {
                Iterator i$ = injectors.iterator();
                while (i$.hasNext()) {
                    ((FragmentMembersInjector) i$.next()).reallyInjectMembers(activityOrFragment);
                }
            }
        }
    }
}
