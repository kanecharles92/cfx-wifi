package roboguice.inject;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import com.google.inject.MembersInjector;
import com.google.inject.Provider;
import com.google.inject.spi.TypeEncounter;
import java.lang.annotation.Annotation;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.WeakHashMap;
import roboguice.fragment.FragmentUtil.C0724f;

public class ViewMembersInjector<T> implements MembersInjector<T> {
    protected static final WeakHashMap<Object, ArrayList<ViewMembersInjector<?>>> VIEW_MEMBERS_INJECTORS = new WeakHashMap<>();
    protected Provider<Activity> activityProvider;
    protected Annotation annotation;
    protected Field field;
    protected Provider fragManager;
    protected C0724f fragUtils;
    protected WeakReference<T> instanceRef;

    public ViewMembersInjector(Field field2, Annotation annotation2, TypeEncounter<T> typeEncounter, C0724f<?, ?> utils) {
        this.field = field2;
        this.annotation = annotation2;
        this.activityProvider = typeEncounter.getProvider(Activity.class);
        if (utils != null) {
            this.fragUtils = utils;
            this.fragManager = typeEncounter.getProvider(utils.fragmentManagerType());
        }
    }

    public void injectMembers(T instance) {
        Object key;
        synchronized (ViewMembersInjector.class) {
            Object obj = (Activity) this.activityProvider.get();
            if ((this.fragUtils != null && this.fragUtils.fragmentType().isInstance(instance)) || (instance instanceof View)) {
                key = instance;
            } else {
                key = obj;
            }
            if (key != null) {
                ArrayList<ViewMembersInjector<?>> injectors = (ArrayList) VIEW_MEMBERS_INJECTORS.get(key);
                if (injectors == null) {
                    injectors = new ArrayList<>();
                    VIEW_MEMBERS_INJECTORS.put(key, injectors);
                }
                injectors.add(this);
                this.instanceRef = new WeakReference<>(instance);
            }
        }
    }

    public void reallyInjectMembers(Object activityOrFragment) {
        if (this.annotation instanceof InjectView) {
            reallyInjectMemberViews(activityOrFragment);
        } else {
            reallyInjectMemberFragments(activityOrFragment);
        }
    }

    /* access modifiers changed from: protected */
    public void reallyInjectMemberViews(Object target) {
        View view;
        boolean isValidFragment = this.fragUtils != null && this.fragUtils.fragmentType().isInstance(target);
        Object obj = isValidFragment ? target : this.instanceRef.get();
        if (obj != null) {
            InjectView injectView = (InjectView) this.annotation;
            int id = injectView.value();
            View containerView = extractContainerView(target, isValidFragment);
            if (id >= 0) {
                view = containerView.findViewById(id);
            } else {
                view = containerView.findViewWithTag(injectView.tag());
            }
            if (view != null || !Nullable.notNullable(this.field)) {
                try {
                    this.field.setAccessible(true);
                    this.field.set(obj, view);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (IllegalArgumentException f) {
                    String str = "Can't assign %s value %s to %s field %s";
                    Object[] objArr = new Object[4];
                    objArr[0] = view != null ? view.getClass() : "(null)";
                    objArr[1] = view;
                    objArr[2] = this.field.getType();
                    objArr[3] = this.field.getName();
                    throw new IllegalArgumentException(String.format(str, objArr), f);
                }
            } else {
                throw new NullPointerException(String.format("Can't inject null value into %s.%s when field is not @Nullable", new Object[]{this.field.getDeclaringClass(), this.field.getName()}));
            }
        }
    }

    private View extractContainerView(Object target, boolean isValidFragment) {
        if (isValidFragment) {
            return this.fragUtils.getView(target);
        }
        if (target instanceof View) {
            return (View) target;
        }
        if (target instanceof Activity) {
            return ((Activity) target).getWindow().getDecorView();
        }
        throw new UnsupportedOperationException("Can't inject view into something that is not a Fragment, Activity or View.");
    }

    /* access modifiers changed from: protected */
    public void reallyInjectMemberFragments(Object activityOrFragment) {
        Object fragment;
        T instance = this.instanceRef.get();
        if (instance != null) {
            if (!(activityOrFragment instanceof Context) || (activityOrFragment instanceof Activity)) {
                Object obj = null;
                try {
                    InjectFragment injectFragment = (InjectFragment) this.annotation;
                    int id = injectFragment.value();
                    if (id >= 0) {
                        fragment = this.fragUtils.findFragmentById(this.fragManager.get(), id);
                    } else {
                        fragment = this.fragUtils.findFragmentByTag(this.fragManager.get(), injectFragment.tag());
                    }
                    if (fragment != null || !Nullable.notNullable(this.field)) {
                        this.field.setAccessible(true);
                        this.field.set(instance, fragment);
                        return;
                    }
                    throw new NullPointerException(String.format("Can't inject null value into %s.%s when field is not @Nullable", new Object[]{this.field.getDeclaringClass(), this.field.getName()}));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (IllegalArgumentException f) {
                    String str = "Can't assign %s value %s to %s field %s";
                    Object[] objArr = new Object[4];
                    objArr[0] = obj != null ? obj.getClass() : "(null)";
                    objArr[1] = obj;
                    objArr[2] = this.field.getType();
                    objArr[3] = this.field.getName();
                    throw new IllegalArgumentException(String.format(str, objArr), f);
                }
            } else {
                throw new UnsupportedOperationException("Can't inject fragment into a non-Activity context");
            }
        }
    }

    protected static void injectViews(Object activityOrFragment) {
        synchronized (ViewMembersInjector.class) {
            ArrayList<ViewMembersInjector<?>> injectors = (ArrayList) VIEW_MEMBERS_INJECTORS.get(activityOrFragment);
            if (injectors != null) {
                Iterator i$ = injectors.iterator();
                while (i$.hasNext()) {
                    ((ViewMembersInjector) i$.next()).reallyInjectMembers(activityOrFragment);
                }
            }
        }
    }
}
