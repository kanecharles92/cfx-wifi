package roboguice.inject;

import android.content.Context;
import com.google.inject.Binding;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Scope;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeConverterBinding;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Set;
import roboguice.inject.ViewListener.ViewMembersInjector;

public class ContextScopedRoboInjector implements RoboInjector {
    protected Context context;
    protected Injector delegate;
    protected ContextScope scope = ((ContextScope) this.delegate.getInstance(ContextScope.class));

    public ContextScopedRoboInjector(Context context2, Injector applicationInjector) {
        this.delegate = applicationInjector;
        this.context = context2;
    }

    public Injector createChildInjector(Iterable<? extends Module> modules) {
        Injector createChildInjector;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                createChildInjector = this.delegate.createChildInjector(modules);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return createChildInjector;
    }

    public Injector createChildInjector(Module... modules) {
        Injector createChildInjector;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                createChildInjector = this.delegate.createChildInjector(modules);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return createChildInjector;
    }

    public <T> List<Binding<T>> findBindingsByType(TypeLiteral<T> type) {
        List<Binding<T>> findBindingsByType;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                findBindingsByType = this.delegate.findBindingsByType(type);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return findBindingsByType;
    }

    public Map<Key<?>, Binding<?>> getAllBindings() {
        Map<Key<?>, Binding<?>> allBindings;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                allBindings = this.delegate.getAllBindings();
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return allBindings;
    }

    public <T> Binding<T> getBinding(Key<T> key) {
        Binding<T> binding;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                binding = this.delegate.getBinding(key);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return binding;
    }

    public <T> Binding<T> getBinding(Class<T> type) {
        Binding<T> binding;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                binding = this.delegate.getBinding(type);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return binding;
    }

    public Map<Key<?>, Binding<?>> getBindings() {
        Map<Key<?>, Binding<?>> bindings;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                bindings = this.delegate.getBindings();
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return bindings;
    }

    public <T> Binding<T> getExistingBinding(Key<T> key) {
        Binding<T> existingBinding;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                existingBinding = this.delegate.getExistingBinding(key);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return existingBinding;
    }

    public <T> T getInstance(Key<T> key) {
        T instance;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                instance = this.delegate.getInstance(key);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return instance;
    }

    public <T> T getInstance(Class<T> type) {
        T instance;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                instance = this.delegate.getInstance(type);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return instance;
    }

    public <T> MembersInjector<T> getMembersInjector(Class<T> type) {
        MembersInjector<T> membersInjector;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                membersInjector = this.delegate.getMembersInjector(type);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return membersInjector;
    }

    public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> typeLiteral) {
        MembersInjector<T> membersInjector;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                membersInjector = this.delegate.getMembersInjector(typeLiteral);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return membersInjector;
    }

    public Injector getParent() {
        Injector parent;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                parent = this.delegate.getParent();
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return parent;
    }

    public <T> Provider<T> getProvider(Key<T> key) {
        Provider<T> provider;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                provider = this.delegate.getProvider(key);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return provider;
    }

    public <T> Provider<T> getProvider(Class<T> type) {
        Provider<T> provider;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                provider = this.delegate.getProvider(type);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return provider;
    }

    public Map<Class<? extends Annotation>, Scope> getScopeBindings() {
        Map<Class<? extends Annotation>, Scope> scopeBindings;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                scopeBindings = this.delegate.getScopeBindings();
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return scopeBindings;
    }

    public Set<TypeConverterBinding> getTypeConverterBindings() {
        Set<TypeConverterBinding> typeConverterBindings;
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                typeConverterBindings = this.delegate.getTypeConverterBindings();
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
        return typeConverterBindings;
    }

    public void injectMembers(Object instance) {
        injectMembersWithoutViews(instance);
    }

    public void injectMembersWithoutViews(Object instance) {
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                this.delegate.injectMembers(instance);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
    }

    public void injectViewMembers(Object instance) {
        synchronized (ContextScope.class) {
            this.scope.enter(this.context);
            try {
                ViewMembersInjector.injectViews(instance);
                this.scope.exit(this.context);
            } catch (Throwable th) {
                this.scope.exit(this.context);
                throw th;
            }
        }
    }
}
