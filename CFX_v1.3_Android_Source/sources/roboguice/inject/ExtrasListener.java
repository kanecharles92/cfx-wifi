package roboguice.inject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.google.inject.Guice;
import com.google.inject.HierarchyTraversalFilter;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.MembersInjector;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import com.google.inject.util.Types;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Set;
import roboguice.RoboGuice;

public class ExtrasListener implements TypeListener {
    protected Provider<Context> contextProvider;
    private HierarchyTraversalFilter filter;

    protected static class ExtrasMembersInjector<T> implements MembersInjector<T> {
        protected InjectExtra annotation;
        protected Provider<Context> contextProvider;
        protected Field field;

        public ExtrasMembersInjector(Field field2, Provider<Context> contextProvider2, InjectExtra annotation2) {
            this.field = field2;
            this.contextProvider = contextProvider2;
            this.annotation = annotation2;
        }

        public void injectMembers(T instance) {
            Context context = (Context) this.contextProvider.get();
            if (!(context instanceof Activity)) {
                throw new UnsupportedOperationException(String.format("Extras may not be injected into contexts that are not Activities (error in class %s)", new Object[]{((Context) this.contextProvider.get()).getClass().getSimpleName()}));
            }
            Activity activity = (Activity) context;
            String id = this.annotation.value();
            Bundle extras = activity.getIntent().getExtras();
            if (extras != null && extras.containsKey(id)) {
                Object value = convert(this.field, extras.get(id), RoboGuice.getOrCreateBaseApplicationInjector(activity.getApplication()));
                if (value != null || !Nullable.notNullable(this.field)) {
                    this.field.setAccessible(true);
                    try {
                        this.field.set(instance, value);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    } catch (IllegalArgumentException e2) {
                        String str = "Can't assign %s value %s to %s field %s";
                        Object[] objArr = new Object[4];
                        objArr[0] = value != null ? value.getClass() : "(null)";
                        objArr[1] = value;
                        objArr[2] = this.field.getType();
                        objArr[3] = this.field.getName();
                        throw new IllegalArgumentException(String.format(str, objArr));
                    }
                } else {
                    throw new NullPointerException(String.format("Can't inject null value into %s.%s when field is not @Nullable", new Object[]{this.field.getDeclaringClass(), this.field.getName()}));
                }
            } else if (!this.annotation.optional()) {
                throw new IllegalStateException(String.format("Can't find the mandatory extra identified by key [%s] on field %s.%s", new Object[]{id, this.field.getDeclaringClass(), this.field.getName()}));
            }
        }

        /* access modifiers changed from: protected */
        public Object convert(Field field2, Object value, Injector injector) {
            if (value == null || field2.getType().isPrimitive()) {
                return value;
            }
            Key<?> key = Key.get((Type) Types.newParameterizedType(ExtraConverter.class, value.getClass(), field2.getType()));
            if (injector.getBindings().containsKey(key)) {
                value = ((ExtraConverter) injector.getInstance(key)).convert(value);
            }
            return value;
        }
    }

    public ExtrasListener(Provider<Context> contextProvider2) {
        this.contextProvider = contextProvider2;
    }

    public <I> void hear(TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) {
        if (this.filter == null) {
            this.filter = Guice.createHierarchyTraversalFilter();
        } else {
            this.filter.reset();
        }
        for (Class<?> c = typeLiteral.getRawType(); isWorthScanning(c); c = c.getSuperclass()) {
            Set<Field> allFields = this.filter.getAllFields(InjectExtra.class.getName(), c);
            if (allFields != null) {
                for (Field field : allFields) {
                    if (field.isAnnotationPresent(InjectExtra.class)) {
                        if (Modifier.isStatic(field.getModifiers())) {
                            throw new UnsupportedOperationException("Extras may not be statically injected");
                        }
                        typeEncounter.register((MembersInjector<? super I>) new ExtrasMembersInjector<Object>(field, this.contextProvider, (InjectExtra) field.getAnnotation(InjectExtra.class)));
                    }
                }
                continue;
            }
        }
    }

    private boolean isWorthScanning(Class<?> c) {
        return this.filter.isWorthScanningForFields(InjectExtra.class.getName(), c);
    }
}
