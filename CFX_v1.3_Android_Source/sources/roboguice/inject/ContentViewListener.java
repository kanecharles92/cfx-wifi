package roboguice.inject;

import android.app.Activity;
import android.content.Context;
import com.google.inject.Inject;
import roboguice.context.event.OnCreateEvent;
import roboguice.event.Observes;

@ContextSingleton
public class ContentViewListener {
    @Inject
    protected Activity activity;

    public void optionallySetContentView(@Observes OnCreateEvent<?> onCreateEvent) {
        for (Class<?> c = this.activity.getClass(); c != Context.class; c = c.getSuperclass()) {
            ContentView annotation = (ContentView) c.getAnnotation(ContentView.class);
            if (annotation != null) {
                this.activity.setContentView(annotation.value());
                return;
            }
        }
    }
}
