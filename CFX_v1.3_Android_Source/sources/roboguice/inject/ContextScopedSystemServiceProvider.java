package roboguice.inject;

import android.content.Context;
import com.google.inject.Provider;

public class ContextScopedSystemServiceProvider<T> implements Provider<T> {
    protected Provider<Context> contextProvider;
    protected String serviceName;

    public ContextScopedSystemServiceProvider(Provider<Context> contextProvider2, String serviceName2) {
        this.contextProvider = contextProvider2;
        this.serviceName = serviceName2;
    }

    public T get() {
        return ((Context) this.contextProvider.get()).getSystemService(this.serviceName);
    }
}
