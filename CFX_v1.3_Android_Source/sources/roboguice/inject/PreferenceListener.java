package roboguice.inject;

import android.app.Application;
import android.content.Context;
import android.preference.PreferenceActivity;
import com.google.inject.Guice;
import com.google.inject.HierarchyTraversalFilter;
import com.google.inject.MembersInjector;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Set;

public class PreferenceListener implements TypeListener {
    protected Application application;
    protected Provider<Context> contextProvider;
    private HierarchyTraversalFilter filter;
    protected ArrayList<PreferenceMembersInjector<?>> preferencesForInjection = new ArrayList<>();

    class PreferenceMembersInjector<T> implements MembersInjector<T> {
        protected InjectPreference annotation;
        protected Provider<Context> contextProvider;
        protected Field field;
        protected WeakReference<T> instanceRef;

        public PreferenceMembersInjector(Field field2, Provider<Context> contextProvider2, InjectPreference annotation2) {
            this.field = field2;
            this.annotation = annotation2;
            this.contextProvider = contextProvider2;
        }

        public void injectMembers(T instance) {
            this.instanceRef = new WeakReference<>(instance);
            PreferenceListener.this.registerPreferenceForInjection(this);
        }

        public void reallyInjectMembers() {
            T instance = this.instanceRef.get();
            if (instance != null) {
                Object value = null;
                try {
                    value = ((PreferenceActivity) this.contextProvider.get()).findPreference(this.annotation.value());
                    if (value != null || !Nullable.notNullable(this.field)) {
                        this.field.setAccessible(true);
                        this.field.set(instance, value);
                        return;
                    }
                    throw new NullPointerException(String.format("Can't inject null value into %s.%s when field is not @Nullable", new Object[]{this.field.getDeclaringClass(), this.field.getName()}));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (IllegalArgumentException e2) {
                    String str = "Can't assign %s value %s to %s field %s";
                    Object[] objArr = new Object[4];
                    objArr[0] = value != null ? value.getClass() : "(null)";
                    objArr[1] = value;
                    objArr[2] = this.field.getType();
                    objArr[3] = this.field.getName();
                    throw new IllegalArgumentException(String.format(str, objArr));
                }
            }
        }
    }

    public PreferenceListener(Provider<Context> contextProvider2, Application application2) {
        this.contextProvider = contextProvider2;
        this.application = application2;
    }

    public <I> void hear(TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) {
        if (this.filter == null) {
            this.filter = Guice.createHierarchyTraversalFilter();
        } else {
            this.filter.reset();
        }
        for (Class<?> c = typeLiteral.getRawType(); isWorthScanning(c); c = c.getSuperclass()) {
            Set<Field> allFields = this.filter.getAllFields(InjectPreference.class.getName(), c);
            if (allFields != null) {
                for (Field field : allFields) {
                    if (field.isAnnotationPresent(InjectPreference.class)) {
                        if (Modifier.isStatic(field.getModifiers())) {
                            throw new UnsupportedOperationException("Preferences may not be statically injected");
                        }
                        typeEncounter.register((MembersInjector<? super I>) new PreferenceMembersInjector<Object>(field, this.contextProvider, (InjectPreference) field.getAnnotation(InjectPreference.class)));
                    }
                }
                continue;
            }
        }
    }

    private boolean isWorthScanning(Class<?> c) {
        return this.filter.isWorthScanningForFields(InjectPreference.class.getName(), c);
    }

    public void registerPreferenceForInjection(PreferenceMembersInjector<?> injector) {
        this.preferencesForInjection.add(injector);
    }

    public void injectPreferenceViews() {
        for (int i = this.preferencesForInjection.size() - 1; i >= 0; i--) {
            ((PreferenceMembersInjector) this.preferencesForInjection.remove(i)).reallyInjectMembers();
        }
    }
}
