package roboguice.inject;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Scope;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import roboguice.util.RoboContext;

public class ContextScope implements Scope {
    protected Application application;
    protected Map<Key<?>, Object> applicationScopedObjects = new HashMap();
    protected ThreadLocal<Stack<WeakReference<Context>>> contextThreadLocal = new ThreadLocal<>();

    public ContextScope(Application application2) {
        this.application = application2;
        enter(application2);
    }

    public void enter(Context context) {
        synchronized (ContextScope.class) {
            Stack<WeakReference<Context>> stack = getContextStack();
            Map<Key<?>, Object> map = getScopedObjectMap(context);
            stack.push(new WeakReference(context));
            Class<?> c = context.getClass();
            do {
                map.put(Key.get(c), context);
                c = c.getSuperclass();
            } while (c != Object.class);
        }
    }

    public void exit(Context context) {
        synchronized (ContextScope.class) {
            Context c = (Context) ((WeakReference) getContextStack().pop()).get();
            if (c != null && c != context) {
                throw new IllegalArgumentException(String.format("Scope for %s must be opened before it can be closed", new Object[]{context}));
            }
        }
    }

    public <T> Provider<T> scope(final Key<T> key, final Provider<T> unscoped) {
        return new Provider<T>() {
            public T get() {
                T current;
                synchronized (ContextScope.class) {
                    Map<Key<?>, Object> objectsForScope = ContextScope.this.getScopedObjectMap((Context) ((WeakReference) ContextScope.this.getContextStack().peek()).get());
                    if (objectsForScope == null) {
                        current = null;
                    } else {
                        current = objectsForScope.get(key);
                        if (current == null && !objectsForScope.containsKey(key)) {
                            current = unscoped.get();
                            objectsForScope.put(key, current);
                        }
                    }
                }
                return current;
            }
        };
    }

    public Stack<WeakReference<Context>> getContextStack() {
        Stack<WeakReference<Context>> stack = (Stack) this.contextThreadLocal.get();
        if (stack != null) {
            return stack;
        }
        Stack<WeakReference<Context>> stack2 = new Stack<>();
        this.contextThreadLocal.set(stack2);
        return stack2;
    }

    /* access modifiers changed from: protected */
    public Map<Key<?>, Object> getScopedObjectMap(Context origContext) {
        Context context = origContext;
        while (!(context instanceof RoboContext) && !(context instanceof Application) && (context instanceof ContextWrapper)) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (context instanceof Application) {
            return this.applicationScopedObjects;
        }
        if (context instanceof RoboContext) {
            return ((RoboContext) context).getScopedObjectMap();
        }
        throw new IllegalArgumentException(String.format("%s does not appear to be a RoboGuice context (instanceof RoboContext)", new Object[]{origContext}));
    }
}
