package roboguice.inject;

import android.app.Application;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Movie;
import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.google.inject.Guice;
import com.google.inject.HierarchyTraversalFilter;
import com.google.inject.MembersInjector;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Set;

public class ResourceListener implements TypeListener {
    protected Application application;
    private HierarchyTraversalFilter filter;

    protected static class ResourceMembersInjector<T> implements MembersInjector<T> {
        protected InjectResource annotation;
        protected Application application;
        protected Field field;

        public ResourceMembersInjector(Field field2, Application application2, InjectResource annotation2) {
            this.field = field2;
            this.application = application2;
            this.annotation = annotation2;
        }

        public void injectMembers(T instance) {
            Object value = null;
            try {
                Resources resources = this.application.getResources();
                int id = getId(resources, this.annotation);
                Class<?> t = this.field.getType();
                if (String.class.isAssignableFrom(t)) {
                    value = resources.getString(id);
                } else if (Boolean.TYPE.isAssignableFrom(t) || Boolean.class.isAssignableFrom(t)) {
                    value = Boolean.valueOf(resources.getBoolean(id));
                } else if (ColorStateList.class.isAssignableFrom(t)) {
                    value = resources.getColorStateList(id);
                } else if (Integer.TYPE.isAssignableFrom(t) || Integer.class.isAssignableFrom(t)) {
                    value = Integer.valueOf(resources.getInteger(id));
                } else if (Drawable.class.isAssignableFrom(t)) {
                    value = resources.getDrawable(id);
                } else if (String[].class.isAssignableFrom(t)) {
                    value = resources.getStringArray(id);
                } else if (int[].class.isAssignableFrom(t) || Integer[].class.isAssignableFrom(t)) {
                    value = resources.getIntArray(id);
                } else if (Animation.class.isAssignableFrom(t)) {
                    value = AnimationUtils.loadAnimation(this.application, id);
                } else if (Movie.class.isAssignableFrom(t)) {
                    value = resources.getMovie(id);
                }
                if (value != null || !Nullable.notNullable(this.field)) {
                    this.field.setAccessible(true);
                    this.field.set(instance, value);
                    return;
                }
                throw new NullPointerException(String.format("Can't inject null value into %s.%s when field is not @Nullable", new Object[]{this.field.getDeclaringClass(), this.field.getName()}));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (IllegalArgumentException e2) {
                String str = "Can't assign %s value %s to %s field %s";
                Object[] objArr = new Object[4];
                objArr[0] = value != null ? value.getClass() : "(null)";
                objArr[1] = value;
                objArr[2] = this.field.getType();
                objArr[3] = this.field.getName();
                throw new IllegalArgumentException(String.format(str, objArr));
            }
        }

        /* access modifiers changed from: protected */
        public int getId(Resources resources, InjectResource annotation2) {
            int id = annotation2.value();
            return id >= 0 ? id : resources.getIdentifier(annotation2.name(), null, this.application.getPackageName());
        }
    }

    public ResourceListener(Application application2) {
        this.application = application2;
    }

    public <I> void hear(TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) {
        if (this.filter == null) {
            this.filter = Guice.createHierarchyTraversalFilter();
        } else {
            this.filter.reset();
        }
        Class<?> c = typeLiteral.getRawType();
        while (isWorthScanning(c)) {
            Set<Field> allFields = this.filter.getAllFields(InjectResource.class.getName(), c);
            if (allFields != null) {
                for (Field field : allFields) {
                    if (field.isAnnotationPresent(InjectResource.class) && !Modifier.isStatic(field.getModifiers())) {
                        typeEncounter.register((MembersInjector<? super I>) new ResourceMembersInjector<Object>(field, this.application, (InjectResource) field.getAnnotation(InjectResource.class)));
                    }
                }
                c = c.getSuperclass();
            }
        }
    }

    private boolean isWorthScanning(Class<?> c) {
        return this.filter.isWorthScanningForFields(InjectResource.class.getName(), c);
    }
}
