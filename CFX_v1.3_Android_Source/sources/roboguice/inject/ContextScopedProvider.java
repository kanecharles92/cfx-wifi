package roboguice.inject;

import android.content.Context;
import com.google.inject.Inject;
import com.google.inject.Provider;
import roboguice.RoboGuice;

public class ContextScopedProvider<T> {
    @Inject
    protected Provider<T> provider;

    public T get(Context context) {
        T t;
        ContextScope scope = (ContextScope) RoboGuice.getInjector(context).getInstance(ContextScope.class);
        synchronized (ContextScope.class) {
            scope.enter(context);
            try {
                t = this.provider.get();
                scope.exit(context);
            } catch (Throwable th) {
                scope.exit(context);
                throw th;
            }
        }
        return t;
    }
}
