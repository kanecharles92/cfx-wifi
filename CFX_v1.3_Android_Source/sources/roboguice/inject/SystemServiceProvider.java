package roboguice.inject;

import android.app.Application;
import com.google.inject.Provider;

public class SystemServiceProvider<T> implements Provider<T> {
    protected Application application;
    protected String serviceName;

    public SystemServiceProvider(Application application2, String serviceName2) {
        this.serviceName = serviceName2;
        this.application = application2;
    }

    public T get() {
        return this.application.getSystemService(this.serviceName);
    }
}
