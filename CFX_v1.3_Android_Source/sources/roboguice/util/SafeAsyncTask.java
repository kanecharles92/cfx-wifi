package roboguice.util;

import android.os.Handler;
import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

@Deprecated
public abstract class SafeAsyncTask<ResultT> implements Callable<ResultT> {
    protected static final Executor DEFAULT_EXECUTOR = Executors.newFixedThreadPool(25);
    public static final int DEFAULT_POOL_SIZE = 25;
    protected Executor executor;
    protected FutureTask<Void> future;
    protected Handler handler;

    public class SafeAsyncTaskAndroidCallable extends AndroidCallable<ResultT> {
        public SafeAsyncTaskAndroidCallable() {
        }

        public ResultT doInBackground() throws Exception {
            return SafeAsyncTask.this.call();
        }

        public void onException(Exception e) {
            SafeAsyncTask.this.onException(e);
        }

        public void onFinally() {
            SafeAsyncTask.this.onFinally();
        }

        public void onPreCall() {
            try {
                SafeAsyncTask.this.onPreExecute();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public void onSuccess(ResultT result) {
            try {
                SafeAsyncTask.this.onSuccess(result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public SafeAsyncTask() {
        this.executor = DEFAULT_EXECUTOR;
    }

    public SafeAsyncTask(Handler handler2) {
        this.handler = handler2;
        this.executor = DEFAULT_EXECUTOR;
    }

    public SafeAsyncTask(Executor executor2) {
        this.executor = executor2;
    }

    public SafeAsyncTask(Handler handler2, Executor executor2) {
        this.handler = handler2;
        this.executor = executor2;
    }

    public FutureTask<Void> future() {
        this.future = new FutureTask<>(newTask(), null);
        return this.future;
    }

    public SafeAsyncTask<ResultT> executor(Executor executor2) {
        this.executor = executor2;
        return this;
    }

    public Executor executor() {
        return this.executor;
    }

    public SafeAsyncTask<ResultT> handler(Handler handler2) {
        this.handler = handler2;
        return this;
    }

    public Handler handler() {
        return this.handler;
    }

    public void execute() {
        execute(C0729Ln.isDebugEnabled() ? Thread.currentThread().getStackTrace() : null);
    }

    /* access modifiers changed from: protected */
    public void execute(StackTraceElement[] launchLocation) {
        this.executor.execute(future());
    }

    public boolean cancel(boolean mayInterruptIfRunning) {
        if (this.future != null) {
            return this.future.cancel(mayInterruptIfRunning);
        }
        throw new UnsupportedOperationException("You cannot cancel this task before calling future()");
    }

    public boolean isCancelled() {
        if (this.future == null) {
            return false;
        }
        return this.future.isCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() throws Exception {
    }

    /* access modifiers changed from: protected */
    public void onSuccess(ResultT resultt) throws Exception {
    }

    /* access modifiers changed from: protected */
    public void onInterrupted(Exception e) {
        onException(e);
    }

    /* access modifiers changed from: protected */
    public void onException(Exception e) throws RuntimeException {
        onThrowable(e);
    }

    /* access modifiers changed from: protected */
    public void onThrowable(Throwable t) throws RuntimeException {
        Log.e("roboguice", "Throwable caught during background processing", t);
    }

    /* access modifiers changed from: protected */
    public void onFinally() throws RuntimeException {
    }

    /* access modifiers changed from: protected */
    public Runnable newTask() {
        return new SafeAsyncTaskAndroidCallable();
    }
}
