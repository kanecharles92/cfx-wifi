package roboguice.util;

import android.content.Context;
import android.os.Handler;
import java.util.concurrent.Executor;
import roboguice.RoboGuice;

public abstract class RoboAsyncTask<ResultT> extends SafeAsyncTask<ResultT> {
    protected Context context;

    protected RoboAsyncTask(Context context2) {
        this.context = context2;
        RoboGuice.getInjector(context2).injectMembers(this);
    }

    protected RoboAsyncTask(Context context2, Handler handler) {
        super(handler);
        this.context = context2;
        RoboGuice.getInjector(context2).injectMembers(this);
    }

    protected RoboAsyncTask(Context context2, Handler handler, Executor executor) {
        super(handler, executor);
        this.context = context2;
        RoboGuice.getInjector(context2).injectMembers(this);
    }

    protected RoboAsyncTask(Context context2, Executor executor) {
        super(executor);
        this.context = context2;
        RoboGuice.getInjector(context2).injectMembers(this);
    }

    public Context getContext() {
        return this.context;
    }
}
