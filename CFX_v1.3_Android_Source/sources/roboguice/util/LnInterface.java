package roboguice.util;

public interface LnInterface {
    /* renamed from: d */
    int mo10560d(Object obj, Object... objArr);

    /* renamed from: d */
    int mo10561d(Throwable th);

    /* renamed from: d */
    int mo10562d(Throwable th, Object obj, Object... objArr);

    /* renamed from: e */
    int mo10563e(Object obj, Object... objArr);

    /* renamed from: e */
    int mo10564e(Throwable th);

    /* renamed from: e */
    int mo10565e(Throwable th, Object obj, Object... objArr);

    int getLoggingLevel();

    /* renamed from: i */
    int mo10569i(Object obj, Object... objArr);

    /* renamed from: i */
    int mo10570i(Throwable th);

    /* renamed from: i */
    int mo10571i(Throwable th, Object obj, Object... objArr);

    boolean isDebugEnabled();

    boolean isVerboseEnabled();

    String logLevelToString(int i);

    void setLoggingLevel(int i);

    /* renamed from: v */
    int mo10578v(Object obj, Object... objArr);

    /* renamed from: v */
    int mo10579v(Throwable th);

    /* renamed from: v */
    int mo10580v(Throwable th, Object obj, Object[] objArr);

    /* renamed from: w */
    int mo10581w(Object obj, Object... objArr);

    /* renamed from: w */
    int mo10582w(Throwable th);

    /* renamed from: w */
    int mo10583w(Throwable th, Object obj, Object... objArr);
}
