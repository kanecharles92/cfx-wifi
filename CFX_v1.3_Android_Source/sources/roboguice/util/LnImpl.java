package roboguice.util;

import android.app.Application;
import android.util.Log;
import com.google.inject.Inject;
import java.util.Locale;

public class LnImpl implements LnInterface {
    protected int minimumLogLevel = 2;
    protected String packageName = "";
    protected String tag = "";

    public LnImpl() {
    }

    @Inject
    public LnImpl(Application context) {
        int i = 2;
        try {
            this.packageName = context.getPackageName();
            if ((context.getPackageManager().getApplicationInfo(this.packageName, 0).flags & 2) == 0) {
                i = 4;
            }
            this.minimumLogLevel = i;
            this.tag = this.packageName.toUpperCase(Locale.US);
            C0729Ln.m184d("Configuring Logging, minimum log level is %s", C0729Ln.logLevelToString(this.minimumLogLevel));
        } catch (Exception e) {
            try {
                Log.e(this.packageName, "Error configuring logger", e);
            } catch (RuntimeException e2) {
            }
        }
    }

    /* renamed from: v */
    public int mo10579v(Throwable t) {
        if (getLoggingLevel() <= 2) {
            return println(2, Log.getStackTraceString(t));
        }
        return 0;
    }

    /* renamed from: v */
    public int mo10578v(Object s1, Object... args) {
        if (getLoggingLevel() > 2) {
            return 0;
        }
        return println(2, formatArgs(Strings.toString(s1), args));
    }

    /* renamed from: v */
    public int mo10580v(Throwable throwable, Object s1, Object[] args) {
        if (getLoggingLevel() > 2) {
            return 0;
        }
        return println(2, formatArgs(Strings.toString(s1), args) + 10 + Log.getStackTraceString(throwable));
    }

    /* renamed from: d */
    public int mo10561d(Throwable t) {
        if (getLoggingLevel() <= 3) {
            return println(3, Log.getStackTraceString(t));
        }
        return 0;
    }

    /* renamed from: d */
    public int mo10560d(Object s1, Object... args) {
        if (getLoggingLevel() > 3) {
            return 0;
        }
        return println(3, formatArgs(Strings.toString(s1), args));
    }

    /* renamed from: d */
    public int mo10562d(Throwable throwable, Object s1, Object... args) {
        if (getLoggingLevel() > 3) {
            return 0;
        }
        return println(3, formatArgs(Strings.toString(s1), args) + 10 + Log.getStackTraceString(throwable));
    }

    /* renamed from: i */
    public int mo10570i(Throwable t) {
        if (getLoggingLevel() <= 4) {
            return println(4, Log.getStackTraceString(t));
        }
        return 0;
    }

    /* renamed from: i */
    public int mo10571i(Throwable throwable, Object s1, Object... args) {
        if (getLoggingLevel() > 4) {
            return 0;
        }
        return println(4, formatArgs(Strings.toString(s1), args) + 10 + Log.getStackTraceString(throwable));
    }

    /* renamed from: i */
    public int mo10569i(Object s1, Object... args) {
        if (getLoggingLevel() > 4) {
            return 0;
        }
        return println(4, formatArgs(Strings.toString(s1), args));
    }

    /* renamed from: w */
    public int mo10582w(Throwable t) {
        if (getLoggingLevel() <= 5) {
            return println(5, Log.getStackTraceString(t));
        }
        return 0;
    }

    /* renamed from: w */
    public int mo10583w(Throwable throwable, Object s1, Object... args) {
        if (getLoggingLevel() > 5) {
            return 0;
        }
        return println(5, formatArgs(Strings.toString(s1), args) + 10 + Log.getStackTraceString(throwable));
    }

    /* renamed from: w */
    public int mo10581w(Object s1, Object... args) {
        if (getLoggingLevel() > 5) {
            return 0;
        }
        return println(5, formatArgs(Strings.toString(s1), args));
    }

    /* renamed from: e */
    public int mo10564e(Throwable t) {
        if (getLoggingLevel() <= 6) {
            return println(6, Log.getStackTraceString(t));
        }
        return 0;
    }

    /* renamed from: e */
    public int mo10565e(Throwable throwable, Object s1, Object... args) {
        if (getLoggingLevel() > 6) {
            return 0;
        }
        return println(6, formatArgs(Strings.toString(s1), args) + 10 + Log.getStackTraceString(throwable));
    }

    /* renamed from: e */
    public int mo10563e(Object s1, Object... args) {
        if (getLoggingLevel() > 6) {
            return 0;
        }
        return println(6, formatArgs(Strings.toString(s1), args));
    }

    public boolean isDebugEnabled() {
        return getLoggingLevel() <= 3;
    }

    public boolean isVerboseEnabled() {
        return getLoggingLevel() <= 2;
    }

    public String logLevelToString(int loglevel) {
        switch (loglevel) {
            case 2:
                return "VERBOSE";
            case 3:
                return "DEBUG";
            case 4:
                return "INFO";
            case 5:
                return "WARN";
            case 6:
                return "ERROR";
            case 7:
                return "ASSERT";
            default:
                return "UNKNOWN";
        }
    }

    public int getLoggingLevel() {
        return this.minimumLogLevel;
    }

    public void setLoggingLevel(int level) {
        this.minimumLogLevel = level;
    }

    public int println(int priority, String msg) {
        return Log.println(priority, getTag(), processMessage(msg));
    }

    /* access modifiers changed from: protected */
    public String processMessage(String msg) {
        if (getLoggingLevel() > 3) {
            return msg;
        }
        return String.format("%s %s", new Object[]{Thread.currentThread().getName(), msg});
    }

    /* access modifiers changed from: protected */
    public String getTag() {
        if (getLoggingLevel() > 3) {
            return this.tag;
        }
        StackTraceElement trace = Thread.currentThread().getStackTrace()[6];
        return this.tag + "/" + trace.getFileName() + ":" + trace.getLineNumber();
    }

    /* access modifiers changed from: protected */
    public String formatArgs(String s, Object... args) {
        return (args == null || args.length != 0) ? String.format(s, args) : s;
    }
}
