package roboguice.util;

public interface AndroidCallableI<ResultT> {
    ResultT doInBackground() throws Exception;

    void onException(Exception exc);

    void onFinally();

    void onPreCall() throws Exception;

    void onSuccess(ResultT resultt);
}
