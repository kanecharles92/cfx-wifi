package roboguice.util;

import android.os.Handler;
import android.os.Looper;
import edu.umd.p004cs.findbugs.annotations.SuppressWarnings;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

public class AndroidCallableWrapper<ResultT> implements Runnable {
    static HashMap<Class<? extends AndroidCallableI<?>>, Boolean> isPreCallOverriddenMap = new HashMap<>();
    protected AndroidCallableI<ResultT> delegate;
    protected Handler handler;
    protected StackTraceElement[] launchLocation;

    @SuppressWarnings({"MALICIOUS_CODE"})
    public AndroidCallableWrapper(Handler handler2, AndroidCallableI<ResultT> delegate2, StackTraceElement[] launchLocation2) {
        this.delegate = delegate2;
        this.launchLocation = launchLocation2;
        if (handler2 == null) {
            handler2 = new Handler(Looper.getMainLooper());
        }
        this.handler = handler2;
    }

    public void run() {
        ResultT result = null;
        Exception exception = null;
        try {
            if (isPreCallOverriden(this.delegate.getClass())) {
                beforeCall();
            }
            result = doDoInBackgroundThread();
        } catch (Exception e) {
            exception = e;
        } finally {
            afterCall(result, exception);
        }
    }

    /* access modifiers changed from: 0000 */
    public void beforeCall() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        final Exception[] exceptions = new Exception[1];
        this.handler.post(new Runnable() {
            public void run() {
                try {
                    new Callable<Object>() {
                        public Object call() throws Exception {
                            AndroidCallableWrapper.this.doOnPreCall();
                            return null;
                        }
                    }.call();
                } catch (Exception e) {
                    exceptions[0] = e;
                } finally {
                    latch.countDown();
                }
            }
        });
        latch.await();
        if (exceptions[0] != null) {
            throw exceptions[0];
        }
    }

    /* access modifiers changed from: 0000 */
    public void afterCall(final ResultT result, final Exception e) {
        this.handler.post(new Runnable() {
            public void run() {
                try {
                    if (e != null) {
                        if (AndroidCallableWrapper.this.launchLocation != null) {
                            StackTraceElement[] stackTrace = e.getStackTrace();
                            StackTraceElement[] result = new StackTraceElement[(stackTrace.length + AndroidCallableWrapper.this.launchLocation.length)];
                            System.arraycopy(stackTrace, 0, result, 0, stackTrace.length);
                            System.arraycopy(AndroidCallableWrapper.this.launchLocation, 0, result, stackTrace.length, AndroidCallableWrapper.this.launchLocation.length);
                            e.setStackTrace(result);
                        }
                        AndroidCallableWrapper.this.doOnException(e);
                    } else {
                        AndroidCallableWrapper.this.doOnSuccess(result);
                    }
                } finally {
                    AndroidCallableWrapper.this.doOnFinally();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void doOnPreCall() throws Exception {
        this.delegate.onPreCall();
    }

    /* access modifiers changed from: protected */
    public ResultT doDoInBackgroundThread() throws Exception {
        return this.delegate.doInBackground();
    }

    /* access modifiers changed from: protected */
    public void doOnSuccess(ResultT result) {
        this.delegate.onSuccess(result);
    }

    /* access modifiers changed from: protected */
    public void doOnException(Exception e) {
        this.delegate.onException(e);
    }

    /* access modifiers changed from: protected */
    public void doOnFinally() {
        this.delegate.onFinally();
    }

    static boolean isPreCallOverriden(Class<? extends AndroidCallableI<?>> subClass) {
        boolean z = false;
        try {
            Boolean tmp = (Boolean) isPreCallOverriddenMap.get(subClass);
            if (tmp != null) {
                return tmp.booleanValue();
            }
            if (subClass.getMethod("onPreCall", new Class[0]).getDeclaringClass() != AndroidCallableWrapper.class) {
                z = true;
            }
            Boolean tmp2 = Boolean.valueOf(z);
            isPreCallOverriddenMap.put(subClass, tmp2);
            return tmp2.booleanValue();
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}
