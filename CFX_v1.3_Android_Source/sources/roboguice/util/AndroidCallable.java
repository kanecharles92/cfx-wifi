package roboguice.util;

public abstract class AndroidCallable<ResultT> implements AndroidCallableI<ResultT>, Runnable {
    protected StackTraceElement[] creationLocation;

    public AndroidCallable() {
        this.creationLocation = C0729Ln.isDebugEnabled() ? Thread.currentThread().getStackTrace() : null;
    }

    public void run() {
        new AndroidCallableWrapper(null, this, this.creationLocation).run();
    }

    public void onPreCall() throws Exception {
    }

    public void onFinally() {
    }
}
