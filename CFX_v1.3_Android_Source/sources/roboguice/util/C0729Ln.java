package roboguice.util;

import com.google.inject.Inject;
import edu.umd.p004cs.findbugs.annotations.SuppressWarnings;

@SuppressWarnings({"ImplicitArrayToString"})
/* renamed from: roboguice.util.Ln */
public final class C0729Ln {
    @SuppressWarnings({"MS_SHOULD_BE_FINAL"})
    @Inject(optional = true)
    protected static LnInterface lnImpl = new LnImpl();

    private C0729Ln() {
    }

    /* renamed from: v */
    public static int m194v(Throwable t) {
        return lnImpl.mo10579v(t);
    }

    /* renamed from: v */
    public static int m193v(Object s1, Object... args) {
        return lnImpl.mo10578v(s1, args);
    }

    /* renamed from: v */
    public static int m195v(Throwable throwable, Object s1, Object... args) {
        return lnImpl.mo10580v(throwable, s1, args);
    }

    /* renamed from: d */
    public static int m185d(Throwable t) {
        return lnImpl.mo10561d(t);
    }

    /* renamed from: d */
    public static int m184d(Object s1, Object... args) {
        return lnImpl.mo10560d(s1, args);
    }

    /* renamed from: d */
    public static int m186d(Throwable throwable, Object s1, Object... args) {
        return lnImpl.mo10562d(throwable, s1, args);
    }

    /* renamed from: i */
    public static int m191i(Throwable t) {
        return lnImpl.mo10570i(t);
    }

    /* renamed from: i */
    public static int m190i(Object s1, Object... args) {
        return lnImpl.mo10569i(s1, args);
    }

    /* renamed from: i */
    public static int m192i(Throwable throwable, Object s1, Object... args) {
        return lnImpl.mo10571i(throwable, s1, args);
    }

    /* renamed from: w */
    public static int m197w(Throwable t) {
        return lnImpl.mo10582w(t);
    }

    /* renamed from: w */
    public static int m196w(Object s1, Object... args) {
        return lnImpl.mo10581w(s1, args);
    }

    /* renamed from: w */
    public static int m198w(Throwable throwable, Object s1, Object... args) {
        return lnImpl.mo10583w(throwable, s1, args);
    }

    /* renamed from: e */
    public static int m188e(Throwable t) {
        return lnImpl.mo10564e(t);
    }

    /* renamed from: e */
    public static int m187e(Object s1, Object... args) {
        return lnImpl.mo10563e(s1, args);
    }

    /* renamed from: e */
    public static int m189e(Throwable throwable, Object s1, Object... args) {
        return lnImpl.mo10565e(throwable, s1, args);
    }

    public static boolean isDebugEnabled() {
        return lnImpl.isDebugEnabled();
    }

    public static boolean isVerboseEnabled() {
        return lnImpl.isVerboseEnabled();
    }

    public static int getLoggingLevel() {
        return lnImpl.getLoggingLevel();
    }

    public static void setLoggingLevel(int level) {
        lnImpl.setLoggingLevel(level);
    }

    public static String logLevelToString(int loglevel) {
        return lnImpl.logLevelToString(loglevel);
    }
}
