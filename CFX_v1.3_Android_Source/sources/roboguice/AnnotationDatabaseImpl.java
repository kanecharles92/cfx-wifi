package roboguice;

import com.google.inject.AnnotationDatabase;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import roboguice.fragment.FragmentUtil;

public class AnnotationDatabaseImpl extends AnnotationDatabase {
    public void fillAnnotationClassesAndFieldsNames(HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToFieldSet) {
        String annotationClassName = "com.google.inject.Inject";
        Map<String, Set<String>> mapClassWithInjectionNameToFieldSet = (Map) mapAnnotationToMapClassWithInjectionNameToFieldSet.get(annotationClassName);
        if (mapClassWithInjectionNameToFieldSet == null) {
            mapClassWithInjectionNameToFieldSet = new HashMap<>();
            mapAnnotationToMapClassWithInjectionNameToFieldSet.put(annotationClassName, mapClassWithInjectionNameToFieldSet);
        }
        Set<String> fieldNameSet = new HashSet<>();
        fieldNameSet.add("application");
        mapClassWithInjectionNameToFieldSet.put("roboguice.inject.SharedPreferencesProvider", fieldNameSet);
        Set<String> fieldNameSet2 = new HashSet<>();
        fieldNameSet2.add("activity");
        mapClassWithInjectionNameToFieldSet.put("roboguice.fragment.provided.NativeFragmentUtil$FragmentManagerProvider", fieldNameSet2);
        Set<String> fieldNameSet3 = new HashSet<>();
        fieldNameSet3.add("context");
        mapClassWithInjectionNameToFieldSet.put("roboguice.inject.AssetManagerProvider", fieldNameSet3);
        Set<String> fieldNameSet4 = new HashSet<>();
        fieldNameSet4.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboTabActivity", fieldNameSet4);
        Set<String> fieldNameSet5 = new HashSet<>();
        fieldNameSet5.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboSherlockActivity", fieldNameSet5);
        Set<String> fieldNameSet6 = new HashSet<>();
        fieldNameSet6.add("application");
        mapClassWithInjectionNameToFieldSet.put("roboguice.inject.RoboApplicationProvider", fieldNameSet6);
        Set<String> fieldNameSet7 = new HashSet<>();
        fieldNameSet7.add("activity");
        mapClassWithInjectionNameToFieldSet.put("roboguice.fragment.support.SupportFragmentUtil$FragmentManagerProvider", fieldNameSet7);
        Set<String> fieldNameSet8 = new HashSet<>();
        fieldNameSet8.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboSherlockPreferenceActivity", fieldNameSet8);
        Set<String> fieldNameSet9 = new HashSet<>();
        fieldNameSet9.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboExpandableListActivity", fieldNameSet9);
        Set<String> fieldNameSet10 = new HashSet<>();
        fieldNameSet10.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboListActivity", fieldNameSet10);
        Set<String> fieldNameSet11 = new HashSet<>();
        fieldNameSet11.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboActivityGroup", fieldNameSet11);
        Set<String> fieldNameSet12 = new HashSet<>();
        fieldNameSet12.add("context");
        mapClassWithInjectionNameToFieldSet.put("roboguice.inject.ContentResolverProvider", fieldNameSet12);
        Set<String> fieldNameSet13 = new HashSet<>();
        fieldNameSet13.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboSherlockAccountAuthenticatorActivity", fieldNameSet13);
        Set<String> fieldNameSet14 = new HashSet<>();
        fieldNameSet14.add("handlerProvider");
        mapClassWithInjectionNameToFieldSet.put("roboguice.event.eventListener.factory.EventListenerThreadingDecorator", fieldNameSet14);
        Set<String> fieldNameSet15 = new HashSet<>();
        fieldNameSet15.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboSherlockFragmentActivity", fieldNameSet15);
        Set<String> fieldNameSet16 = new HashSet<>();
        fieldNameSet16.add("provider");
        mapClassWithInjectionNameToFieldSet.put("roboguice.inject.ContextScopedProvider", fieldNameSet16);
        Set<String> fieldNameSet17 = new HashSet<>();
        fieldNameSet17.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboLauncherActivity", fieldNameSet17);
        Set<String> fieldNameSet18 = new HashSet<>();
        fieldNameSet18.add("context");
        mapClassWithInjectionNameToFieldSet.put("roboguice.inject.AccountManagerProvider", fieldNameSet18);
        Set<String> fieldNameSet19 = new HashSet<>();
        fieldNameSet19.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboFragmentActivity", fieldNameSet19);
        Set<String> fieldNameSet20 = new HashSet<>();
        fieldNameSet20.add("context");
        mapClassWithInjectionNameToFieldSet.put("roboguice.event.EventManager", fieldNameSet20);
        Set<String> fieldNameSet21 = new HashSet<>();
        fieldNameSet21.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboActivity", fieldNameSet21);
        Set<String> fieldNameSet22 = new HashSet<>();
        fieldNameSet22.add("activity");
        mapClassWithInjectionNameToFieldSet.put("roboguice.inject.ContentViewListener", fieldNameSet22);
        Set<String> fieldNameSet23 = new HashSet<>();
        fieldNameSet23.add("activity");
        mapClassWithInjectionNameToFieldSet.put("roboguice.inject.FragmentManagerProvider", fieldNameSet23);
        Set<String> fieldNameSet24 = new HashSet<>();
        fieldNameSet24.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboSherlockListActivity", fieldNameSet24);
        Set<String> fieldNameSet25 = new HashSet<>();
        fieldNameSet25.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboPreferenceActivity", fieldNameSet25);
        Set<String> fieldNameSet26 = new HashSet<>();
        fieldNameSet26.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboActionBarActivity", fieldNameSet26);
        Set<String> fieldNameSet27 = new HashSet<>();
        fieldNameSet27.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboMapActivity", fieldNameSet27);
        Set<String> fieldNameSet28 = new HashSet<>();
        fieldNameSet28.add("ignored");
        mapClassWithInjectionNameToFieldSet.put("roboguice.activity.RoboAccountAuthenticatorActivity", fieldNameSet28);
        Set<String> fieldNameSet29 = new HashSet<>();
        fieldNameSet29.add("value");
        mapClassWithInjectionNameToFieldSet.put("roboguice.inject.SharedPreferencesProvider$PreferencesNameHolder", fieldNameSet29);
        Set<String> fieldNameSet30 = new HashSet<>();
        fieldNameSet30.add("lnImpl");
        mapClassWithInjectionNameToFieldSet.put("roboguice.util.Ln", fieldNameSet30);
    }

    public void fillAnnotationClassesAndMethods(HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToMethodsSet) {
        String annotationClassName = "com.google.inject.Provides";
        Map<String, Set<String>> mapClassWithInjectionNameToMethodSet = (Map) mapAnnotationToMapClassWithInjectionNameToMethodsSet.get(annotationClassName);
        if (mapClassWithInjectionNameToMethodSet == null) {
            mapClassWithInjectionNameToMethodSet = new HashMap<>();
            mapAnnotationToMapClassWithInjectionNameToMethodsSet.put(annotationClassName, mapClassWithInjectionNameToMethodSet);
        }
        Set<String> methodSet = new HashSet<>();
        methodSet.add("providesAndroidId");
        methodSet.add("providesPackageInfo");
        mapClassWithInjectionNameToMethodSet.put("roboguice.config.DefaultRoboModule", methodSet);
        String annotationClassName2 = "roboguice.event.Observes";
        Map<String, Set<String>> mapClassWithInjectionNameToMethodSet2 = (Map) mapAnnotationToMapClassWithInjectionNameToMethodsSet.get(annotationClassName2);
        if (mapClassWithInjectionNameToMethodSet2 == null) {
            mapClassWithInjectionNameToMethodSet2 = new HashMap<>();
            mapAnnotationToMapClassWithInjectionNameToMethodsSet.put(annotationClassName2, mapClassWithInjectionNameToMethodSet2);
        }
        Set<String> methodSet2 = new HashSet<>();
        methodSet2.add("optionallySetContentView:roboguice.context.event.OnCreateEvent");
        mapClassWithInjectionNameToMethodSet2.put("roboguice.inject.ContentViewListener", methodSet2);
    }

    public void fillAnnotationClassesAndConstructors(HashMap<String, Map<String, Set<String>>> mapAnnotationToMapClassWithInjectionNameToConstructorsSet) {
        String annotationClassName = "com.google.inject.Inject";
        Map<String, Set<String>> mapClassWithInjectionNameToConstructorSet = (Map) mapAnnotationToMapClassWithInjectionNameToConstructorsSet.get(annotationClassName);
        if (mapClassWithInjectionNameToConstructorSet == null) {
            mapClassWithInjectionNameToConstructorSet = new HashMap<>();
            mapAnnotationToMapClassWithInjectionNameToConstructorsSet.put(annotationClassName, mapClassWithInjectionNameToConstructorSet);
        }
        Set<String> constructorSet = new HashSet<>();
        constructorSet.add("<init>:roboguice.inject.SharedPreferencesProvider$PreferencesNameHolder");
        mapClassWithInjectionNameToConstructorSet.put("roboguice.inject.SharedPreferencesProvider", constructorSet);
        Set<String> constructorSet2 = new HashSet<>();
        constructorSet2.add("<init>:android.app.Application");
        mapClassWithInjectionNameToConstructorSet.put("roboguice.inject.ResourcesProvider", constructorSet2);
        Set<String> constructorSet3 = new HashSet<>();
        constructorSet3.add("<init>:android.content.res.Resources");
        mapClassWithInjectionNameToConstructorSet.put("roboguice.inject.StringResourceFactory", constructorSet3);
        Set<String> constructorSet4 = new HashSet<>();
        constructorSet4.add("<init>:android.app.Application");
        mapClassWithInjectionNameToConstructorSet.put("roboguice.util.LnImpl", constructorSet4);
    }

    public void fillClassesContainingInjectionPointSet(HashSet<String> classesContainingInjectionPointsSet) {
        classesContainingInjectionPointsSet.add("roboguice.inject.SharedPreferencesProvider");
        classesContainingInjectionPointsSet.add("roboguice.inject.ResourcesProvider");
        classesContainingInjectionPointsSet.add("roboguice.config.DefaultRoboModule");
        classesContainingInjectionPointsSet.add("roboguice.inject.StringResourceFactory");
        classesContainingInjectionPointsSet.add("roboguice.fragment.provided.NativeFragmentUtil$FragmentManagerProvider");
        classesContainingInjectionPointsSet.add("roboguice.util.LnImpl");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboTabActivity");
        classesContainingInjectionPointsSet.add("roboguice.inject.AssetManagerProvider");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboSherlockActivity");
        classesContainingInjectionPointsSet.add("roboguice.inject.RoboApplicationProvider");
        classesContainingInjectionPointsSet.add("roboguice.fragment.support.SupportFragmentUtil$FragmentManagerProvider");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboExpandableListActivity");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboSherlockPreferenceActivity");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboListActivity");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboActivityGroup");
        classesContainingInjectionPointsSet.add("roboguice.inject.ContentResolverProvider");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboSherlockAccountAuthenticatorActivity");
        classesContainingInjectionPointsSet.add("roboguice.event.eventListener.factory.EventListenerThreadingDecorator");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboSherlockFragmentActivity");
        classesContainingInjectionPointsSet.add("roboguice.inject.ContextScopedProvider");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboLauncherActivity");
        classesContainingInjectionPointsSet.add("roboguice.inject.AccountManagerProvider");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboFragmentActivity");
        classesContainingInjectionPointsSet.add("roboguice.event.EventManager");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboActivity");
        classesContainingInjectionPointsSet.add("roboguice.inject.ContentViewListener");
        classesContainingInjectionPointsSet.add("roboguice.inject.FragmentManagerProvider");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboActionBarActivity");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboPreferenceActivity");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboSherlockListActivity");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboMapActivity");
        classesContainingInjectionPointsSet.add("roboguice.inject.SharedPreferencesProvider$PreferencesNameHolder");
        classesContainingInjectionPointsSet.add("roboguice.activity.RoboAccountAuthenticatorActivity");
        classesContainingInjectionPointsSet.add("roboguice.util.Ln");
    }

    public void fillBindableClasses(HashSet<String> injectedClasses) {
        injectedClasses.add("com.google.inject.Provider");
        injectedClasses.add("roboguice.util.LnInterface");
        injectedClasses.add("roboguice.inject.ContentViewListener");
        injectedClasses.add("android.app.Activity");
        injectedClasses.add("android.content.Context");
        injectedClasses.add("roboguice.inject.SharedPreferencesProvider$PreferencesNameHolder");
        injectedClasses.add("roboguice.context.event.OnCreateEvent");
        injectedClasses.add("java.lang.String");
        injectedClasses.add("android.app.Application");
        injectedClasses.add("android.content.res.Resources");
        if (FragmentUtil.hasNative) {
            injectedClasses.add("android.app.FragmentManager");
        }
        if (FragmentUtil.hasSupport) {
            injectedClasses.add("android.support.v4.app.FragmentManager");
        }
    }
}
