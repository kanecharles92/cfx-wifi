package roboguice.context.event;

import android.content.Context;

public class OnDestroyEvent<T extends Context> {
    protected T context;

    public OnDestroyEvent(T context2) {
        this.context = context2;
    }

    public T getContext() {
        return this.context;
    }
}
