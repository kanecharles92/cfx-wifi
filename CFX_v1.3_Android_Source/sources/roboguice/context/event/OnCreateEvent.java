package roboguice.context.event;

import android.content.Context;
import android.os.Bundle;

public class OnCreateEvent<T extends Context> {
    protected T context;
    protected Bundle savedInstanceState;

    public OnCreateEvent(T context2, Bundle savedInstanceState2) {
        this.savedInstanceState = savedInstanceState2;
        this.context = context2;
    }

    public Bundle getSavedInstanceState() {
        return this.savedInstanceState;
    }

    public T getContext() {
        return this.context;
    }
}
