package roboguice.context.event;

import android.content.Context;

public class OnStartEvent<T extends Context> {
    protected T context;

    public OnStartEvent(T context2) {
        this.context = context2;
    }

    public T getContext() {
        return this.context;
    }
}
