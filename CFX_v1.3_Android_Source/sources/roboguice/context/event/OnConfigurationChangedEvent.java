package roboguice.context.event;

import android.content.Context;
import android.content.res.Configuration;

public class OnConfigurationChangedEvent<T extends Context> {
    protected T context;
    protected Configuration newConfig;
    protected Configuration oldConfig;

    public OnConfigurationChangedEvent(T context2, Configuration oldConfig2, Configuration newConfig2) {
        this.oldConfig = oldConfig2;
        this.newConfig = newConfig2;
        this.context = context2;
    }

    public Configuration getOldConfig() {
        return this.oldConfig;
    }

    public Configuration getNewConfig() {
        return this.newConfig;
    }

    public T getContext() {
        return this.context;
    }
}
