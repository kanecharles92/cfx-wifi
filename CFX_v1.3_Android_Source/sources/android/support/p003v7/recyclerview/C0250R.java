package android.support.p003v7.recyclerview;

import com.zhenbang.project_532.R;

/* renamed from: android.support.v7.recyclerview.R */
public final class C0250R {

    /* renamed from: android.support.v7.recyclerview.R$attr */
    public static final class attr {
        public static final int layoutManager = 2130903195;
        public static final int reverseLayout = 2130903251;
        public static final int spanCount = 2130903265;
        public static final int stackFromEnd = 2130903271;
    }

    /* renamed from: android.support.v7.recyclerview.R$dimen */
    public static final class dimen {
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131099754;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131099755;
        public static final int item_touch_helper_swipe_escape_velocity = 2131099756;
    }

    /* renamed from: android.support.v7.recyclerview.R$id */
    public static final class C0251id {
        public static final int item_touch_helper_previous_elevation = 2131230853;
    }

    /* renamed from: android.support.v7.recyclerview.R$styleable */
    public static final class styleable {
        public static final int[] RecyclerView = {16842948, R.attr.layoutManager, R.attr.reverseLayout, R.attr.spanCount, R.attr.stackFromEnd};
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_layoutManager = 1;
        public static final int RecyclerView_reverseLayout = 2;
        public static final int RecyclerView_spanCount = 3;
        public static final int RecyclerView_stackFromEnd = 4;
    }
}
